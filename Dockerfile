FROM node:16-bullseye

# Default values for env vars to pass build test
ARG REACT_APP_API_KEY=${REACT_APP_API_KEY}
ARG REACT_APP_PORT=${REACT_APP_PORT}
ARG REACT_APP_PROTOCOL=${REACT_APP_PROTOCOL}
ARG REACT_APP_HOST=${REACT_APP_HOST}
ARG REACT_APP_LIGHTGALLERY_LICENSE_KEY=${REACT_APP_LIGHTGALLERY_LICENSE_KEY}

RUN mkdir -p /home/node/app/node_modules \
  && chown -R node:node /home/node/app
WORKDIR /home/node/app
USER node
COPY --chown=node:node . .
RUN npm install --force

# generate glossary and json files
RUN npm run docusaurus parse
RUN npm run docusaurus parseFrench
RUN npm run docusaurus glossary
RUN npm run docusaurus termOfTheDay
Run npm run docusaurus frenchTermOfTheDay

RUN npm run build
EXPOSE 3000

CMD ["npm", "run", "serve"]
