/** @format */

// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion
const tablePlugin = require('remark-grid-tables')
const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");
require("dotenv").config();

const TwitterSvg =
  '<svg alt="Twitter Logo" style="fill: #FFFFFF;" width="40" height="32.49" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg>';

const YoutubeSvg =
  '<svg alt="Youtube Logo" style="fill: #FFFFFF;" width="40" height="32.49" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg>';

const GitLabSvg =
  '<svg alt="GitLab Logo" style="fill: #FFFFFF;" width="40" height="32.49" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M503.5 204.6L502.8 202.8L433.1 21.02C431.7 17.45 429.2 14.43 425.9 12.38C423.5 10.83 420.8 9.865 417.9 9.57C415 9.275 412.2 9.653 409.5 10.68C406.8 11.7 404.4 13.34 402.4 15.46C400.5 17.58 399.1 20.13 398.3 22.9L351.3 166.9H160.8L113.7 22.9C112.9 20.13 111.5 17.59 109.6 15.47C107.6 13.35 105.2 11.72 102.5 10.7C99.86 9.675 96.98 9.295 94.12 9.587C91.26 9.878 88.51 10.83 86.08 12.38C82.84 14.43 80.33 17.45 78.92 21.02L9.267 202.8L8.543 204.6C-1.484 230.8-2.72 259.6 5.023 286.6C12.77 313.5 29.07 337.3 51.47 354.2L51.74 354.4L52.33 354.8L158.3 434.3L210.9 474L242.9 498.2C246.6 500.1 251.2 502.5 255.9 502.5C260.6 502.5 265.2 500.1 268.9 498.2L300.9 474L353.5 434.3L460.2 354.4L460.5 354.1C482.9 337.2 499.2 313.5 506.1 286.6C514.7 259.6 513.5 230.8 503.5 204.6z"></path></svg>';


/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "LINCS",
  tagline:
    "LINCS provides the tools and infrastructure to make humanities data more discoverable, searchable, and shareable. Discover how you can explore, create, and publish cultural data.",
  url: "https://portal.lincsproject.ca",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon-(c-LINCS).ico",
  organizationName: "LINCS", // Usually your GitHub org/user name.
  projectName: "LINCS Portal", // Usually your repo name.

  customFields: {
    LightGalleryLicense: process.env.REACT_APP_LIGHTGALLERY_LICENSE_KEY,
  },

  i18n: {
    defaultLocale: "en",
    locales: ["en", "fr"],
    localeConfigs: {
      en: {
        label: "English",
      },
      fr: {
        label: "Français",
      },
    },
  },
  plugins: [
    // "@docusaurus-terminology/parser",
    /*[
      "@docusaurus-terminology/parser",
      {
        glossaryFilepath: "./docs/get-started/glossary.md",
        termOfTheDayFilePath: "./src/json-generator/term.json",
        termsDir: "./docs/terms",
      },
    ],*/
    [
      require.resolve("./src/plugins/terminology-parser-v2"),
      {
        glossaryFilepath: "./docs/get-started/glossary.md",
        termOfTheDayFilePath: "./src/plugins/terminology-parser-v2/term.json",
        frenchTermOfTheDayFilePath:
          "./src/plugins/terminology-parser-v2/frenchTerm.json",
        termsDir: "./docs/terms",
      },
    ],
  ],
  scripts: [
    {
      src: "/matomo.js",
      async: "true",
    },
  ],
  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          editUrl: "https://gitlab.com/calincs/admin/docusaurus/-/tree/main/",
          remarkPlugins: [tablePlugin],
        },
        blog: {
          showReadingTime: true,
          include: ["**/*.{md,mdx}"],
          editUrl: "https://gitlab.com/calincs/admin/docusaurus/-/tree/main/",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],
  themes: ["docusaurus-theme-search-typesense"],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    {
      announcementBar: {
        id: "making-LINCS-conference-2023",
        content:
          'Welcome to the LINCS beta site! Content under development. Not all links work yet. Translation ongoing. Please be patient as we grow! 🌱 <a target="_blank" href="#">Feedback is welcome!</a>',
        backgroundColor: "#fafbfc",
        textColor: "#091E42",
        isCloseable: false,
      },
      typesense: {
        typesenseCollectionName: "lincsaurus",
        typesenseServerConfig: {
          nodes: [
            {
              host: process.env.REACT_APP_HOST,
              port: process.env.REACT_APP_PORT,
              protocol: process.env.REACT_APP_PROTOCOL,
            },
          ],
          apiKey: process.env.REACT_APP_API_KEY,
        },
        // Optional: Typesense search parameters: https://typesense.org/docs/0.21.0/api/documents.md#search-parameters
        typesenseSearchParameters: {},
        // Optional
        contextualSearch: true,
      },
      docs: {
        sidebar: {
          hideable: true,
          autoCollapseCategories: true,
        },
      },
      colorMode: {
        defaultMode: "light",
        disableSwitch: true,
      },
      navbar: {
        title: "LINCS Portal (beta)",
        logo: {
          alt: "LINCS Logo",
          src: "img/lincs-logo-(c-LINCS).png",
          style: { height: "32px" },
        },
        items: [
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "aboutLincsSidebar",
            label: "About LINCS",
            className: "text--truncate navbar-item",
          },
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "getStartedSidebar",
            label: "Get Started",
            className: "text--truncate navbar-item",
          },
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "exploreDataSidebar",
            label: "Explore Data",
            className: "text--truncate navbar-item",
          },
          {
            type: "doc",
            position: "left",
            docId: "create-data/create-data",
            label: "Create Data",
            className: "text--truncate navbar-item",
          },
          {
            type: "docSidebar",
            position: "left",
            sidebarId: "toolsSidebar",
            label: "Tools",
            className: "navbar-item",
          },
          {
            to: "/blog",
            label: "Blog",
            position: "right",
            className: "navbar-item",
          },
          {
            href: "https://gitlab.com/calincs",
            label: "GitLab",
            position: "right",
            className: "navbar-item",
          },
          {
            type: "localeDropdown",
            position: "right",
            className: "navbar-item",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Contact LINCS",
            items: [
              {
                label: "Feedback",
                href: "#",
              },
              {
                label: "Email Us",
                href: "mailto://lincs@uoguelph.ca",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "Sign up for the Newsletter",
                href: "http://eepurl.com/gZ0rzb",
              },
              {
                html: `
                <div class="svg-icons">
                  <a href="https://twitter.com/lincsproject" title="Twitter">${TwitterSvg}</a>
                  <a href="https://www.youtube.com/channel/UCuLMD1dLD6_AAMeXlyZgzQQ" title="Youtube">${YoutubeSvg}</a>
                  <a href="https://gitlab.com/calincs" title="GitLab">${GitLabSvg}</a>
                </div>
              `,
              },
            ],
          },
          {
            title: "Policies",
            items: [
              {
                label: "Privacy Policy",
                href: "/docs/about-lincs/terms-and-conditions/privacy",
              },
              {
                label: "Terms and Conditions",
                href: "/docs/about-lincs/terms-and-conditions",
              },
            ],
          },
          {
            title: "Partners and Funding",
            items: [
              {
                html: `
                <div class="cfi-logo">
                  <a href="https://www.innovation.ca/" target="_blank" rel="noreferrer noopener" aria-label="CFI">
                    <img src="/img/logos/cfi-logo-new-(c-owner).svg" alt="Canada Foundation for Innovation (CFI) Logo" />
                  </a>
                </div>  
              `,
              },
              {
                label: "Funding Partners",
                href: "/docs/about-lincs/people/funding-partners",
              },
              {
                label: "Infrastructure Partners",
                href: "/docs/about-lincs/people/infrastructure-partners",
              },
            ],
          },
          {
            items: [
              {
                html: `
                <div class="land-acknowledgement">
                  <p>We would like to acknowledge that LINCS activities take place on the traditional, ancestral, unceded, or treaty territories of many Indigenous peoples.</p>
                </div>
              `,
              },
            ],
          },
        ],
        copyright: `<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0;margin-right:2px;height:calc(1em - 4px);" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a>This work is licensed under a <a rel="license" class="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ["turtle", "sparql", "python", "cypher"],
      },
    },
};

module.exports = config;
