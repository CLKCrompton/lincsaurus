/** @format */

import React from "react";

import people from "@site/static/teamMembers.json";
import TeamCard from "./cards/TeamCard";
import GridLayout from "./layouts/GridLayout";

function TeamList({ section }) {
  people[section].sort((a, b) => (a["Last Name"] > b["Last Name"] ? 1 : -1));

  return (
    <GridLayout>
      {people[section].map((x) => (
        <TeamCard
          key={`${x["First Name"]}${x["Last Name"]}`}
          firstName={x["First Name"]}
          lastName={x["Last Name"]}
          imageExists={x["Image available"]}
          jobTitle={x["Job Title"]}
          uni={x["University"]}
          website={x["Website"]}
          github={x["Github"]}
          gitlab={x["GitLab"]}
          orcid={x["Orcid"]}
          start={x["Start Date"]}
          end={x["End Date"]}
        />
      ))}
    </GridLayout>
  );
}

export default TeamList;
