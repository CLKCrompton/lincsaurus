/** @format */

import React from "react";


function GalleryItem({ path, altlabel, title, caption }) {
  // Potentially could include copyright?

  const fullCaption = `<h4>${title}</h4><em>${caption}</em>`;

  return (
        <a href={path} data-sub-html={fullCaption} data-src={path}>
          <img alt={altlabel} title={title} src={path} />
        </a>
  );
}

export default GalleryItem;
