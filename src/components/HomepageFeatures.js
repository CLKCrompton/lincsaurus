import React from "react";
import styles from "./homepage.module.css";
import Translate from '@docusaurus/Translate';
import PrimaryButton from "../components/buttons/PrimaryButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import {
  faUpRightFromSquare,
  faUsersLine,
  faFileLines,
  faMagnifyingGlass,
  faArrowsRotate,
  faBroom,
  faEye,
  faLink,
  faDiagramProject,
  faPenToSquare,
  faHandSparkles,
  faBoxesStacked,
  faCodePullRequest,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faUpRightFromSquare,
  faUsersLine,
  faFileLines,
  faMagnifyingGlass,
  faArrowsRotate,
  faBroom,
  faEye,
  faLink,
  faDiagramProject,
  faPenToSquare,
  faHandSparkles,
  faBoxesStacked,
  faCodePullRequest,
  faGitlab
);

const DrawerList = [
  {
    heading: <Translate id="homepage.drawers.rsheader" description="Access Research Space drawer header">Access ResearchSpace</Translate>,
    id: "Access ResearchSpace",
    description: <Translate id="homepage.drawers.rsdesc" description="Access Research Space drawer description">Explore datasets, review and enhance data</Translate>,
    open: true,
  },
  {
    heading: <Translate id="homepage.drawers.edheader" description="Explore Data drawer header">Explore Data</Translate>,
    id: "Explore Data",
    description:
      <Translate id="homepage.drawers.eddescription" description="Explore Data drawer description">Browse, search, and visualize Linked Open Data using LINCS tools</Translate>,
  },
  {
    heading: <Translate id="homepage.drawers.cdheader" description="Create Data drawer header">Create Data</Translate>,
    id: "Create Data",
    description:
      <Translate id="homepage.drawers.cddescription" description="Create Data drawer description">Use the LINCS data conversion workflow to prepare datasets for ingestion, or use individual tools for specific tasks</Translate>,
  },
  {
    heading: <Translate id="homepage.drawers.connectheader" description="Connect with the Code drawer header">Connect with the Code</Translate>,
    id: "Connect with the Code",
    description: <Translate id="homepage.drawers.connectdescription" description="Connect with the Code drawer description">Learn how LINCS works and how you can contribute</Translate>,
  },
];

const FeatureList = {
  "Access ResearchSpace": [
    {
      icon: "fa-up-right-from-square",
      external: true,
      description: <><Translate id="homepage.drawers.rsItem1Desc" description="Access Research Space item 1 description">Visit LINCS ResearchSpace to discover data</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.rsItem1Label" description="Access Research Space item 1 label">Go to ResearchSpace</Translate>,
      link: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=home",
    },
    {
      icon: "fa-users-line",
      external: true,
      description: <><Translate id="homepage.drawers.rsItem2Desc" description="Access Research Space item 2 description">See what researchers are doing with LINCS</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.rsItem2Label" description="Access Research Space item 2 label">View Datasets</Translate>,
      link: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=home",
    },
    {
      icon: "fa-pen-to-square",
      external: true,
      description: (
        <><Translate id="homepage.drawers.rsItem3Desc" description="Access Research Space item 3 description">Review or enhance your data-in-progress (account required)</Translate></>
      ),
      buttonLabel: <Translate id="homepage.drawers.rsItem3Label" description="Access Research Space item 3 label">ReviewSpace</Translate>,
      link: "https://rs.lincsproject.ca/resource/ThinkingFrames?view=home",
    },
  ],
  "Explore Data": [
    {
      icon: "fa-magnifying-glass",
      description: <><Translate id="homepage.drawers.edItem1Desc" description="Explore Data item 1 description">Query the data in simple and advanced ways</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.edItem1Label" description="Explore Data item 1 label">Search</Translate>,
      link: "/docs/explore-data/search",
    },
    {
      icon: "fa-eye",
      description: <><Translate id="homepage.drawers.edItem2Desc" description="Explore Data item 2 description">Browse datasets and discover connections</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.edItem2Label" description="Explore Data item 2 label">Browse</Translate>,
      link: "/docs/explore-data/browse",
    },
    {
      icon: "fa-diagram-project",
      description: <><Translate id="homepage.drawers.edItem3Desc" description="Explore Data item 3 description">Visualize networks, maps, texts, and more</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.edItem3Label" description="Explore Data item 3 label">Visualize</Translate>,
      link: "/docs/explore-data/visualize",
    },
  ],
  "Create Data": [
    {
      icon: "fa-hand-sparkles",
      description: <><Translate id="homepage.drawers.cdItem1Desc" description="Create Data item 1 description">Produce or enhance Linked Open Data</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.cdItem1Label" description="Create Data item 1 label">Create</Translate>,
      link: "/docs/create-data",
    },
    {
      icon: "fa-broom",
      description: <><Translate id="homepage.drawers.cdItem2Desc" description="Create Data item 2 description">Regularize and fix source data</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.cdItem2Label" description="Create Data item 2 label">Clean</Translate>,
      link: "/docs/create-data/clean",
    },
    {
      icon: "fa-link",
      description: <><Translate id="homepage.drawers.cdItem3Desc" description="Create Data item 3 description">Connect entities to Linked Open Data vocabularies</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.cdItem3Label" description="Create Data item 3 label">Reconcile</Translate>,
      link: "/docs/create-data/reconcile",
    },
    {
      icon: "fa-arrows-rotate",
      description: <><Translate id="homepage.drawers.cdItem4Desc" description="Create Data item 4 description">Produce linked data from spreadsheets, markup, or text</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.cdItem4Label" description="Create Data item 4 label">Convert</Translate>,
      link: "/docs/create-data/convert",
    },
  ],
  "Connect with the Code": [
    {
      icon: "fa-boxes-stacked",
      description: <><Translate id="homepage.drawers.connectItem1Desc" description="Connect with the Code item 1 description">System Overview</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.connectItem1Label" description="Connect with the Code item 1 label">Technical Development</Translate>,
      link: "/docs/about-lincs/#technical-development",
    },
    {
      icon: "fa-code-pull-request",
      description: <><Translate id="homepage.drawers.connectItem2Desc" description="Connect with the Code item 2 description">Help develop LINCS</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.connectItem2Label" description="Connect with the Code item 2 label">Contribute Code</Translate>,
      link: "/docs/about-lincs/get-involved/contribute-code",
    },
    {
      icon: "fa-brands fa-gitlab",
      description: <><Translate id="homepage.drawers.connectItem3Desc" description="Connect with the Code item 3 description">LINCS GitLab</Translate></>,
      buttonLabel: <Translate id="homepage.drawers.connectItem3Label" description="Connect with the Code item 3 label">GitLab</Translate>,
      link: "https://gitlab.com/calincs",
      external: true,
    },
  ],
};

function DropDown({ heading, description, itemList, open }) {
  return (
    <div className={styles.row}>
      <details open={open}>
        <summary className={styles.title}>
          {heading}
          <br></br>
          <p className={styles.drawerDescription}>{description}</p>
          <hr className={styles.line}></hr>
        </summary>
        <div className={styles.drawerContent}>
          {itemList.map((object, i) => (
            <Feature {...object} key={i} />
          ))}
        </div>
      </details>
    </div>
  );
}

function Feature({
  icon,
  img = null,
  description,
  buttonLabel,
  link,
  external = false,
  brand = false,
}) {
  let image;

  if (img) {
    image = <img src={img} className={styles.featureSvg} />;
  } else {
    image = <FontAwesomeIcon icon={icon} className={styles.featureSvg} />;
  }

  return (
    <div className={styles.item}>
      {image}
      <div className={styles.itemDescription}>
        <p>{description}</p>
      </div>
      <div className={styles.buttonStyle}>
        <PrimaryButton
          link={link}
          buttonName={buttonLabel}
          icon={external ? "fa-up-right-from-square" : null}
        />
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section>
      <div className={styles.drawerContainer}>
        <h1 className="header">
          <Translate
            id="homepage.drawers.header" description="The drawers header"
          >
            What can you do with LINCS?
          </Translate>
        </h1>
        {DrawerList.map((object, i) => (
          <DropDown
            {...object}
            itemList={FeatureList[object.id]}
            key={i}
          />
        ))}
      </div>
    </section>
  );
}
