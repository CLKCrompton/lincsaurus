/** @format */

import React from "react";
import Translate from "@docusaurus/Translate";

import { Navigation, Pagination, A11y, Keyboard } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/keyboard";

import styles from "./homepage.module.css";
// import landscapeArt from "@site/static/img/index-page-images/art-landscape.jpg";
import orlandoImage from "@site/static/img/index-page-images/orlando.png";
import anthologiaPalatina from "@site/static/img/index-page-images/anthologia-palatina.jpeg";
import CarouselCard from "./cards/CarouselCard";
import MobileCarouselCard from "./cards/MobileCarouselCard";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
} from "@fortawesome/free-solid-svg-icons";

const carouselContents = [
  {
    id: "Anthologia Palatina",
    src: anthologiaPalatina,
    title: (
      <Translate
        id="homepage.carousel.apTitle"
        description="Anthologia Palatina carousel title">
        Anthologia Palatina
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.apDescription"
        description="Anthologia Palatina carousel description">
        The project is centred around the design of a collaborative digital
        edition of the Palatine Anthology. The PA is a collection of Greek
        epigrams, whose source dates back to the Hellenistic period (323-30 BC).
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="homepage.carousel.edLabel"
        description="Carousel Explore Dataset label">
        Explore Anthologia Palatina
      </Translate>
    ),
    link1:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-anthologiaPalatina",
  },
  {
    id: "Yellow Nineties",
    title: (
      <Translate
        id="homepage.carousel.ynTitle"
        description="Yellow Nineties carousel title">
        Yellow Nineties
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.ynDescription"
        description="Yellow Nineties carousel description">
        Yellow Nineties 2.0 is a peer-reviewed, open-access scholarly resource
        for the study of eight fin-de-siècle little magazines in the context of
        their production and reception.
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="homepage.carousel.ynLabel"
        description="Carousel Explore Yellow Nineties button label">
        Explore Yellow Nineties
      </Translate>
    ),
    link1:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-yellowNineties",
  },
  {
    id: "University of Saskatchewan Art Collection",
    src: "https://images.unsplash.com/photo-1633443245758-6a507463c89c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80",
    title: (
      <Translate
        id="homepage.carousel.usaskTitle"
        description="University of Saskatchewan Art Collection carousel title">
        University of Saskatchewan Art Collection
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.usaskDescription"
        description="University of Saskatchewan Art Collection carousel description">
        Digitized art collection from University of Saskatchewan Art Gallery
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="homepage.carousel.edLabel"
        description="Carousel Explore Dataset label">
        Explore University of Saskatchewan Art Collection
      </Translate>
    ),
    link1:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-usaskart",

    entityLink: "https://unsplash.com/photos/Ro4u_Y86Kx4",
  },
  {
    id: "Orlando",
    src: orlandoImage,
    title: (
      <Translate
        id="homepage.carousel.orlandoTitle"
        description="Orlando carousel title">
        Orlando
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.carousel.orlandoDescription"
        description="Orlando carousel description">
        The Orlando Project explores and harnesses the power of digital tools
        and methods to advance feminist literary scholarship.
      </Translate>
    ),
    buttonName1: (
      <Translate
        id="homepage.carousel.orlandoLabel"
        description="Carousel Explore Orlando label">
        Explore Orlando
      </Translate>
    ),
    link1:
      "https://rs.lincsproject.ca/resource/ThinkingFrames?view=search-orlando",
  },
];

function HomepageCarousel() {
  return (
    <div>
      <div className={styles.mobileCarousel}>
        <Swiper
          modules={[Navigation, Pagination, A11y, Keyboard]}
          spaceBetween={8}
          breakpoints={{
            880: {
              slidesPerView: 2.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            780: {
              slidesPerView: 2.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            680: {
              slidesPerView: 2.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            580: {
              slidesPerView: 1.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            480: {
              slidesPerView: 1.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            380: {
              slidesPerView: 1.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
          }}
          rewind={true}
          pagination={{ clickable: true }}
          keyboard={{ enabled: true }}
          style={{ "--swiper-pagination-color": "#107386" }}>
          {carouselContents.map((object) => (
            <SwiperSlide key={object.id}>
              <MobileCarouselCard {...object} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>

      <div className={styles.carouselContainer}>
        <div className={styles["carousel-swiper-button-next"]}>
          <h2>
            <FontAwesomeIcon icon={faChevronRight} />
          </h2>
        </div>
        <div className={styles["carousel-swiper-button-prev"]}>
          <h2>
            <FontAwesomeIcon icon={faChevronLeft} />
          </h2>
        </div>
        <div className={styles.carousel}>
          <Swiper
            modules={[Navigation, A11y, Keyboard]}
            spaceBetween={12}
            slidesPerView={2.25}
            navigation={{
              nextEl: `.${styles["carousel-swiper-button-next"]}`,
              prevEl: `.${styles["carousel-swiper-button-prev"]}`,
            }}
            keyboard={{ enabled: true }}
            loop={true}
            style={{ "--swiper-pagination-color": "#107386" }}>
            {carouselContents.map((object) => (
              <SwiperSlide key={object.id}>
                <CarouselCard {...object} />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </div>
  );
}

export default HomepageCarousel;
