import Translate from "@docusaurus/Translate";
import React from "react";

const functionTypes = {
  Clean: {
    id: <Translate id="functionButton.cleanLabel" description="Clean Function Button Label">Clean</Translate>,
    link: "/docs/create-data/clean",
    icon: "fa-broom",
  },
  Reconcile: {
    id: <Translate id="functionButton.reconcileLabel" description="Reconcile Function Button Label">Reconcile</Translate>,
    link: "/docs/create-data/reconcile",
    icon: "fa-link",
  },
  Convert: {
    id: <Translate id="functionButton.convertLabel" description="Convert Function Button Label">Convert</Translate>,
    link: "/docs/create-data/convert",
    icon: "fa-arrows-rotate",
  },
  Browse: {
    id: <Translate id="functionButton.browseLabel" description="Browse Function Button Label">Browse</Translate>,
    link: "/docs/explore-data/browse",
    icon: "fa-eye",
  },
  Search: {
    id: <Translate id="functionButton.searchLabel" description="Search Function Button Label">Search</Translate>,
    link: "/docs/explore-data/search",
    icon: "fa-magnifying-glass",
  },
  Visualize: {
    id: <Translate id="functionButton.visualizeLabel" description="Visualize Function Button Label">Visualize</Translate>,
    link: "/docs/explore-data/visualize",
    icon: "fa-diagram-project",
  },
};

export default functionTypes;