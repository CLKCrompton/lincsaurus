/** @format */

import React from "react";
import Translate from "@docusaurus/Translate";
import { Navigation, A11y, Keyboard } from "swiper";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import "swiper/css/keyboard";

import styles from "./homepage.module.css";
import TileCard from "./cards/TileCard";
import MobileTileCard from "./cards/MobileTileCard";
import creativeWorks from "@site/static/img/index-page-images/creativeWorks.jpg";
import events from "@site/static/img/index-page-images/events-(c-unsplash).jpg";
import organizations from "@site/static/img/index-page-images/groups-(c-unsplash).jpg";
import people from "@site/static/img/index-page-images/people.jpg";
import places from "@site/static/img/index-page-images/places.jpg";
import sources from "@site/static/img/index-page-images/sources.jpg";

const tileContents = [
  {
    id: "People",
    src: "https://upload.wikimedia.org/wikipedia/commons/c/c9/Ann_of_Swansea_by_William_Watkeys_%281835%29.jpg",
    title: (
      <Translate
        id="homepage.tileBanner.peopleTitle"
        description="People Tile Banner title">
        People
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.tileBanner.peopleDescription"
        description="People Tile Banner description">
        People, famous & obscure, linked to cultural processes
      </Translate>
    ),
    link: "http://rs.lincsproject.ca/resource/search/people",
    buttonName: (
      <Translate
        id="homepage.tileBanner.peopleLabel"
        description="People Tile Banner button label">
        Explore People
      </Translate>
    ),
  },

  {
    id: "Places",
    src: "https://upload.wikimedia.org/wikipedia/commons/8/81/AshtonHouse_Devon_ByJohnSwete_1794.jpg",
    title: (
      <Translate
        id="homepage.tileBanner.placesTitle"
        description="Places Tile Banner title">
        Places
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.tileBanner.placesDescription"
        description="Places Tile Banner description">
        Spatial starting points ranging from buildings to countries
      </Translate>
    ),
    link: "http://rs.lincsproject.ca/resource/search/place",
    buttonName: (
      <Translate
        id="homepage.tileBanner.placesLabel"
        description="Places Tile Banner button label">
        Explore Places
      </Translate>
    ),
  },
  {
    id: "Organizations",
    src: organizations,
    title: (
      <Translate
        id="homepage.tileBanner.organizationsTitle"
        description="Organizations Tile Banner title">
        Groups
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.tileBanner.organizationsDescription"
        description="Organizations Tile Banner description">
        Social groups, formal & informal, small or large
      </Translate>
    ),
    link: "http://rs.lincsproject.ca/resource/search/groups",
    buttonName: (
      <Translate
        id="homepage.tileBanner.organizationsLabel"
        description="Organizations Tile Banner button label">
        Explore Groups
      </Translate>
    ),
  },
  {
    id: "Sources",
    src: sources,
    title: (
      <Translate
        id="homepage.tileBanner.sourcesTitle"
        description="Sources Tile Banner title">
        Sources
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.tileBanner.sourcesDescription"
        description="Sources Tile Banner description">
        Publications and other evidence for the cultural record
      </Translate>
    ),
    link: "http://rs.lincsproject.ca/resource/search/bibliographic",
    buttonName: (
      <Translate
        id="homepage.tileBanner.sourcesLabel"
        description="Sources Tile Banner button label">
        Explore Sources
      </Translate>
    ),
  },
  {
    id: "Events",
    src: events,
    title: (
      <Translate
        id="homepage.tileBanner.eventsTitle"
        description="Events Tile Banner title">
        Events
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.tileBanner.eventsDescription"
        description="Events Tile Banner description">
        Occurrences and processes historic & quotidian
      </Translate>
    ),
    link: "http://rs.lincsproject.ca/resource/search/event",
    buttonName: (
      <Translate
        id="homepage.tileBanner.eventsLabel"
        description="Events Tile Banner button label">
        Explore Events
      </Translate>
    ),
  },
  {
    id: "Creative Works",
    src: creativeWorks,
    title: (
      <Translate
        id="homepage.tileBanner.cwTitle"
        description="Creative Works Tile Banner title">
        Creative Works
      </Translate>
    ),
    description: (
      <Translate
        id="homepage.tileBanner.cwDescription"
        description="Creative Works Tile Banner description">
        Textual, visual, performative & other creative forms
      </Translate>
    ),
    link: "http://rs.lincsproject.ca/resource/search/artwork",
    buttonName: (
      <Translate
        id="homepage.tileBanner.cwLabel"
        description="Creative Works Tile Banner button label">
        Explore Works
      </Translate>
    ),
  },
];

function HomepageTileBanner() {
  return (
    <div>
      <div className={styles.mobileTileBanner}>
        <Swiper
          modules={[Navigation, A11y, Keyboard]}
          slidesPerView={5}
          spaceBetween={8}
          breakpoints={{
            880: {
              slidesPerView: 3.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            780: {
              slidesPerView: 3.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            680: {
              slidesPerView: 3.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            580: {
              slidesPerView: 2.75,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            480: {
              slidesPerView: 2.5,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            380: {
              slidesPerView: 1.25,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
            0: {
              slidesPerView: 1,
              slidesPerGroup: 1,
              spaceBetween: 8,
            },
          }}
          rewind={true}
          loopFillGroupWithBlank={false}
          keyboard={{ enabled: true }}
          style={{ "--swiper-pagination-color": "#107386" }}>
          {tileContents.map((object) => (
            <SwiperSlide key={object.id}>
              <MobileTileCard {...object} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>

      <div className={styles.tileBannerContainer}>
        <div className={styles.tileBanner}>
          <Swiper
            modules={[A11y]}
            slidesPerView={6}
            spaceBetween={8}
            loop={true}
            loopFillGroupWithBlank={false}>
            {tileContents.map((object) => (
              <SwiperSlide key={object.id}>
                <TileCard {...object} />
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </div>
    </div>
  );
}

export default HomepageTileBanner;
