import React from "react";
import Tools from "@site/static/tools.json";
import ToolCard from "./cards/ToolCard";
import buttonStyles from "../components/buttons/buttons.module.css";
import styles from "./toolCatalogue.module.css";
import { useState, useEffect } from "react";
import Translate, { translate } from "@docusaurus/Translate";
import functionTypes from "./functions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  faMagnifyingGlass,
  faDiagramProject,
  faFileLines,
  faBroom,
  faLink,
  faArrowsRotate,
  faEye
);


function ToolFilters({ activeFilters, setActiveFilters }) {
  const filterLevels = [
    translate({ id: "toolCatalogue.beginnerLabel", description: "Tool Catalogue Beginner Button Label", message: "Beginner" }),
    translate({ id: "toolCatalogue.intermediateLabel", description: "Tool Catalogue Intermediate Button Label", message: "Intermediate" }),
    translate({ id: "toolCatalogue.ExpertLabel", description: "Tool Catalogue Expert Button Label", message: "Expert" }),
  ];

  const handleClick = (filter) => {
    let _activeFilters = [...activeFilters];
    // Resetting filter
    if (activeFilters.includes(filter)) {
      _activeFilters = _activeFilters.filter((item) => item !== filter);
    } else {
      _activeFilters = _activeFilters.filter((item) => item !== "All");
      _activeFilters.push(filter);
    }

    //Resets and sets filters
    if (_activeFilters.length === 0) {
      setActiveFilters(["All"]);
    } else {
      setActiveFilters(_activeFilters);
    }
  };

  return (
    <div className={styles.filters}>
      <div className="function-button-row">
        <p className={styles["filter-label"]}>
          <Translate id="toolCatalogue.filterByToolHeader" description="Tool Catalogue Filter by Tool Header">Filter by Tool Function</Translate>
        </p>
        {/* generates function filter buttons */}
        {Object.keys(functionTypes).map((filter) => (
          <button
            key={filter}
            className={`${activeFilters.includes(filter)
              ? [buttonStyles.functionButton, styles.filterButton].join(" ")
              : buttonStyles.functionButton
              } `}
            onClick={() => handleClick(filter)}
          >
            <div>
              <FontAwesomeIcon icon={functionTypes[filter].icon} />
            </div>
            <div className={styles.functionButtonName}>
              {functionTypes[filter].id}
            </div>
          </button>
        ))}
      </div>

      <div className="function-button-row">
        <p className={styles["filter-label"]}>
          <Translate id="toolCatalogue.filterByUserLevelHeader" description="Tool Catalogue Filter by User Level Header">Filter by User Level</Translate>
        </p>
        {/* generates user level filter buttons */}
        {filterLevels.map((filter) => (
          <button
            key={filter}
            className={`${activeFilters.includes(filter)
              ? [styles.filterButton, buttonStyles.functionButton].join(" ")
              : buttonStyles.functionButton
              } `}
            onClick={() => handleClick(filter)}
          >
            <div className={styles.functionButtonName}>{filter}</div>
          </button>
        ))}
      </div>
    </div>
  );
}

const ToolList = ({ tools, resetFilter, resetQuery }) => {
  if (tools.length === 0) {
    return (
      <div className={styles["empty-list"]}>
        <p>
          <Translate id="toolCatalogue.noToolsFoundLabel" description="Tool Catalogue No Tools Found label">No Tools Found!</Translate>
        </p>
        <button
          className={buttonStyles.primaryButton}
          onClick={() => {
            resetFilter(["All"]);
            resetQuery("");
          }}
        >
          <Translate id="toolCatalogue.resetFiltersLabel" description="Tool Catalogue Reset Filters label">Reset Filters</Translate>
        </button>
      </div>
    );
  }

  return (
    <div className={styles["tool-list"]}>
      {tools.map((x) => (
        <ToolCard
          key={x["Tool Name"]}
          {...{
            tool: x["Tool Name"],
            image: x["Image Path"],
            description: x["Description"],
            path: x["Path"],
            functions: x["Functions"],
          }}
        ></ToolCard>
      ))}
    </div>
  );
};

function ToolCatalogue({ preFilter = "All" }) {
  const [items, setItems] = useState([]);
  const [query, setQuery] = useState("");
  const [searchParam] = useState([
    "Tool Name",
    "Description",
    "Functions",
    "Keywords",
    "Level",
  ]);
  const [filters, setFilters] = useState(Array.isArray(preFilter) ? preFilter : [preFilter]);

  useEffect(() => {
    setItems(search(Tools));
  }, []);

  const search = (items) => {
    return items.filter((item) => {
      // Checking both lvl and fx filters for a match
      if (
        filters.every(
          (filt) => item.Functions.includes(filt) || item.Level.includes(filt)
        )
      ) {
        // Looking for exact string match, may want to change this to a more forgivable regex
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      } else if (filters == "All") {
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      }
    });
  };

  // Allows list to be prefiltered to be used on other pages
  // REVIEW: is it desirable to have input field and filters visible
  return preFilter != "All" ? (
    <ToolList tools={search(items)} />
  ) : (
    <>
      <ToolFilters
        setActiveFilters={setFilters}
        activeFilters={filters}
      ></ToolFilters>

      <input
        type="search"
        className={styles["tool-search"]}
        placeholder={translate({ id: "toolCatalogue.searchBarPlaceholder", description: "Tool Catalogue Search Bar Placeholder", message: "Search by title or keyword" })}
        value={query}
        onChange={(e) => setQuery(e.target.value)}
      ></input>
      <ToolList
        tools={search(items)}
        resetFilter={setFilters}
        resetQuery={setQuery}
      />
    </>
  );
}

export default ToolCatalogue