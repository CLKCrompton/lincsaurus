/** @format */

import React from "react";
import styles from "./links.module.css";
import Link from "@docusaurus/Link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPersonDigging } from "@fortawesome/free-solid-svg-icons";


function UnderConstruction({ children, link }) {
    if (link) {
        return (
          <Link className={styles.UnderConstruction} title="Coming Soon..." to={link}>
            {children} <FontAwesomeIcon icon={faPersonDigging} />
          </Link>
        );
    }
    return (
      <span className={styles.UnderConstruction} title="Coming Soon...">
        {children} <FontAwesomeIcon icon={faPersonDigging} />
      </span>
    );
}

export default UnderConstruction;
