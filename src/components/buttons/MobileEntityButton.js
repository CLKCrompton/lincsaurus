import React from 'react';
import Link from '@docusaurus/Link';
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

function MobileEntityButton({ link }) {
    return (
        <Link
            className={styles.mobileEntityButton}
            to={link}>
            View Entity
            <div className={styles.entityIconShift}>
                <FontAwesomeIcon icon={faUpRightFromSquare} />
            </div>
        </Link>
    );
}

export default MobileEntityButton;