import React from 'react';
import Link from '@docusaurus/Link';
import ErrorBoundary from '@docusaurus/ErrorBoundary';
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faMagnifyingGlass, faDiagramProject, faFileLines, faBroom, faLink, faArrowsRotate, faEye } from "@fortawesome/free-solid-svg-icons";
import functionTypes from "../functions";

library.add(
    faMagnifyingGlass,
    faDiagramProject,
    faFileLines,
    faBroom,
    faLink,
    faArrowsRotate,
    faEye
);

function ParseProps({ link, buttonName, icon }) {
    if (icon == null && link == null && buttonName == null) {
        console.error("Missing link, icon, and buttonName")
        throw new Error("Missing link, icon, and buttonName");
    }

    if (link && icon == null && buttonName == null) {
        console.warn("Missing icon and buttonName")
        return (
            <Link
                className={styles.functionButton}
                to={link}>
                <div>

                </div>
                <div className={styles.functionButtonName}>

                </div>
            </Link>
        )
    }

    if (icon && link == null && buttonName == null) {
        console.warn("Missing link and buttonName")
        return (
            <Link
                className={styles.functionButton}
            >
                <div>
                    <FontAwesomeIcon icon={icon} />
                </div>
                <div className={styles.functionButtonName}>

                </div>
            </Link>
        )
    }

    if (icon && link && buttonName == null) {
        console.warn("Missing buttonName")
        return (
            <Link
                className={styles.functionButton}
                to={link}>
                <div>
                    <FontAwesomeIcon icon={icon} />
                </div>
                <div className={styles.functionButtonName}>

                </div>
            </Link>
        )
    }

    if (link && buttonName && icon == null) {
        console.warn("Missing icon")
        return (
            <Link
                className={styles.functionButton}
                to={link}>
                <div>
                </div>
                <div className={styles.functionButtonName}>
                    {buttonName}
                </div>
            </Link>
        )
    }

    if (icon && buttonName && link == null) {
        console.warn("Missing link")
        return (
            <Link
                className={styles.functionButton}
            >
                <div>
                    <FontAwesomeIcon icon={icon} />
                </div>
                <div className={styles.functionButtonName}>
                    {buttonName}
                </div>
            </Link>
        )
    }

    if (icon && link && buttonName) {
        return (
            <Link
                className={styles.functionButton}
                to={link}>
                <div>
                    <FontAwesomeIcon icon={icon} />
                </div>
                <div className={styles.functionButtonName}>
                    {buttonName}
                </div>
            </Link>
        )
    }

    if (icon == null && link == null && functionTypes.hasOwnProperty(buttonName) == false) {
        console.error("Button name is invalid, needs to be one of 'Clean', 'Reconcile', 'Convert', 'Browse', 'Search' or 'Visualize'.")
        throw new Error("Button name is invalid, needs to be one of 'Clean', 'Reconcile', 'Convert', 'Browse', 'Search' or 'Visualize'")
    }

    return (
        <Link
            className={styles.functionButton}
            to={functionTypes[buttonName].link}>
            <div>
                <FontAwesomeIcon icon={functionTypes[buttonName].icon} />
            </div>
            <div className={styles.functionButtonName}>
                {functionTypes[buttonName].id}
            </div>
        </Link>
    );
}

function FunctionButton({ link = null, buttonName = null, icon = null }) {
    return (
        <ErrorBoundary
            fallback={({ error, tryAgain }) => (
                <div>
                    <p>
                        This component crashed because of the error:<br></br>
                        {error.message}.
                    </p>

                </div>
            )}
        >
            <ParseProps link={link} buttonName={buttonName} icon={icon} />
        </ErrorBoundary>
    );
}

export default FunctionButton;