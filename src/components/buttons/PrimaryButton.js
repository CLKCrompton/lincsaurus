import React from "react";
import Link from "@docusaurus/Link";
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function PrimaryButton({ link, buttonName, icon = null }) {
  if (icon) {
    return (
      <Link className={styles.primaryButton} to={link}>
        {buttonName}
        <div className={styles.iconShift}>
          {icon ? <FontAwesomeIcon icon={icon} /> : null}
        </div>
      </Link>
    );
  }

  return (
    <Link className={styles.primaryButton} to={link}>
      {buttonName}
      {icon ? <FontAwesomeIcon icon={icon} /> : null}
    </Link>
  );
}

export default PrimaryButton;
