import React from 'react';
import Link from '@docusaurus/Link';
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

function TileButton({ link, buttonName }) {
    return (
      <Link className={styles.tileButton} to={link}>
        {buttonName}
        <div className={styles.entityIconShift}>
          <FontAwesomeIcon icon={faUpRightFromSquare} />
        </div>
      </Link>
    );
}

export default TileButton;