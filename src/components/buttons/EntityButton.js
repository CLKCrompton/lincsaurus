import React from 'react';
import Translate from '@docusaurus/Translate';
import Link from '@docusaurus/Link';
import styles from "./buttons.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

function EntityButton({ link }) {
    return (
        <Link
            className={styles.entityButton}
            to={link}>
            <Translate id="homepage.carousel.viewEntityLabel" description="Carousel View Entity Button Label">View Image Source</Translate>
            <div className={styles.entityIconShift}>
                <FontAwesomeIcon icon={faUpRightFromSquare} />
            </div>
        </Link>
    );
}

export default EntityButton;