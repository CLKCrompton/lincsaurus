/** @format */

import React from "react";
import Link from "@docusaurus/Link";
import styles from "./buttons.module.css";

function HeroButton({ link, buttonName }) {
  return (
    <Link className={styles.heroButton} to={link}>
      {buttonName}
    </Link>
  );
}

export default HeroButton;
