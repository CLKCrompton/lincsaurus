/** @format */

import "@triply/yasgui/build/yasgui.min.css";
import React, { useState, useEffect } from "react";
import useIsBrowser from "@docusaurus/useIsBrowser";

const config = {
  yasqe: {
    persistent: false,
    value: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>`,
    endpoint: "https://fuseki.lincsproject.ca/lincs/sparql", // Set the default endpoint here
  },
  yasr: {
    plugins: {
      prefixes: {
        rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        rdfs: "http://www.w3.org/2000/01/rdf-schema#",
        owl: "http://www.w3.org/2002/07/owl#",
        ii: "http://id.lincsproject.ca/ii",
        cwrc: "http://id.lincsproject.ca/cwrc",
      },
    },
  },
  catalog: {
    endpoints: [
      {
        endpoint: "https://fuseki.lincsproject.ca/lincs/sparql",
        title: "LINCS",
      },
      {
        endpoint: "https://dbpedia.org/sparql",
        title: "DBpedia",
      },
      {
        endpoint: "https://query.wikidata.org/sparql",
        title: "Wikidata",
      },
    ],
  },
};

export default function App() {
  const isBrowser = useIsBrowser();

  useEffect(() => {
    if (isBrowser) {
      import("@triply/yasgui").then(({ default: Yasgui }) => {
        const yasgui = new Yasgui(document.getElementById("yasgui"), {
          requestConfig: {
            endpoint: "https://fuseki.lincsproject.ca/lincs/sparql",
          },
          copyEndpointOnNewTab: false,
          resizeable: true,
          autofocus: true,
          yasqe: {
            value: `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX lincs: <http://www.id.lincsproject.ca/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT * WHERE {
  ?sub ?pred ?obj .
} LIMIT 10

`,
          },
          yasr: {
            plugins: {
              prefixes: {
                rdf: "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                rdfs: "http://www.w3.org/2000/01/rdf-schema#",
                owl: "http://www.w3.org/2002/07/owl#",
                ii: "http://id.lincsproject.ca/ii",
                cwrc: "http://id.lincsproject.ca/cwrc",
                crm: "<http://www.cidoc-crm.org/cidoc-crm/>",
              },
            },
          },
          endpointCatalogueOptions: {
            getData: () => {
              return [
                //List of objects should contain the endpoint field
                //Feel free to include any other fields (e.g. a description or icon
                //that you'd like to use when rendering)
                {
                  title: "LINCS",
                  endpoint: "https://fuseki.lincsproject.ca/lincs/sparql",
                },
                {
                  title: "LINCS Vocabularies",
                  endpoint: "https://fuseki.lincsproject.ca/skosmos/sparql",
                },
                {
                  title: "DBpedia",
                  endpoint: "https://dbpedia.org/sparql",
                },
                {
                  title: "Wikidata",
                  endpoint: "https://query.wikidata.org",
                },
                // ...
              ];
            },
            keys: ["title"],
          },
        });

        console.log(yasgui.config);
      });
    }
    return () => {};
  }, [isBrowser]);

  return <div id="yasgui" />;
}
