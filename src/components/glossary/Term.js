import React from "react";
import styles from "./glossary.module.css";
import { Tooltip } from "@material-ui/core";
import { styled } from "@mui/styles";
import { Link } from "react-router-dom";

const popupStyle = {
  fontSize: "14px",
};

const StyledTooltip = styled(({ className, ...props }) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .MuiTooltip-tooltip`]: {
    backgroundColor: "rgba(0, 0, 0, 0.9)"
  },
}));

export default function Term(props) {
  return (
    <StyledTooltip title={<span style={popupStyle}>{props.popup}</span>} arrow={true} enterTouchDelay={300} leaveDelay={200}>
      <Link to={{ pathname: props.reference }} className={styles.termStyle}>
        {props.children}
      </Link>
    </StyledTooltip>
  );
}
