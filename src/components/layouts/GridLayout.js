import React from 'react';
import styles from "./layouts.module.css";

function GridLayout({ children }) {
    return (
        <div className={styles.gridLayout}>
            {children}
        </div>
    );
}

export default GridLayout;