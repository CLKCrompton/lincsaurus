import React from 'react';
import { useLocation } from 'react-router-dom';
import Translate from '@docusaurus/Translate';
import PrimaryButton from './buttons/PrimaryButton';
import term from "../plugins/terminology-parser-v2/term.json";
import frenchTerm from "../plugins/terminology-parser-v2/frenchTerm.json";
import styles from "./homepage.module.css";
import seedrandom from "seedrandom";

function HomepageTOD({ buttonName1, link1, buttonName2 }) {
    const currentDate = new Date().toISOString().slice(0, 10);

    // Use currentDate as the seed for seedrandom()
    const rng = seedrandom(currentDate);


    const randomTerm = term[Math.floor(rng() * term.length)];
    const randomFrenchTerm = frenchTerm[Math.floor(rng() * frenchTerm.length)];

    const location = useLocation();

    if (location.pathname == "/fr/") {
        return (
            <div className={styles.termOfTheDayContainer}>
                <div className={styles.termOfTheDay}>
                    <h3><Translate id="homepage.tod.todHeader" description="LINCS Term of the Day header">LINCS Term of the Day: </Translate>{randomFrenchTerm.term}</h3>
                    <p>{randomFrenchTerm.definition}</p>

                    <div className="tod-primary-button-row">
                        <PrimaryButton link={randomFrenchTerm.link.replace("/i18n/fr/docusaurus-plugin-content-docs/current/", "/docs/")} buttonName={buttonName1} />
                        <PrimaryButton link={link1} buttonName={buttonName2} />
                    </div>

                </div>
            </div>

        );
    }

    return (
        <div className={styles.termOfTheDayContainer}>
            <div className={styles.termOfTheDay}>
                <h3><Translate id="homepage.tod.todHeader" description="LINCS Term of the Day header">LINCS Term of the Day: </Translate>{randomTerm.term}</h3>
                <p>{randomTerm.definition}</p>

                <div className="tod-primary-button-row">
                    <PrimaryButton link={randomTerm.link} buttonName={buttonName1} />
                    <PrimaryButton link={link1} buttonName={buttonName2} />
                </div>

            </div>
        </div>

    );
}

export default HomepageTOD;