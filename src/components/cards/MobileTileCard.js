import React, { useState } from 'react';
import styles from "./cards.module.css";
import TileButton from '../buttons/TileButton';

function MobileTileCard({ src, title, description, link, buttonName }) {
    const [content, setContent] = useState(false);

    function handleClick() {
        setContent(!content);
    }

    return (
        <div onClick={handleClick} className={styles.mobileTile}>
            {content ?
                <img src={src} className={styles.mobileTileBackgroundOnClick} />
                :
                <img src={src} className={styles.mobileTileBackground} />
            }

            {content ?
                <div className={styles.mobileTileContentOnClick}>
                    <h1>{title}</h1>
                    <p>{description}</p>
                    <div className="tile-primary-button-row">
                        <TileButton
                            link={link}
                            buttonName={buttonName}
                        />
                    </div>
                </div>
                :
                <div className={styles.mobileTileContent}>
                    <h1>{title}</h1>
                </div>
            }
        </div>
    );
}

export default MobileTileCard;