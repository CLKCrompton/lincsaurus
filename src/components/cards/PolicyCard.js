import React from 'react';
import styles from "./cards.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function PolicyCard({ icon, title, link }) {
    return (
        <div className={styles.policyCard}>
            <FontAwesomeIcon icon={icon} size="4x" />

            <a href={link}>
                <h3>{title}</h3>
            </a>
        </div>
    );
}

export default PolicyCard;