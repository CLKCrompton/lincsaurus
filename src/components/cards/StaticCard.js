/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";
import Link from '@docusaurus/Link';

function StaticCard({
  src,
  title,
  description,
  buttonName1,
  buttonName2,
  link1,
  link2,
  entityLink,
}) {
  return (
    <Link to={link1} className={styles.staticCardLink}>
      <div className={styles.cardContainer}>
        {/* {entityLink ? (
        <div className={styles.entity}>
          <EntityButton link={entityLink} />
        </div>
      ) : null} */}

        <div className={styles.cardTitle}>
          <h2>{title}</h2>
        </div>

        <img src={src} alt="" className={styles.cardImage} />

        <div className={styles.staticCardDescription}>
          <p>{description}</p>
        </div>

        {/*<div className="spotlight-primary-button-row">
          <PrimaryButton link={link1} buttonName={buttonName1} />
          <PrimaryButton link={link2} buttonName={buttonName2} />
      </div>*/}
      </div>
    </Link>
  );
}

export default StaticCard;
