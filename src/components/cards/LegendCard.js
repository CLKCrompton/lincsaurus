import React from 'react';
import clsx from 'clsx';
import uniIcon from '@site/static/img/team-legend/bg-team-university-(c-unknown-icon).png'
import personalIcon from '@site/static/img/team-legend/bg-team-personal-(c-unknown-icon).png'
import projectIcon from '@site/static/img/team-legend/bg-team-project-(c-unknown-icon).png'
import blogIcon from '@site/static/img/team-legend/bg-team-blog-(c-unknown-icon).png'
import githubIcon from '@site/static/img/team-legend/bg-team-github-(c-unknown-icon).png'
import gitlabIcon from '@site/static/img/team-legend/bg-team-gitlab-(c-unknown-icon).png'
import twitterIcon from '@site/static/img/team-legend/bg-team-twitter-(c-unknown-icon).png'

function LegendCard() {
    return (
        <div className={clsx('col-12')}>
            <div className="card">
                <div className="card__header">
                    <h3>Legend</h3>
                </div>
                <div className="card__body">
                    <ul>
                        <img src={uniIcon} alt="" />University
                        <img src={personalIcon} alt="" />Personal
                        <img src={projectIcon} alt="" />Project
                        <img src={blogIcon} alt="" />Blog
                        <img src={githubIcon} alt="" />GitHub
                        <img src={gitlabIcon} alt="" />GitLab
                        <img src={twitterIcon} alt="" />Twitter
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default LegendCard;