import React from 'react';
import styles from "./cards.module.css";
import PrimaryButton from '../buttons/PrimaryButton';
import MobileEntityButton from "../buttons/MobileEntityButton";
import { faUpRightFromSquare } from '@fortawesome/free-solid-svg-icons';

function MobileCarouselCard({ src, title, description, buttonName1, link1, entityLink }) {
    return (
      <div className={styles.background}>
        <div className={styles.mobileCarouselContent}>
          <h2>{title}</h2>
        </div>

        <div className={styles.entityContainer}>
          {src ? (
            <img className={styles.mobileCarouselImage} src={src} alt="" />
          ) : (
            <div className={styles.mobileRectangle}></div>
          )}

          <div className={styles.mobileEntity}>
                    {entityLink ? <MobileEntityButton link={entityLink} /> : null}
                </div>
        </div>

        <div className={styles.mobileCarouselDescription}>
          <p>{description}</p>
        </div>

        <div className="mobile-carousel-primary-button-row">
          <PrimaryButton
            link={link1}
            buttonName={buttonName1}
            icon={faUpRightFromSquare}
          />
        </div>
      </div>
    );
}

export default MobileCarouselCard;