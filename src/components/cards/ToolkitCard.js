import React from 'react';
import Link from '@docusaurus/Link';
import clsx from 'clsx';

function ToolkitCard({ toolTitle, description, src, alt, link, iconSrc, iconAlt }) {
    return (
        <div className={clsx('col-lg-6 col-md-6 col-12')}>
            <div className="card">
                <div className="card__image">
                    <img src={src} alt={alt} />
                </div>

                <div className="card__body">
                    <h4>{toolTitle}</h4>
                    <small>{description}</small>
                </div>

                <div className="card__footer">
                    <a href={link}>
                        <img src={iconSrc} alt={iconAlt} />
                    </a>
                </div>
            </div>
        </div>
    );
}

export default ToolkitCard;