/** @format */

import React from "react";
import styles from "./cards.module.css";
import PrimaryButton from "../buttons/PrimaryButton";
import EntityButton from "../buttons/EntityButton";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

function CarouselCard({
  src,
  title,
  description,
  buttonName1,
  link1,
  entityLink,
}) {
  return (
    <div className={styles.slide}>
      <div className={styles.carouselBackground}>
        <div className={styles.carouselContent}>
          <h1>{title}</h1>
        </div>

        <div className={styles.carouselImage}>
          {src ? (
            <img src={src} alt=""></img>
          ) : (
            <div className={styles.rectangle}></div>
          )}
          <div className={styles.entity}>
            {entityLink ? <EntityButton link={entityLink} /> : null}
          </div>
        </div>

        <div className={styles.carouselDescription}>
          <p>{description}</p>
        </div>
        <div className="carousel-primary-button-row">
          <PrimaryButton
            link={link1}
            buttonName={buttonName1}
            icon={faUpRightFromSquare}
          />
        </div>
      </div>
    </div>
  );
}

export default CarouselCard;
