/** @format */

import React from "react";
import Link from "@docusaurus/Link";
import styles from "./cards.module.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import StaticCard from "./StaticCard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState, useEffect } from "react";

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

library.add(faUpRightFromSquare);

const QUERY = `
SELECT (COUNT(DISTINCT ?graph) AS ?count) (SUM(?tripleCount) AS ?totalTripleCount) {
  {
    SELECT ?graph (COUNT(*) AS ?tripleCount) {
      GRAPH ?graph { 
        ?s ?p ?o 
      }
      FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/"))
    }
    GROUP BY ?graph
  }
}
`;

const queries = {
  datasets: `
  SELECT (COUNT(DISTINCT ?g) as ?count) WHERE {
 GRAPH ?g {
 ?sub ?pred ?obj .
 }
  FILTER(STRSTARTS(STR(?g), "http://graph.lincsproject.ca/"))
}
`,
  triples: `
  SELECT (SUM(?tripleCount) AS ?count) {
  {
    SELECT ?graph (COUNT(*) AS ?tripleCount) {
      GRAPH ?graph { 
        ?s ?p ?o 
      }
      FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/"))
    }
    GROUP BY ?graph
  }
}
`,
  "entity types": `
PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>
select (sum(?entityCount) as ?count) {
SELECT ?entityType  (COUNT(?s) AS ?entityCount) {
  GRAPH ?graph {
    values ?entityType {
      crm:E21_Person
      crm:E53_Place
      crm:E74_Group
      #      Events
      crm:E5_Event
      crm:E7_Activity
      crm:E8_Acquisition
      crm:E12_Production
      crm:E13_Attribute_Assignment
      crm:E67_Birth
      crm:E69_Death
      crm:E65_Creation
      crm:E66_Formation
      frbroo:F51_Pursuit

      #      Creative Works
      frbroo:F1_Work
      crm:E36_Visual_Item
    }
    ?s a ?entityType
  }
  FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/"))
} group by ?entityType
}
`,
};

const queryURIs = {
  datasets:
    "https://fuseki.lincsproject.ca/lincs/sparql?query=SELECT%20%28COUNT%28DISTINCT%20%3Fg%29%20as%20%3Fcount%29%20WHERE%20%7B%0A%20GRAPH%20%3Fg%20%7B%0A%20%3Fsub%20%3Fpred%20%3Fobj%20.%0A%20%7D%0A%20%20FILTER%28STRSTARTS%28STR%28%3Fg%29%2C%20%22http%3A%2F%2Fgraph.lincsproject.ca%2F%22%29%29%0A%7D",
  triples:
    "https://fuseki.lincsproject.ca/lincs/sparql?query=SELECT%20%28SUM%28%3FtripleCount%29%20AS%20%3Fcount%29%20%7B%0A%20%20%7B%0A%20%20%20%20SELECT%20%3Fgraph%20%28COUNT%28%2A%29%20AS%20%3FtripleCount%29%20%7B%0A%20%20%20%20%20%20GRAPH%20%3Fgraph%20%7B%20%0A%20%20%20%20%20%20%20%20%3Fs%20%3Fp%20%3Fo%20%0A%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20FILTER%28STRSTARTS%28STR%28%3Fgraph%29%2C%20%22http%3A%2F%2Fgraph.lincsproject.ca%2F%22%29%29%0A%20%20%20%20%7D%0A%20%20%20%20GROUP%20BY%20%3Fgraph%0A%20%20%7D%0A%7D",
};

function StatCard({ heading, text, queryType }) {
  const [count, setCount] = useState(heading);

  useEffect(() => {
    const fetchDataByQuery = async () => {
      try {
        const response = await fetch(
          "https://fuseki.lincsproject.ca/lincs/sparql",
          {
            method: "POST",
            headers: { "Content-Type": "application/sparql-query" },
            body: queries[queryType],
          },
        );
        if (!response.ok) {
          throw new Error("Failed to fetch results from SPARQL endpoint");
        }
        const data = await response.json();
        const temp = data.results.bindings[0].count.value;
        setCount(
          new Intl.NumberFormat("en-CA", {
            maximumSignificantDigits: 3,
          }).format(temp),
        );
      } catch (error) {
        console.error(error);
      }
    };
    const fetchDataByURL = async () => {
      try {
        const response = await fetch(queryURIs[queryType], {
          method: "POST",
          headers: { "Content-Type": "application/x-www-form-urlencoded" },
        });
        console.log(response);
        if (!response.ok) {
          throw new Error("Failed to fetch results from SPARQL endpoint");
        }
        const data = await response.json();
        const temp = data.results.bindings[0].count.value;
        setCount(temp);
      } catch (error) {
        console.error(error);
      }
    };
    if (queryType) {
      fetchDataByQuery();
    }
  }, []);

  return (
    <div className={styles.stats}>
      <div className={styles.statsContainer}>
        <div className={styles.cardTitle}>
          <h2>
            {count} {queryType != "datasets" ? "+" : null}
          </h2>
        </div>

        <div className={styles.staticCardDescription}>
          <p>{text}</p>
        </div>
      </div>
    </div>
  );
}

export default StatCard;
