/** @format */

import React from "react";
import styles from "./cards.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faGitlab,
  faGithub,
  faOrcid,
} from "@fortawesome/free-brands-svg-icons";
import { faIdCard } from "@fortawesome/free-solid-svg-icons";
library.add(faOrcid, faIdCard, faGithub, faGitlab);
function TeamCard({
  firstName,
  lastName,
  imageExists,
  uni,
  jobTitle,
  website,
  gitlab,
  github,
  orcid,
  start,
  end,
}) {
  // function TeamCard({ src, alt, name, job, uni }) {

  const src = imageExists ? `${firstName}${lastName}-(c-LINCS)` : "anonymous-(c-unknown-icon)";
  const startDate = start ? start : `2020`;
  const endDate = end ? end : ``;
  const date = `${startDate}–${endDate}`;

  return (
    <div className={styles.card} id={`${firstName}${lastName}`}>
      <div className="avatar avatar--vertical">
        <img
          className="avatar__photo avatar__photo--xl margin-vert--md"
          src={`/img/staff/${src}.png`}
        />
        <div className="avatar__intro">
          <div className="avatar__name">
            <h4 className="avatar__subtitle">
              {firstName} {lastName}
            </h4>
          </div>
          <h4 className="avatar__subtitle">{jobTitle}</h4>
          <h5>{date}</h5>

          <small className="avatar__subtitle">{uni}</small>

          <div className={styles["link-row"]}>
            {website ? (
              <a href={website} title="Personal Site">
                <FontAwesomeIcon icon={"fa-solid fa-id-card"} />
              </a>
            ) : null}
            {orcid ? (
              <a href={orcid} title="Orcid ID">
                <FontAwesomeIcon icon={"fa-brands fa-orcid"} />
              </a>
            ) : null}
            {gitlab ? (
              <a href={gitlab} title="GitLab Profile">
                <FontAwesomeIcon icon={"fa-brands fa-gitlab"} />
              </a>
            ) : null}
            {github ? (
              <a href={github} title="Github Profile">
                <FontAwesomeIcon icon={"fa-brands fa-github"} />
              </a>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}

export default TeamCard;
