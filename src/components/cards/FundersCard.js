import React from 'react';

function FundersCard({ src, alt, link }) {
    return (
        <div>
            <a href={link}>
                <img src={src} alt={alt} />
            </a>
        </div>
    );
}

export default FundersCard;