import React from 'react';
// Import the original mapper
import MDXComponents from '@theme-original/MDXComponents';

import PrimaryButton from '@site/src/components/buttons/PrimaryButton';
import FunctionButton from '@site/src/components/buttons/FunctionButton';
import UnderConstruction from "@site/src/components/links/UnderConstructionLink";

/*import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

library.add(faUpRightFromSquare)*/

export default {
  ...MDXComponents,
  primarybutton: PrimaryButton,
  functionbutton: FunctionButton,
  underconstruction: UnderConstruction,
};