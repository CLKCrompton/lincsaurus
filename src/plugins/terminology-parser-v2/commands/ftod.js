const fs = require("fs");
const parser = require("./parser.js");
const path = require("path");
const {
    getFiles,
    preloadTerms,
    getCleanTokens,
    cleanGlossaryTerms,
    filterTypeTerms,
    getRelativePath,
    getTermOfTheDay,
    sortFiles,
    getOrCreateGlossaryFile,
    getOrCreateTermOfTheDayFile
} = require("../lib.js");

async function frenchTermOfTheDay(options) {
    options.dryRun && console.log("\n* Dry run enabled *\n");

    let glossaryContent = "";
    if (options.glossaryPatternSeparator) {
        options.patternSeparator = options.glossaryPatternSeparator;
    }

    // Load the term files
    let termsFiles = [];

    try {
        termsFiles = await getFiles(options.frenchTermsDir, options.noGlossaryFiles);
    } catch (err) {
        console.log(`\u26A0  Not able to get files from folder: ${options.frenchTermsDir}`);
        console.log(`Check the path in option "termsDir"\n\n ${err} \nExiting...`);
        process.exit(1);
    }

    if (termsFiles.length == 0) {
        console.log(`\u26A0  No term files found`);
        console.log(`Might be wrong path "${options.termsDir}" in option ` +
            `"termsDir" or empty folder \nExiting...`);
        process.exit(1);
    }

    const termsData = await preloadTerms(termsFiles);

    // remove terms that don't have title or hoverText
    let cleanTerms = cleanGlossaryTerms(termsData);

    let termsByType = filterTypeTerms(cleanTerms, options.glossaryTermPatterns);

    // sort termsData alphabetically
    sortFiles(termsByType);

    // append terms to the json
    for (const term of termsByType) {
        const current_file_path = path.resolve(process.cwd(), options.frenchTermOfTheDayFilePath);
        const relativePath = getRelativePath(current_file_path, term.filepath, options);
        const glossaryTerm = getTermOfTheDay(term, relativePath);
        glossaryContent = glossaryContent + glossaryTerm;
    }

    if (options.dryRun) {
        console.log(`\n! These changes will not be applied in the glossary file.` +
            `\nShowing the output below:\n\n${(glossaryContent)}\n\n`);
    } else {
        const glossaryFile = getOrCreateTermOfTheDayFile(options.frenchTermOfTheDayFilePath);
        try {
            const result = await fs.promises.writeFile(
                options.frenchTermOfTheDayFilePath, glossaryFile + glossaryContent.substring(0, glossaryContent.length - 4) + '\n]', "utf-8");
        } catch (err) {
            console.log(`\u26A0  An error occurred while writing new data to ` +
                `the file: ${options.frenchTermOfTheDayFilePath}\n ${err} \nExiting...`);
            process.exit(1);
        }
    }
    console.log(`\u2713 ${termsByType.length} terms found.`)
};

module.exports = frenchTermOfTheDay;

