const path = require("path");

const parser = require("./commands/parser.js");
const frenchParser = require("./commands/frenchParser.js");
const glossary = require("./commands/glossary.js");
const termOfTheDay = require("./commands/tod.js");
const frenchTermOfTheDay = require("./commands/ftod.js");
const validateOptions = require("./validator.js");

const DEFAULT_OPTIONS = {
    docsDir: "./docs/",
    frenchDocsDir: "./i18n/fr/docusaurus-plugin-content-docs/current/",
    termsDir: "./docs/terms/",
    frenchTermsDir: "./i18n/fr/docusaurus-plugin-content-docs/current/terms/",
    termsUrl: "/docs/terms",
    frenchTermsUrl: "/fr/docs/terms",
    glossaryFilepath: "./docs/glossary.md",
    termOfTheDayFilePath: "./src/plugins/terminology-parser-v2/term.json",
    frenchTermOfTheDayFilePath: "./src/plugins/terminology-parser-v2/frenchTerm.json",
    patternSeparator: "|",
    noParseFiles: [],
    noGlossaryFiles: [],
    glossaryTermPatterns: [],
    dryRun: false,
    debug: false
};

module.exports = function (context, opts) {
    // initialize options
    let options = {};
    !opts.termsDir && console.log(`\n! No option for terms directory found, ` +
        `using default directory "${DEFAULT_OPTIONS.termsDir}"\n`);
    !opts.frenchTermsDir && console.log(`\n! No option for terms directory found, ` +
        `using default directory "${DEFAULT_OPTIONS.frenchTermsDir}"\n`);
    options = Object.assign({}, DEFAULT_OPTIONS, opts);
    validateOptions(options);
    options.termsUrl = path.join(context.baseUrl, options.termsDir, "/");
    options.frenchTermsUrl = path.join(context.baseUrl, options.frenchTermsDir, "/");
    options.termsDir = path.resolve(options.termsDir) + "/";
    options.frenchTermsDir = path.resolve(options.frenchTermsDir) + "/";
    options.docsDir = path.resolve(options.docsDir) + "/";
    options.frenchDocsDir = path.resolve(options.frenchDocsDir) + "/";
    options.glossaryFilepath = path.resolve(options.glossaryFilepath);
    for (const [index, item] of options.noParseFiles.entries()) {
        options.noParseFiles[index] = path.resolve(process.cwd(), item);
    }
    for (const [index, item] of options.noGlossaryFiles.entries()) {
        options.noGlossaryFiles[index] = path.resolve(process.cwd(), item);
    }

    return {
        name: "terminology-parser",
        extendCli(cli) {
            cli
                .command("parse")
                .option("--dry-run", "see what the command will do")
                .option("--debug", "see all log output of the command")
                .description("Parse all md files to replace terms")
                .action((args) => {
                    // check for dry-run and debug
                    options.dryRun = args.dryRun ? true : false;
                    options.debug = args.debug ? true : false;

                    console.log("Replacing patterns with <Term />");
                    parser(options);
                });
            cli
                .command("parseFrench")
                .option("--dry-run", "see what the command will do")
                .option("--debug", "see all log output of the command")
                .description("Parse all md files to replace terms")
                .action((args) => {
                    // check for dry-run and debug
                    options.dryRun = args.dryRun ? true : false;
                    options.debug = args.debug ? true : false;

                    console.log("Replacing patterns with <Term />");
                    frenchParser(options);
                });
            cli
                .command("glossary")
                .option("--dry-run", "see what the command will do")
                .option("--debug", "see all log output of the command")
                .description("Generate a glossary of terms")
                .action((args) => {
                    // check for dry-run and debug
                    options.dryRun = args.dryRun ? true : false;
                    options.debug = args.debug ? true : false;

                    console.log("Alphabetical list of terms");
                    glossary(options);
                });
            cli
                .command("termOfTheDay")
                .option("--dry-run", "see what the command will do")
                .option("--debug", "see all log output of the command")
                .description("Generate a json of terms")
                .action((args) => {
                    // check for dry-run and debug
                    options.dryRun = args.dryRun ? true : false;
                    options.debug = args.debug ? true : false;

                    console.log("Adding terms to json");
                    termOfTheDay(options);
                });
            cli
                .command("frenchTermOfTheDay")
                .option("--dry-run", "see what the command will do")
                .option("--debug", "see all log output of the command")
                .description("Generate a json of terms")
                .action((args) => {
                    // check for dry-run and debug
                    options.dryRun = args.dryRun ? true : false;
                    options.debug = args.debug ? true : false;

                    console.log("Adding terms to json");
                    frenchTermOfTheDay(options);
                });
        },
    };
};

