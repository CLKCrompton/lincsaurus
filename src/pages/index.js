/** @format */

import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import { useLocation } from "react-router-dom";
import Translate, { translate } from "@docusaurus/Translate";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import styles from "./index.module.css";
import HomepageFeatures from "../components/HomepageFeatures";
import HomepageCarousel from "../components/HomepageCarousel";
import HomepageTileBanner from "../components/HomepageTileBanner";
import HomepageTOD from "../components/HomepageTOD";
import StaticCard from "../components/cards/StaticCard";
import StatCard from "../components/cards/StatCard";
import PrimaryButton from "../components/buttons/PrimaryButton";
import HeroButton from "../components/buttons/HeroButton";
import GridLayout from "../components/layouts/GridLayout";
import blogImage1 from "../../blog/2023-04-14-piecing-the-past/piecing-the-past-unsplash-(cc0).jpg";
import blogImage2 from "../../blog/2023-02-01-marvels-of-api/marvels-of-api-unsplash-(cc0).jpg";
import lincsLogo from "@site/static/img/index-page-images/lincs-stacked-logo-(c-LINCS).png";
import frenchLINCSLogo from "@site/static/img/logos/lincs-logos/LINCS-French-dark-(c-LINCS).png";
import { Grid } from "swiper";

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();

  const location = useLocation();

  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="hero-container">
        {location.pathname == "/fr/" ? (
          <img src={frenchLINCSLogo} alt="LINCS Logo" className="hero-image" />
        ) : (
          <img src={lincsLogo} alt="LINCS Logo" className="hero-image" />
        )}
        <div className="hero-content">
          <h2>
            <Translate
              id="homepage.heroBanner.h2"
              description="The h2 tag for the hero banner">
              Create and explore cultural data
            </Translate>
          </h2>
          <p>
            <Translate
              id="homepage.heroBanner.p"
              description="The p tag for the hero banner">
              LINCS provides the tools and infrastructure to make humanities
              data more discoverable, searchable, and shareable. Discover how
              you can explore, create, and publish cultural data.
            </Translate>
          </p>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`Home`}
      description="Documentation site for the LINCS project">
      <HomepageHeader />
      <section className={styles.homeSection}>
        <div className={styles.container}>
          <div className="hero-primary-button-row">
            <HeroButton
              link="/docs/about-lincs/"
              buttonName={translate({
                id: "homepage.heroBanner.aboutButton",
                message: "What is the LINCS Project?",
                description:
                  "The hero banner about LINCS project button message",
              })}
            />

            <HeroButton
              link="/docs/get-started"
              buttonName={translate({
                id: "homepage.heroBanner.linkedDataButton",
                message: "How do I get started with Linked Open Data? ",
                description: "The hero banner Linked Data button message",
              })}
            />

            <HeroButton
              link="/docs/explore-data/"
              buttonName={translate({
                id: "homepage.heroBanner.exploreButton",
                message: "What kind of data does LINCS have?",
                description: "The hero banner explore button message",
              })}
            />
            {/* TODO: Update ID of button */}
            <HeroButton
              link="/docs/create-data/publish-data/"
              buttonName={translate({
                id: "homepage.heroBanner.contributeDataButton",
                message: "Why should I publish my data with LINCS?",
                description: "The hero banner publish data button message",
              })}
            />
          </div>
        </div>
      </section>

      <div>
        <main>
          <section className={styles.homeSection}>
            <div className={styles.container}>
              <h1 className={styles.header}>
                <Translate
                  id="homepage.carousel.header"
                  description="The carousel header">
                  Explore cultural data with LINCS
                </Translate>
              </h1>
              <HomepageTileBanner />
              <GridLayout>
                <StatCard
                  heading={"5,000,000"}
                  text={"Triples in Progress"}
                  queryType={"triples"}
                />
                <StatCard
                  heading={"250,000"}
                  text={"People, Places, Groups, Events, and Creative Works"}
                  queryType={"entity types"}
                />
                <StatCard
                  heading={"7"}
                  text={"Datasets in Progress"}
                  queryType={"datasets"}
                />
                <StatCard
                  heading={"100"}
                  text={"Researchers, Students, Staff and Partners"}
                />
              </GridLayout>
            </div>
          </section>

          <section className={styles.homeSection}>
            <div className={styles.container}>
              <h1 className={styles.header}>LINCS Datasets</h1>
              <HomepageCarousel />
            </div>
          </section>

          <section className={styles.homeSection}>
            <div className={styles.container}>
              {/* <h1 className={styles.header}>
                <Translate
                  id="homepage.spotlight.header"
                  description="The spotlight header">
                  Events
                </Translate>
              </h1> */}
              <HomepageTOD
                buttonName1={translate({
                  id: "homepage.tod.learnMoreButton",
                  message: "Learn More",
                  description: "The term of the day Learn More button",
                })}
                link1="/docs/get-started/glossary"
                buttonName2={translate({
                  id: "homepage.tod.viewGlossaryButton",
                  message: "View Glossary",
                  description: "The term of the day View Glossary button",
                })}
              />

              {/* <GridLayout>
                <StaticCard
                  src={blogImage1}
                  title={translate({
                    id: "homepage.staticCard.blog1Title",
                    message: "The Good Enough Metadata Specialist",
                    description: "The spotlight blog1 title",
                  })}
                  description={translate({
                    id: "homepage.staticCard.blog1Description",
                    message:
                      "My first job in the museums field was in 2008, right at the height of the Great Recession. The digitization team I joined had just lost roughly a quarter of their staff in a series of buyouts and layoffs, and the mood was grim. We were tasked with getting a large collection of...",
                    description: "The spotlight blog description",
                  })}
                  buttonName1={translate({
                    id: "homepage.staticCard.readMoreButton",
                    message: "Read More",
                    description: "The spotlight Read More button label",
                  })}
                  buttonName2={translate({
                    id: "homepage.staticCard.viewAllBlogsButton",
                    message: "View All Blog Posts",
                    description: "The spotlight View All Blogs button label",
                  })}
                  link1="/blog/metadata-specialist"
                  link2="/blog"
                />

                <StaticCard
                  src={blogImage2}
                  title={translate({
                    id: "homepage.staticCard.blog2Title",
                    message:
                      "Speaking in Different Languages - Working Across Groups and Disciplines",
                    description: "The spotlight blog title",
                  })}
                  description={translate({
                    id: "homepage.staticCard.blog2Description",
                    message:
                      "The situation is this: you’ve been asked to design a way for a researcher to easily move between two tools. You familiarize yourself with both tools, learn about the known issues, and read about what’s already been tried. You spend a few days deciding on the best way to get from...",
                    description: "The spotlight blog description",
                  })}
                  buttonName1={translate({
                    id: "homepage.staticCard.readMoreButton",
                    message: "Read More",
                    description: "The spotlight Read More button label",
                  })}
                  buttonName2={translate({
                    id: "homepage.staticCard.viewAllBlogsButton",
                    message: "View All Blog Posts",
                    description: "The spotlight View All Blogs button label",
                  })}
                  link1="/blog/speaking-in-different-languages"
                  link2="/blog"
                />
              </GridLayout> */}

              <h1 className={styles.header}>
                <Translate
                  id="homepage.spotlight.header"
                  description="The spotlight header">
                  Blog Posts
                </Translate>
              </h1>

              <div className={styles.blogPreview}>
                <GridLayout>
                  <StaticCard
                    src={blogImage1}
                    title={translate({
                      id: "homepage.staticCard.blog1Title",
                      message: "Piecing the Past Together With LOD",
                      description: "The spotlight blog1 title",
                    })}
                    description={translate({
                      id: "homepage.staticCard.blog1Description",
                      message:
                        "I've always found that context changes everything when learning something new, especially when it comes to understanding why that something matters. The first example I can think of is how, for all the general chemistry courses I’ve taken, the concepts never really clicked, nor did I see why I had to learn them.",
                      description: "The spotlight blog description",
                    })}
                    buttonName1={translate({
                      id: "homepage.staticCard.readMoreButton",
                      message: "Read More",
                      description: "The spotlight Read More button label",
                    })}
                    buttonName2={translate({
                      id: "homepage.staticCard.viewAllBlogsButton",
                      message: "View All Blog Posts",
                      description: "The spotlight View All Blogs button label",
                    })}
                    link1="/blog/piecing-the-past"
                    link2="/blog"
                  />

                  <StaticCard
                    src={blogImage2}
                    title={translate({
                      id: "homepage.staticCard.blog2Title",
                      message:
                        "Marvels of API Communication - The Diffbot NLP Vetting Tool",
                      description: "The spotlight blog title",
                    })}
                    description={translate({
                      id: "homepage.staticCard.blog2Description",
                      message:
                        "If you've ever listened in on a technical conversation between computer scientists, you might've heard them refer to something called an Application Programming Interface (API). What exactly is an API? Let's break it down.",
                      description: "The spotlight blog description",
                    })}
                    buttonName1={translate({
                      id: "homepage.staticCard.readMoreButton",
                      message: "Read More",
                      description: "The spotlight Read More button label",
                    })}
                    buttonName2={translate({
                      id: "homepage.staticCard.viewAllBlogsButton",
                      message: "View All Blog Posts",
                      description: "The spotlight View All Blogs button label",
                    })}
                    link1="/blog/marvels-of-api"
                    link2="/blog"
                  />
                </GridLayout>
              </div>
            </div>
          </section>
        </main>
      </div>
    </Layout>
  );
}
