# The LINCS portal

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

## Installation

You will need Git (with an ssh key set up to connect to GitLab), NodeJS (v16 or newer), and VSCode to develop this website.

```bash
git clone git@gitlab.com:calincs/admin/lincsaurus.git
cd lincsaurus
```

## Local development

```bash
npm install --force
npm start
```

This command starts a local development server at `http://localhost:3000/` and opens up a browser window. Most changes are reflected live without having to restart the server.

### Making Edits

Prior to making any edits for the first time, you must review the [Project Documentation Guide](https://portal.lincsproject.ca/about/resources/lincsaurus/lincsaurus-overview), particularly the [Accessibility](https://portal.lincsproject.ca/about/resources/lincsaurus/accessibility-guide) & [Style](https://portal.lincsproject.ca/about/resources/lincsaurus/style-guide) Guide for guidelines on writing documentation. 

See the [Maintenance Guide](https://portal.lincsproject.ca/about/resources/lincsaurus/maintenance) for instructions on how to get started making edits.

## Deployment

Commit and push the changes back to the upstream repo on GitLab. Please look at the _translation_, _glossary_, and _index_ sections below to update these add-ons.

## Translating the site

We use the [Translate-Docusaurus](https://github.com/GBADsInformatics/Translate-Docusaurus) repository to translate the Docusaurus site.

### Installing for the first time

Clone the _Translate-Docusaurus_ repo above, `cd` into it, and run `./install.sh` to install dependencies. Then run `./convertLang.py` and enter the ISO code and the full path of the Docusaurus repo. Once the program is done, you will find the translated files under `i18n/your_iso_code`.

### Updating translations

If a new folder is added that you would also like translated after you have translated the site before, do not run `./convertLang.py` again. Instead, `cd` to the `old_versions` directory in the _Translate-Docusaurus_ repo. Once there, run `python3 translate-markdown.py` and enter the ISO code and the full path of the new folder that was created in your Docusaurus repo. The newly translated files will also appear under `i18n/your_iso_code`.

### View changes

To see the converted ISO locally at `http://localhost:3000/`, run the following command in your Docusaurus repo:

```bash
npm run start -- --locale your_iso_code
```

If you want to be able to see both the English version and your ISO code translations, run `npm run build` and then `npm run serve`.

The translations are not always perfect, please double check your files have the correct translation after conversion before committing your changes.

## Updating the glossary

### Defining new terms

We use the [Terminology plugin](https://gitlab.grnet.gr/terminology/docusaurus-terminology) to manage our glossary.

To create terms that will appear throughout the site, create a `.md(x)` file inside a `.docs/term` directory and have it contain the following structure:

```md
---
id: term_name
title: Term page
hoverText: This hover text will appear in the documentation page where you reference this term
---

content here
```

### Term explanation

In order to refer the term in any other file in the `docs` folder, use the following syntax:

```md
%%term_text|term_name%%
```

where `term_text` is the term text that will be visible in your documentation and `term_name` is the value of the `id` attribute in the term file.

**For a more detailed example, please follow the instructions [here](https://gitlab.grnet.gr/terminology/docusaurus-terminology)**

#### Term Styling

In order to style the term correctly, please replace `import Term from "@docusaurus-terminology/term";` with `import Term from "@site/src/components/glossary/Term";`

### Updating the repository

Once all your terms have been referenced and the relevant term pages have been created, run the following commands to process the changes locally and generate a glossary page:

```bash
npm run docusaurus parse
npm run docusaurus glossary
```

The term files and glossary should now be visible on `http://localhost:3000/` after running `npm run start`.

You can now commit changes to the git repository.

## Rebuilding the search index

We use a _Typesense_ index for searching the Lincsaurus site. The index is hosted on LINCS infrastruture and is updated with the CI pipeline. If you want to host an index locally, you'd need to rebuild it manually if we wish to update it.

Start the Typesense server:

```bash
./typesense.sh
```

Then run the scraper in a new terminal, which will index `https://portal.lincsproject.ca` with the current configuration and write the index to your local typesense server:

```bash
./scraper.sh
```
