#!/usr/bin/python3
import os

def listfiles(path):
    files = []
    for dirName, subdirList, fileList in os.walk(path):
        dir = dirName.replace(path, '')
        if len(fileList) == 0 and len(subdirList) == 0:
            print(F"Empty directory Found: {dirName}")
        for fname in fileList:
            files.append(os.path.join(dir, fname))
    return files

def main():
    dir_1 = "docs"
    dir_2 = "i18n/fr/docusaurus-plugin-content-docs/current"
    file_paths_1 = listfiles(dir_1)
    file_paths_2 = listfiles(dir_2)
    files_only_in_either = set(file_paths_1) ^ set(file_paths_2)
    

    print()
    print(F"{len(file_paths_1)} files is within {dir_1}")
    print(F"{len(file_paths_2)} files is within {dir_2}\n")

    if (len(files_only_in_either) > 0):
        print("Files that only exist in one directory:")
        for file in files_only_in_either:
            if file in file_paths_1:
                print(F"{dir_1} has {file}")
            elif file in file_paths_2:
                print(F"{dir_2} has {file}")
        print("\n"*2)
        exit(1)
    else:
        print("Good Job! File hierarchy is in sync")
        exit(0)


if __name__ == '__main__':
    main()
    
