---
title: "Overview Template" # Replace with PAGE TITLE (This will be the first heading and also the name on your tab)
description: "template for default landing pages of sub-categories" # REPLACE DESCRIPTION (this will appear in cards and social links and metadata about the page, keep it short)
draft: true # REMOVE THIS LINE
---

<!-- @format -->

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# TITLE

<!-- REPLACE TITLE ABOVE -->

<!-- REPLACE TEXT BELOW-->

Non veniam nisi cupidatat nulla duis. Duis et amet elit ipsum nisi. Commodo incididunt enim nostrud adipisicing enim occaecat in deserunt officia. Irure eu pariatur laboris laborum esse enim velit incididunt ex nisi adipisicing consectetur tempor et. Mollit ullamco veniam aute eu consectetur aliquip reprehenderit veniam quis.

Ad irure id qui consequat do ut id aliqua. Incididunt sint nostrud ex non reprehenderit enim fugiat dolor laboris non adipisicing velit ex. Ad amet exercitation eu culpa consequat eu esse eu. Do exercitation sit exercitation eu velit id duis minim voluptate veniam ipsum est est eiusmod. Minim duis aliqua incididunt fugiat dolor fugiat ea id non labore eiusmod aliquip exercitation.

<!-- REPLACE TEXT ABOVE-->

<!-- auto-generates a list of cards that show immediately internal pages -->
<DocCardList/>

<!-- REPLACE TEXT BELOW-->

## Exercitation ullamco aliqua

Mollit labore laborum excepteur elit veniam non. Ea sit tempor mollit dolor magna. Proident culpa ex ipsum ipsum est nisi qui. Eu in enim sunt commodo et sunt anim irure ut.

Officia quis quis culpa sint mollit laborum ullamco officia labore. In labore consequat consequat duis cupidatat amet tempor occaecat Lorem ut ea. Veniam veniam veniam id nostrud sunt aute nulla qui dolore. Do non ullamco fugiat ullamco consectetur esse aliqua veniam culpa cillum reprehenderit aliquip eiusmod.

Esse tempor reprehenderit do ad. Est proident adipisicing irure qui est velit tempor ad culpa dolore consequat veniam. Et enim officia in ullamco nostrud cillum nulla deserunt et tempor Lorem dolor laboris adipisicing.

<!-- REPLACE TEXT ABOVE-->
