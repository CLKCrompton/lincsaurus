---
draft: true # REMOVE THIS LINE
title: Tool Template # Replace with PAGE TITLE (This will be the first heading and also the name on your tab)
description: "Template for overview of any particular tool" # REPLACE DESCRIPTION (this will appear in cards and social links and metadata about the page, keep it short)
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

# {TOOL NAME}

<!-- For more details on how to work the function button, view this link: https://gitlab.com/calincs/admin/lincsaurus/-/snippets/2408164 -->
<div className="function-button-row">
<functionbutton link="/docs/explore-data" buttonName="Example Button" icon={faUpRightFromSquare}></functionbutton>
<functionbutton buttonName="Clean"></functionbutton>
</div>

<!-- ADD ONE LINE (BRIEF DESCRIPTION)-->

Cupidatat ad nisi nostrud velit amet nisi.

<!-- Path of images will be in `static/` -->
<div className="banner">
<img src="/img/Placeholder.png" alt="ADD ALT TEXT"/>
</div>

<!-- REPLACE `link=""` WITH `link="/docs/PATH/TO/MARKDOWNFILE" or link="ANY URL" -->
<div className="primary-button-row">
<primarybutton link="" buttonName="To the Tool" icon={faUpRightFromSquare}></primarybutton>
<primarybutton link="" buttonName="To the Documentation" icon={faUpRightFromSquare}></primarybutton>
<primarybutton link="" buttonName="To GitLab" icon={faUpRightFromSquare}></primarybutton>
</div>

## {TOOL NAME} and LINCS

Reprehenderit occaecat commodo ad officia anim eu culpa proident ipsum nostrud dolor amet duis. Aute sunt proident ullamco officia laborum Lorem ad ad enim. Aute Lorem aute commodo commodo ut cupidatat deserunt nulla et. Reprehenderit eiusmod incididunt fugiat laboris magna id non et anim ad.

Nulla nisi laborum nostrud officia aliqua aute amet officia adipisicing non voluptate velit. Dolor laborum commodo esse enim enim ex aliquip. Officia amet voluptate sint sint ipsum ullamco aute sint non commodo proident minim adipisicing ad. Adipisicing laborum anim dolor exercitation magna exercitation aliquip proident pariatur minim.

## Prerequisites

Users of {TOOL NAME}:

- {Need/Do not Need} to come with their own dataset
- {Need/Do not Need} to create a user account
- Need a {TYPE OF NEEDED KNOWLEDGE/BACKGROUND}
- {Need/Do not Need} a technical background

{TOOL NAME} supports the following inputs and outputs:

- **Input:** <!-- ADD INPUTS -->
- **Output:** <!-- ADD OUTPUTS -->

## Resources

To learn more about {TOOL NAME}, see the following resources.

- {RESOURCE 1} <!-- ADD RESOURCE -->
- {RESOURCE 2} <!-- ADD RESOURCE -->
