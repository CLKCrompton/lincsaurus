---
sidebar_position: 5
title: "Presentations"
description: "Papers, Keynotes, Lectures, Talks, Workshops"
---

<!-- @format -->

## Conference Papers

**2022:**

- Brown, Susan, Deb Stacey, and Alliyya Mo. “Swirling and Shackling Literary Graphs.” Paper presented at _Graphs and Networks in the Humanities 2022_. Online. February 4, 2022.
- Brown, Susan, and Kim Martin. “Acts of (Re)Mediation: The Importance of Human Infrastructure in a Linked Data Ecosystem.” Paper presented at _DH Unbound 2022_. Association for Computers and the Humanities, Canadian Society for Digital Humanities, and Humanistica. Online. May 17, 2022.
- Brown, Susan, Kim Martin, Deb Stacey, and Peter Patel-Schneider. “LINCS Overview and Update.” Presentation at _LINCS Conference_. June 23, 2022.
- Buckland, Amy. “Cross-Sectoral Opportunities and Challenges.” Presentation at _LINCS Conference_. June 24, 2022.
- Canning, Erin. “Connecting Diverse Humanities Datasets with CIDOC CRM.” Paper presented at _ICOM CIDOC Conference_. Online. August 2022.
- Canning, Erin, Bri Watson, and the LD4 Ethics in Linked Data Affinity Group. “Linking Ethics & Data: The Creation and Use of Ethics in Linked Data Checklist.” Paper presented at _LD4 2022_. July 11, 2022.
- Crompton, Constance. “Sustaining Open Source Software.” Presentation at _LINCS Conference_. June 23, 2022.
- Goddard, Lisa. “Grants Menus.” Presentation at _LINCS Conference_. June 23, 2022.
- Hervieux, Natalie, Kostyantyn Guzhva, Abeer Waheed, and Denilson Barbosa. “Natural Language Processing and Linked Infrastructure for Networked Cultural Scholarship.” Paper presented at _Amii TechAid Reverse Expo_. October 20, 2022.
- Jakacki, Diane. “Linked Editorial Academic Framework (LEAF).” Presentation at _LINCS Conference_. June 23, 2022.
- Jakacki, Diane, Susan Brown, James Cummings, Mihaela Ilovan, and Carolyn Black. “The Linked Editorial Academic Framework: Creating an Editorial Environment for Collaborative Scholarship and Publication.” Paper presented at _DH2022_. Online. July 29, 2022.
- Liebe, Lauren, and Lauren Mandell. “Rethinking the Advanced Research Consortium: Disciplinary Restructuring and Linked Open Data.” Paper presented at _DH2022_. Online. July 27, 2022.
- Liebe, Lauren. “Flexibility as Sustainability in Digital Humanities Projects.” Paper presented at _DH2022_. Online. July 26, 2022.
- Michon, Philippe. “Canadian Heritage Information Network Vocabulary and LOD Activities Update.” Presentation at _LINCS Conference_. June 23, 2022.
- Rockwell, Geoffrey, Natalie Hervieux, Huma Zafar, Kaylin Land, Andrew MacDonald, Denilson Barbosa, Lucani Frizzera, Mihaela Ilovan, and Susan Brown. “Web Services for Voyant: LINCS, Voyant and NSSI.” Paper presented at _DH2022_. Online. July 28, 2022.
- Ross, Rebecca. “Canadian PID Strategy.” Presentation at _LINCS Conference_. June 23, 2022.
- Schoenberger, Zachary, Natalie Hervieux, Diane Jakacki, Kaylin Land, Andrew MacDonald, Geoffrey Rockwell. “LINCS: A Linked Open Data Ecosystem." Paper presented at _DH Unbound 2022_. Association for Computers and the Humanities, Canadian Society for Digital Humanities, and Humanistica. Online. May 18, 2022.

**2021:**

- Brown, Susan. “Community Microgrants Information Session.” Presentation at _LINCS Conference_. Online. May 5, 2021.
- Brown, Susan. “Linked Open Data in Open Source.” Paper presented at _World Open Library Foundation Conference (WOLFcon)_. Online. June 2, 2021. [https://www.youtube.com/watch?v=kfq9w_hMNRc](https://www.youtube.com/watch?v=kfq9w_hMNRc).
- Brown, Susan. “(Re:)Platforming.” Paper presented at _Infrastructural Interventions: Digital Humanities and Critical Infrastructure Studies_. King’s College London, London, England. June 20, 2021. Invited.
- Brown, Susan, Kim Martin, and Deb Stacey. “LINCS Overview.” Presentation at _LINCS Conference_. Online. April 30, 2021.
- Canning, Erin. “Ontologies at LINCS.” Presentation at _LINCS Conference_. Online. May 4, 2021.
- Crompton, Constance, Huma Zafar, Candice Lipski, and Alice Defours. “Mapping and Vetting: Converting TEI to Linked Data.” Paper presented at _TEI 2021_. Online. October 25–30, 2021.
- Jakacki, Diane, Evan A. Klimas, Charlotte E. Simon, and Twity Gitonga. “Delving and Deciphering: New Researchers Bringing New Insights to Ongoing Projects.” Paper presented at _TEI 2021_. Online. October 25–30, 2021.
- Jakacki, Diane and John Douglas Bradley. “Combining the Factoid Model with TEI: Examples and Conceptual Challenges.” Paper presented at _TEI 2021_. Online. October 25–30, 2021.
- Jakacki, Diane, John Edward Westbrook, Rebecca Marie Heintzelmann, Juliya Harnood, and Sarah Harber. “Suzette’s Object Lessons or Pedagogy Cubed.” Paper presented at _TEI 2021_. Online. October 25–30, 2021.
- Jenstad, Janelle. “Using &lt;listPerson>, &lt;person>, &lt;name>, and &lt;reg> to Make Editorial Arguments about Characters in Early Modern Drama.” Paper presented at _TEI 2021_. Online. October 25–30, 2021.
- Land, Kaylin, Andrew MacDonald, and Geoffrey Rockwell. “Spyral Notebooks as a Collaborative Supplement to Voyant Tools.” Paper presented at _CSDH-SCHN 2021: Making the Network_. Online. May 30–June 3, 2021. [https://doi.org/10.17613/2bsr-xp53](https://doi.org/10.17613/2bsr-xp53).
- Land, Kaylin, Andrew MacDonald, and Geoffrey Rockwell. “Spyral Notebooks as a Collaborative Supplement to Voyant Tools.” Paper presented at _European Association for Digital Humanities (EADH)_. Online. September 21–25, 2021. [https://doi.org/10.17613/2bsr-xp53](https://doi.org/10.17613/2bsr-xp53).
- Liebe, Lauren. “WOLFcon 2021—The Advanced Research Consortium.” Paper presented at _World Open Library Foundation (WOLFcon)_. Online. June 3, 2021. [https://youtu.be/pl07jneFGfI](https://youtu.be/pl07jneFGfI).
- Mo, Alliyya, Sarah Roger, Thomas Smith, and Hannah Stewart. “Agile LINCS: Building Student Community through Project Management.” Lightning Talk at _Digital Humanities Summer Institute (DHSI) 2021: Project Management in the Humanities._ Online. June 9, 2021.
- Schoenberger, Zachary. “Linked Data Technologies for Canadian Humanities Research Infrastructure.” Lightning Talk at _Canadian Research Software Conference_. Online. July 7, 2021.
- Tchoh, Bennett Kuwan, Kaylin Land, Andrew MacDonald, Milena Radzikowska, Stan Ruecker, Gerry Derksen, Jingwei Wang and Geoffrey Rockwell. “Speculating with Voyant: Designs for Data Walls.” Paper presented at _CSDH-SCHN 2021: Making the Network_. Online. May 30–June 3, 2021. [https://doi.org/10.17613/fsre-kd07](https://doi.org/10.17613/fsre-kd07).

**2020:**

- Brown, Susan, and Kim Martin. “Linking Research(ers) In: Building Infrastructure for Open Scholarship.” Paper presented at _Implementing New Knowledge Environments (INKE)_. Online. December 10, 2020.
- Brown, Susan, and Kim Martin. “Towards a Linked Infrastructure for Networked Cultural Scholarship.” Paper presented at _CSDH-SCHN 2020: Building Community Online_. Online. June 2, 2020.
- Goddard, Lisa. “Long Term Preservation of Linked Data.” Paper presented at _Access 2020_. Online. October 20, 2020.
- Mandell, Laura. “Faculty Curation and Linked Open Data: BigDIVA.org.” Paper presented at _LD4 Conference_. Online. July 23, 2020. [https://youtu.be/Vt9aOZTmCqE](https://youtu.be/Vt9aOZTmCqE). [1:00:43]

## Conference Panels

**2022:**

- Apavoo, Clare, Geoffrey Rockwell, and Michael Sinatra. “Consortial Models.” Panel at _LINCS Conference_. June 24, 2022.
- Brown, Susan. “Plenary Roundtable Honoring Stéfan Sinclair.” Panel at _DH Unbound 2022_. Association for Computers and the Humanities, Canadian Society for Digital Humanities, and Humanistica. May 18, 2022.
- Johnson, Kelly, Karen Nicholson, and Lisa Quinn. “Ecosystem Roundtable: Visions and Needs Across Sectors (Libraries, Archives, Museums, Publishing).” Panel at _LINCS Conference_. June 23, 2022.

**2021:**

- Botha, Pieter. “LINCS DevOps.” Panel at _LINCS Conference_. Online. May 3, 2021. [https://youtu.be/3AVPWa9BcwU](https://youtu.be/3AVPWa9BcwU).
- Hervieux, Natalie. “Improving Efficiency for Entity Linking.” Panel at _LINCS Conference_. Online. May 3, 2021. [https://youtu.be/WF4YhGFvMig](https://youtu.be/WF4YhGFvMig).
- Martin, Kim, Diane Jakacki, Alison Hedley, Michelle Meagher, and Jana Smith Elford. “LINCS Researcher Q&A Panel.” Panel at _LINCS Conference_. May 5, 2021.
- Martin, Kim, Hannah Stewart, Rashmeet Kaur, Thomas Smith, and Michaela Rye. “THINC Lab Student Talks and Q&A Panel.” Panel at _LINCS Conference_. April 29, 2021.
- Martin, Kim, Sarah Roger, Erin Canning, Zach Schoenberger, and Susan Brown. “Linking Communities of Practice.” Panel at _CSDH-SCHN 2021: Making the Network_. Online. May 30, 2021.

**2020:**

- Brown, Susan. “Partnerships and Collaborations.” Panel at _CRKN_. Online. October 29, 2020. [https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/3_Partnerships_0-Complete%20-%20EN.pdf](https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/3_Partnerships_0-Complete%20-%20EN.pdf).
- Brown, Susan, Kim Martin, Lisa Goddard, Sharon Farnell, Dan Scott, and Stacy Allison-Cassin. “[Collaborative Constructions: Linked Data and Canadian Cultural Scholarship](https://www.youtube.com/watch?v=4wEl2saJJ7s#t=2h41m0s).” Panel at _Access 2020_. Online. October 20, 2020. [https://youtu.be/4wEl2saJJ7s](https://youtu.be/4wEl2saJJ7s). [2:40:50]
- Goddard, Lisa. “Persistent Identifiers (PIDs) in Canada.” Panel at _CRKN_. Online. October 27, 2020. [https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/4_PIDs%20in%20Canada_0_Complete%20-%20EN.pdf](https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/4_PIDs%20in%20Canada_0_Complete%20-%20EN.pdf)

## Keynote Addresses

**2022:**

- Allison-Cassin, Stacy. “Sustainable Knowledge Organization for Indigenous Communities.” Keynote address at _LINCS Conference_. University of Guelph. June 23, 2022.
- Brown, Susan. “Situating Knowledge through Linking Data.” Keynote address at _CGPS Interdisciplinary Studies_. University of Saskatchewan. Online. April 13, 2022.
- Crompton, Constance. “Situated, Partial, Common, Shared: TEI Data as Capta.” Keynote address at _TEI Conference and Members’ Meeting 2022_. Newcastle University. September 13, 2022.
- Sanderson, Rob. “Data Is Our Product: Thoughts on Sustainability of Cultural Heritage Linked Open Data.” Keynote address at _LINCS Conference_. University of Guelph. June 23, 2022. [https://www.youtube.com/watch?v=XojV8m9PttA&feature=youtu.be](https://www.youtube.com/watch?v=XojV8m9PttA&feature=youtu.be)

**2021:**

- Susan Brown. “Meaningful Collaborations: Semantic Technologies and Cultural Research.” Keynote address at _Seminar on Digital Humanities In and About the Americas_. Institute des Amériques, University of Avignon. Online. February 11, 2021.

**2020:**

- Crompton, Constance. “Linked Data Across Time and Space: The Challenge of Modelling Temporal Identities and Places as Linked Data.” Keynote Address at _Linked Pasts_. University of London and British Library. London, England. December 3, 2020. [https://ics.sas.ac.uk/podcasts/linked-data-across-time-and-space-challenge-modelling-temporal-identities-and-places](https://ics.sas.ac.uk/podcasts/linked-data-across-time-and-space-challenge-modelling-temporal-identities-and-places).

## Summit Presentations

**2020:**

- Crompton, Constance. “Building an Inclusive Web.” Summit presentation at _Royal Society Meeting: Science Trust and Society_. Inclusive Culture Under Siege. Ottawa, Canada. September 9, 2020.
- Crompton, Constance. “When the Web is Big, and the Archives are Deep: Collaboration in Lesbian and Gay Liberation in Canada Project.” Summit presentation at _Spectrums of DH_. McGill University. Montreal, Canada. November 19, 2020.

## Guest Lectures

**2021:**

- Brown, Susan. “Metadata for Gender.” Lecture for a meeting of the Reference and Research Working Group and Editorial Standards Working Group of the Situated Networks and Archival Contexts. Online. May 28, 2021. Invited.
- Canning, Erin. “Linked Open Data at LINCS.” Lecture for INF2186H: Metadata Schemas & Applications. University of Toronto. Toronto, Canada. 2021.
- Rockwell, Geoffrey. “Voyant, Spyral: A Discourse on Practice in the Digital Humanities.” Lecture for the Department of Languages, Literatures, and Cultures. McGill University. Online. November 2021.

## Public Talks

**2022:**

- Allison-Cassin, Stacy, Sharon Farnel, and Deanna Reder. “The Ethics of Indigenous Digital Scholarship.” Public talk for the University of Ottawa’s DH Hub and the University of Guelph THINC Lab. March 23, 2022.

**2021:**

- Bath, Jon. “Is this Data? Research Process as Data in the Arts.” Public talk for “Research Data Management for Digitally Curious Humanists.” Online. June 14, 2021. [https://osf.io/6vepj/wiki/virtual%20panel%20--%20bath/](https://osf.io/6vepj/wiki/virtual%20panel%20--%20bath/).
- Brown, Susan. “PIDs: What Do Researchers Need to Know?” Public talk for _CRKN_’s “The Who, What, and Where of Persistent Identifiers (PIDs)!” Online. March 17, 2021. [https://vimeo.com/525702819](https://vimeo.com/525702819).
- Brown, Susan and Kim Martin. “Building Connections with LINCS.” Public talk for the British Library’s “Wikipedia Wednesdays.” Online. June 30, 2021.
- Goddard, Lisa. “What’s Next for PIDs in Canada?” Public talk for _CRKN_’s “The Who, What, and Where of Persistent Identifiers (PIDs)!” Online. May 12, 2021.

**2020:**

- Huculak, Matthew, Sarah Simpkin, Constance Crompton, Kim Martin, and Amy Tector. “The Importance of Collaboration in Digital Humanities.” Public talk for Library and Archives Canada’s “Wallot-Sylvestre Seminar.” Online. September 22, 2020. [https://www.bac-lac.gc.ca/eng/about-us/events/Pages/2020/Wallot-Sylvestre-Seminar-Leslie-Weir-Librarian-and-Archivist-of-Canada.aspx](https://www.bac-lac.gc.ca/eng/about-us/events/Pages/2020/Wallot-Sylvestre-Seminar-Leslie-Weir-Librarian-and-Archivist-of-Canada.aspx). Invited.

## Workshops

**2022:**

- Brown, Susan, and Kim Martin. “DHN 3312/5101/0900: Introduction to Linked Open Data.” Workshop for _DHSITE 2022_. University of Ottawa. May 26-27, 2022.
- Crompton, Constance. “Planning Web Projects for Longevity and Sustainability.” Workshop for _DHSITE 2022_. University of Ottawa. May 24-25, 2022.
- Hervieux, Natalie. “Linked Open Data with LINCS: OpenRefine.” Workshop for _DHSITE 2022_. University of Ottawa. May 2022.
- Liebe, Lauren, and Lauren Mandell. “Getting Started with the Advanced Research Consortium.” Workshop for _DH2022_. July 25, 2022.
- MacDonald, Andrew James, Geoffrey Rockwell, and Kaylin Land. “Spyral Launch Workshop.” Workshop for _DH Unbound 2022_. May 18, 2022.

**2021:**

- Brown, Susan. “Canadian Writing Research Collaboratory.” Workshop for _CIstudies_org Initiative_. June 10, 2021. [https://youtu.be/pf8Z2lM96fA](https://youtu.be/pf8Z2lM96fA). Invited.
- Brown, Susan and Kim Martin. “A Very Brief Introduction to Linked Open Data with LINCS.” Workshop for _DH Toolbox_. University of Ottawa. Online. June 10, 2021. [https://youtu.be/1Qe-k9mbErA](https://youtu.be/1Qe-k9mbErA).
- Brown, Susan and Kim Martin. “Linking Cultures.” Workshop for _DHSITE_. University of Ottawa. Ottawa, Canada. May 2021.
- Brown, Susan, Kim Martin, and Deb Stacey. “Ontologies for Diversity.” Workshop for _DH@Guelph_. Online. June 14–17, 2021.
- Canning, Erin. “Vocabularies vs/and Ontologies: Comparing CWRC and LINCS Ontologies.” Workshop for _DH@Guelph_. Online. June 14–17, 2021.
- Martin, Kim, Rashmeet Kaur, and Kathleen McCulloch-Cop. “LINCS UX.” Workshop for _LD4_. Online. October 2021.
- Martin, Kim, Susan Brown, Erin Canning, Rashmeet Kaur, Kathleen McCullough-Cop, Alliyya Mo, and Sarah Roger. “Linked Data for Context.” Workshop for _LD4_. Online. July 21, 2021. [https://youtu.be/flaRrw6of-8](https://youtu.be/flaRrw6of-8).
- Rockwell, Geoffrey and Kaylin Land. “Using Voyant Tools with Spyral Notebooks for Text Analysis.” _DH Workbench_. Ryerson University. Toronto, Canada. March 11, 2021.
- Rockwell, Geoffrey and Kaylin Land. “Voyant and Spyral.” Workshop for _DigiPhiLit_. University of Antwerp. 2021.
- Rockwell, Geoffrey and Kaylin Land. “Voyant and Spyral.” Workshop for _Digital Humanities Summer Institute (DHSI) 2021_. Online. June 23, 2021.
- Tarpley, Bryan and Lauren Liebe. “Corpora Demonstration.” Workshop for _LINCS Conference_. Online. May 3, 2021. [https://youtu.be/2X8gr3RoH18](https://youtu.be/2X8gr3RoH18).
- Zafar, Huma, Luciano Frizzera, and Mihaela Ilovan. “NERVE Demonstration.” Workshop for _LINCS Conference_. Online. May 4, 2021. [https://youtu.be/Empg691TkbE](https://youtu.be/Empg691TkbE).

**2020:**

- Brown, Susan, Deb Stacey, and Alliyya Mo. “CWRC Ontologies.” Workshop for _LINCS Project_. Online. August 13, 2020.
- Crompton, Constance. “Linked Open Data: Understand It, Use It, Make It—Hands On with Open Refine, Wikidata, and SPARQL Queries.” Workshop for _LINCS Project_. Online. August 28, 2020. [https://youtu.be/uSKO_r6xlvs](https://youtu.be/uSKO_r6xlvs).
- Crompton, Constance. “Linked Open Data: Understand It, Use It, Make It—Introduction to LOD.” Workshop for _LINCS Project_. Online. August 14, 2020. [https://youtu.be/mZzNWS7-Vpk](https://youtu.be/mZzNWS7-Vpk).
- Ilovan, Mihaela and Luciano Frizzera. “XML Editor CWRC-Writer and the Named Entity Recognition Vetting Environment (NERVE).” Workshop for _LINCS Project_. Online. April 29, 2020.
