---
sidebar_position: 10
title: "Documentation Credits"
description: "Authors, Contributors"
---

<!-- @format -->

For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite/).

## Translation

- Els Thant (Lead Translator)
- Constance Crompton
- Alice Defours
- Jasmine Drudge-Willson

## Context Plugin

- Jordan Lum
- Dawson MacPhee

## Conversion Workflows

- Natalie Hervieux (Lead Author)
- Sarah Roger
- Kate LeBere
- Constance Crompton

## Corpora

- Lauren Liebe
- Bryan Tarpley

## CWRC 2.0

- Luciano Frizzera
- Mihaela Ilovan

## LEAF-Writer

- Kate LeBere (Lead Author)
- Luciano Frizzera
- Mihaela Ilovan
- Sam Peacock
- Sarah Roger
- Megan Sellmer 

## LINCS Authority Service

- Natalie Hervieux (Lead Author)
- Kate LeBere

## LINCS Vocabulary Browser

- Kate LeBere

## Linked Data Enhancement API

- Natalie Hervieux

## NERVE

- Luciano Frizzera

## NSSI

- Huma Zafar

## OpenRefine

- Natalie Hervieux (Lead Author)
- Sarah Roger
- 
## ResearchSpace

- Robin Bergart (Lead Author)
- Sarah Roger
- Zach Schoenberger

## Rich Prospect Browser

- Bryan Tarpley

## SPARQL Endpoint
- Kate LeBere
- Alliyya Mo
- Sarah Roger

## Spyral

- Andrew MacDonald
- Jingyi Long

## VERSD

- Alliya Mo

## Voyant

- Andrew MacDonald
- Jingyi Long

## X3ML (3M)

- Jessica Ye (Lead Author)
- Erin Canning
- Natalie Hervieux
- Sarah Roger
