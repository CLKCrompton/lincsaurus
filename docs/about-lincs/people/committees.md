---
sidebar_position: 3
title: "Committees and Boards"
description: "Committee members and board members"
---

<!-- @format -->

import ZoomableImage from "@site/src/components/imageGallery/ZoomableImage";

## Executive Committee

- **Susan Brown**, Project Lead, University of Guelph
- **Deborah Stacey**, Technical Chair, University of Guelph
- **Kim Martin**, Research Board Chair, University of Guelph
- **Pieter Botha**, Technical Manager, University of Guelph
- **Sarah Roger**, LINCS Project Manager, University of Guelph

## Research Board

- **Kim Martin**, Research Board Chair, University of Guelph
- **Susan Brown**, Project Lead, University of Guelph
- **Stacy Allison-Cassin**, Theme Lead (Navigating Scale), University of Toronto
- **Jon Bath**, Theme Lead (Building Knowledge), University of Saskatchewan
- **Janelle Jenstad**, Theme Lead (Making Connections), University of Victoria
- **Sarah Roger**, LINCS Project Manager, University of Guelph

## Technical Committee

- **Deborah Stacey**, Technical Chair, University of Guelph
- **Susan Brown**, Component Lead (Access), University of Guelph
- **Denilson Barbosa**, Component Lead (Conversion), University of Alberta
- **Constance Crompton**, Site Tech Lead (Ottawa), University of Ottawa
- **Kate Davis**, Site Tech Lead (Toronto/Scholars Portal), Scholars Portal/University of Toronto
- **Lisa Goddard**, Component Lead (Infrastructure), University of Victoria
- **Carlos McGregor**, Scholars Portal/University of Toronto
- **Geoffrey Rockwell**, Component Lead (Voyant), University of Alberta
- **John Simpson**, Site Tech Lead (Digital Research Alliance of Canada), Digital Research Alliance of Canada
- **Amaz Taufique**, Scholars Portal/University of Toronto
- **Pieter Botha**, Technical Manager, University of Guelph

## Board of Directors

- **Susan Brown**, Project Lead, University of Guelph
- **Deborah Stacey**, Technical Chair, University of Guelph
- **Kim Martin**, Research Board Chair, University of Guelph
- **Stacy Allison-Cassin**, Theme Lead (Navigating Scale), University of Toronto
- **Denilson Barbosa**, Component Lead (Conversion), University of Alberta
- **Jon Bath**, Theme Lead (Building Knowledge), University of Saskatchewan
- **Constance Crompton**, Site Tech Lead (Ottawa), University of Ottawa
- **Kate Davis**, Site Tech Lead (Toronto/Scholars Portal), Scholars Portal/University of Toronto
- **Lisa Goddard**, Component Lead (Infrastructure), University of Victoria
- **Rebecca Graham**, University Librarian, University of Guelph
- **Diane Jakacki**, Site Tech Lead (LEAF), Bucknell University
- **Janelle Jenstad**, Theme Lead (Making Connections), University of Victoria
- **Pieter Botha**, Technical Manager, University of Guelph
- **Sarah Roger**, LINCS Project Manager, University of Guelph

## Technical Advisory Board

- **Arianna Ciula**, Deputy Director and Senior Research Software Analyst at King’s Digital Lab, King’s College London
- **Sandra Fauconnier**, Freelance Art Historian and Project Manager
- **Thomas Padilla** Deputy Director of Archiving and Data Services, Internet Archive
- **Cristina Pattuelli**, Co-director of the Semantic Lab, Pratt Institute School of Information
- **Stéphane Pouyllau**, CNRS Research Engineer; Co-Founder of Huma-Num, Huma-Num
- **Pat Riva**, Associate University Librarian in Collection Services, Concordia University
- **Rob Sanderson**, Director for Cultural Heritage Metadata, Yale University
- **Rainer Simon**, Senior Researcher at Data Science & Artificial Intelligence Group/Cultural Data Science, Austrian Institute of Technology
- **Peter Patel-Schneider**, Principal Scientist, PARC

## Organizational Structure

<ZoomableImage path="/img/lincs_org_chart-(c-LINCS).png" title="" altlabel="" caption=""/>