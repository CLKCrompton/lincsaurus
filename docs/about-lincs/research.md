---
sidebar_position: 1
title: "Research"
cited: "Brown, Susan, Erin Canning, Kim Martin, and Sarah Roger. “Ethical Considerations in the Development of Responsible Linked Open Data Infrastructure.” In Ethics in Linked Data, edited by Kathleen Burlingame, Alexandra Provo, and B.M. Watson. Litwin Books, 2023."
---

LINCS is undertaking a ground-breaking, multi-disciplinary research program organized across three themes—Making Connections, Navigating Scale, and Building Knowledge—alongside collaborations with researchers engaging with a variety of domains and areas of inquiry.

The large body of data that LINCS mobilizes for cultural research includes a spectrum—from datasets to be fully converted to %%Linked Open Data (LOD)|linked-open-data%% with human validated, through datasets that will receive some validation as part of the conversion process, to datasets that will be algorithmically converted and poised for further validation and enhancement by interested researchers and citizen scholars.

## Research Themes

### Making Connections

**Lead:** Janelle Jenstad, University of Victoria

LINCS will shine light on the dark matter of history: the hidden and unexpected connections between people, places, events, and cultural works across time, space, and media.

Currently, the ability to ask large questions of digitized cultural datasets is hampered by the lack of good %%metadata|metadata%% that connects primary sources to contexts: when they were written, by whom, where, how, and why. LINCS will allow researchers to engage with digital archives’ content and context simultaneously. A deeply contextualized understanding is fundamental to answering big questions like how social discrimination persists despite formal equality, or what policies and practices fuel the creative economy.

A major aim of LINCS is to bring together overlapping datasets across multiple academic and linguistic fields. LINCS will bridge the two solitudes of linguistically divided cultural datasets as never before, partnering with the scholarly platform Érudit on French Canadian %%ontology|ontology%% development and on mobilizing LOD for scholarly ends.

Beyond academia, the interconnectivity LINCS creates will propel Canadian culture to greater prominence on the web, provide deep contextualization to search results, and give journalists, schoolchildren, and the public better access to quality knowledge sources.

### Navigating Scale

**Lead:** Stacy Allison-Cassin, Dalhousie University

The ability to see patterns in large datasets and then zoom in to examine evidence is essential to humanities research, but it has been elusive in most contexts until now. LINCS will allow movement between granular data and distant views for those probing the complex interactions that contribute to cultural change.

We will mobilize a rich set of researchers’ musical data—entertainment records, early music scores, and ethnomusicological datasets covering Canadian Indigenous, East Indian, other folk music, and European traditions—to enable comparative investigation of influences, movements, and networks. Interlinking this data with that of our partners will enable an even broader analysis of the impacts of cultural policies and funding, such as the creation of the NPR in the US as compared to the CBC in Canada.

Such prosopographical datasets help trace how cultural identities circulate within avant-garde literary circles, or as applied to Indigenous and settler citizens in Canadian prison records. They offer glimpses of many who are otherwise lost to history, and have the potential to link to Canadians within inclusive projects such as the Digital Panopticon and other datasets of “ordinary” people.

The interplay of macro and micro is vitally important in work on material and textual culture. Editorial theorists and practitioners will use LOD to mobilize new kinds of editions and scholarly journal content. These probings of textual dynamics, using data that itself enacts networked textuality, will yield crucial insights in a world where textual conventions have been disrupted by digital tools. LINCS will therefore enable experiments in new forms of publication and application prototypes.

### Building Knowledge

**Lead:** Jon Bath, University of Saskatchewan

LINCS will change how researchers work on the web by combining the many advantages of highly structured data. For example, it will provide more explicit documentation of data organization than is available for most databases, with links back to sources in support of provenance, access, and analysis.

A major promise of LOD is that it can tell us what we do not yet know through inferencing, the computational extrapolation of information not explicitly stated in, but emergent from, LOD. LINCS will transform knowledge discovery by extrapolating inferences from millions of data points. In this underdeveloped area of %%Semantic Web|semantic-web%% research, the interests of scientific team members inevitably overlap with some in the technical team.

Another promise is that new forms of serendipity will arise from the semantically meaningful data. Linking datasets provides exciting opportunities to locate sources in unexpected places, recreating the serendipitous discoveries that once awaited scholars in archives or libraries on a larger, more complex scale. Such advances will propel knowledge discovery rapidly and are relevant to the design of search engines like Google.

A real challenge of LOD for humanities research is incorporating nuance and epistemological differences, including the accommodation of boundary objects that have different meanings in different fields. LINCS will work to ensure that its ontologies can represent non-hegemonic epistemologies and push alternative knowledge representations into the Semantic Web. LOD will also improve over time, increasingly capturing nuance and shedding more light on corners of darkness.

In its information architecture, LINCS will attend to difference and diversity, including the ways of knowing of marginalized groups. Above all, LINCS will provide a networked infrastructure of expertise and knowledge in %%Linked Data (LD)|linked-data%% that will enable Canadian researchers to contribute to building a better information ecology. Canada has nothing approaching this kind of infrastructure for the study of human history and culture.

## Areas of Inquiry

LINCS ingests a broad range of cultural heritage data. We can accept structured, semi-structured, and unstructured datasets covering everything from images to texts, maps to music—so long as the source data itself is readily available. The following Areas of Inquiry reflect the interests of the researchers involved at the beginning stages of LINCS. We recognize that these will change and grow over time.

* **Canadian Publishing**​: Researchers will dive into Canadian publishing histories looking for patterns and local effects—the sorts of connections that elucidate recent controversies over gender bias, concepts of nationhood, or claims to Indigenous identity within the Canadian literary establishment.

* **Geohumanities**​: This group will focus on tools and methods for grappling historical change, reconciliation, and cultural and methodological differences as they apply to engaging with spatial entities in LOD. Interconnections of data with sources will leverage the largely quantitative spatial turn in recent science in ways that also support qualitative inquiry. The group will liaise with the Linked Pasts and Pelagios network.

* **Indigenous Knowledges**: Researchers will grapple with the challenges associated with representing Indigenous materials and perspectives in LD form, including questions of sovereignty, trust, access, and decolonizing metadata, and relationships to community and land. This group could go forward independently or might combine with the resistant epistemologies group.

* **Knowledge Systems**​: Researchers with expertise in different forms of knowledge representation will grapple with the promises and the challenges of LOD in order to advance scholarship for humanities researchers worldwide.

* **Literary and Performance History**​: Rich coverage of literary and performance history, especially of women’s writing, networks, and reception will allow scholars to explore patterns of movement and formative professional networks in the history of entertainment.

* **London and the British Empire**​: Researchers will investigate Britain’s cultural, political, and economic impact, and Canada as a settler colony, thanks to the aggregation of multiple datasets spanning from the middle ages to the present.

* **Material and Textual Cultures**​: Researchers engaging with material culture within the museum community will mobilize LOD to invite analysis based on aggregating physical properties of numerous historical artifacts. Digital textual scholars will plumb computationally tractable documents to identify cross-textual patterns that are currently inaccessible.

* **Prosopography**​: Digital methods have renewed the field of prosopography or personography, the study of people in groups, by mobilizing granular data on individuals as the basis for larger inquiry. Prosopographical clusters will benefit from linked biographical data to exploit the unrealized transdisciplinary, transgeneric, and transnational dimensions of semantic research ecologies.

* **Resistant Epistemologies**​: LINCS includes researchers whose datasets embed non-hegemonic understandings of history or contemporary culture, and of how knowledge works to resist dominant worldviews. The inclusion of data based in such epistemologies will help to push marginalized and resistant knowledge representations into the Semantic Web.
