---
sidebar_position: 11
title: "Portal Credits"
description: "Credits for the creation of this site"
---

<!-- @format -->

For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite/).

## Development

- Alliyya Mo (Lead Developer)
- Pieter Botha
- Ibrahim Rather

## Content

- Sarah Roger (Lead Editor)
- Kate LeBere (Assistant Lead Editor)
- Els Thant (Lead Translator)
- Robin Bergart
- Hannah Stewart
- Deb Stacey
- Susan Brown
- Erin Canning
- Jasmine Drudge-Willson
- Natalie Hervieux
- Lauren Liebe
- Jingyi Long
- Kim Martin
- Alliyya Mo
- Sam Peacock
- Jessica Ye
- Huma Zafar

## UX Testing and Design

- Kim Martin (UX Lead)
- Robin Bergart
- Nem Brunell
- Jordan Lum
- Farhad Omarzad
- Evan Rees
