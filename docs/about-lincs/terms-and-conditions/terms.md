---
sidebar_position: 1
title: "Terms"
description: "Terms of use for lincsproject.ca"
---

<!-- @format -->

## Introductory Statement

This policy governs your use of the website of the Linked Infrastructure for Networked Cultural Scholarship (LINCS) project (herein referred to as the “LINCS portal”); by using the LINCS portal, you accept this policy in full. If you disagree with this policy or any part of this policy, you must not use this website.

The materials on the LINCS portal are provided on an “as is” basis. The LINCS project’s managing and supporting institutions, collaborators, and team (herein referred to as the “LINCS collaboration”) make no warranties, expressed or implied, and hereby disclaim and negate all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, the LINCS collaboration does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.

## External Links

The LINCS collaboration does not regularly review all of the sites that link to it or are linked to from it and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by the LINCS collaboration of the site. Use of any such linked website is at the visitor's own risk. You are advised to review the Privacy Policy of every site you visit, as this Privacy Policy does not apply to this third-party content. The LINCS collaboration has no control over and assumes no responsibility for the content, privacy policies, or practices of any third-party sites or services.  

The LINCS portal provides access to sites, tools, and interfaces (herein referred to as “LINCS resources”) built and maintained by the LINCS collaboration. LINCS resources that are linked to from the LINCS portal are considered external links, and are governed by their own terms. For more information about the terms of LINCS resources, see the terms pages associated with each resource. For more information about the LINCS collaboration’s approach to data, see the LINCS portal overview of [terms and conditions](/docs/about-lincs/terms-and-conditions).

## Legal Requirements

The LINCS project is hosted and managed in Canada. The terms of this policy have been prepared in accordance with Canadian legal requirements.

## Data Collection and Storage

The LINCS collaboration uses [Matomo Analytics](https://matomo.org/) to collect aggregated, anonymized data about the visitors to the LINCS portal. Matomo Analytics is compliant with Canadian legal requirements and with the [GDPR](https://matomo.org/gdpr-analytics/).

Data that we collect shall not be kept for longer than is necessary for the purpose(s) for which it is collected.

You may opt out of data collection on the LINCS portal [privacy page](/docs/about-lincs/terms-and-conditions/privacy).

## Cookies

The LINCS portal does not use cookies.

## Copyright

The LINCS portal is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

## Changes to this Policy

This policy may be updated at any time. Updates will be posted on this page. You are encouraged to review this privacy notice frequently to be informed of how we are protecting your information.

## Contact Us

If you have questions or comments about this policy, [contact us](mailto:lincs@uoguelph.ca).

## Acknowledgements

In preparing this policy, the LINCS collaboration built upon similar policy documents from [KgBase](https://www.kgbase.com/pages/terms), [Metaphacts](https://metaphacts.com/terms-conditions), and [Mobi](https://inovexcorp.com/privacy-policy/).
