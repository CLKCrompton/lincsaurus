---
sidebar_position: 2
title: "Participate in UX Testing"
description: "Sign up to participate in usability studies"
---

Thank you for your interest in helping the LINCS team build usable, open platforms.

The UX team conducts usability studies regularly. Your participation is voluntary and there is no compensation for participating, but the studies are fun!

All studies are conducted under the University of Guelph Research Ethics Board #22-01-009. You will receive a full information letter prior to beginning user-testing for LINCS.

For more information about any of these studies, contact:

Kim Martin<br/>
kmarti20@uoguelph.ca<br/>
LINCS Research Board Chair<br/>
University of Guelph

:::info

Fill in the [User Testing Sign-up Form](https://docs.google.com/forms/d/e/1FAIpQLSeTYpbrSbES-8eUdq5SZGlYuWv3Y8Z78MpuVGnnq-kHgAf0NQ/viewform) to be contacted to participate!

:::
