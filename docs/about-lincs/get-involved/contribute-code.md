---
title: Contribute Code
description: "Collaborate with LINCS by contributing code and improving our code base" 
---

LINCS welcomes contributions to and extensions of our code. Developers who are interested in collaborating on %%Linked Open Data (LOD)|linked-open-data%% conversion, storage, and access projects are encouraged to [contact us](mailto:lincs@uoguelph.ca).

Our code is accessible on [GitLab](https://gitlab.com/calincs/) and, where possible, it has been made available for reuse under open source licenses.

Code and technical documentation for each tool can be accessed via the **To GitLab** button on each tool’s landing page.

Information about the team behind each tool is available on the [Tool Credits](/docs/about-lincs/tools-credits) page. LINCS encourages you to [credit our developers](/docs/about-lincs/cite) for their work.
