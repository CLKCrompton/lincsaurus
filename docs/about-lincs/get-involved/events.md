---
sidebar_position: 1
title: "Events"
description: "Learn about upcoming and past LINCS events"
---

<!-- @format -->

## Upcoming Events

There are currently no upcoming events.

---

## Past Events

### LINCS Conference 2021 (29 April–06 May)

The 2021 Linked Infrastructure for Networked Cultural Scholarship (LINCS) conference featured tool demos, technical and research talks, and social gatherings. Videos from some of the sessions are provided below!

#### THINC Lab Student Talks and Q&A, 29 April

**Presenters:** Hannah Stewart, Michaela Rye, Rashmeet Kaur, Thomas Smith

**Chair:** Kim Martin

**Abstract:** THINC Lab wouldn’t work without the dedication of some wonderful undergraduates! Join us for a discussion with four of these undergraduate students about their research, their thoughts on digital humanities, and learn about the ways that their time in THINC Lab has changed their time at university.

**Bios:** Hannah Stewart, Michaela Rye, Rashmeet Kaur, and Thomas Smith are all undergraduate students at the University of Guelph who have worked on DH projects as THINC Lab undergraduate research assistants.

#### THINC Lab Open House Social, 29 April

To celebrate the Day of DH 2021, the [THINC lab](https://www.uoguelph.ca/arts/dhguelph/thinc) at the University of Guelph is hosting an open house event to introduce members of the University of Guelph community and the public at large to the lab. Please join us for a casual conversation that will provide an opportunity to meet members of the lab, learn about their research interests and projects, and discuss potential opportunities for getting involved with the THINC lab as an affiliated faculty or a fellow.

#### LINCS Conference Kick-Off and Overview, 30 April

**Presenters:** Susan Brown, Kim Martin, and Deb Stacey

**Chair:** Sarah Roger

**Abstract:** This session will officially launch the online conference, provide a brief overview of what linked open data is, what LINCS is doing and what it has done so far, followed by a Q&A session.

#### Corpora Demo, 3 May ([**Youtube**](https://www.youtube.com/watch?v=2X8gr3RoH18&ab_channel=-lincs))

**Presenters:** Bryan Tarpley and Lauren Liebe

**Chair:** Pieter Botha

**Abstract:** Corpora is a “Dataset Studio for the Digital Humanities” under development at the Center of Digital Humanities Research at Texas A&M University. It allows researchers to create, search, explore, and transform large, custom datasets. This demo will introduce audience members to the various affordances of Corpora by walking through the process by which data from the Advanced Research Consortium was imported, by showing how that data is being enriched via the attribution of authoritative URI’s for entities, and by showing how the data will be transformed into RDF (linked open data) suitable for ingestion by the LINCS triplestore.

**Bios:** Bryan Tarpley is the lead developer for the Center of Digital Humanities Research at Texas A&M University, as well as the Assistant Director of Technology for the Advanced Research Consortium. Lauren Liebe is the Project Manager for the Advanced Research Consortium at Texas A&M University and a PhD candidate specializing in digital humanities and early modern drama.

#### LINCS Technical Panel, 3 May

**Presenters:** Huma Zafar, Pieter Botha, and Natalie Hervieux

**Chair:** Deb Stacey

##### Linked Data Conversion using Microservices, Huma Zafar ([**Youtube**](https://www.youtube.com/watch?v=tTUCt0atSPY&ab_channel=-lincs))

**Abstract:** Converting humanities texts to linked open data comprises three main steps: entity recognition, entity linking/reconciliation (both automatic and manual), and linked data generation—but performing this work efficiently at scale can be a challenge. In this talk, we’ll introduce concepts relating to the use of microservices and go over how they can help address some of the challenges of scale. We’ll then discuss the work we’ve done over the past year to convert the NERVE (Named Entity Recognition and Vetting Environment) backend to a microservices architecture, and what benefits we’ve seen as a result of this design.

##### LINCS DevOps, Pieter Botha ([**Youtube**](https://www.youtube.com/watch?v=3AVPWa9BcwU&ab_channel=-lincs))

**Abstract:** DevOps is a hot topic in the IT industry right now and it wasn’t something that LINCS could ignore when we set out to plan and implement our development environment and operational infrastructure. In this presentation we will give a quick overview of DevOps and then discuss how it influenced the LINCS systems architecture from an operational point of view. We will show what LINCS has implemented for its development environment and how this environment is connected to the operational infrastructure through CI/CD pipelines. We’ll conclude the presentation with advantages that our current solution provides over classic alternatives and cite reasons for some of the choices we made.

##### Improving Efficiency for Entity Linking, Natalie Hervieux ([**Youtube**](https://www.youtube.com/watch?v=WF4YhGFvMig&ab_channel=-lincs))

**Abstract:** Comparing two datasets is a common task while moving towards linked open data. This could be during the data cleaning phase when you need to remove or merge duplicate entities. It is also important during the reconciliation phase when linking entities in your dataset to authority files or other linked open data sources. However, this can be a slow process when working with large files containing thousands of entities. In this presentation, we will talk about our approach for efficiently linking large datasets of bibliographic entities to authority files. We take advantage of the open-source record linkage package SPLINK and data processing engine Apache Spark to parallelize the processing.

**Bios:** Huma Zafar is a developer with the Humanities+Data Lab at the University of Ottawa, and a former senior software engineer from the world of library tech. Pieter Botha is the LINCS technical manager. Natalie Hervieux is a developer with the LINCS project, responsible for converting structured data and natural language text into linked open data.

#### Ontologies at LINCS, 4 May

**Presenter:** Erin Canning

**Chair:** Susan Brown

**Abstract:** Ontologies are key tools for structuring linked data: they describe classes for typing entities, and the relationships between those entities, effectively building up a view of the world out of our data. This talk will cover an introduction to what ontologies are in the linked data context, critical theory around ontologies as information infrastructures, and detail the ontology work at LINCS. The focus will be on LINCS ontology work, covering the theoretical and policy work that has taken place, along with ontology decisions that have been made and implemented. A key ontology decision has been around adopting CIDOC-CRM as a core ontology for the project; therefore, this presentation will also include an introduction to the CIDOC-CRM ontology and event-centric data modeling. The talk will conclude with a summary of the ontology work done at LINCS to dates, as well as next steps for the project.

**Bio:** Erin Canning is the Ontology Systems Analyst for LINCS, responsible for defining the ontologies to be used by the project as well as overseeing the mapping and integration of LINCS datasets.

#### NERVE Demo, 4 May ([**Youtube**](https://www.youtube.com/watch?v=Empg691TkbE&ab_channel=-lincs))

**Presenters:** Huma Zafar, Luciano Frizzera, Mihaela Ilovan

**Chair:** Deb Stacey

**Abstract:** The Named Entity Recognition and Vetting Environment, or NERVE is a front end tool that CWRC started developing in 2015 and that is currently being implemented as an extension of CWRC-Writer, the XML and RDF online editor. We will demo the tool in its current incarnation, share the wireframes for its next version and discuss how we plan to integrate it with the rest of the LINCS toolkit.

**Bios:** Huma Zafar is a developer with the Humanities+Data Lab at the University of Ottawa, and a former senior software engineer from the world of library tech. Mihaela Ilovan is the CWRC Assistant Director. She is managing the development and maintenance of CWRC-Writer and has been involved with NERVE since 2015. Luciano Frizzera is a Javascript developer at CWRC and a PhD candidate in Media Studies at Concordia University.

#### LINCS Researcher Q&A, 5 May

**Presenters:** Diane Jakacki, Alison Hedley, Michelle Meagher, and Jana Smith Elford

**Chair:** Kim Martin

**Abstract:** Over the past year, members of the core LINCS team have been meeting with humanities researchers about their data and the steps required to create it as LOD. Join us for a discussion with Diane Jakacki ([REED London](https://cwrc.ca/reed)), Alison Hedley ([Yellow 90s Personography](https://personography.1890s.ca/)) and Jana Smith Elford and Michelle Meagher (Ad Archive) to hear what the process of working with LINCS has been like, and how they plan to answer their research questions once it’s a part of the semantic web.

#### Community Microgrants Information Session, 6 May

**Presenter:** Susan Brown

**Chair:** Sarah Roger

**Abstract:** LINCS is converting and interlinking Canadian research datasets on cultural identities and cultural heritage, making them accessible as Linked Open Data for the benefit of scholars and the public. Through its community grants scheme LINCS will provide modest support in several forms to researchers who have datasets that they would like to convert but need assistance to process. You need not have been previously involved with LINCS to apply. Come find out more about the microgrants!

#### LINCS Social Hour, 6 May

Join LINCS on gather.town for a LINCS social hour. Meet new people and catch up with those you already know. Hang out, chat, and play games—all the fun of a conference coffee hour but without the lousy coffee!
