---
sidebar_position: 1
title: "Mission Statement"
description: "Our commitment to access, sustainability, and equity"
---

## Our Mission

LINCS enables Canadian researchers to create interlinked and contextualized online data about culture to benefit scholars and the public.

**LINCS’s mission is to make cultural data more readily available, shareable, searchable, and reusable.**

It is committed to: (1) empowering researchers to link their data; (2) hosting linked datasets; (3) providing tools for creating, querying, filtering, visualizing, sharing, annotating, and refining linked data; and (4) prioritizing voices and communities that are underrepresented on the Web.  

### Our Commitment to Access

**LINCS is committed to free and open access.** Wherever possible, LINCS materials are licensed to encourage reuse: software and code under an [open-source license](https://opensource.org/) and data under the Creative Commons Attribution license ([CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)). LINCS is committed to the [CARE Principles for Indigenous Data Governance](https://www.gida-global.org/care) and respects the sovereignty of Indigenous and Traditional Knowledge.

### Our Commitment to Acknowledgement

**LINCS is committed to recognizing the work of all of its contributors.** All kinds of work are equally deserving of credit.

### Our Commitment to Sustainability

**LINCS is committed to the ongoing accessibility, sustainable management, and long-term preservation of its data.** LINCS aims to ensure that its system and the data it hosts are maintained sustainably. We recognize that sustainability is not a metaphor: it consumes human labour as well as material resources, which are our responsibility to steward.

### Our Commitment to Equity

**LINCS is committed to diversity, inclusion, and decolonization in all areas of the project: from dataset selection to resource access and allocation; from community involvement to team composition.** LINCS’s ongoing work to enact reconciliation and anti-racism is more than just a policy: it is an ongoing commitment to listen, learn, engage, and take concrete action to dismantle discrimination.

**LINCS is committed to representing difference and diversity, including non-hegemonic epistemologies and alternative knowledge representations.** LINCS seeks to address systemic bias through its handling and presentation of data, by promoting situated and contextually embedded data, and via transparency about how LINCS datasets are culturally produced.

**LINCS is committed to equity among its participants.** The LINCS [Project Charter](/docs/about-lincs/policies/project-charter.md) outlines project-wide values and practices for preserving mutual respect and fostering goodwill.

## Document Details

**Version:** 1.0 <!-- UPDATE -->

**Authors:** <!-- ADD -->

**Last Updated:** 2023-03-31 <!-- UPDATE -->

**Released:** 0000-00-00 <!-- UPDATE -->
