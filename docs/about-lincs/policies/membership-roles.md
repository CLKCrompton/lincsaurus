---
sidebar_position: 3
title: "Membership Roles"
description: "Our project participants’ roles and responsibilities"
---

## General Principles

* Individuals and organizations interested in advancing linked data for cultural scholarship are welcome to become involved with LINCS, regardless of whether they were involved in the application process. We are happy to see the LINCS network grow!
* People involved in LINCS may have varying levels of involvement and responsibility over the course of the project.
* These roles are not mutually exclusive. People can move between various roles and in some cases roles will overlap. People may be involved individually as well as through their work with an organization.
* People do not need to be a collaborator with LINCS to use the system or contribute their data.

## Types of Membership

### Users

Users use the system and its contents without any formal association with the project.

There is no process required for a person to become a User who avails themselves of open data, software, or code created by the project to use or create linked data or tools, although we would love to hear if you have found what we are doing helpful. Please [email us](mailto:lincs@uoguelph.ca) or [@lincsproject](https://twitter.com/lincsproject) on Twitter.

### Data Contributors

Data Contributors contribute their data to LINCS without necessarily having any other formal association with the project.

Data Contributors will sign the Data Contribution Agreement.<!-- ADD LINK -->

### Research Collaborator

Research Collaborators are formally involved in LINCS through their research interests and are listed on the project website, as are Technical Collaborators and Student Collaborators. Research Collaborators include those named in the grant application as part of the Research (Scientific) or Technical teams, and others who join those teams, LINCS working groups, or other initiatives.

Research Collaborators will be actively involved in the development of the LINCS infrastructure and datasets. The process for becoming a Research Collaborator is the submission of a letter to the Executive Committee co-written by the applicant and the Lead of the Research Board or Technical Committee area. Should a Research Collaborator choose to leave the project they will inform in writing both their area Lead and the Executive Committee. Research Collaborators who have been inactive for a year will be assumed to have left the project. All Research Collaborators sign the [Project Charter](/docs/about-lincs/policies/project-charter.md).

### Technical Collaborator

Technical Collaborators are formally involved in the development of the LINCS infrastructure. Some are employed in positions funded in part or in full through funding obtained through the CFI Cyberinfrastructure grant or other research grants, others are employed by partner universities or organizations, others work with collaborating or parallel projects, and others may be voluntary contributors.

Technical Collaborators are those directly responsible for the expenditure of funds and staff supervision towards the infrastructure development. Because of the constraints around CFI funding it is not expected that new developers will be brought on through the project. Any changes to the status of a Developer will be handled through the process outlined in the Inter-Institutional Agreement. All Technical Collaborators sign the [Project Charter](/docs/about-lincs/policies/project-charter.md).

### Student Collaborator

Student Collaborators are hired to contribute their knowledge and skills to its success.

Student Collaborators sign the [Project Charter](/docs/about-lincs/policies/project-charter.md) The researcher to whom a student reports is responsible for ensuring that they read, understand, and sign this document. This researcher also co-signs the student document as the person responsible for their work.

### Organizational Partners

Organizational Partners include universities funded through the initial CFI grant, as well as universities, public- and private-sector partners, projects, and organizations that are involved beyond the scope of the CFI grant.

Organizational Partners are involved in various ways either informally or by way of an Inter-Institutional Agreement (IIA), Memorandum of Understanding, contract, or other agreement that formalizes a relationship.

If the nature of a person’s involvement with LINCS shifts significantly in terms of responsibilities, an email exchange regarding the change of role with one of the Project Lead, Research Board Chair, or Implementation Chair will serve as confirmation. More formal letters confirming involvement will be provided upon request.

## Rights and Responsibilities

With respect to their work in association with LINCS, collaborators:

* Follow the principles and practices laid out in the LINCS [Project Charter](/docs/about-lincs/policies/project-charter.md);
* Abide by policies governing the use of project grant funds as well as the regulations of their local institutions, with respect to funds received from LINCS;
* Use LINCS resources, including human resources and travel funds, only in the pursuit of LINCS research objectives;
* Respect the confidentiality of unpublished LINCS materials (including source code) and expect the confidentiality of their own unpublished materials to be respected in return; the agreement surrounding this and other related concerns are found in the LINCS Intellectual Property policy;
* Participate in the meetings of their area of activity within LINCS;
* Receive named co-authorship credit on presentations and publications that make direct use of research or development in which they took an active role (i.e., research to which the individual made a discernible contribution);
* Are listed on the LINCS project website. Involvement will be deemed to have ended if someone is inactive in meetings or communications in their areas of activity for a year. Past collaborators will be listed on the website with the term of their activity.

## Document Details

**Version:** 1.2 <!-- UPDATE -->

**Authors:** Susan Brown (University of Guelph), Jon Bath (University of Saskatchewan), Constance Crompton (University of Ottawa), Janelle Jenstad (University of Victoria), Kim Martin (University of Guelph), Sarah Roger (University of Guelph)

**Last Updated:** 2023-03-31 <!-- UPDATE -->

**Released:** 0000-00-00 <!-- UPDATE -->
