---
sidebar_position: 4
title: "Software License"
description: "Our LINCS software licenses"
---

Computer Software developed during and within the scope of LINCS available to each other and to the general public under an open source license.

LINCS recommends the use of the [GNU General Public License](https://opensource.org/license/gpl-3-0/) or the [MIT License](https://opensource.org/license/mit/).

If another license is required, it should be chosen from the license list provided by [opensource.org](https://opensource.org/); the license must also be approved by the LINCS [Executive Committee](/docs/about-lincs/people/committees#technical-committee) before use.

License choices for LINCS tools and interfaces are provided on the tool- or interface-specific GitLab page.

## Document Details

**Version:** 1.0 <!-- UPDATE -->

**Authors:** <!-- ADD -->

**Last Updated:** 2023-03-21 <!-- UPDATE -->

**Released:** 2023-03-08 <!-- UPDATE -->
