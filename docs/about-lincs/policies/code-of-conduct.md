---
sidebar_position: 5
title: "Code of Conduct"
description: "Our guidelines for LINCS event participants"
---

LINCS is founded on the values of equity and inclusivity:

* The LINCS [Mission Statement](docs/about-lincs/policies/mission-statement) outlines our commitment to diversity, inclusion, and decolonization in all areas of the project, including representing difference and diversity in the data and projects LINCS works with, and ensuring equity among project participants.
* The LINCS [Project Charter](/docs/about-lincs/policies/project-charter) states the project’s key objectives and provides guidelines for how to work together to achieve these goals. All LINCS [project collaborators and data contributors](docs/about-lincs/policies/membership-roles) are required to sign the project charter.

The LINCS Code of Conduct is a framework for participating in LINCS events such as conferences and workshops. The LINCS community extends far beyond collaborators and contributors, and we call upon all members of this broader community to support our inclusive, diverse, and equitable communities of practice.

LINCS events are anti-oppression and embrace intersectionalities and difference. The best problem solving and critical thinking happen when people with a wide array of experiences and perspectives work together in comfort and safety as peers. We therefore expect participants in all LINCS events to contribute to a thoughtful, respectful environment that supports such work.

## How to Be

LINCS fosters collaborative, inclusive, and harassment-free environments. Participants’ actions—both big and small—will help us meet this goal. For example:

* Listen as much as you speak, and remember that colleagues may have expertise of which you are unaware
* Encourage contributions from and yield the floor to those whose viewpoints may be under-represented in a group
* Use welcoming language, including correct pronouns and gender-neutral collective nouns (e.g., “people” rather than “guys”)
* Accept critique graciously and offer it constructively
* Give credit where it is due
* Seek concrete ways to make physical spaces and online resources accessible
* Act as an [active bystander](http://web.mit.edu/bystanders/definition/index.html) to ensure the welfare of those around you

Likewise, it is important to understand behaviors that may constitute harassment. These can include (but are not limited to):

* Unwelcome or offensive verbal comments or nonverbal expressions related to: age; appearance or body size; employment status; ethnicity; gender identity or expression; individual lifestyles; marital status; national origin; physical or cognitive ability; political affiliation; sexual orientation; race; or religion
* Use of sexual and/or discriminatory images in public spaces (including online)
* Deliberate intimidation
* Stalking or following
* Harassing photography or recording
* Sustained disruption of talks or other events
* Bullying behavior
* Inappropriate physical contact or unwelcome sexual attention

Sexually explicit, demeaning, discriminatory, or probable [triggering](https://everydayfeminism.com/2015/06/guide-to-triggering/) language and imagery are generally inappropriate at LINCS events. However, the LINCS Code of Conduct is not intended to constrain responsible scholarly or professional discourse. We welcome respectful engagement with difficult topics. Participants and presenters who will be using potentially offensive content are asked to flag the content beforehand (e.g., in program descriptions ahead of the event, and in face-to-face introductions immediately prior to using the content).

## How to Report

We will not tolerate harassment of LINCS community members in any form. If you are being harassed, notice that someone else is being harassed, or have any other concerns, we encourage you to take any or all of the following actions either during or after the event:

* For in-person events, speak with LINCS event organizers, who will be identified at the start of the event and via their name badges
* For virtual events, contact the event organizer at the email address provided on the event invitation, or via a private direct message on one of the event platforms (e.g., Zoom, Slack)
* For all events, contact the [LINCS ombud](mailto:lincs@uoguelph.ca) or one or more of the LINCS leads: [Susan Brown](mailto:sbrown@uoguelph.ca) (project lead), [Kim Martin](mailto:kmarti20@uoguelph.ca) (research lead), and [Deb Stacey](mailto:dastacey@uoguelph.ca) (technical lead)

All reports and inquiries will be handled in confidence. Event organizers will help those experiencing harassment to feel safe by, for example, moderating conversations in person and online, providing escorts, or contacting campus security or local law enforcement.

:::danger

If you or others are in imminent danger, **please first phone emergency services at 911**. Emergency contact details for in-person events will be provided in the materials for each event. For events held at the University of Guelph, the **campus police emergency number is 519-824-4120 x52000**.

:::

Participants at all LINCS-organized events who are asked to stop harassing or intimidating behaviors are expected to comply immediately. Those who violate this Code of Conduct may be warned, sanctioned, or expelled at the discretion of the LINCS leads and the LINCS ombud. If the matter is sufficiently serious—including if there is debate over whether a violation has occurred—we will seek venue-specific guidance for dealing with the situation; for events held virtually or at the University of Guelph, this may include the University of Guelph [Diversity and Human Rights office](https://www.uoguelph.ca/diversity-human-rights/).

We value your presence and constructive participation in our shared community, and thank you for your attention to the comfort, safety, and wellbeing of LINCS collaborators and event attendees.

## Sources of Inspiration

[DLF](https://www.diglib.org/about/code-of-conduct/); [Everyday Feminism](https://everydayfeminism.com/); [Geek Feminism](https://geekfeminism.wikia.org/wiki/Conference_anti-harassment); [Code4Lib](https://2016.code4lib.org/conduct.html); [ALA](https://2016.alaannual.org/statement-appropriate-conduct); [LITA](https://forum.lita.org/about/statement-of-appropriate-conduct/); [AMIA](http://www.amiaconference.net/amia-code-of-conduct/); [SAA](https://www2.archivists.org/statements/saa-code-of-conduct#.Vykgrj8XQ69); [US OpenGLAM](https://meta.wikimedia.org/wiki/US_OpenGLAM_Launch/Friendly_space_policy); [ADHO](http://adho.org/administration/conference-coordinating-program-committee/adho-conference-code-conduct); [National Women’s Studies Association](https://www.nwsa.org/content.asp?contentid=46); [Recurse Center](https://www.recurse.com/manual); [Contributor Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct.html); [Vox Media](https://code-of-conduct.voxmedia.com/); and [Scholars’ Lab](https://scholarslab.lib.virginia.edu/charter/).

## Modify and Re-Use

This document has been made available under a [CC-BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) license. Please feel free to adapt and re-use for your conference or event! We suggest altering the first section to reflect your group’s own mission statement and identity, and we appreciate credit for building on our work.

## Document Details

**Version:** 1.0 <!-- UPDATE -->

**Authors:** <!-- ADD -->

**Last Updated:** 2023-03-31 <!-- UPDATE -->

**Released:** 0000-00-00 <!-- UPDATE -->
