---
sidebar_position: 6
title: "Data Publication License Agreement"
description: "Our terms for publishing data with LINCS"
---

Complete the [Data Publication License Agreement supplementary form](https://docs.google.com/document/d/1dA01sGhzetsTytrhoIbrv5I4PXBkRNtXur0aSbGeHaM/edit?usp=sharing).

## Parties

The parties are:

1. The person(s) or organization(s) publishing the Linked Dataset(s), hereafter referred to as the “Licensor.”

2. The University of Guelph, herein referred to as “Licensee,” in its capacity as host and manager of the Linked Infrastructure for Networked Cultural Scholarship, hereafter referred to as “LINCS.” LINCS can be contacted at:

     The Linked Infrastructure for Networked Cultural Scholarship Project (LINCS)

     THINC Lab, McLaughlin Library

      University of Guelph, 50 Stone Road East

      Guelph, Ontario, N1G 2W1, Canada

     email: lincs@uoguelph.ca

3. “Supporting Institutions,” currently the Universities of Guelph, Alberta, Ottawa, Saskatchewan, Toronto, and Victoria, and McGill University. The number and identities of the Supporting Institutions are subject to change.

## Recitals

A. LINCS is a multi-institutional distributed collaboration, hereafter referred to as “LINCS Collaboration,” established in 2019 by the Universities of Guelph, Alberta, Ottawa, Saskatchewan, Toronto, and Victoria, and McGill and York Universities. LINCS is supported by the Digital Research Alliance of Canada and by Scholars’ Portal, as well as other academic, industry, and library partners.

B. For the purposes of this agreement, at the time of signing, LINCS Collaboration is represented by the University of Guelph, the home institution of Dr. Susan Brown, LINCS Project Lead.

C. LINCS is currently unincorporated and relies on supporting Institutions, including but not limited to the University of Guelph. When, if, and as, LINCS becomes incorporated or absorbed into an existing or newly created entity, that entity shall assume all rights and responsibilities set out herein. In future, LINCS may change its name, involve other organizations, or be otherwise represented by an organization or legal entity.

D. In addition to being a collaboration among Supporting Institutions, the term LINCS is also used to refer to (a) the data repository and (b) the infrastructure for creating and accessing the data, hereafter referred to as “LINCS Infrastructure."

## Licensor Details

The Licensor shall make reasonable efforts to advise the Licensee of any changes to name, address, organization, department, email address, project website, project participant names, and/or any other details.

     Full name:

     Address:

     Email address:

     Organization, department, and/or other affiliations:

     Linked Dataset name(s):

     Names and affiliations of projects and/or participants to be credited in Dataset metadata:

## Agreement

The Licensor represents and warrants that it has full power and authority and has taken all necessary actions and obtained all authorizations, licenses, consents, and approvals, to allow it to enter into this Agreement and grant the licenses set out herein and undertake the following:

**1. We are creating a repository that will make linked data more readily available, shareable, searchable, and reusable by other researchers and the larger public.**

**1A.** The Licensor grants the Licensee a permanent, irrevocable, non-exclusive use of a digital linked dataset (hereafter referred to collectively as the “Linked Dataset”) which may include some or all of the following: entity or authorities data or records, metadata, or linked data statements, as well as any enhancements to the Linked Dataset made in preparation for addition to the repository. This use includes, without limitation, the right to alter, copy, combine, download, publish, and otherwise use and share the Linked Dataset, and to make such rights available to third parties in accordance with the terms and conditions through which the Licensee allows access to its collections.

**1B.** The Licensor may also share related data files, hereafter referred to as the “Source Collection,” to which the Licensor grants a temporary, revocable, non-exclusive permission to alter, copy, combine, and download for internal use in the creation of the Collection, but not to publish and not for other uses unless otherwise agreed. Where the Source Collection is provided to the Licensee for the purposes of generating Linked Open Data, this agreement will be signed with reference to the Linked Dataset to be generated from that data in consultation with the Licensor.

**1C.** The Licensor acknowledges that some data, including but not limited to those related to Indigenous peoples and Indigenous or Traditional Knowledge, may require different enhancement and sharing arrangements and/or may not be freely shared (see Principle 6).[^2] Linked Datasets that meet the requirements for this exception are indicated as such on the signing page of this agreement.

**2. We are making Collections available in a way that complies with the law.**

**2A.** The Licensor declares that he, she, or it has full authority to grant this license to the Linked Dataset and, if there are other rights holders to the Linked Dataset, then the Licensor is fully authorized by all rights holders to grant this license.

**2B.** The Licensor undertakes to immediately advise the Licensee if the Licensor becomes aware of any claim that could be reasonably expected to affect the Licensor’s authority and/or the Linked Dataset.

**2C.** The Licensor declares that the Source Collection contains no data or other elements that are contrary to Canadian law.

**2D.** The Licensee is entitled, but not obliged, to act independently in respect of claims or rights affecting the Source Collection, provided that where the rights of the Licensor could be reasonably expected to be affected, the Licensee undertakes to advise the Licensor at the address provided by the Licensor.

**2E.** The Licensee will deaccession any data that is found to be illegal or to be facilitating illegal activity.

**2F.** The Licensee recognizes the EU General Data Protection Regulation (GDPR), and it extends the right to be forgotten to all individuals who are represented within the data it holds. The Licensee will deaccession, at the request of an individual and with proof of the individual’s identity, any data pertaining to that individual within the terms of the GDPR.

**2G.** The Licensee is entitled to deaccession data if there is a dispute over the ownership of the Source Collection, if there is a dispute among the owners of the data as to whether the Linked Dataset should be included in the LINCS Infrastructure, or for any other reason. If required, an arm’s-length arbitrator or arbitration committee will adjudicate on disputes. If the dispute is resolved in favour of including the Linked Dataset, the data shall be included again in the LINCS Infrastructure.

**3. We have a shared interest in the ongoing accessibility, sustainable management, and long-term preservation of the published data.**

**3A.** The Licensor recognizes that the presentation of the data via the LINCS Infrastructure or other websites will necessarily change over time, and that the Licensee does not guarantee to sustain the web interface for the Linked Datasets in the long term. The Licensee shall use its best efforts to ensure ongoing Web access to the Linked Datasets in a sustainable manner that keeps it as accessible as reasonably possible, taking into account funding, the size and scope of the collections and the evolving standards for the preservation of access to data.

**3B.** The Licensee shall use its best efforts to ensure that the Linked Dataset is preserved according to current best practices for long-term preservation, and for that purpose reserves the right to modify the format and/or functionality of the Linked Dataset where such modification is necessary in the judgment of the Licensee for reasons including, but not limited to, facilitating the digital sustainability, distribution, or re-use of the Linked Dataset.

**3C.** The Licensee, associated institutions, or host organizations shall not be liable to the Licensor in any way, including, but not limited to, damage, losses, or legal action if the Source Collection or Linked Dataset is damaged, altered, or lost, whether such damage or losses resulting from acts or omissions by the Licensee, its agents, third or other parties.

**3D.** The Licensor shall indemnify the Licensee against all claims, losses, damages, or expenses of any kind arising from Licensor’s provision of the Linked Dataset.

**4. We recognize that linked open data that is derived from other sources requires stable URIs/URLs at which to be able to refer to those sources for data provenance.**

**4A.** Because the Licensee is collecting Linked Open Data, the Licensor commits to storing any Source Collection(s) from which the Linked Dataset is derived or to which it links in a stable environment, such as an Institutional Repository (such as that of the Licensor’s University), a [Trustworthy Digital Repository](https://en.wikipedia.org/wiki/Trustworthy_Repositories_Audit_%26_Certification) (such as Scholars Portal), and/or Canada’s [Federated Research Data Repository](https://www.frdr-dfdr.ca/repo/).

**4B.** The Licensee strongly recommends, where appropriate, the development of a comprehensive Data Management Plan with respect to Source Collections, in consultation with a Research Data Management expert.

**4C.** The Licensee will not store the Source Collection from which the Linked Dataset is derived. The Licensor commits to updating the Licensee on the external storage arrangements for the Source Collection, and on any changes to URLs.

**4D.** Where source data is not in the control of the Licensor, the Licensee will advise on best practices and preferred sources in the use of external URIs/URLs (e.g., HathiTrust, Internet Archive), as well as backup options.

**4E.** Where no external URIs exist, the Licensee may, in some cases, mint its own URIs. All URIs minted by the Licensee will be the property of the Licensee. Deaccessioning of a Dataset or partial Dataset will not also result in the deprecation of any URIs originally minted to support the Dataset, except in exceptional circumstances.

**5. We recognize that data may need to be deaccessioned or deprecated, or access conditions may need to change.**

**5A.** The Licensor may request that the Licensee not make the Linked Dataset or parts of the Linked Dataset available for a temporary period (e.g., where the Licensor wishes to make corrections to the Linked Dataset or make preparations for publication). Where such a request is reasonably made by the Licensor and does not pose undue difficulties for the Licensee, the Licensee shall use its best efforts to prevent access to the Linked Dataset, or parts thereof, for the requested period.

**5B.** The Licensee may deaccession the Linked Dataset wholly or in part, or restrict or prevent access to the Linked Dataset on a temporary or permanent basis (see Principle 2), for reasons including but not limited to the data no longer being functional due to broken links or no longer being Linked Open Data. The Licensee shall make reasonable attempts to inform the Licensor at the address provided by the Licensor in such cases.

**6. We are committed to the principle of free and open access.**

**6A.** Linked Datasets deposited with the Licensee are benefiting from public monies such as Canada Foundation for Innovation funds and are required to be open access. This requirement does not apply to Source Collections.

**6B.** The Linked Dataset materials will be made available under a Creative Commons or similar license as may be determined by LINCS from time to time, initially a [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/) license. The Licensor hereby agrees to such license arrangements as adopted by the Licensee.

**6C.** In consultation with the Licensor, the Licensee may opt to change the license on the data to an open-access license (such as, but not limited to, a [Creative Commons 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) license). The Licensor shall indicate on a supplementary form whether they accept, wish to be consulted in the event of, or refuse such a change.

**6D.** Unless agreed otherwise in a written schedule attached to this agreement, the use of the Linked Dataset is subject to the General Terms and Conditions of Use laid down by the Licensee and as changed by the Licensee from time to time.

**6E.** The Licensor will make reasonable efforts to advise of privacy concerns including, but not limited to, data containing personal or otherwise sensitive details, and the Licensee will use its best efforts to make sure that such data not be made freely available.

**6F.** An exemption to this open-access requirement will be allowed where a legitimate request for doing so has been established and indicated on the signing page of this agreement. One such case will be for data related to Indigenous peoples and their information, languages, peoples, cultures, identities, knowledge, and/or Traditional Knowledge, whether shared by Indigenous knowledge holders or by other data holders. The Licensee respects data sovereignty and adheres to the [CARE Principles for Indigenous Data Governance](https://www.gida-global.org/care).

**7. We will adhere to ethical guidelines about data collection and information sharing.**

**7A.** The Licensee adheres to the [Tri-Council Policy Statement: Ethical Conduct for Research Involving Humans](https://ethics.gc.ca/eng/policy-politique_tcps2-eptc2_2018.html) and requires adherence from Licensors and their data.

**7B.** Where required by the Licensor’s institution, the Licensor will secure ethics clearance from their institution prior to collecting, using, or depositing data.

**7C.** The Licensor will share with the Licensee a copy of their institutional ethics clearance. If this clearance is deemed insufficient by the Licensee, the Licensor will meet any additional requirements set out by the Licensee prior to the inclusion of their data.

**7D.** For data related to Indigenous peoples and their information, languages, peoples, cultures, identities, knowledge, and/or Traditional Knowledge, whether shared by Indigenous knowledge holders or by other data holders, the Licensor will provide evidence of a Community licensing agreement or participation in a Community approval process, where relevant.

**8. We believe that individuals and projects should receive acknowledgement for creation of their collections.**

**8A.** LINCS Infrastructure will make use of licenses (e.g., a [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/)), metadata standards, and guidelines to attribute a Linked Dataset to its creators.

**8B.** The names of individuals to receive attribution under the license are provided by the Licensor at the top of this agreement; the Licensor is responsible for providing an updated list, as needed.

**8C.** The Licensee shall use its best efforts to make attribution information accessible and to encourage acknowledgement of use by third parties to whom the Linked Dataset (or substantial parts thereof) is accessible. Formats for attribution and acknowledgement will be included in the LINCS Infrastructure Terms and Conditions of Use and made available through the LINCS Infrastructure tools.

**9. We agree that it is desirable to allow other members of the LINCS community to build further on, update, or enhance materials in the Collection.**

**9A.** LINCS Infrastructure materials will be subject to modification (updating, correction, or enhancement) by LINCS members in accordance with procedures for ensuring the currency and accuracy of materials.

**9B.** Members of the public may suggest modifications to the material in the Linked Datasets. Such annotations will be separate from but linked to the items they annotate, in accordance with the Conditions of Use for the LINCS Infrastructure.

**9C.** Any suggested changes to materials in the Linked Dataset itself will be managed in accordance with LINCS Infrastructure policy, which may include delegating responsibility for processing changes to the Licensor or their designate, if desired. Modifications will be attributed to the party responsible for them.

**9D.** The Licensee may make automated or manual changes to the Linked Dataset related to the revision of ontologies or vocabularies, updating URLs, or making general data enhancements, corrections, or deprecations. The Licensee will announce these changes through usual project communication channels of routine changes, and will make best efforts to inform the Licensor directly, in advance, of any plans for substantive changes to their Linked Dataset. If a Licensor has not been responsive to communications about their Linked Dataset for more than 12 months, its management will be deemed to have been delegated to the Licensee.

**10. We conceive of this as a long-term arrangement, but recognize that circumstances may arise in which continuation under these terms is impossible.**

**10A.** This Agreement shall come into effect on the date on which the Licensee receives the Source Collection and/or Linked Dataset (hereafter the deposit date) and shall remain valid for one hundred years.

**10B.** LINCS reserves the right to reassign this agreement from the University of Guelph to another organization or legal entity, if such an entity is elected to or is established to represent LINCS. Reasonable efforts will be made to provide appropriate notice to the Licensor of any such changes.

**10C.** If the Licensee ceases to exist or terminates its data-archiving activities, the Licensee shall attempt to transfer the data files to a similar organization that will continue the Agreement with the Licensor under similar conditions if possible. In such a case, the Licensee will make reasonable efforts to notify the Licensor at the address provided by the Licensor.

**11. We wish to communicate and resolve conflicts, should they arise, and to resolve them outside of the courts if possible.**

**11A.** Any problems or concerns arising under this agreement should be submitted in writing to the other party. If disputes arise, the parties will attempt to resolve them informally.

**11B.** If the dispute is not resolved and the parties can agree on a mediator, the parties may submit their dispute to that mediator. Should the parties be unable to agree on a mediator, arbitrator, or process, they will resolve their differences by way of arbitration in Guelph, Ontario, under the Ontario Arbitration Act, or successor legislation.

**12. We are governed by Canadian law and this agreement is binding on our heirs and successors.**

**12A.** This agreement shall be governed by and interpreted according to the laws of Canada and the Province of Ontario. This agreement shall be binding on all heirs and successors of the parties. This Agreement shall be valid where the parties enter into acceptance by facsimile transmission and/or execute this agreement in counterparts.
________

The Licensor is publishing data, or a data subset, that has been submitted to and approved by LINCS to be excluded from LINCS’s requirements (stated in Principles 6 and 9 of this agreement) for freely sharing and/or modifying where necessary.

     ___ Yes

     ___ No

If yes, the restrictions on this data are as follows:

     _____________________________________________________

**SIGNED AS AN AGREEMENT** by the Parties’ respective authorized representatives in a manner and form sufficient to bind them.

**UNIVERSITY OF GUELPH**

     Per:

     Signature:

     Name:

     Title:

     Date:

**LICENSOR**

     Per:

     Signature:

     Name:

     Title:

     Date:

## Document Details

**Version:** 1.3[^1] <!-- UPDATE -->

**Authors:** <!-- ADD -->

**Last Updated:** 2023-03-21 <!-- UPDATE -->

**Released:** 2023-03-08 <!-- UPDATE -->

<!-- Footnotes themselves at the bottom. -->

[^1]:
     The LINCS Data Publication License Agreement was prepared by Susan Brown, Stacy Allison-Cassin, Jon Bath, Pieter Botha, Janelle Jenstad, Kim Martin, and Sarah Roger, with consultation from Sharon Farnel and Mihaela Ilovan. Its contents are adaptations from the Interdisciplinary Project Charter template by Stan Ruecker, the DANS (Data Archiving and Networked Services) License Agreement Version 3.2, the DARIAH (Digital Research Infrastructure for the Arts and Humanities) Model Deposit License Version 1.0, and the CWRC (Canadian Writing Research Collaboratory) Data Donation License Agreement Version 1.0.

[^2]:
     We recognize that there is no pan-Indigenous approach for respectfully working with data related to Indigenous peoples and their information, language, people, culture, identity, knowledge, and/or Traditional Knowledge, whether provided by Indigenous knowledge holders or others. We have been general in the terms provided in this agreement out of awareness that there is no single way to think about the things this policy attempts to cover. Our intention is to be respectful of the needs of the communities with whose data we are being entrusted. This agreement is part of an iterative, reflective process, and is open to modification to meet the needs of those sharing data and those for whom the sharing of the data has meaning.
