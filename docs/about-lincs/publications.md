---
sidebar_position: 6
title: "Publications"
description: "Journal Articles, Book Chapters, Position Papers, Vocabularies"
---

<!-- @format -->

## Journal Articles

**Under Review:**

- Martin, Kim, Susan Brown, Abi Lemak, Alliyya Mo, Deb Stacey, Jasmine Drudge-Willson, and Joel Cummings. “Cultural Formations: Representing Intersectionality in Linked Data.” _Journal of Cultural Analytics_.

**2022:**

- Brown, Susan. “Same Difference: Identity and Diversity in Linked Open Cultural Data.” _International Journal of Humanities and Arts Computing_ 16, no. 1 (2022): 1-16. [https://doi.org/10.3366/ijhac.2022.0273](https://doi.org/10.3366/ijhac.2022.0273)
- Canning, Erin, Susan Brown, Sarah Roger, and Kim Martin. “The Power to Structure: Making Meaning from Metadata through Ontologies.” Special Issue, _KULA: Knowledge Creation, Dissemination, and Preservation Studies_ 6, no. 3 (2022): 1-15. [https://doi.org/10.18357/kula.169](https://doi.org/10.18357/kula.169)

**2021:**

- Brown, Susan and John Simpson. “Qu’y a-t-il dans un nom ?, Les données ouvertes liées comme base d’une écologie pour la publication scientifique, dynamique et décentrée.” Translated by Jasmine Drudge-Willson. _Sens public_ (March 1, 2021). [http://sens-public.org/articles/1484/](http://sens-public.org/articles/1484/).
- Goddard, Lisa. “Persistent Identifiers as Open Research Infrastructure to Reduce Administrative Burden.” _Pop!_, no. 3 (November 9, 2021). [https://doi.org/10.54590/pop.2021.006](https://doi.org/10.54590/pop.2021.006).
- Rockwell, Geoffrey, Kaylin Land, and Andrew MacDonald. “Social Analytics Through Spyral.” _Pop!_, no. 3 (November 9, 2021). [https://doi.org/10.54590/pop.2021.004](https://doi.org/10.54590/pop.2021.004).

**2020:**

- Brown, Susan. “Categorically Provisional.” _PMLA_ 135, no. 1 (January 2020): 165–174. [https://doi.org/10.1632/pmla.2020.135.1.165](https://doi.org/10.1632/pmla.2020.135.1.165).

## Book Chapters

**2022:**

- Brown, Susan, Erin Canning, Kim Martin, and Sarah Roger. “Ethical Considerations in the Development of Responsible Linked Open Data Infrastructure.” In _Ethics in Linked Data_, edited by Kathleen Burlingame, Alexandra Provo, and B.M. Watson. Litwin Books, 2022.

## Position Papers

**2022:**

- Brown, Susan [Contributor]. _The Open Science Dialogues: Summary of Stakeholders Round Tables_. Office of the Chief Science Advisor of Canada, 2022. [https://science.gc.ca/site/science/sites/default/files/attachments/2022/open-science-dialogues-february-2022.pdf](https://science.gc.ca/site/science/sites/default/files/attachments/2022/open-science-dialogues-february-2022.pdf)
- Brown, Susan [Co-Author]. _Research Software Current State Assessment_. Digital Research Alliance of Canada, 2022. [https://alliancecan.ca/sites/default/files/2022-03/RS_Current_State_Report.pdf](https://alliancecan.ca/sites/default/files/2022-03/RS_Current_State_Report.pdf)

**2020:**

- Brown, Susan and Deb Stacey. “The Future of Cloud as DRI.” _New Digital Research Infrastructure_. 2020.
- Brown, Susan and Jeffery Antoniuk. “Interface Matters.” _New Digital Research Infrastructure_. 2020.
- Brown, Susan. “Sustaining Digital Research Infrastructure in the Humanities.” _New Digital Research Infrastructure_. 2020.

## Vocabularies

**2022:**

- Drudge-Willson, Jasmine. Canadian Writing Research Collaboratory Vocabulary. 2022. [https://skosmos.lincsproject.ca/Skosmos/cwrc/en/](https://skosmos.lincsproject.ca/Skosmos/cwrc/en/)
- Drudge-Willson, Jasmine. CWRC - Injuries and Illnesses Vocabulary. 2022. [https://skosmos.lincsproject.ca/Skosmos/ii/en/](https://skosmos.lincsproject.ca/Skosmos/ii/en/)
- Drudge-Willson, Jasmine. CWRC - Genre Vocabulary. 2022. [https://skosmos.lincsproject.ca/Skosmos/genre/en/](https://skosmos.lincsproject.ca/Skosmos/genre/en/)
- Drudge-Willson, Jasmine. EML Place Types Vocabulary. 2022. [https://skosmos.lincsproject.ca/Skosmos/eml/en/index](https://skosmos.lincsproject.ca/Skosmos/eml/en/index)
