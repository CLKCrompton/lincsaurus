---
sidebar_position: 9
title: "Tool Credits"
description: "Directors, Developers, Contributors"
---

<!-- @format -->

Credits are provided for tools built by LINCS collaborators. For more information on how to construct citations for LINCS, see [Cite](/docs/about-lincs/cite).

## Context Plugin

- **Pieter Botha**, Lead Developer (2020–)
- **Dawson MacPhee**, Developer (2021–)
- **Kathleen McCulloch-Cop**, Developer (2020–2022)

## Corpora

## CWRC 2.0

For more information, see CWRC 2.0’s [Credits and Acknowledgments](https://cwrc.ca/about-lincs/credits-and-acknowledgments).

- **Susan Brown**, Director (2010–)
- **Jeffery Antoniuk**, Technical Director (2021–), Programmer and Systems Analyst (2010–2021)
- **Michael Brundin**, Data Integrity and Metadata Co-ordinator (2010–2017)
- **Mihaela Ilovan**, Assistant Director (2021–), Project Manager (2014–2021), Interface Design and Development Consultant (2010–)

## LEAF-Writer

For more information, see LEAF-Writer’s [About](https://leaf-writer.stage.lincsproject.ca/).

- **Susan Brown**, Director (2011–)
- **James Chartrand**, Lead Developer (2011–2018)
- **Luciano Frizzera**, Lead Developer (2020–), Developer (2019–2020)
- **Andrew MacDonald**, Lead Developer (2018–2020), Developer (2013–2018)
- **Mihaela Ilovan**, Project Manager (2014–)
- **Geoffrey Rockwell**, Scope and Direction Manager (2011–2014)
- **Megan Sellmer**, Tester and Documentation Writer (2011–2016)

## Linked Data Enhancement API

- **Natalie Hervieux**, Developer (2020–)
- **Mohammed Marzookh Farook**, Developer (2021–2022)
- **Ananya Rao**, Developer (2022)
- **Justin Francis**, Developer (2021)

## LINCS Authority Service

- **Pieter Botha**, Developer (2022–)
- **Matej Kosmajac**, Developer (2022)
- **Natalie Hervieux**, Developer (2022–)
- **Dawson MacPhee**, Contributor  (2022–)

## NERVE

- **Susan Brown**, Director (2015–)
- **Luciano Frizzera**, Lead Developer (2020–), Developer (2019–2020)
- **Andrew MacDonald**, Lead Developer (2019–2020)
- **Edward Armstrong**, Lead Developer (2015–2019)
- **Mihaela Ilovan**, Project Manager (2015–)
- **Huma Zafar**, Developer (2020–2022)

## NSSI

For more information, see NSSI’s [Wiki](https://gitlab.com/calincs/conversion/NSSI/-/wikis/home).

- **Huma Zafar**, Lead Developer (2020–2022)
- **Dawson MacPhee**, Contributor (2021–)
- **Natalie Hervieux**, Contributor (2020–)

## ResearchSpace

LINCS ResearchSpace is an instance of the ResearchSpace platform developed by the British Museum. More information is available on the [ResearchSpace](https://researchspace.org/about-us/) team page.

- **Zach Schoenberger**, LINCS Lead Developer (2020–)
- **Rajvi Khatri**, Developer (2022–)
- **Jordan Lum**, UX (2021–)
- **Kim Martin**, UX (2020–)
- **Evan Rees**, UX (2021–2022)

## ResearchSpace SPARQL Interface

- **Zach Schoenberger**, LINCS Lead Developer (2020–)
- **Alliyya Mo**, Developer (2020–)

## Rich Prospect Browser

- **Jordan Lum**, UX (2022–)
- **Bryan Tarpley**, Lead Developer (2020–)
- **Akseli Palen**, Developer (2022–)

## SPARQL Endpoint

## Spyral

For more information, see Voyant/Spyral’s [Credits](https://voyant.lincsproject.ca//#!/guide/about-section-credits).

- **Stéfan Sinclair**, Project Lead, Principal Designer, and Principal Programmer (2017–2020)
- **Geoffrey Rockwell**, Project Lead (2020–)
- **Andrew MacDonald**, Principal Programmer (2019–2020), Programmer (2008–2020)

## VERSD

- **Alliyya Mo**, Lead Developer (2020–)
- **Natalie Hervieux**, Reconciliation API Developer (2020-)

## Voyant

For more information, see Voyant/Spyral’s [Credits](https://voyant.lincsproject.ca//#!/guide/about-section-credits).

- **Stéfan Sinclair**, Project Lead, Principal Designer, and Principal Programmer (2003–2020)
- **Geoffrey Rockwell**, Project Lead (2020–)
- **Andrew MacDonald**, Principal Programmer (2020–), Programmer (2008–2020)

## XTriples

LINCS XTriples is based on [XTriples](https://xtriples.lod.academy/index.html), a conversion tool created by Professor Torsten Schrade at[ Digitale Akademie](https://www.adwmainz.de/digitalitaet/digitale-akademie.html), Akademie der Wissenschaften und der Literatur, Mainz.

LINCS XTriples is funded by
— **CFI**, LINCS
- **SSHRC**, Canada Research Chair in Digital Humanities, University of Ottawa
  
- **Torsten Schrader**
- **Huma Zafar**
- **Alice Defours**
- **Nem Brunell**
- **Rajvi Khatri**
- **Constance Crompton**

## Yasgui SPARQL Interface

- **ALliyya Mo**, LINCS Lead Developer (2020–)