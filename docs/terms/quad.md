---
id: quad
title: Quad
hoverText: An extension of a triple to include a fourth section that provides context for the triple, such as the Uniform Resource Identifier (URI) of the graph as a whole (subject-predicate-object-context).
---

<!-- @format -->

A quad is an extension of a %%triple|triple%% to include a fourth section—thus the change from triple, three-part, to quad, four-part—that provides context for the triple, such as the %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% of the graph as a whole. While an triple takes the form of `<subject><predicate><object>`, a quad extends that to be `<subject><predicate><object><context>`. In the case of a %%named graph|named-graph%%, this `<context>` piece is the `<graphname>`. Although casual speech about %%Linked Open Data (LOD)|linked-open-data%% tends to refer most often to triples and %%triplestores|triplestore%%, many %%Linked Data (LD)|linked-data%% repositories are now quad stores and quads are commonly used, including by LINCS.

## Examples

- W3C (2014) _[RDF 1.1 N-Quads](https://www.w3.org/TR/n-quads/)_: The following example shows a subject (_spiderman_), predicate (_relationship/enemyOf_), and object (_green-goblin_), and provides context to the triple by referring to a graph about the Spiderman domain (_graphs/spiderman_).

```text
    <http://example.org/#spiderman>
    <http://www.perceive.net/schemas/relationship/enemyOf>
    <http://example.org/#green-goblin>
    <http://example.org/graphs/spiderman> .
```

## Further Resources

- W3C (2014) _[RDF 1.1 N-Quads](https://www.w3.org/TR/n-quads/)_
