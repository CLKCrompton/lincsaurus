---
id: entity
title: Entity
hoverText: A discrete thing, often described as the subject and object (or the domain and range) of a triple (subject-predicate-object).
---

<!-- @format -->

Entities are discrete things in existence, which can be linked together using attributes that build relationships between them. In %%Linked Open Data (LOD)|linked-open-data%%, entities are often described as the subject and object (or the %%domain|domain%% and %%range|range%%) of an %%triple|triple%% (subject-predicate-object).

## Examples

- CIDOC CRM (2022) [“E1 CRM Entity in version 7.1.1”](https://cidoc-crm.org/Entity/E1-CRM-Entity/version-7.1.1)

## Further Resources

- [Entity (Wikipedia)](https://en.wikipedia.org/wiki/Entity)
