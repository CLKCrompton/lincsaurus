---
id: optical-character-recognition
title: Optical Character Recognition (OCR)
hoverText: The automatic conversion of images of words to a text file that users can then search and edit.
---

<!-- @format -->

Optical Character Recognition is the conversion of images of words to a text file that users can then search and edit. It can be used to create text files from images of handwritten text, scanned documents, pictures that have words appearing somewhere in them, or any other form of non-text document that visually features words.

## Examples

- Scanning to make text or searchable versions of books ([Project Gutenberg](https://www.gutenberg.org/), [Google Books](https://books.google.com/))
- Taking a photo of a cheque to deposit it
- Automatic vehicle license plate recognition for electronic toll collection ([Highway 407](https://www.on407.ca/en/tolls/tolls/tolls-explained.html))

## Further Resources

- Computerphile (2017) [“Optical Character Recognition (OCR)”](https://www.youtube.com/watch?v=ZNrteLp_SvY) [Video]
- Khandelwal (2020) [“An Introduction to Optical Character Recognition for Beginners”](https://towardsdatascience.com/an-introduction-to-optical-character-recognition-for-beginners-14268c99d60)
- [Optical Character Recognition (Wikipedia)](https://en.wikipedia.org/wiki/Optical_character_recognition)
