---
id: cidoc-crm
title: CIDOC CRM
hoverText: A suite of event-centric ontologies for describing data in the cultural heritage domain, developed to link together heterogeneous sets of data managed by museums, galleries, and other heritage institutions.
---

<!-- @format -->

CIDOC CRM is an %%event-centric ontology|event-oriented-ontology%% for describing data in the cultural heritage domain. It is the conceptual reference model developed by the [International Council of Museums (ICOM)](https://icom.museum/en/). The %%ontology|ontology%% was developed with the intention of providing a way to link together heterogeneous sets of collections data detailing tangible and intangible cultural heritage managed by museums, galleries, and other heritage institutions.

CIDOC CRM has since grown into a suite of ontologies. CIDOC CRM, also referred to as CRMbase or CRMcore, is the original core ontology, and compatible models cover a number of additional domains, including bibliographic/library data, archaeology, scientific observation, and more.

## Examples

- [CIDOC CRM (Explorable Version)](http://cidoc-crm.org/html/cidoc_crm_v7.1.1_with_translations.html)
- [CIDOC CRM (Navigable Version)](http://www.cidoc-crm.org/Version/version-7.1.1)
- [CIDOC CRM (OWL Version)](http://erlangen-crm.org/current-version)
- [CIDOC CRM (RDFS Version)](https://cidoc-crm.org/rdfs/7.1.1/CIDOC_CRM_v7.1.1.rdfs)
- [CIDOC CRM (XML Version)](http://cidoc-crm.org/html/cidoc_crm_v7.1.1.xml)

## Further Resources

- Bruseker (2019) [“Learning Ontology & CIDOC CRM”](https://www.archesproject.org/wp-content/uploads/2020/04/Learning-Ontology-CRM.pdf)
- Bruseker, Carboni, & Guillem (2017) [“Cultural Heritage Data Management: The Role of Formal Ontology and CIDOC CRM”](https://link.springer.com/chapter/10.1007/978-3-319-65370-9_6)
- Bruseker & Guillem (2021) [“CIDOC CRM Game”](http://www.cidoc-crm-game.org/) [Game]
- Canning (2021) [“Ontologies at LINCS—Part 5: LINCS and CIDOC-CRM”](https://www.youtube.com/watch?v=VOkF7A_gTd0) [Video]
- CIDOC CRM (2022) [“Compatible Models & Collaborations”](https://cidoc-crm.org/collaboration_resources)
- CIDOC CRM (2022) [“What is the CIDOC CRM?”](https://cidoc-crm.org/)
- Doerr (2003) [“The CIDOC Conceptual Reference Module: An Ontological Approach to Semantic Interoperability of Metadata”](https://ojs.aaai.org//index.php/aimagazine/article/view/1720)
- Oldman, Doerr, de Jong, Norton, & Wikman (2014) [“Realizing Lessons of the Last 20 Years: A Manifesto for Data Provisioning & Aggregation Services for the Digital Humanities”](http://www.dlib.org/dlib/july14/oldman/07oldman.html)
- Oldman & Kurtz (2014) [“The CIDOC Conceptual Reference Model (CIDOC-CRM): PRIMER”](http://www.cidoc-crm.org/sites/default/files/CRMPrimer_v1.1_1.pdf)
- PARTHENOS (2022) [“Formal Ontologies: A Complete Novice’s Guide”](http://training.parthenos-project.eu/sample-page/formal-ontologies-a-complete-novices-guide/)
- Stead (2022) [“CIDOC CRM Tutorial”](https://cidoc-crm.org/cidoc-crm-tutorial) [Slides and Video]
