---
id: virtual-international-authority-file
title: Virtual International Authority File (VIAF)
hoverText: A service that aggregates the catalogues of many national libraries and miscellaneous authority files.
---

<!-- @format -->

The Virtual International Authority File (VIAF) is a service that aggregates the catalogues of many national libraries and miscellaneous %%authority records|authority-record%% to provide convenient access to the world’s major name %%authority files|authority-file%%. At LINCS, VIAF is used as a source of %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% for people and for bibliographic entities during %%reconciliation|reconciliation%%.

## Further Resources

- OCLC (2022) [“VIAF Overview”](https://www.oclc.org/en/viaf.html)
- [VIAF: The Virtual International Authority File](https://viaf.org/)
