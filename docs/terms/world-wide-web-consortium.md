---
id: world-wide-web-consortium
title: World Wide Web Consortium (W3C)
hoverText: An international community that works to develop Web standards.
---

<!-- @format -->

The World Wide Web Consortium (W3C) is an international community led by Tim Berners-Lee and Jeffrey Jaffe that works to develop Web standards. W3C’s mission is to “lead the World Wide Web to its full potential by developing protocols and guidelines that ensure the long-term growth of the Web.”

## Further Resources

- W3C (2022) [“Facts About W3C”](https://www.w3.org/Consortium/facts)
- W3C (2022) [“W3C Mission”](https://www.w3.org/Consortium/mission)
