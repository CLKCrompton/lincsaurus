---
id: reconciliation
title: Reconciliation
hoverText: The process of ensuring that an entity in a dataset refers to a stable Uniform Resource Identifier (URI), ideally from a stable namespace, to make data more accessible, interoperable, and efficient when searching, storing, and retrieving.
---

<!-- @format -->

Reconciliation is a process of ensuring that people, places, attributes, events, and other %%entities|entity%% in a dataset refer to stable %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%%, ideally from stable, standard %%namespaces|namespace%%. The benefit of entity reconciliation is that it improves access and interoperability across institutions, is more efficient when searching, storing, and retrieving data, improves the quality of description, and requires fewer resources to maintain over time. It also allows multiple datasets to be connected, as they refer to the same things in the same way. Reconciling entities within a discrete dataset to standard %%authority records|authority-record%% allows machines to recognize entities across platforms and systems.

## Examples

- Virtual International Authority File (2021) [“Cuthland, Ruth”](https://viaf.org/viaf/266374245/#skos:Concept)

## Further Resources

- OpenRefine (2022) [“Reconciling”](https://docs.openrefine.org/manual/reconciling)
- Rob Sanderson (2016) [“Linked Data Snowball or Why We Need Reconciliation”](https://www.slideshare.net/azaroth42/linked-data-snowball-or-why-we-need-reconciliation) [PowerPoint]
- Smith-Yoshimura (2016) [“Metadata Reconciliation”](https://hangingtogether.org/metadata-reconciliation/)
