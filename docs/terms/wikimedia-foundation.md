---
id: wikimedia-foundation
title: Wikimedia Foundation
hoverText: The umbrella organization that manages Wikipedia, Wikibase, Media Wiki, Wiktionary, and other Wiki projects and chapters.
---

<!-- @format -->

The Wikimedia Foundation, often referred to as Wikimedia, is the umbrella organization that manages Wikipedia, %%Wikibase|wikibase%%, Media Wiki, Wiktionary, and other Wiki projects and chapters.

## Examples

- [Wiktionary](https://www.wiktionary.org)
- [Wikisource](https://wikisource.org/wiki/Main_Page)
- [Wikispecies](https://species.wikimedia.org/wiki/Main_Page)

## Further Resources

- [Wikimedia](https://www.wikimedia.org)
