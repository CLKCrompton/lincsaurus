---
id: thinc-lab
title: THINC Lab
hoverText: A space at the University of Guelph that supports collaborative, interdisciplinary, and digital humanities research.
---

<!-- @format -->

The Humanities Interdisciplinary Collaboration (THINC) Lab is a space in the McLaughlin Library at the University of Guelph that supports collaborative, interdisciplinary, and %%Digital Humanities (DH)|digital-humanities%% research. The THINC Lab provides space, programming, resources, and support for those interested in adopting digital methods to conduct humanities research at Guelph, and reaches out to the broader community through online events and training such as the DH@Guelph Summer Workshops.

## Further Resources

- University of Guelph (2022) [“THINC Lab”](https://www.uoguelph.ca/arts/dhguelph/thinc)
