---
id: federated-sparql-search
title: Federated SPARQL Search
hoverText: A single entry point to access remote SPARQL Endpoints so that a query service can retrieve information from several data sources.
---

<!-- @format -->

Federated SPARQL search provides access to remote %%SPARQL Endpoints|sparql-endpoint%% so that a query service can retrieve information from several data sources. Federated search provides a single entry point to multiple data sources. A SERVICE clause in the body of a %%SPARQL query|sparql-protocol-and-rdf-query-language%% can be used to call on a remote SPARQL Endpoint.

## Examples

- The following example shows an empty SPARQL query.

```sparql
SELECT [...]
WHERE {
     SERVICE <http://example1.example.org/sparql> {
          [...]
          OPTIONAL {
               [...]
               SERVICE <http://example2.example.org/sparql> {
                    [...]   }   }
     }
}
```

## Further Resources

- [Federated Search (Wikipedia)](https://en.wikipedia.org/wiki/Federated_search)
- W3C (2013) [“SPARQL 1.1 Federated Query”](https://www.w3.org/TR/sparql11-federated-query/)
