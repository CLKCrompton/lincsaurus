---
id: object-oriented-ontology
title: Object-Oriented Ontology
hoverText: An ontology that uses objects to connect things, concepts, people, time, and place.
---

<!-- @format -->

An object-oriented %%ontology|ontology%% uses objects to connect things, mimicking human thinking in its connections of, for example, an object and its maker. They place objects at the center of their structure and connect pieces of information directly as attributes of those objects, as opposed to %%event-oriented ontologies|event-oriented-ontology%%.

## Examples

- The following example shows a book that is modelled as a central object, with its author and publication date as connected attributes.

![Object-Oriented Ontology](/img/documentation/glossary-object-oriented-ontology-(c-LINCS).jpg)

## Further Resources

- Bogost (2023) [“What is Object-Oriented Ontology?”](http://bogost.com/writing/blog/what_is_objectoriented_ontolog/)
- [Object-Oriented Ontology (Wikipedia)](https://en.wikipedia.org/wiki/Object-oriented_ontology)
