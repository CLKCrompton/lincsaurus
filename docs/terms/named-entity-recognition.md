---
id: named-entity-recognition
title: Named Entity Recognition (NER)
hoverText: The process of identifying and categorizing entities—a word or set of words that refer to the same thing—in text.
---

<!-- @format -->

Named Entity Recognition (NER) involves identifying and categorizing %%entities|entity%%—a word or set of words that refers to the same thing—in text. NER, therefore, involves two steps: (1) identifying the entity and (2) categorizing it. Examples of entity categories may be Persons, Locations, Times, Monetary Values, etc. The process of assigning a unique identifier to entities is referred to as %%Named Entity Disambiguation (NED)|named-entity-disambiguation%%. NER is an application of %%Natural Language Processing (NLP)|natural-language-processing%%.

## Examples

- [NERVE](https://github.com/cwrc/NERVE)
- [Stanford Named Entity Recognizer](https://nlp.stanford.edu/software/CRF-NER.html)

## Further Resources

- Hooland et al. (2015) [“Exploring Entity Recognition and Disambiguation for Cultural Heritage Collections”](https://freeyourmetadata.org/publications/named-entity-recognition.pdf)
- [Named Entity Recognition (Wikipedia)](https://en.wikipedia.org/wiki/Named-entity_recognition)
- Selig (2021) [“Entity Extraction: How Does It Work?”](https://expertsystem.com/entity-extraction-work/)
