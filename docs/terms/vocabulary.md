---
id: vocabulary
title: Vocabulary
hoverText: A collection of terms that could be concretely described in an ontology, taxonomy, or thesaurus.
---

<!-- @format -->

A vocabulary is a set or collection of terms. It is a generic term to refer to sets of concepts that could be concretely described in an %%ontology|ontology%%, %%taxonomy|taxonomy%%, or %%thesaurus|thesaurus%%.

|                   | Ontology                                                                                             | Taxonomy                                                                                      | Thesaurus                                                      | Vocabulary                                                            |
| ----------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | -------------------------------------------------------------- | --------------------------------------------------------------------- |
| **Describes**     | Concepts (content) and the relationships between them (structure), including axioms and restrictions | Hierarchical relationships among concepts, and specifies the term to be used to refer to each | Hierarchical and non-hierarchical relationships among concepts | General term for a collection of concepts (words) to do with a domain |
| **Relationships** | Typed hierarchical and associative                                                                   | Basically hierarchical, but all modeled using same notation                                   | Untyped hierarchical, associative, and equivalence             | Unspecified (abstract concept)                                        |
| **Properties**    | RDFS defines relationship properties and restrictions                                                | None                                                                                          | Can be described in scope notes if required                    | Unspecified (abstract concept)                                        |
| **Structured**    | Network                                                                                              | Tree                                                                                          | Cross-branch tree                                              | Unspecified (abstract concept)                                        |

## Further Resources

- [Vocabulary (Wikipedia)](https://en.wikipedia.org/wiki/Vocabulary)
