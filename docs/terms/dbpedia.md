---
id: dbpedia
title: DBPedia
hoverText: A project that creates publicly available structured data for the Linked Open Data (LOD) Cloud.
---

<!-- @format -->

DBPedia is a project that creates publicly available structured data for the %%Linked Open Data (LOD)|linked-open-data%% Cloud. It primarily makes available transformed Wikipedia and %%Wikimedia Foundation|wikimedia-foundation%% projects in data in %%triples|triple%%, making this information available in an open %%Knowledge Graph (KG)|knowledge-graph%% freely available to anyone on the web. DBPedia has been managed since 2014 by the [DBPedia Association](https://www.dbpedia.org/), which is currently affiliated with [Institute for Applied Informatics (InfAI)](https://infai.org/en/) at the University of Leipzig. In its earliest days, it was affiliated with the Free University of Berlin, where it was started in 2010 under the auspices of the Web Based Systems Group.

## Examples

- DBPedia (2022) [“Latest Core Releases”](https://www.dbpedia.org/resources/latest-core/)
- DBPedia (2022) [“Popular Individual Datasets”](https://www.dbpedia.org/resources/individual/)

## Further Resources

- [DBPedia (Wikipedia)](https://en.wikipedia.org/wiki/DBpedia)
- DBPedia (2022) [“About DBPedia”](https://www.dbpedia.org/about/)
- Uyi Idehen (2016) [“What is DBPedia and Why is it Important?”](https://medium.com/openlink-software-blog/what-is-dbpedia-and-why-is-it-important-d306b5324f90)
