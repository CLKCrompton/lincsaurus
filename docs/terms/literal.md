---
id: literal
title: Literal
hoverText: An object in a triple that does not refer to a resource with a Uniform Resource Identifier (URI), but instead conveys a value, such as text, a number, or a date.
---

<!-- @format -->

Some objects in %%Resource Description Framework (RDF)|resource-description-framework%% statements do not refer to other resources with a %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%, but instead convey a value, such as text, a number, or a date. These objects are known as literals.

There are two types of literals:

1. Plain literals: strings with a language tag
2. Typed literals: strings with a datatype URI reference

## Examples

- University of Saskatchewan Art Gallery: The following %%TTL|turtle%% snippet states that the title for 1998.004.001 has symbolic content, a plain literal of “Night Fire” with an English language tag.

```turtle
<http://id.lincsproject.ca/kl4QYIdodRH>
        a crm:E35_Title ;
        rdfs:label "Title of University of Saskatchewan Art Gallery object 1998.004.001"@en ;
        crm:P190_has_symbolic_content "Night Fire"@en .
```

## Further Resources

- Apache Jena (2022) [“Typed Literals How-To”](https://jena.apache.org/documentation/notes/typed-literals.html)
- Emmons et al. (2011) [“RDF Literal Data Types in Practice”](http://iswc2011.semanticweb.org/fileadmin/iswc/Papers/Workshops/SSWS/Emmons-et-all-SSWS2011.pdf)
- W3C (2004) [“3.4 Literals”](https://www.w3.org/TR/2004/REC-rdf-concepts-20040210/#section-Literals)
- W3C (2004) [“6.5 RDF Literals”](https://www.w3.org/TR/2004/REC-rdf-concepts-20040210/#section-Graph-Literal)
