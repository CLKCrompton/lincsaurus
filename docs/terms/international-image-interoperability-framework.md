---
id: international-image-interoperability-framework
title: International Image Interoperability Framework (IIIF)
hoverText: A set of tools and standards that make digital images interoperable, providing a standardized method of describing and delivering images online.
---

<!-- @format -->

The International Image Interoperability Framework (IIIF) is a set of tools and standards that make digital images interoperable, providing a standardized method of describing and delivering images online. IIIF allows the delivery of both image and structural and presentational %%metadata|metadata%%, structured as %%Linked Open Data (LOD)|linked-open-data%% generally using JSON-LD.

IIIF comprises two main %%Application Programming Interfaces (APIs)|application-programming-interface%%: an Image API and a Presentation API. Image formats are delivered in a %%Uniform Resource Locator (URL)|uniform-resource-locator%% string that specify the format of the final image, ensuring consistency across platforms. The Presentation API gives contextual information, in JSON-LD, and comprises metadata such as a title label, the sequence of the image in its original context, and other such information. These APIs are realized in IIIF-compatible software, such as [Universal Viewer](http://uvviewsoft.com/uviewer/).

## Examples

- IIIF (2017) [“What is IIIF?”](https://www.youtube.com/watch?v=8LiNbf4ELZM): The following image demonstrates IIIF compatibility, showing the presentation information on the left and the image information on the right.

![alt=""](/img/documentation/glossary-iiif-example-(fair-dealing).png)

## Further Resources

- Cramer (2017) [“03 Introduction to IIIF”](https://www.youtube.com/watch?v=EE1YskDrzPs) [Video]
- IIIF (2017) [“What is IIIF?”](https://www.youtube.com/watch?v=8LiNbf4ELZM) [Video]
- [International Image Interoperability Framework (Wikipedia)](https://en.wikipedia.org/wiki/International_Image_Interoperability_Framework)
- GitHub Inc (2022) [“International Image Interoperability Framework”](https://github.com/iiif)
