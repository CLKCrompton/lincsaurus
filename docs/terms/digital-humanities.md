---
id: digital-humanities
title: Digital Humanities (DH)
hoverText: A scholarly field in which digital tools and technologies are used to explore humanities research questions.
---

<!-- @format -->

Digital Humanities (DH) is a scholarly field in which digital tools and technologies are used to explore humanities research questions. Projects in DH can be large or small; large projects are often transdisciplinary and involve a team of collaborators, which can include faculty members, students, information technology specialists, and institutional partners (universities, archives, libraries, and museums). Common activities in DH include digitization, data compilation, database design, Geographic Information System (GIS) %%mapping|mapping%%, text extraction and analysis, data visualization, and web development.

## Examples

- [Map of Early Modern London](https://mapoflondon.uvic.ca/)
- [Orlando](http://www.artsrn.ualberta.ca/orlando/)
- [Yellow Nineties](https://1890s.ca/)

## Further Resources

- [Digital Humanities (Wikipedia)](https://en.wikipedia.org/wiki/Digital_humanities)
- Heppler (2015) [“What Is Digital Humanities?”](https://whatisdigitalhumanities.com/)
