---
id: digital-humanities-summer-institute
title: Digital Humanities Summer Institute (DHSI)
hoverText: An annual digital scholarship training institute held at the University of Victoria.
---

<!-- @format -->

The Digital Humanities Summer Institute (DHSI) is an annual digital scholarship training institute held at the University of Victoria. DHSI brings together faculty, staff, and students from the arts, humanities, library, and archives communities to discuss and learn new technologies and methods through coursework, seminars, and lectures.

## Further Resources

- Digital Humanities Summer Institute (2022) [“About DHSI”](https://dhsi.org/about-dhsi/)
