---
id: triplestore
title: Triplestore
hoverText: An NoSQL database that stores triples.
---

<!-- @format -->

A triplestore is a kind of %%NoSQL database|nosql-database%% that stores %%triples|triple%%. It has similarities to %%Graph databases|graph-database%%>, but the two terms are not synonymous—see comparison table below. Triplestores can be queried using %%SPARQL|sparql-protocol-and-rdf-query-language%%.

| Graph Database                                           | Triplestore                             |
| -------------------------------------------------------- | --------------------------------------- |
| Accommodates a variety of query languages such as Cypher | Uses SPARQL as the query language       |
| Stores various types of graphs                           | Stores rows of triples                  |
| Node/property-centric                                    | Edge-centric                            |
| Does not provide inferences on data                      | Provides inferences on data             |
| Less academic                                            | More synonymous with the “semantic web” |

## Examples

- [Apache Jena](https://jena.apache.org/)
- [BlazeGraph](https://blazegraph.com/)
- [GraphDB](https://www.ontotext.com/products/graphdb/)
- [Virtuoso](https://virtuoso.openlinksw.com/)

## Further Resources

- Gilbert (2016) [“Triplestores 101: Storing Data for Efficient Inferencing”](https://www.dataversity.net/triplestores-101-storing-data-efficient-inferencing/#)
- Ontotext (2022) [“What is an RDF Triplestore?”](https://www.ontotext.com/knowledgehub/fundamentals/what-is-rdf-triplestore/)
- [Triplestore (Wikipedia)](https://en.wikipedia.org/wiki/Triplestore)
