---
id: natural-language-processing
title: Natural Language Processing (NLP)
hoverText: A branch of artificial intelligence that involves automatic processing and/or manipulation of speech, text, and other unstructured data forms that represent how humans communicate with each other.
---

<!-- @format -->

Natural language processing is a branch of artificial intelligence that involves automatic processing and/or manipulation of speech, text, and other unstructured data forms that represent how humans communicate with each other. It describes the work involved in trying to get computers to understand our natural language and ways of communication. Most natural language processing techniques rely on machine learning to derive meaning from human communications.

## Examples

- [Google Translate](https://translate.google.ca/)
- Alexa, Siri, and other personal assistant devices that use speech recognition
- Chatbots

## Further Resources

- Lopez Yse (2019) [“Your Guide to Natural Language Processing (NLP)”](https://towardsdatascience.com/your-guide-to-natural-language-processing-nlp-48ea2511f6e1)
- [Natural Language Processing (Wikipedia)](https://en.wikipedia.org/wiki/Natural_language_processing)
- SparkCognition (2018) [“The Basics of Natural Language Processing”](https://www.youtube.com/watch?v=d4gGtcobq8M) [Video]
