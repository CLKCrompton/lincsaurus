---
id: classing
title: Classing
hoverText: To declare an entity to be an instance of a class using rdf:type within the chosen ontology for the dataset.
---

<!-- @format -->

Classing is declaring an %%entity|entity%% to be an instance of a class using rdf:type within the chosen %%ontology|ontology%% for the dataset. Declaring an entity’s class assigns that entity potential properties that take that class as their %%domain|domain%% or %%range|range%%, as well as declaring in broad terms what that type of “thing” that entity is. Classing structures entities and should not be confused with %%typing|typing%%.

## Examples

- AdArchives Project: In the following %%TTL|turtle%% snippet, wikidata:Q8030842, the Women’s Caucus for Art, is classed as an E74_Group, under the following definition provided by CIDOC CRM: “This class comprises any gatherings or organizations of human individuals or groups that act collectively or in a similar way due to any form of unifying relationship.” Because this entity is classed as a E74_Group, it can then be assigned a current or former residence using the property P74, with the entity acting as the domain and the location as the range. Likewise, it is the range of a relationship built with the P95i property. This property links formation events with groups, with the former acting as the domain and the latter the range.

```turtle
wikidata:Q8030842 a crm:E74_Group ;
  rdfs:label "Women's Caucus for Art"@en ;
  crm:P2_has_type wikidata:Q15911314 ;
  crm:P74_has_current_or_former_residence lincs:y0cAxtVpahj ;
  crm:P95i_was_formed_by lincs:HKa2CAjHuYd .
```

## Further Resources

- [Class (Wikipedia)](<https://en.wikipedia.org/wiki/Class_(knowledge_representation)>)
- Semantic University (2022) [“RDFS Introduction”](https://cambridgesemantics.com/blog/semantic-university/learn-owl-rdfs/)
- W3C (2014) [“2. Classes”](https://www.w3.org/TR/rdf-schema/#ch_classes)
