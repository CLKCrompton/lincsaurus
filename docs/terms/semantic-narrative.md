---
id: semantic-narrative
title: Semantic Narrative (ResearchSpace)
hoverText: An interactive document within the ResearchSpace environment that combines textual narrative and Linked Data (LD) to communicate ideas about people, places, and events.
---

<!-- @format -->

A semantic narrative is an interactive document that combines textual narrative and %%Linked Data (LD)|linked-data%% to communicate ideas about people, places, and events. Within the [ResearchSpace](/docs/tools/researchspace) environment, a semantic narrative takes the form of an interactive text document that incorporates embedded data, data visualizations, and is connected to the %%knowledge graph|knowledge-graph%% to ensure that the narrative is automatically updated when altered or expanded.

## Further Resources

- British Museum (2021) [“ResearchSpace: Semantic Tools”](https://researchspace.org/semantic-tools/)
- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://link.springer.com/chapter/10.1007/978-3-030-00668-6_20)
