---
id: functional-requirements-for-bibliographic-records
title: Functional Requirements for Bibliographic Records (FRBR)
hoverText: A model developed by the International Federation of Library Associations and Institutions (IFLA) to restructure catalogues around the conceptual structure of WEMI.
---

Functional Requirements for Bibliographic Records (FRBR) is a model developed by the [International Federation of Library Associations and Institutions (IFLA)](https://www.ifla.org/) to restructure catalogues around the conceptual structure of %%WEMI|wemi%%. It has subsequently been replaced by the Library Reference Model in [Resource Description and Access (RDA)](https://www.loc.gov/aba/rda/).

## Further Resources

- [Functional Requirements for Bibliographic Records (Wikipedia)](https://en.wikipedia.org/wiki/Functional_Requirements_for_Bibliographic_Records)
- IFLA (2023) [“Functional Requirements for Bibliographic Records (FRBR)”](https://www.ifla.org/references/best-practice-for-national-bibliographic-agencies-in-a-digital-age/resource-description-and-standards/bibliographic-control/functional-requirements-the-frbr-family-of-models/functional-requirements-for-bibliographic-records-frbr/)
- Tillett (2004) _[What is FRBR?: A Conceptual Model for the Bibliographic Universe](https://www.loc.gov/cds/downloads/FRBR.PDF)_
- Žumer, Aalberg, & O’Neill (2019) [“Application of the FRBR/LRM Model to Continuing Resources”](http://library.ifla.org/id/eprint/2464/1/208-zumer-en.pdf)
