---
id: crosswalking
title: Crosswalking
hoverText: The conceptual process of associating equivalent metadata values or fields from one schema with another.
---

<!-- @format -->

Crosswalking refers to the conceptual process of associating equivalent %%metadata|metadata%% values or fields from one schema with metadata values or fields from another schema. Crosswalking is also called %%mapping|mapping%%.

## Examples

- The following table shows how you would map “title” across data models.

| Starting Model Name | Starting Model Term | Destination Model Name | Destination Model Term                             |
| ------------------- | ------------------- | ---------------------- | -------------------------------------------------- |
| CDWA                | Title Text          | VRA Core               | `<vra: title> in <vra: work> or <vra: collection>` |
| Dublin Core         | Tile                | MODS                   | `<titleInfo><title>`                               |
| DACS                | 2.3 Title           | CIDOC CRM              | `E35_Title`                                        |

## Further Resources

- Harping (2022) [“Metadata Standards Crosswalk”](https://www.getty.edu/research/publications/electronic_publications/intrometadata/crosswalks.html)
- [Schema Crosswalk (Wikipedia)](https://en.wikipedia.org/wiki/Schema_crosswalk)
- TEI (2018) [“Crosswalks”](https://wiki.tei-c.org/index.php/Crosswalks)
