---
id: shapes-constraint-language
title: Shapes Constraint Language (SHACL)
hoverText: A standard for describing Resource Description Framework (RDF) graphs and validating them against a set of conditions.
---

<!-- @format -->

Shapes Constraint Language (SHACL) is a standard for describing %%Resource Description Framework (RDF)|resource-description-framework%% graphs and validating them against a set of conditions, provided in the form of an RDF graph and referred to as _shapes graphs_. RDF graphs that are validated against a shapes graph are referred to as _data graphs_. SHACL uses %%SPARQL|sparql-protocol-and-rdf-query-language%% to validate data graphs against shapes graphs.

## Examples

- Cagle (2016) [“Meet SHACL, the Next OWL”](https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/): The following instance describes a User shape requiring two properties: the presence of a single foaf:name as a string and of at least one foaf:mbox that is a valid email address.

  ```text
      SHACL "User" shape:
      ex:UserShape
            a sh:Shape ;
           sh:property [
                sh:predicate foaf:name ;
                sh:datatype xsd:string ;
                sh:minCount 1 ;
                sh:maxCount 1 ;
                ] ;
            sh:property [
                sh:predicate foaf:mbox ;
                sh:nodeKind sh:IRI ;
                sh:minCount 1 ;
                 ] .
  ```

- Cagle (2016) [“Meet SHACL, the Next OWL”](https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/): The following instance would satisfy the SHACL statement. It passes because it meets the conditions laid out by the shape: one (and only one) foaf:name that is a string, and at least one foaf:mbox that is a web resource.

  ```text
      inst:User1
           a foaf:Person ;
           foaf:name "Jane Doe" ;
           foaf:mbox mailto:jd@example.org> ;
           foaf:mbox mailto:janedoe@gmail.com> .
  ```

- Cagle (2016) [“Meet SHACL, the Next OWL”](https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/): The following instance would fail to satisfy the SHACL statement. This instance fails because there are two entries for foaf:name instead of one, thus breaking the sh:maxCount 1 rule.

  ```text
      inst:User2
           a foaf:Person ;
           foaf:name "Sarah Doe", "Sara Doe" ;
           foaf:mbox mailto:sd@example.org> ;
           foaf:mbox mailto:sarahdoe@gmail.com> .
  ```

## Further Resources

- [SHACL (Wikipedia)](https://en.wikipedia.org/wiki/SHACL)
- TopQuadrant (2017) [“An Overview of SHACL Shapes Constraint Language”](https://www.youtube.com/watch?v=_i3zTeMyRzU) [Video]
- W3C (2017) _[Shapes Constraint Language (SHACL)](https://www.w3.org/TR/shacl/)_
