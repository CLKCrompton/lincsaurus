---
id: wikimedia-commons
title: Wikimedia Commons
hoverText: A media file repository (images, sounds, and video clips) that makes public domain and freely licensed media content available, and which acts as the digital asset manager for all projects of the Wikimedia Foundation.
---

<!-- @format -->

Wikimedia Commons is a media file repository (images, sounds, and video clips) that makes public domain and freely licensed media content available to everyone, in multiple languages. It acts as the digital asset manager for all projects of the %%Wikimedia Foundation|wikimedia-foundation%%, but the files are available to external users as well. Like Wikipedia, Wikimedia Commons uses a wiki-technology that allows widespread public editing.

## Examples

- [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)

## Further Resources

- Wikimedia Commons (2021) [“What is Wikimedia Commons?”](https://commons.wikimedia.org/wiki/Commons:Welcome)
