---
id: resource-description-framework-schema
title: Resource Description Framework Schema (RDFS)
hoverText: An extension of the basic Resource Description Framework (RDF) vocabulary that can be used to define the vocabulary (terms) to be used in an RDF graph.
---

<!-- @format -->

Resource Description Framework Schema (RDFS) is an extension of the basic %%Resource Description Framework (RDF)|resource-description-framework%% vocabulary that provides a data-modelling %%vocabulary|vocabulary%% for RDF data. RDFS can be used to define the terms to be used in an RDF graph. While RDF defines instances, RDFS defines types of instances.

RDFS information is represented using RDF. RDFS resources all have %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% with the prefix `<http://www.w3.org/2000/01/rdf-schema>#`. RDFS specifies classes (rdfs: resource, literal, langString, HTML, XMLLiteral, class, property, datatype, statement, bag, seq, alt, container, containerMembershipProperty, and list) and %%properties|property%% (rdfs: type, subClassOf, subPropertyOf, domain, range, label, comment, member, first, rest, seeAlso, isDefinedBy, value, subject, predicate, and object). Using RDFS as well as RDF helps users to make more detailed descriptions of their data and to construct a simple %%ontology|ontology%%. %%Web Ontology Language (OWL)|web-ontology-language%% can be used instead to create a more expressively powerful ontology.

## Examples

- W3C (2014) _[RDF Schema 1.1](https://www.w3.org/TR/rdf-schema/)_

## Further Resources

- Gandon et al. (2015) [“The Resource Description Framework and its Schema”](https://hal.inria.fr/hal-01171045/document) \*[RDF Schema (Wikipedia)](https://en.wikipedia.org/wiki/RDF_Schema)
