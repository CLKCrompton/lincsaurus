---
id: digital-object-identifier
title: Digital Object Identifier (DOI)
hoverText: A type of Uniform Resource Identifier (URI) that is used to uniquely identify various academic, professional, and government information objects, such as journal articles, research reports, data sets, official publications, and videos.
---

<!-- @format -->

A Digital Object Identifier (DOI) is a type of %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% that is used to uniquely identify various academic, professional, and government information objects, such as journal articles, research reports, datasets, official publications, and videos. Each DOI begins with the number 10 and has a prefix and a suffix, separated by a slash. The prefix is made up of four or more digits, which are assigned to an organization. The publisher of the information objects creates the suffix that adheres to their particular identification standards.

DOIs are often included in the %%metadata|metadata%% of the information the object to which they refer. The DOI for an information object remains fixed over its lifetime, even when its location or other metadata changes. Therefore, a DOI tends to be a more stable way to refer to an information object than a %%Uniform Resource Locator (URL)|uniform-resource-locator%%.

## Further Resources

- [Digital Object Identifier (Wikipedia)](https://en.wikipedia.org/wiki/Digital_object_identifier)
- DOI (2021) [“The DOI System”](https://www.doi.org/index.html)
