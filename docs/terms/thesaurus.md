---
id: thesaurus
title: Thesaurus
hoverText: A structured vocabulary that shows hierarchical, associative, and equivalence relationships between concepts, so users not only will find terms that are broader and narrower than others, but also terms that are synonymous, antonymous, or otherwise related (associated) in a defined manner.
---

<!-- @format -->

A thesaurus is a structured %%vocabulary|vocabulary%% that shows basic relationships between concepts: hierarchical, associative, and equivalence. Users of a thesaurus not only will find terms that are broader and narrower than others, but also terms that are synonymous, antonymous, or otherwise related (associated) in a defined manner.

|                   | Ontology                                                                                             | Taxonomy                                                                                      | Thesaurus                                                      | Vocabulary                                                            |
| ----------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | -------------------------------------------------------------- | --------------------------------------------------------------------- |
| **Describes**     | Concepts (content) and the relationships between them (structure), including axioms and restrictions | Hierarchical relationships among concepts, and specifies the term to be used to refer to each | Hierarchical and non-hierarchical relationships among concepts | General term for a collection of concepts (words) to do with a domain |
| **Relationships** | Typed hierarchical and associative                                                                   | Basically hierarchical, but all modeled using same notation                                   | Untyped hierarchical, associative, and equivalence             | Unspecified (abstract concept)                                        |
| **Properties**    | RDFS defines relationship properties and restrictions                                                | None                                                                                          | Can be described in scope notes if required                    | Unspecified (abstract concept)                                        |
| **Structured**    | Network                                                                                              | Tree                                                                                          | Cross-branch tree                                              | Unspecified (abstract concept)                                        |

## Examples

- [Getty Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Library of Congress Subject Headings (LCSH)](http://id.loc.gov/authorities/subjects.html)

## Further Resources

- Schwarz (2005) [“Domain Model Enhanced Search—A Comparison of Taxonomy, Thesaurus and Ontology”](https://pdfs.semanticscholar.org/5b63/f9696de8627500ee62e13ae32b5dfe0c857b.pdf)
- [Thesaurus (Wikipedia)](https://en.wikipedia.org/wiki/Thesaurus)
- [Thesaurus (Wikipedia)](<https://en.wikipedia.org/wiki/Thesaurus_(information_retrieval)>)
