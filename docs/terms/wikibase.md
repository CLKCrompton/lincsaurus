---
id: wikibase
title: Wikibase
hoverText: A suite of free and open-source knowledge-base software for storing, managing, and accessing Linked Open Data (LOD), written for and used by the Wikidata project.
---

<!-- @format -->

Wikibase is a suite of free and open-source knowledge-base software for storing, managing, and accessing %%Linked Open Data (LOD)|linked-open-data%%, originally written for and still used by %%Wikidata|wikidata%%. After the success of Wikidata, Wikibase was made freely available to other %%Linked Data (LD)|linked-data%% projects.

## Examples

- [Enslaved](https://enslaved.org) (runs on Wikibase platform)
- [Wikibase](https://wikiba.se)
- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) (original project developed on Wikibase platform)

## Further Resources

- MediaWiki (2021) [“Wikibase/FAQ”](https://www.mediawiki.org/wiki/Wikibase/FAQ#:~:text=General-,What%20is%20the%20difference%20between%20Wikibase%20and%20Wikidata%3F,base%20that%20anyone%20can%20edit.)
- MediaWiki (2022) [“Wikibase/Federation”](https://www.mediawiki.org/wiki/Wikibase/Federation)
- UCLA Library (2017) [“Semantic Web and Linked Data: Wikibase”](https://guides.library.ucla.edu/semantic-web/wikidata)
