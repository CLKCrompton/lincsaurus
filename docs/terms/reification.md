---
id: reification
title: Reification
hoverText: The process of making an abstract concept concrete, such as taking the notion of a relationship and viewing it as an entity or expressing something using a programming language, so it can be programmatically manipulated.
---

<!-- @format -->

Reification is the process of making an abstract concept concrete. In conceptual and data modeling, this process would mean taking something abstract like the notion of a relationship and viewing it as an %%entity|entity%%: converting an idea into a logical or conceptual thing. By doing this, you can programmatically manipulate the abstract concept and attach additional information to it. Reification is to express something using a programming language so that it can be manipulated and treated by that language: so, expressing something using the %%Resource Description Framework (RDF)|resource-description-framework%% so that it can become treatable.

## Examples

- Expressing parent-child (_isA_) relationships: You can describe this relationship in the abstract concept of a tree and then construct a Tree object to express it, thus reifying the abstract concept of this tree-like relationship into a manipulatable Tree object.
- Expressing group-membership relationships: You can have group (_Committee_) and individual (_Person_) classes with abstract concepts that describe the relationship (_Membership_) between the two. To reify this, you would declare a method (_isMemberOf_) that states if a _Person_ instance is a member of a _Committee_, thus making explicit and manipulatable the abstract notion of a group-membership relationship.

## Further Resources

- Berners-Lee (2017) [“Reifying RDF (Properly), and N3”](https://www.w3.org/DesignIssues/Reify.html)
- [Reification (Wikipedia)](<https://en.wikipedia.org/wiki/Reification_(computer_science)>)
- Techopedia (2022) [“Reification”](https://www.techopedia.com/definition/21674/reification)
