---
id: provenance
title: Provenance
hoverText: The history of ownership, custody, or location of an object being described or the data describing that object.
---

<!-- @format -->

Broadly defined, provenance is the history of ownership, custody, or location of something. In the context of LINCS, it can refer to two things: the provenance of an object being described, or the provenance of the data describing that object. In the case of the former, provenance data would include names of owners and custodians, along with geographic locations of those actors and the timespan during which they held control of the object, in a chronologically that tracks the object from its creation to its current location. In the case of the latter, provenance data would include information about who made or edited the data and dates of creation and edits. Data provenance is also sometimes referred to as _data lineage_.

## Examples

- [Art Tracks](http://www.museumprovenance.org/)
- [Cleveland Museum of Art](https://www.clevelandart.org/art/collection/search) (includes provenance information for objects)

## Further Resources

- Brody (2018) [“Do You Know Where Your Data Came From?”](https://www.dataversity.net/know-data-came/)
- [Data Lineage (Wikipedia)](https://en.wikipedia.org/wiki/Data_lineage)
- [Provenance (Wikipedia)](https://en.wikipedia.org/wiki/Provenance#Data_provenance)
