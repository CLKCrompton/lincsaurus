---
id: event-oriented-ontology
title: Event-Oriented Ontology
hoverText: An ontology that uses events to connect things, concepts, people, time, and place.
---

<!-- @format -->

An event-oriented %%ontology|ontology%% uses events to connect things, concepts, people, time, and place ([Fichtner & Ribaud](https://www.researchgate.net/publication/267462327_Paths_and_Shortcuts_in_an_Event-Oriented_Ontology)). Event-oriented ontologies place events at the centre of their structure and describe information as the results or outputs of these events, as opposed to %%object-oriented ontologies|object-oriented-ontology%%, which place objects at the centre.

Using an event-oriented ontology allows the creation of %%metadata|metadata%% that provides a more complete path of human activities, better enabling machine learning. Additionally, the explicit modelling of events leads to a more complete integration of cultural information while still enabling the interconnection, mediation, and interchange of heterogenous cultural heritage datasets.

## Examples

- The following example shows a book that is modelled as a product of a “creation event,” with the author and publication date as aspects of that event.

![Event-Oriented Ontology](/img/documentation/glossary-event-oriented-ontology-(c-LINCS).jpg)

## Further Resources

- CIDOC CRM (2021) _[Volume A: Definition of the CIDOC Conceptual Reference Model, 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
- Fichtner & Ribaud (2012) [“Paths and Shortcuts in an Event-Oriented Ontology”](https://www.researchgate.net/publication/267462327_Paths_and_Shortcuts_in_an_Event-Oriented_Ontology)
