---
id: cypher
title: Cypher
hoverText: A query language for graph databases that reflects the semantic nature of triples but does so with its own unique syntax and formatting.
---

<!-- @format -->

Cypher is a query language that allows you to query %%graph database|graph-database%%. Like %%SPARQL|sparql-protocol-and-rdf-query-language%%, Cypher query language reflects the semantic nature of %%triples|triple%% but does so with its own unique syntax and formatting. Due to this similarity, Cypher can also be thought of as a set of sentences with blanks in them. The graph database will take this query and find every set of matching statements that correctly fills in those blanks.

## Examples

- The following query tells the database to search for all Person nodes that are connected to Movie nodes through a relationship of `ACTED_IN`. It further narrows the Movie nodes to only ones where the title property starts with the letter “t.” Finally, it tells the database to return all of this information as two columns: the movie.title in one (_title_) and all of the actor names (_cast_) together in a second and to order this list alphabetically by title.

```cypher
MATCH (actor:Person)-[:ACTED_IN]->(movie:Movie)
    WHERE movie.title STARTS WITH "T"
    RETURN movie.title AS title, collect(actor.name) AS cast
    ORDER BY title;
```

## Further Resources

- [Cypher (Wikipedia)](<https://en.wikipedia.org/wiki/Cypher_(query_language)>)
- Neo4j (2017) [“Intro to Cypher”](https://www.youtube.com/watch?v=pMjwgKqMzi8) [Video]
- Neo4j (2022) [“Cypher Query Language”](https://neo4j.com/developer/cypher-basics-i/)
