---
id: semantic-web
title: Semantic Web
hoverText: The idea of extending the World Wide Web by including additional data descriptors to web-published content so computers can make meaningful interpretations of the published data.
---

<!-- @format -->

The Semantic Web is the idea of extending the World Wide Web by including additional data descriptors to web-published content so computers can make meaningful interpretations of the published data. %%Linked Open Data (LOD)|linked-open-data%% is essential for the realization of the Semantic Web, as in order for it to function, computers must be able to access interlinked sets of structured data, along with sets of rules that can be used for inferencing. By supporting the creation of a web of machine-readable data, LOD works to advance the dream of a Semantic Web. The Semantic Web technology stack involves the %%Resource Description Framework (RDF)|resource-description-framework%%, %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%, %%SPARQL|sparql-protocol-and-rdf-query-language%%, and %%Web Ontology Language (OWL)|web-ontology-language%%.

## Examples

- [Semantic Web (Wikipedia)](https://en.wikipedia.org/wiki/Semantic_Web): “The Semantic Web Stack is an illustration of the hierarchy of languages, where each layer exploits and uses capabilities of the layers below. It shows how technologies that are standardized for Semantic Web are organized to make the Semantic Web possible.”

![Diagram of the Semantic Web stack, showing a colour-coded hierarchy of semantic languages.](/img/documentation/glossary-semantic-web-example-(cc0).png)

## Further Resources

- Berners-Lee, Hendler, & Lassila (2001) [“The Semantic Web”](https://www-sop.inria.fr/acacia/cours/essi2006/Scientific%20American_%20Feature%20Article_%20The%20Semantic%20Web_%20May%202001.pdf)
- Cambridge Semantics (2016) [“An Introduction to the Semantic Web”](https://www.youtube.com/watch?v=V6BR9DrmUQA) [Video]
- [Semantic Web (Wikipedia)](https://en.wikipedia.org/wiki/Semantic_Web)
