---
id: text-encoding-initiative
title: Text Encoding Initiative (TEI)
hoverText: An encoding language that supports detailed encoding of complex documents and is used extensively by a number of different Digital Humanities (DH) projects.
---

<!-- @format -->

The Text Encoding Initiative (TEI) is an encoding language intended to support detailed encoding of complex documents and is used extensively by a number of different %%Digital Humanities (DH)|digital-humanities%% projects. As an encoding language, it is used to describe both the physical characteristics of a text (line breaks, pagination, special characters, etc.), classify subjects, tag people, places, and events, and to add commentary.

## Examples

- [Map of Early Modern London](https://mapoflondon.uvic.ca/)
- [Orlando](http://www.artsrn.ualberta.ca/orlando/)

## Further Resources

- Crompton, et al. (2021), [“Mapping and Vetting: Converting TEI to Linked Data”](https://www.dropbox.com/s/lj94h40odvrlnuk/Crompton-Zafar-Canning-Lipski-Defours-LINCS-TEI-2021.pdf?dl=0) [PowerPoint]
- Crompton (2022) [“What the Computer Doesn’t Know: Representing Primary Source Documents in TEI”](http://constancecrompton.com/2015/cin/CromptonTEI.pdf) [PowerPoint]
- Text Encoding Initiative (2022) [“Projects Using the TEI”](https://tei-c.org/activities/projects/)
