---
id: turtle
title: Turtle
hoverText: A human- and machine-readable markup language that allows users to serialize triples.
---

<!-- @format -->

Turtle (Terse RDF Triple Language) is a highly human- and machine-readable markup language that allows users to express %%triples|triple%% according to the %%Resource Description Framework (RDF)|resource-description-framework%%.

Turtle’s syntax allows users to group three %%Uniform Resource Identifiers (URI)|uniform-resource-identifier%% to make a triple. It also provides ways to abbreviate the information in each triple, since users can define prefixes at the beginning of their TTL file so common portions of URIs can be factored out.

## Examples

- The following example shows a basic TTL statement where the relationship between Margaret Laurence and her novel, _The Stone Angel_, is expressed:

```turtle

<http://example.org/person/Margaret_Laurence>
   <http://example.org/relation/author>
   <http://example.org/books/The_Stone_Angel> .

```

<!-- TODO: add example using prefixes -->

## Further Resources

- DBpedia (2023) [“About: Turtle (Syntax)”](https://dbpedia.org/page/Turtle_(syntax))
- [Turtle (Wikipedia)](https://en.wikipedia.org/wiki/Turtle_(syntax))
- W3C (2014) [“RDF 1.1 Turtle”](https://www.w3.org/TR/turtle/)
