---
id: wemi
title: WEMI
hoverText: An acronym standing for Work, Expression, Manifestation, and Item—terms deriving from Functional Requirements for Bibliographic Records (FRBR), which is the primary means of describing bibliographic records.
---

<!-- @format -->

WEMI is an acronym standing for Work, Expression, Manifestation, and Item—terms deriving from %%Functional Requirements for Bibliographic Records (FRBR)|functional-requirements-for-bibliographic-records%%, which is the primary means of describing bibliographic records.

Consequently, WEMI is the foundation both for [FRBRoo](https://www.cidoc-crm.org/frbroo/home-0), the CRM-compliant %%Linked Open Data (LOD)|linked-open-data%% version of the conceptual model, and [LRMoo](https://www.cidoc-crm.org/frbroo/Issue/ID-360-lrmoo), which will replace FRBRoo once approved by the %%CIDOC CRM|cidoc-crm%% Special Interest Group.

In WEMI, the Work represents the story, song, poem, or intangible form of a work. The Expression represents a specific form of the Work, such as an English text (Expression) of a story (Work). A Manifestation represents a specific published or produced Expression, such as a Penguin edition (Manifestation) of an English text (Expression) of a story (Work). The Item represents the individual unit of the Manifestation, such as the McLaughlin Library’s copy (Item) of the Penguin edition (Manifestation) of an English text (Expression) of a story (Work). In short, a Work is realized through an Expression; an Expression is embodied in a Manifestation; and a Manifestation is exemplified by an Item ([Library of Congress, 2004](https://www.loc.gov/cds/downloads/FRBR.PDF)).

## Further Resources

- [FRBRoo (Wikipedia)](https://en.wikipedia.org/wiki/FRBRoo)
- [IFLA Library Reference Model (Wikipedia)](https://en.wikipedia.org/wiki/IFLA_Library_Reference_Model)
- RDA Basics (2012) [“Theoretical Foundations of RDA (or, what is WEMI?)”](https://rdabasics.com/2012/08/24/theoretical-foundations/)
- Riva, Le Bœuf, & Žumer (2017) _[IFLA Library Reference Model: A Conceptual Model for Bibliographic Information](https://www.ifla.org/wp-content/uploads/2019/05/assets/cataloguing/frbr-lrm/ifla-lrm-august-2017_rev201712.pdf)_
