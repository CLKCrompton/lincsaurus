---
id: sparql-protocol-and-rdf-query-language
title: SPARQL Protocol and RDF Query Language (SPARQL)
hoverText: A query language for triplestores that translates graph data into normalized, tabular data with rows and columns.
---

<!-- @format -->

SPARQL Protocol and RDF Query Language (SPARQL) is a query language that allows you to query %%triplestores|triplestore%%. It translates graph data into normalized, tabular data with rows and columns. It is useful to think of a SPARQL query as a Mad Lib—a set of sentences with blanks in them. The database will take this query and find every set of matching statements that correctly fill in those blanks. What makes SPARQL powerful is the ability to create complex queries that reference many %%variables|query-variable%% at a time.

## Examples

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL): The following query tells the database to search for all values of `?painting` that properly complete the %%Resource Description Framework (RDF)|resource-description-framework%% statement `<has medium><oil on canvas>` . `?painting` stands in for the node(s) that the database will return.

  ```sparql
  SELECT ?painting
  WHERE {
     ?painting <has medium> <oil on canvas> .
   }
  ```

- The following query has a second variable: `?artist`. The database will return all matching combinations of `?artist` and `?painting` that fulfill both of these statements.

  ```sparql
     SELECT ?artist ?painting
     WHERE {
        ?artist <has nationality> <Dutch> .
        ?painting <was created by> ?artist .
     }
  ```

- CWRC Linked Data (2022) [“CWRC SPARQL Endpoint”](https://yasgui.lincsproject.ca/#): The SPARQL query at the above link returns all of the people in CWRC’s current LODset (currently the first extraction from Orlando’s dataset on British women’s writing) who were born in the nineteenth century, ordered by birthdate.

## Further Resources

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL)
- [Semantic Queries (Wikipedia)](https://en.wikipedia.org/wiki/Semantic_query)
- [SPARQL (Wikipedia)](https://en.wikipedia.org/wiki/SPARQL)
- Wikibooks (2018) [“XQuery/SPARQL Tutorial”](https://en.wikibooks.org/wiki/XQuery/SPARQL_Tutorial)
