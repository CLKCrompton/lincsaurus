---
id: canada-foundation-for-innovation
title: Canada Foundation for Innovation (CFI)
hoverText: A non-profit corporation that invests in research infrastructure at Canadian universities, colleges, research hospitals, and non-profit research institutions.
---

<!-- @format -->

The Canada Foundation for Innovation (CFI) is a non-profit corporation that invests in research infrastructure at Canadian universities, colleges, research hospitals, and non-profit research institutions. LINCS is funded by a Cyberinfrastructure grant from the CFI.

## Further Resources

- Canada Foundation for Innovation (2022) [“About Us”](https://www.innovation.ca/about)
