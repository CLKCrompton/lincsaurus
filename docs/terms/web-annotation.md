---
id: web-annotation
title: Web Annotation
hoverText: An online annotation of a web resource that denotes a connection between different resources.
---

<!-- @format -->

A web annotation is an online annotation of a web resource, usually in the format of a comment or tag, that denotes a connection between different resources. Web annotations are often conceptualized as being a layer that exists on top of an existing resource. If a user uses a specific annotation system to create their annotations, other users using the same annotation system can sometimes see the annotations of the other user.

Different standards exist for formatting web annotations, such as the %%Web Annotation Data Model (WADM)|web-annotation-data-model%%.

## Further Resources

- [Web Annotation (Wikipedia)](https://en.wikipedia.org/wiki/Web_annotation)