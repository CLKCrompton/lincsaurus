---
id: xml
title: XML
hoverText: A human- and machine-readable markup language that allows users to create their own tags to describe documents.
---

<!-- @format -->

XML (Extensible Markup Language) is a markup language that was released by the %%World Wide Web Consortium (W3C)|world-wide-web-consortium%% in 1998. It allows users to create their own tags to “mark up” (i.e., describe) documents. XML documents can be easily read by both humans and machines. Since XML stores data in a plain text format, it is software- and hardware-independent, making it useful for data sharing. XML also supports [Unicode](https://home.unicode.org/), which means that it can be used to transmit information in most languages.

XML uses elements to create the structure of XML documents. Elements typically describe what something is. Each element has an opening tag (e.g., `<title>`) and a closing tag (e.g., `</title>`), unless the element is self-closing (e.g., `<lb/>`). All XML documents are formed as “element trees” which begin at a root element that contains all the other elements. By nesting elements, encoders can create parent-child and sibling-sibling relationships. Attributes can be added to elements to add supplemental information and values are used to specify attributes.

## Examples

- The following example shows a basic XML tree where the element `<person>` has been specified by the attribute `@type` and the value `"author"`.

```xml

<person type="author">Margaret Laurence</person>

```

- The following example shows a basic XML tree where `<person>` is the parent of `<persName>` and `<persName>` is the child of `<person>`. The elements `<reg>`, `<forename>`, and `<surname>` are all the children of `<persName>` and `<reg>`, `<forename>`, and `<surname>` are siblings of each other.

```xml
<person>
    <persName>
        <reg>Margaret Laurence</reg>
        <forename>Margaret</forename>
        <surname>Laurence</surname>
    </persName>
</person>
```

## Further Resources

- Birnbaum (2021) [“What is XML and Why Should Humanists Care? An Even Gentler Introduction to XML”](http://dh.obdurodon.org/what-is-xml.xhtml)
- Flynn, Silmaril Consultants, & Textual Therapy Division (2022) [“Section 1: Basics General Information About XML”](http://xml.silmaril.ie/basics.html)
- Walsh (1998) [“A Technical Introduction to XML”](https://www.xml.com/pub/a/98/10/guide0.html)
- W3C (2015) [“XML Essentials”](https://www.w3.org/standards/xml/core)
- W3C (2016) [“Extensible Markup Language (XML)”](https://www.w3.org/XML/)
- W3Schools (2022) [“XML Tree”](https://www.w3schools.com/xml/xml_tree.asp)
- W3Schools (2022) [“XML Tutorial”](https://www.w3schools.com/xml/)
