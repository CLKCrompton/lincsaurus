---
id: namespace
title: Namespace
hoverText: A directory of concepts that are used to identify and refer to entities within a dataset.
---

<!-- @format -->

A namespace is a directory of concepts that are used to identify and refer to %%entities|entity%% within a dataset. Namespaces can be internal, used as a basis for %%minting Uniform Resource Identifiers (URIs)|uniform-resource-identifier-minting%% for a discrete dataset, or they can be external %%ontologies|ontology%%, %%vocabularies|vocabulary%%, and %%thesauri|thesaurus%% that could be either encyclopedic or industry-specific. It is best practice to use or extend a pre-existing vocabulary and link to that vocabulary using stable URIs provided by their namespace, rather than build a bespoke vocabulary. By using standard vocabularies maintained within stable namespaces, the achievements of %%Linked Open Data (LOD)|linked-open-data%% are realized.

## Examples

- The following example shows the declaration of namespaces for Y90s Personography in the %%TTL|turtle%% file header. Namespaces provide URIs for each listed entity. The namespaces above consist of a domain name and a path, as well as a prefix that allows shortening of resources within the TTL file (for example, bf:title would refer be a truncated version of <http://id.loc.gov/ontologies/bibframe/title>). Declaring and using namespaces allows both machine and human readers to differentiate between identically named elements from multiple datasets.

```turtle
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix bibo: <http://purl.org/ontology/bibo/> .
@prefix crm: <http://www.cidoc-crm.org/cidoc-crm/> .
```

## Further Resources

- Linked Conservation Data (2022) [“What is a Namespace?”](https://www.ligatus.org.uk/lcd/faq/211)
- [Namespace (Wikipedia)](https://en.wikipedia.org/wiki/Namespace)
- W3C (2014) [“6. Standard Vocabularies”](https://www.w3.org/TR/ld-bp/#VOCABULARIES)
