---
id: argument
title: Argument
hoverText: A series of reasons, statements, or facts in a metadata schema intended to support or establish a point of view, rather than a neutral statement, that describes a person, event, or object.
---

<!-- @format -->

An argument is a series of reasons, statements, or facts intended to support or establish a point of view, rather than a neutral statement that describes a person, event, or object. Standard %%metadata|metadata%% schema do not necessarily allow for the demarcation of an argument, or its assignment to an author. However, using %%CIDOC CRM|cidoc-crm%%, it is possible to record instances of argumentation within metadata, allowing for different vantage points and perspectives to be represented, compared, %%reconciled|reconciliation%%, and progressed by researchers.

## Examples

- British Museum (2021) [“ResearchSpace: Argument and Uncertainty”](https://researchspace.org/argument/)

## Further Resources

- Doerr, Kritsotaki, & Boutsika (2011) [“Factual Argumentation—A Core Model for Assertions Making”](https://dl.acm.org/doi/10.1145/1921614.1921615)
- Paveprime Ltd (2019) _[CRMinf: The Argumentation Model](https://cidoc-crm.org/crminf/sites/default/files/CRMinf%20ver%2010.1.pdf)_
- Stead (2015) [“CRMinf: The Argumentation Model”](https://www.youtube.com/watch?v=iZz9Q-wdyY0) [Video]
