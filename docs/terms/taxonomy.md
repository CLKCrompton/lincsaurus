---
id: taxonomy
title: Taxonomy
hoverText: A system that identifies hierarchical relationships among concepts within a domain.
---

<!-- @format -->

A taxonomy identifies hierarchical relationships among concepts within a domain. A taxonomy categorizes items within a single dimension: high-level categories are broken down into sub-categories, and then into further sub-categories in a branching but linear fashion, all the way down to an individual entity. A taxonomy does not represent relationships across categories, only hierarchically.

|                   | Ontology                                                                                             | Taxonomy                                                                                      | Thesaurus                                                      | Vocabulary                                                            |
| ----------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | -------------------------------------------------------------- | --------------------------------------------------------------------- |
| **Describes**     | Concepts (content) and the relationships between them (structure), including axioms and restrictions | Hierarchical relationships among concepts, and specifies the term to be used to refer to each | Hierarchical and non-hierarchical relationships among concepts | General term for a collection of concepts (words) to do with a domain |
| **Relationships** | Typed hierarchical and associative                                                                   | Basically hierarchical, but all modeled using same notation                                   | Untyped hierarchical, associative, and equivalence             | Unspecified (abstract concept)                                        |
| **Properties**    | RDFS defines relationship properties and restrictions                                                | None                                                                                          | Can be described in scope notes if required                    | Unspecified (abstract concept)                                        |
| **Structured**    | Network                                                                                              | Tree                                                                                          | Cross-branch tree                                              | Unspecified (abstract concept)                                        |

## Examples

- [Dewey Decimal Classification System](https://www.oclc.org/en/dewey.html)
- [Library of Congress Classification System](https://www.loc.gov/catdir/cpso/lcco/)

## Further Resources

- Schwarz (2005) [“Domain Model Enhanced Search—A Comparison of Taxonomy, Thesaurus and Ontology”](https://pdfs.semanticscholar.org/5b63/f9696de8627500ee62e13ae32b5dfe0c857b.pdf)
- [Taxonomy (Wikipedia)](<https://en.wikipedia.org/wiki/Taxonomy_(general)>)
