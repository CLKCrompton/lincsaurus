---
id: application-programming-interface
title: Application Programming Interface (API)
hoverText: A code library that enables third-party applications to communicate with a web service platform.
---

<!-- @format -->

An Application Programming Interface (API) is a code library assembled by a web service company to enable third-party applications to communicate with a web service platform. Though an API is an interface, it is an interface for computer-to-computer communication and therefore is not presented to the human user in the way that you normally think of when you hear the word “interface.” Instead, a company may build a front end display on top of their API to support human use. Every time you use a web service like Facebook or Twitter, you are interacting with an API through the front-end tools developed for human users. However, as APIs are for computer-to-computer communication, there are more direct ways to interact with APIs as well using programming languages.

## Examples

- [Cooper Hewitt Smithsonian Design Museum API](https://collection.cooperhewitt.org/api/)
- [Europeana APIs](https://pro.europeana.eu/page/apis)
- [Library of Congress API](https://libraryofcongress.github.io/data-exploration/)
- [Rijksmuseum API](https://www.rijksmuseum.nl/en/api/-rijksmuseum-oai-api-instructions-for-use)
- [WCLC APIs](https://www.oclc.org/developer/develop/web-services/worldcat-search-api.en.html)

## Further Resources

- [API (Wikipedia)](https://en.wikipedia.org/wiki/Application_programming_interface)
- Wrubel (2018) [“Unboxing the Library: Introduction to APIs”](https://labs.loc.gov/static/labs/meta/Wrubel-UnboxingtheLibrary-IntroductiontoAPIs.pdf) [PowerPoint]
- Wuyts (2018) _[Cultivating APIs in the Cultural Heritage Sector](http://jolanwuyts.eu/files/Cultivating_APIs_in_the_cultural_heritage_sector_Jolan_Wuyts_2018.pdf)_
