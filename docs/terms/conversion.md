---
id: conversion
title: Conversion
hoverText: The process of changing data from one format to another.
---

<!-- @format -->

Data conversion is the process of changing data from one format to another. The goal of data conversion is to prevent data loss or corruption by maintaining the integrity of the data and embedded structures. Converting data should aim to result in maintaining or increasing the meaning communicated by the data and its structure and format.

## Further Resources

- [Data Conversion (Wikipedia)](https://en.wikipedia.org/wiki/Data_conversion)
- Langmann (2022) [“How to Convert an Excel Spreadsheet to XML”](https://spreadsheeto.com/xml/)
- W3C (2015) [“Generating RDF from Tabular Data on the Web”](https://www.w3.org/TR/csv2rdf/)
