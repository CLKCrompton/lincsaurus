---
id: ingestion
title: Ingestion
hoverText: The process by which data is moved from one or more sources to a new destination where it can be stored and further analyzed.
---

<!-- @format -->

Ingestion is a process by which data is moved from one or more sources to a destination where it can be stored and further analyzed. Ingesting heterogeneous datasets from a variety of sources can involve %%crosswalking|crosswalking%% and other steps in data cleaning and preparation for ingestion to go smoothly. Data can be ingested in real time, each piece coming in as it is pushed out by the source, or in batches periodically.

## Further Resources

- Big Data Trunk (2016) [“Introduction to Data Ingestion”](https://www.youtube.com/watch?v=YD6QCEVDw20) [Video]
