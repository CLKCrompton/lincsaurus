---
id: edge
title: Edge
hoverText: A line that connects one node to another in a graph database, representing a relationship between the nodes.
---

<!-- @format -->

An edge is a line that connects one %%node|node%% to another in a %%graph database|graph-database%%, representing a relationship between them. Edges can be directed or undirected. When they are directed they have different meanings depending on their direction.

## Examples

- The following image shows a %%Resource Description Framework (RDF)|resource-description-framework%% graph highlighting the edges that connect three nodes.

![alt=""](/img/documentation/glossary-edge-example-(c-LINCS).jpg)
