---
id: uniform-resource-locator
title: Uniform Resource Locator (URL)
hoverText: A statement that describes the location of something on the web specifically for locating online assets.
---

<!-- @format -->

A Uniform Resource Locator (URL) describes the location of something on the web. It is a type of %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% (all URLs are URIs but not all URIs are URLs) specifically for locating online assets. URLs are most commonly used to reference web pages, but have additional applications such as file transfers and email addresses. This application is indicated by the first part of the URL, called the protocol: http denotes a webpage, ftp a file transfer, mailto an email address, etc.

## Examples

- In the following URI for the LINCS project’s “About” page, you know it is a secure webpage (https) with the LINCS hostname (lincsproject.ca) and a path, ultimately leading to a filename (what-is-lincs) to uniquely locate that page on the LINCS website, and on the internet at large.

  `https://lincsproject.ca/what-is-lincs/`

## Further Resources

- Computer Hope (2021) [“URL”](https://www.computerhope.com/jargon/u/url.htm)
- Themeisle (2022) [“What Is a Website URL? The 3 Most Important Parts Explained”](https://themeisle.com/blog/what-is-a-website-url/)
- [URL (Wikipedia)](https://en.wikipedia.org/wiki/URL)
