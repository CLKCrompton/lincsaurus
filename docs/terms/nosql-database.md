---
id: nosql-database
title: NoSQL Database
hoverText: A database that is modeled in a way that is different to the tabular relations used in a relational database, such as a document database, key-value store, wide-column store, or graph database.
---

<!-- @format -->

NoSQL databases refer to databases that provide storage of data that is modeled in a way that is different to the tabular relations used in %%relational databases|relational-database%%. The “No” part of the term is best understood to mean “not only” as opposed to none at all. NoSQL databases may support structured, relational data and %%SQL|structured-query-language%%-like query languages, but they are not bound to only this method. NoSQL databases are used for Big Data and as such are designed to be fast and scalable.

There are four major types of NoSQL databases:

1. Document databases: data is stored in “documents” similar to %%JSON|json%%
2. Key-value stores: data values are accessed by keys
3. Wide-column stores: data is stored in columns like a two-dimensional key-value store
4. %%Graph databases|graph-database%%>: data is stored in %%nodes|node%%, %%edges|edge%%, and %%properties|property%%

| Relational Database                           | NoSQL Database                              |
| --------------------------------------------- | ------------------------------------------- |
| Low flexibility, high structure               | High flexibility, low structure             |
| Effective for data storage on a single server | Designed for use across distributed systems |
| Single query language: SQL                    | Query language specific to product          |

## Examples

- [Apache Cassandra](https://cassandra.apache.org/) (wide-column store)
- [MongoDB](https://www.mongodb.com/) (document database)
- [Redis](https://redis.io/) (key-value store)

## Further Resources

- MongoDB (2022) [“What is NoSQL?”](https://www.mongodb.com/nosql-explained)
- [NoSQL (Wikipedia)](https://en.wikipedia.org/wiki/NoSQL)
- Yegulalp (2017) [“What is NoSQL? Databases for a Cloud-Scale Future”](https://www.infoworld.com/article/3240644/what-is-nosql-databases-for-a-cloud-scale-future.html)
