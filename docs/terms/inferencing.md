---
id: inferencing
title: Inferencing
hoverText: The automated discovery of new facts generated from existing triples.
---

<!-- @format -->

Inferencing involves the automated discovery of new facts based on a combination of data and rules. Inference with %%triples|triple%% means that automatic procedures can generate new facts from existing triples. Inference allows us to say, for example, that since an instance is a member of a subclass set, that it is also a member of the parent class. If you say that Susan is a corgi, and that a corgi is a subclass (in this case, breed) of dog, then it follows that Susan the corgi is a dog. Inference helps to improve the quality of data by discovering new relationships and automatically analyzing the content of the data. Inferencing is sometimes referred to as a _reasoning_.

## Examples

- Corgi is a subclass of dog, and instances of the corgi subclass are also to be considered as members of the dog parent class. The following inference allows us to see what entities are members of parent classes when they have been only modeled as members of a subclass. You can ask questions such as “how many dogs are in this dataset” and have Susan be counted, without needing to model her explicitly as a dog and not just as a corgi.

```text
    Susan isA corgi +
    corgi isA dog >
    Susan isA dog
```

- The following inference allows us to enforce rules to help clean our dataset. If a series of conditions holds (in this case, if two people have the same name, email address, and home address) then some meaning about the relationship between the two entities is derived (they are in fact referring to the same person and should likely be reconciled).

```text
    IF personA.name == personB.name
    personA.emailAddress == personB.emailAddress +
    personA.homeAddress == personB.homeAddress
    THEN personA = personB
```

## Further Resources

- Ontotext (2022) [“What is Inference?”](https://www.ontotext.com/knowledgehub/fundamentals/what-is-inference/)
- [Semantic Reasoner (Wikipedia)](https://en.wikipedia.org/wiki/Semantic_reasoner)
- W3C (2015) [“Inference”](https://www.w3.org/standards/semanticweb/inference)
