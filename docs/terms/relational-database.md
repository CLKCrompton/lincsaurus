---
id: relational-database
title: Relational Database
hoverText: A database that stores data in tabular form (columns and rows), where columns hold attributes of data (such as datatypes), rows hold “records,” and relationships are defined between tables using rules.
---

<!-- @format -->

A relational database is a type of database that stores data in tabular form (columns and rows). Relational databases are based on the relational data model, thus allowing for a consistent method and language of querying: %%Structured Query Language (SQL)|structured-query-language%%. In relational databases, columns hold attributes of data (such as datatype) and rows hold instances called “records.” Relationships can be defined between tables using rules such as foreign key constraints which declare that a value in one table must come from an existing value in a separate table. Tables can be linked together (related) based on common data to support querying across multiple tables.

| Relational Database                           | NoSQL Database                              |
| --------------------------------------------- | ------------------------------------------- |
| Low flexibility, high structure               | High flexibility, low structure             |
| Effective for data storage on a single server | Designed for use across distributed systems |
| Single query language: SQL                    | Query language specific to product          |

## Examples

- [Microsoft Access](https://www.microsoft.com/en-ca/microsoft-365/access)
- [MySQL](https://www.mysql.com/)
- [Oracle DB](https://www.oracle.com/ca-en/database/)
- [PostgreSQL](https://www.postgresql.org/)
- [SQLite](https://www.sqlite.org/index.html)

## Further Resources

- Ahuja & Vasudevan (2022) [“Databases and SQL for Data Science with Python”](https://www.coursera.org/learn/sql-data-science)
- Codecademy (2022) [“What is a Relational Database Management System?”](https://www.codecademy.com/articles/what-is-rdbms-sql)
- [Relational Database (Wikipedia)](https://en.wikipedia.org/wiki/Relational_database)
