---
id: controlled-vocabulary
title: Controlled Vocabulary
hoverText: A standardized and organized arrangement of words and phrases, which provides a consistent way to describe data.
---

<!-- @format -->

Controlled vocabularies are standardized and organized arrangements of words and phrases that provide a consistent way to describe data. They provide concise and consistent language, allowing easier search and retrieval.

## Examples

- [Getty Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Getty Thesaurus of Geographic Names (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn/)
- [Getty Union List of Artist Names (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/)
- [Homosaurus](https://homosaurus.org/)
- [Library of Congress Name Authority File (NAF)](http://id.loc.gov/authorities/names.html)
- [Library of Congress Subject Headings (LCSH)](http://id.loc.gov/authorities/subjects.html)
- [Nomenclature](https://www.nomenclature.info/)
- [Virtual International Authority File (VIAF)](http://viaf.org/)

## Further Resources

- [Controlled Vocabulary (Wikipedia)](https://en.wikipedia.org/wiki/Controlled_vocabulary)
- Harping (2010) [“What Are Controlled Vocabularies?”](https://www.getty.edu/research/publications/electronic_publications/intro_controlled_vocab/what.pdf)
- Library and Archives Canada (2018) [“Controlled Vocabularies”](https://www.bac-lac.gc.ca/eng/services/government-information-resources/controlled-vocabularies/Pages/controlled-vocabularies.aspx)
