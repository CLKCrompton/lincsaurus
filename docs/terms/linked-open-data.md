---
id: linked-open-data
title: Linked Open Data (LOD)
hoverText: Data that is linked and uses open sources.
---

<!-- @format -->

Linked Open Data (LOD) is data that fulfills the requirements of both %%Linked Data (LD)|linked-data%% and Open Data: it is both linked and uses open sources.

5-Star LOD describes data that achieves ideal linked-ness and open-ness:

1. ☆: Make your stuff available on the web under an open license
2. ☆☆: Make it available as structured data (e.g., Excel instead of a scan of a table)
3. ☆☆☆: Use non-proprietary formats (e.g., CSV instead of Excel)
4. ☆☆☆☆: Use %%Uniform Resource Locators (URLs)|uniform-resource-locator%% to identify things, so that people can point at your stuff
5. ☆☆☆☆☆: Link your data to other people’s data to provide context

## Examples

- [Linked Open Data Cloud](https://lod-cloud.net/)

## Further Resources

- Blaney (2017) [“Introduction to the Principles of Linked Open Data”](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- 5-Star Open Data (2015) [“5-Star Open Data”](https://5stardata.info/en/)
- Uyi Idehen (2019) [“What is the Linked Open Data Cloud, and Why is it Important?”](https://medium.com/virtuoso-blog/what-is-the-linked-open-data-cloud-and-why-is-it-important-1901a7cb7b1f)
