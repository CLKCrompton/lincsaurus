---
id: regular-expression
title: Regular Expression (Regex)
hoverText: A syntax that can be used in programming languages to find, manipulate, or replace patterns in texts.
---

<!-- @format -->

Regular Expressions (Regex) is not a programming language. Instead, it follows a syntax used in many different languages to find, manipulate, or replace patterns in texts. The word “pattern” is important in understanding Regex: Regex is a way for you to specify the patterns in data that you are looking for, so each time a certain thing occurs, such as letters existing in the same order (thus forming a specific word), it can be found. Regex can be used to clean data, validate data, specify the patterns of data that you want to scrape from a webpage, transform data, extract data, and more.

## Examples

- [RegexOne](https://regexone.com/)
- [Regular-Expressions](https://www.regular-expressions.info/)
- [RexEgg](https://www.rexegg.com/regex-quickstart.html)

## Further Resources

- Fox (2017) [“Regex Tutorial—A Quick Cheatsheet by Examples”](https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285)
- [Regular Expression (Wikipedia)](https://en.wikipedia.org/wiki/Regular_expression)
- Turner O’Hara (2021) [“Cleaning OCR’d Text with Regular Expressions”](https://programminghistorian.org/en/lessons/cleaning-ocrd-text-with-regular-expressions)
