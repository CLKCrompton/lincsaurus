---
id: dereferenceable
title: Dereferenceable
hoverText: An adjective used in relation to Uniform Resource Identifiers (URIs) that can turn from an abstract reference into something more concrete, namely a web resource.
---

<!-- @format -->

To dereference is to access a value or object located in a memory location stored in a pointer—the pointer directs you to the stored value. In the context of %%Linked Data (LD)|linked-data%%, dereferencing is used in relation to %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% and whether or not they are dereferenceable. A dereferenceable URI is one that resolves to a webpage: it can turn from an abstract reference into something more concrete, namely a web resource. If you can put a URI into the address bar of a browser and access a webpage through that address, then it is dereferenceable.

## Further Resources

- [Deference Operator (Wikipedia)](https://en.wikipedia.org/wiki/Dereference_operator)
- [Reference (Wikipedia)](<https://en.wikipedia.org/wiki/Reference_(computer_science)>)
