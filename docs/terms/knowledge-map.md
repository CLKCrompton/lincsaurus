---
id: knowledge-map
title: Knowledge Map (ResearchSpace)
hoverText: A visualization tool within the ResearchSpace environment that displays the different data entities in the triplestore and how they are connected to other data entities.
---

<!-- @format -->

[ResearchSpace](/docs/tools/researchspace) uses the term knowledge map to describe how their %%knowledge graph|knowledge-graph%% holds and visualizes %%Linked Data (LD)|linked-data%%. ResearchSpace’s knowledge map is a visualization tool to display the different data %%entities|entity%% in the %%triplestore|triplestore%% and how these are connected to other data entities using specific relations.

## Further Resources

- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://pdfs.semanticscholar.org/9bc8/63036314d24f2d9851b3dc6ae727a55b8b9e.pdf)
