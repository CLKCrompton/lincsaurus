---
id: open-data
title: Open Data
hoverText: Data that can be accessed, used, and reused by anyone, based on the idea that data should be freely available to everyone to see and make use of, without copyright restrictions.
---

<!-- @format -->

Open Data is data that can be accessed, used, and re-used by anyone. It is the idea that data should be freely available to everyone to see and make use of, without copyright restrictions.​ To be Open Data, a dataset must be published under a license that explicitly allows freedom of use, such as %%Creative Commons (CC)|creative-commons%% or [Open Data Commons Public Domain Dedication and License (PDDL)](https://opendatacommons.org/licenses/pddl/1-0/).

Data is considered “open” when it meets five requirements:

1. Available: it can be found online and downloaded
2. Accessible: it is in a modifiable format
3. Reusable: it is published under a license that permits reuse
4. Redistributable: it can be combined with other data from other sources
5. Unrestricted: anyone and everyone can use, transform, combine, and share the data, regardless of how they use it, whether it be for educational, non-commercial, or commercial purposes

## Examples

- [ArcGIS Hub](https://hub.arcgis.com/search)
- [Data Portals](https://dataportals.org/)
- [Government of Canada Open Data Portal](https://open.canada.ca/en/open-data)
- [Global Open Data Index](https://index.okfn.org/)
- [IEEE DataPort](https://ieee-dataport.org/datasets?t%5B%5D=open_access)

## Further Resources

- Open Data Charter (2022) [“Who We Are”](https://opendatacharter.net/who-we-are/)
- [Open Data (Wikipedia)](https://en.wikipedia.org/wiki/Open_data)
- Open Knowledge Foundation (2022) [“What is Open?”](https://okfn.org/opendata/)
