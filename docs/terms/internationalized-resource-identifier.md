---
id: internationalized-resource-identifier
title: Internationalized Resource Identifier (IRI)
hoverText: An identifier that builds upon the Universal Resource Identifier (URI) protocol by expanding the set of permitted characters to include most of the Universal Character Set.
---

<!-- @format -->

Internationalized Resource Identifiers, or IRIs, expand on the Universal Resource Identifier protocol by expanding the set of permitted characters to include most of the Universal Character Set. By internationalizing the character set available to create %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%%, IRI further internationalized the Internet by allowing people to identify web resources in their own languages.

## Further Resources

- Dürst & Suignard (2005) [“Internationalized Resource Identifiers (IRIs)”](https://www.researchgate.net/publication/200034559_Internationalized_Resource_Identifiers_IRIs)
- [Internationalized Resource Identifier (Wikipedia)](https://en.wikipedia.org/wiki/Internationalized_Resource_Identifier)
- W3C (2005) [“World Wide Web Consortium Supports the IETF URI Standard and IRI Proposed Standard”](https://www.w3.org/2004/11/uri-iri-pressrelease)
