---
id: typing
title: Typing
hoverText: To use a standardized path to link any entity to an external vocabulary, thesauri, or ontology within CIDOC CRM.
---

<!-- @format -->

Within %%CIDOC CRM|cidoc-crm%%, typing an %%entity|entity%% describes using a standardized path to link any entity to an external %%vocabulary|vocabulary%% source, %%thesauri|thesaurus%%, or %%ontology|ontology%% by linking a E1_CRM_Entity to an E55_Type using the %%property|property%% P2_has_type. Most ontologies are intended to focus on high-level entities (classes) and relationships (properties) needed to describe data infrastructure, and do not specialize beyond this immediate purpose. Consequently, a class assigned by the entity type might not be sufficiently specific to describe the entity for end users. By assigning a type through linking to domain–specific vocabulary sources—or a local vocabulary designed for a specific dataset—sufficient granularity is introduced into the dataset to ensure usability for users.

## Examples

- University of Saskatchewan Art Gallery: The following %%TTL|turtle%% snippet refers to an artwork. It is given a type using crm:P2_has_type, which specifies that this entity is a photograph, a work of art, and a colour photograph, using the external [Getty Art and Architecture Thesaurus](https://www.getty.edu/research/tools/vocabularies/aat/).

```turtle
<https://saskcollections.org/kenderdine/Detail/objects/4201> a crm:E22_Human-Made_Object ;
        rdfs:label "Gyhldeptis, Spirit of the Cedar"@en ;
        crm:P2_has_type <http://vocab.getty.edu/aat/300046300> ,
   <http://vocab.getty.edu/aat/300133025> ,
    <http://vocab.getty.edu/aat/300128359> .
```

## Further Resources

- CIDOC CRM (2021) _[Volume A: Definition of the CIDOC Conceptual Reference Model, 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
