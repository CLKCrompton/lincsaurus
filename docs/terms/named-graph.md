---
id: named-graph
title: Named Graph
hoverText: An extension of the Resource Description Framework (RDF) data model in which an RDF graph is identified using a Uniform Resource Identifier (URI), thus allowing for the publishing and presentation of metadata about that graph as a whole.
---

<!-- @format -->

Named graphs are an extension of the %%Resource Description Framework (RDF)|resource-description-framework%% data model in which an RDF graph is identified using a %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%, thus allowing for the publishing and presentation of %%metadata|metadata%% about that graph as a whole. The concept behind named graphs is that having multiple RDF graphs in a single repository and naming them with URIs provides useful additional functionality. Named graphs can be represented as the fourth part of a %%quad|quad%%, with `<graphname>` being the URI of the named graph. This process allows for tracking the %%provenance|provenance%% and source of the RDF data and supports versioning by providing a way to describe things such as information about the creation and modification of the %%triples|triple%% in the named graph. A named graph is sometimes referred to as a _quad store_.

## Further Resources

- Dodds & Davis (2012) [“Named Graphs”](https://patterns.dataincubator.org/book/named-graphs.html)
- [Named Graph (Wikipedia)](https://en.wikipedia.org/wiki/Named_graph)
- W3C (2010) [“RDF Graph Literals and Named Graphs”](https://www.w3.org/2009/07/NamedGraph.html#named-graphs)
