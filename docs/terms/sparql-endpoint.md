---
id: sparql-endpoint
title: SPARQL Endpoint
hoverText: A location on the internet that is identified by a Uniform Resource Locator (URL) and is capable of receiving and processing SPARQL queries, allowing users to access a collection of triples.
---

<!-- @format -->

A SPARQL Endpoint is a location on the internet—more formally described as a Point of Presence on an HTTP network—that is identified by a %%Uniform Resource Locator (URL)|uniform-resource-locator%% and is capable of receiving and processing %%SPARQL queries|sparql-protocol-and-rdf-query-language%%. A user can access the collection of %%triples|triple%% that you have in a %%triplestore|triplestore%% through a SPARQL Endpoint: this is the doorway that connects people to your data. A SERVICE clause in the body of a SPARQL query can also be used to call on a remote SPARQL Endpoint.

## Examples

- [CWRC SPARQL Endpoint](https://yasgui.lincsproject.ca/#)
- [Wikidata List of SPARQL Endpoints](https://www.wikidata.org/wiki/Wikidata:Lists/SPARQL_endpoints)
- [W3C List of SPARQL Endpoints](https://www.w3.org/wiki/SparqlEndpoints)

## Further Resources

- [SPARQL (Wikipedia)](https://en.wikipedia.org/wiki/SPARQL)
- Uyi Idehen (2018) [“What is a SPARQL Endpoint, and Why is it Important?”](https://medium.com/virtuoso-blog/what-is-a-sparql-endpoint-and-why-is-it-important-b3c9e6a20a8b)
