---
id: authority-record
title: Authority Record
hoverText: A stable, persistent Uniform Resource Identifier (URI) for a concept in the Linked Data (LD) ecosystem.
---

<!-- @format -->

An authority record is a stable, persistent %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% for a concept in the %%Linked Data (LD)|linked-data%% ecosystem. Authority records are usually aggregated in an %%authority file|authority-file%%.
