---
id: structured-query-language
title: Structured Query Language (SQL)
hoverText: A query language for relational databases that allows users to specify what data to return, what tables to look in, what relations to follow, and how to order the data that meets these set conditions.
---

<!-- @format -->

Structured Query Language (SQL) is the query language for %%relational databases|relational-database%%. To create a SQL query the user must specify what data to return, what tables to look in, what relations to follow (how to connect tables together, called JOINs), and—optionally—how to order the data that meets all of these set conditions.

## Examples

- The following query tells the database that you want two columns (_displayName_ and _title_), with one of those columns consisting of data from the _artist_ table (containing information about artists) and the other consisting of data from the _object_ table (containing information about art objects). To get data from both tables, you need to join them on a common field (_artistID_) that appears on both tables. You can imagine that an artist will have a unique _artistID_ and that _artistID_ will also appear on the same row on the _object_ table for each art object that is known to have been made by that artist. While it is a unique field for the _artist_ table, it is not for the _object_ table, since you can have multiple objects in the database by the same artist. This query tells the database to return all rows where (1) there is a match between these columns, (2) where the _artist.displayName_ field has the word “Rembrandt” somewhere in it, and (3) where the _object.dateMade_ field contains a date after 1 January 1660. Finally, this query tells the database to return all of these rows to us in order of the _objectID_.

  ```sql
  SELECT artist.displayName, object.title
  FROM artist
  JOIN object ON artist.artistID = object.artistID
  WHERE artist.displayName LIKE ‘%Rembrandt%’ AND object.dateMade > ‘1/01/1660’
  ORDER BY object.objectID
  ```

## Further Resources

- Heller (2019) [“What is SQL? The Lingua Franca of Data Analysis”](https://www.infoworld.com/article/3219795/what-is-sql-the-first-language-of-data-analysis.html)
- [SQL (Wikipedia)](https://en.wikipedia.org/wiki/SQL)
- W3Schools (2022) [“SQL Tutorial”](https://www.w3schools.com/sql/)
