---
id: uniform-resource-identifier
title: Uniform Resource Identifier (URI)
hoverText: A reliable and usable way to identify a unique entity so multiple datasets from various sources can communicate that they are all referring to the same thing.
---

<!-- @format -->

A Uniform Resource Identifier (URI) is a way to represent a unique %%entity|entity%% in a way that is reliable and usable by others. By naming an entity not by a local name but through reference to its authority URI, multiple datasets from various sources can communicate that they are all referring to the same thing. The easiest way to explain a URI is through an example. Take the URI example for Rembrandt: <http://vocab.getty.edu/page/ulan/500011051>. The most granular part of this URI is the number (500011051), also called the path. Without context, this number does not uniquely identify the Rembrandt that you are trying to describe. The full URI provides that necessary context: it is clear that you are using the Getty vocabulary Union List of Artist Names as our %%authorities|authority-file%% (vocab.getty.edu/page/ulan/), and it is in this context that the number, and the full URI, becomes a unique way to identify something. In addition to the path, URIs often have a fragment, which is an identifier that follows a hash symbol (#) placed at the end of the path in the URI, to point to a specific part of a document.

While this example looks like a %%Uniform Resource Locator (URL)|uniform-resource-locator%%, a URI does not need to look like this: it just needs to be reliably unique. Furthermore, while URI formats may be the same as a URL, they do not need to link to a web page or have a network location⁠—all URLs are URIs but not all URIs are URLs because a URI can describe anything at all, but a URL only describes the location of something on the web. In practice, however, not all URLs serve well as URIs for the purposes of %%Linked Open Data (LOD)|linked-open-data%%, since some are more persistent and authoritative than others. For instance, using the URL for a person’s [LinkedIn](https://www.linkedin.com/) profile is not as desirable as using on [ORCID identifier](https://orcid.org/), or using a link to a downloadable PDF of an article from a faculty member’s departmental web page is not good practice compared to using a %%Digital Object Identifier (DOI)|digital-object-identifier%% for the same article. If a URI does resolve at a webpage, this means it is %%dereferenceable|dereferenceable%%.

A URI should not change, and URIs should be designed with three things in mind: simplicity, stability, and manageability.

## Examples

- [The Getty Art & Architecture Thesaurus Online](https://www.getty.edu/research/tools/vocabularies/aat/): [“Birth Names URI”](http://vocab.getty.edu/aat/300404681)
- [Library of Congress Genre/Form Terms](http://id.loc.gov/authorities/genreForms.html): [“Genre Form URI”](http://id.loc.gov/authorities/genreForms/gf2011026387)
- [Homosaurus](https://homosaurus.org/): [“Sexual Identity URI”](http://homosaurus.org/terms/sexualIdentity)

## Further Resources

- Blaney (2017) [“Introduction to the Principles of Linked Open Data”](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- Digital Guide IONOS (2020) [“URI: The Uniform Resource Identifier Explained”](https://www.ionos.ca/digitalguide/websites/web-development/uniform-resource-identifier-uri/)
- [Uniform Resource Identifier (Wikipedia)](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)
