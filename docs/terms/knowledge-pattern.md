---
id: knowledge-pattern
title: Knowledge Pattern (ResearchSpace)
hoverText: Predefined graph paths that abstract real-world activities using classes and properties within the ResearchSpace environment.
---

<!-- @format -->

Within the [ResearchSpace](/docs/tools/researchspace) environment, knowledge patterns are predefined graph paths that abstract real-world activities using classes and %%properties|property%%. Knowledge patterns are created by subject experts and are intended to mirror narrative conventions of the domain in which the subject expert works. While semantically complete on their own, knowledge patterns can be combined with others to further enrich a %%knowledge graph|knowledge-graph%%.

A sample knowledge pattern or abstraction might be %%mapping|mapping%% how an artist is attributed as a maker of an artwork. A knowledge pattern might state that an artwork is created in a production event, with the artist linked to the event rather than directly to the artwork itself. The knowledge pattern is then utilized across a dataset in each instance where an artist is cited as the maker of a work to ensure consistency across the knowledge graph.

## Further Resources

- [Abstraction (Wikipedia)](<https://en.wikipedia.org/wiki/Abstraction_(computer_science)>)
- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://link.springer.com/chapter/10.1007/978-3-030-00668-6_20)
- Oldman, Tanase, & Satschi (2019) [“The Problem of Distance in Digital Art History: A ResearchSpace Case Study on Sequencing Hokusai Print Impressions to Form a Human Curated Network of Knowledge”](https://www.zora.uzh.ch/id/eprint/197623/1/Oldman%2C_Dominic%2C_Diana_Tanase%2C_and_Stephanie_Santschi._2019._The_Problem_of_Distance_in_Digital_Art_History.pdf)
