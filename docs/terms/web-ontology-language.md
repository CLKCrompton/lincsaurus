---
id: web-ontology-language
title: Web Ontology Language (OWL)
hoverText: A knowledge representation language for ontologies that explicitly represents the meaning of terms in vocabularies and the relationships between those terms, as well as between groups of terms.
---

<!-- @format -->

Web Ontology Language (OWL) is a knowledge representation language for %%ontologies|ontology%%. Designed for authoring ontologies, OWL can be used to explicitly represent the meaning of terms in %%vocabularies|vocabulary%% and the relationships between those terms as well as groups of terms. OWL addresses the limitations of %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%⁠—namely that it is unable to localize %%range|range%% and %%domain|domain%% constraints, define cardinality constraints, or represent inverse or symmetrical %%properties|property%%⁠—and as such is more capable of expressing meaning and semantics. OWL is capable of addressing these issues, and of supporting %%inferencing|inferencing%% across an ontology, and thus goes beyond RDFS in its ability to represent machine interpretable content on the Web.

There are three variants of OWL, with different levels of expressiveness: OWL Lite, OWL DL, and OWL Full. Because their relationship lies along degrees of complexity, every (legal) OWL Lite ontology is also an OWL DL ontology, which is also an OWL Full ontology. OWL Lite provides a simplified classification hierarchy and basic constraints for users hoping to quickly migrate simple datasets such as %%thesauri|thesaurus%% and other %%taxonomies|taxonomy%%. It is a less formally complex ontology than OWL DL. OWL DL or OWL Description Logic is a constrained version of OWL Full which allows for the use of powerful reasoning systems that rely on Description Logic in order to function. OWL Full represents the complete OWL language. It allows the total adoption and adaptation of the %%Resource Description Framework (RDF)|resource-description-framework%%, and so it is preferred for those who want to use the expressivity of OWL along with the flexibility and metamodelling strengths of RDF. However, its use renders it less usable for reasoning systems than OWL Lite or OWL DL.

## Further Resources

- [Web Ontology Language (Wikipedia)](https://en.wikipedia.org/wiki/Web_Ontology_Language)
- W3C (2004) [“OWL Web Ontology Language Overview”](https://www.w3.org/TR/owl-features/)
- W3C (2004) [“OWL Web Ontology Language Reference”](https://www.w3.org/TR/owl-ref/)
- W3C (2013) [“OWL”](https://www.w3.org/OWL/)
