---
id: range
title: Range
hoverText: One of two entities in a triple, representing the object in a subject-predicate-object relationship.
---

<!-- @format -->

The range is one of two %%entities|entity%% in a %%triple|triple%%, representing the object in a subject-predicate-object relationship. In %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%, the scope (rdfs:domain) and the set of allowable values (rdfs:range) are defined for a given %%property|property%%. The range represents a value for the subject (or %%domain|domain%%), with the property connecting domain and range defining what that relationship is. A simple example might be that a dog is a type of corgi, with corgi representing the range of this “type” property.

In %%CIDOC CRM|cidoc-crm%%, property names are meant to be read moving from domain to range, but ultimately it is possible to read and construct triples from range to domain using an inverse property (given in parentheticals, or constructed using an “i” to indicate inverse). Shifting from domain-predicate-range to range-predicate-domain is analogous to shifting from an active to a passive voice in a sentence construction.

## Examples

- In the following %%TTL|turtle%% snippet, a title, represented by a id.lincsproject.ca %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%, is the domain in the predicate relationship (P102_has_title), meaning that it is the object of that triple.

```turtle
<https://saskcollections.org/kenderdine/Detail/objects/2840>
        a crm:E22_Man-Made_Object ;
        rdfs:label "LIII Frederico Elodi"@en ;
        crm:P102_has_title <http://id.lincsproject.ca/FOuyTD5XkxE> ;
   crm:P108i_was_produced_by
  <http://id.lincsproject.ca/e8g2kwVOiQE> .
```

## Further Resources

- CIDOC CRM (2021) _[Volume A: Definition of the CIDOC Conceptual Reference Model, 7.1.1.](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
- Mitchell (2013) [“Chapter Two: Building Blocks of Linked Open Data in Libraries”](https://journals.ala.org/index.php/ltr/article/view/4692/5585)
- [Range of a Function (Wikipedia)](https://en.wikipedia.org/wiki/Range_of_a_function)
