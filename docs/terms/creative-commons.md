---
id: creative-commons
title: Creative Commons (CC)
hoverText: A nonprofit organization that provides free licences so people can grant copyright permission to their work in a standardized way.
---

<!-- @format -->

Creative Commons (CC) is a nonprofit organization that provides free licences so people can grant copyright permission to their work in a standardized way.

All CC licences are a combination of the following permissions:

- BY: Credit must be given to the creator
- SA: Adaptations must be shared under the same terms
- NC: Only noncommercial uses of the work are permitted
- ND: No derivatives or adaptations of the work are permitted

There are six different CC licences:

- CC BY
- CC BY-SA
- CC BY-NC
- CC BY-NC-SA
- CC BY-ND
- CC BY-NC-ND

Creative Commons also provides the option of CC0, a “no rights reserved” alternative to using a license.

## Further Resources

- [Creative Commons (Wikipedia)](https://en.wikipedia.org/wiki/Creative_Commons)
- Creative Commons (2022) [“About CC Licenses”](https://creativecommons.org/about/cclicenses/)
