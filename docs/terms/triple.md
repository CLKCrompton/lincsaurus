---
id: triple
title: Triple
hoverText: A statement in the form of subject-predicate-object that follows the Resource Description Framework (RDF).
---

<!-- @format -->

A triple is a statement in the form of subject-predicate-object. These assertions are made using the %%Resource Description Framework (RDF)|resource-description-framework%% and are what make up the %%Semantic Web|semantic-web%%.
