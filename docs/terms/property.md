---
id: property
title: Property
hoverText: A specified relationship between two classes or entities, such as the predicate in a triple (subject-predicate-object).
---

<!-- @format -->

A property defines a specified relationship between two classes or %%entities|entity%%. In the `<subject><predicate><object>` statements (%%triples|triple%%) that comprise the %%Semantic Web|semantic-web%%, the property is the predicate or “verb” in the sentence and is defined with reference to both the subject (%%domain|domain%%) and object (%%range|range%%) of the %%triple|triple%%.

## Examples

- The following example shows the property, or predicate, connecting two entities or classes in a triple statement.

![alt=""](/img/documentation/glossary-property-example-(c-LINCS).jpg)
