---
id: knowledge-graph
title: Knowledge Graph
hoverText: A representation of a set of linked triples that illustrates the relationships between them.
---

<!-- @format -->

A knowledge graph represents a finite set of linked %%triples|triple%% and illustrates the relationships between them. The information is stored in a %%graph database|graph-database%% and visualized as a graph structure. The %%entities|entity%% and relationships that form the linked triples must be created using formal semantics, contribute to one another, and represent diverse data connected and described by semantic %%metadata|metadata%%. A knowledge graph is similar in structure to a %%property graph|property-graph%%, but it has formalized schemas and other more sophisticated structures that extend its utility across different platforms and with diverse datasets.

## Examples

- WC3 (2014) _[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)_: The following diagram depicts an informal graph of simple triples.

![Knowledge graph showing the relationship of an example person to The Mona Lisa and related metadata in triples.](/img/documentation/glossary-knowledge-graph-example-(fair-dealing).png)

_Permitted with [W3C license](https://www.w3.org/Consortium/Legal/2015/doc-license)._

## Further Resources

- British Museum (2021) [“ResearchSpace: What is a Knowledge Graph?”](https://researchspace.org/knowledge-graph-and-patterns/)
- [Knowledge Graph (Wikipedia)](https://en.wikipedia.org/wiki/Knowledge_graph)
- Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://link.springer.com/content/pdf/10.1007/978-3-030-00668-6.pdf)
- Ontotext (2022) [“What is a Knowledge Graph?”](https://www.ontotext.com/knowledgehub/fundamentals/what-is-a-knowledge-graph/)
- WC3 (2014) _[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)_
