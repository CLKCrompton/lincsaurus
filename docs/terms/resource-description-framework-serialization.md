---
id: resource-description-framework-serialization
title: Resource Description Framework (RDF) Serialization
hoverText: A syntax that can be used to write triples, including Turtle (TTL), XML (XML-RDF), and JSON.
---

<!-- @format -->

The %%Resource Description Framework (RDF)|resource-description-framework%% is a standard for data exchange and the standard format for %%Linked Data (LD)|linked-data%%. RDF represents information in a series of three-part “statements” called a %%triple|triple%% that comprise a subject, predicate, and an object in the form: `<subject><predicate><object>`. In this way, RDF describes data by defining relationships between data objects. RDF can be serialized in different ways, with common formats including %%Turtle (TTL)|turtle%%, %%XML|xml%% (XML-RDF), and JSON. The LINCS %%triplestore|triplestore%% ingests data using the TTL format.

Turtle (TTL), or Terse RDF Triple Language, is a highly human readable RDF serialization format. The TTL format defines prefixes at the beginning of the file and encourages blocking of triples with the same subject, allowing the reader to interpret the data much more effectively. It is more costly to parse as it is a relatively verbose system. However, it remains one of the simpler serialization formats to edit by hand.

XML-RDF is the oldest and most difficult to parse RDF serialization format. It combines an older, hierarchical tree-like format with the triple-based graph format required for LD, meaning that it does not clearly reflect the triple model and can cause confusion in readers.

JSON-LD is a LD version of %%JSON|json%%, an open standard file format and data interchange format. JSON is the most popular way to serialize data in web applications and consequently its adoption for LD benefits from JSON’s familiarity to many programmers. However, JSON-LD is difficult and costly to parse, meaning that its feasibility in more complex LD projects is limited.

## Examples

- Meindertma (2019) [“What’s the Best RDF Serialization Format?"](https://ontola.io/blog/rdf-serialization-formats/): The following RDF snippets state that Tim Berners Lee was born on 8 June 1955 in London, England.

TTL:

```turtle
@prefix tim: <https://www.w3.org/People/Berners-Lee/>.
@prefix schema: <http://schema.org/>.
@prefix dbpedia: <http://dbpedia.org/resource/>.

<tim> schema:birthDate "1955-06-08"^^<http://www.w3.org/2001/XMLSchema#date>.
<tim> schema:birthPlace <dbpedia:London>.
```

RDF/XML:

```xml
<?xml version="1.0"?>
<rdf:RDF
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:schema="http://schema.org/">
<rdf:Description rdf:about="https://www.w3.org/People/Berners-Lee/">
  <schema:birthDate>1966-06-08</schema:birthDate>
  <schema:birthPlace rdf:resource="http://dbpedia.org/resource/London"/>
</rdf:Description>
</rdf:RDF>
```

JSON-LD:

```json
{
  "@context": {
    "dbpedia": "http://dbpedia.org/resource/",
    "schema": "http://schema.org/"
  },
  "@id": "https://www.w3.org/People/Berners-Lee/",
  "schema:birthDate": "1955-06-08",
  "schema:birthPlace": {
    "@id": "dbpedia:London"
  }
}
```

## Further Resources

- Digital Scholarship Resources (2019) [“Linked Data Basics: RDF Serializations and Triplestores”](https://heardlibrary.github.io/digital-scholarship/lod/serialization/)
- Meindertma (2019) [“What’s the Best RDF Serialization Format?”](https://ontola.io/blog/rdf-serialization-formats/)
- [RDF Serialization Formats (Wikipedia)](https://en.wikipedia.org/wiki/Resource_Description_Framework#Serialization_formats)
