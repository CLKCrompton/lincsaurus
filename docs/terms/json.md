---
id: json
title: JSON
hoverText: A human- and machine-readable data interchange format.
---

<!-- @format -->

JSON (JavaScript Object Notation) is a human- and machine-readable data interchange format that is often used to send data between computers.

JSON represents things as object literals. Each object literal is surrounded by curly braces (`{}`). Inside the object literal are key-value pairs. Keys and values are separated by a colon and key-value pairs are separated by a comma.

All keys are strings. All values need to be a valid JSON data type: a string, number, object, array, boolean, or null.

## Examples

- The following example shows a basic JSON string where the object literal has three keys-value pairs:

```json

{"name":"Margaret Laurence", "birth":1926, "nationality":Canadian}

```

## Further Resources

- [JSON](https://www.json.org/json-en.html)
- [JSON (Wikipedia)](https://en.wikipedia.org/wiki/JSON)
- Mozilla (2023) [“Working with JSON”](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON)
- W3Schools (2023) [“JSON - Introduction”](https://www.w3schools.com/js/js_json_intro.asp)
- W3Schools (2023) [“JSON Object Literals”](https://www.w3schools.com/js/js_json_objects.asp)
