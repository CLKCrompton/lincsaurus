---
id: node
title: Node
hoverText: A representation of an entity or instance to be tracked in a graph database or triplestore, such as a person, object, organization, or event.
---

<!-- @format -->

A node represents an entity or instance to be tracked in a %%graph database|graph-database%% or %%triplestore|triplestore%%, such as a person, object, or organization. Nodes are connected by %%edges|edge%% and information about a node is called a property.

## Examples

- The following image shows a %%Resource Description Framework (RDF)|resource-description-framework%% graph highlighting the edges that connect three nodes.

![alt=""](/img/documentation/glossary-node-example-(c-LINCS).jpg)
