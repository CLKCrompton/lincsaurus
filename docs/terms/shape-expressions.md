---
id: shape-expressions
title: Shape Expressions (ShEx)
hoverText: A language for validating and describing Resource Description Framework (RDF) graph structures.
---

<!-- @format -->

Shape Expressions is a language for validating and describing %%Resource Description Framework (RDF)|resource-description-framework%% graph structures. ShEx descriptions identify predicates and their associated cardinalities and datatypes. ShEx shapes can be used to communicate data structures associated with some process or interface, generate or validate data, or drive user interfaces.

## Examples

- [ShEX (Wikipedia)](https://en.wikipedia.org/wiki/ShEx): The following example states that Person nodes must have one property schema:name with a string value, and zero or more properties schema:knows whose value must be a Person.

  ```
      PREFIX: <http://example.org/>
      PREFIX schema: <http://schema.org/>
      :Person {
        schema:name  xsd:string   ;
        schema:knows @:Person   * ;
      }
  ```

## Further Resources

- [ShEX (Wikipedia)](https://en.wikipedia.org/wiki/ShEx)
- W3C (2019) _[Shape Expressions (ShEx) 2.1 Primer](https://shex.io/shex-primer/)_
- W3C (2019) [“ShEx”](https://github.com/shexSpec/shex/wiki/ShEx)
