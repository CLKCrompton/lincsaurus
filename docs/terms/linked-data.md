---
id: linked-data
title: Linked Data (LD)
hoverText: Structured data that is linked with other data through the web and builds upon standard web technologies to share machine-readable data between computers.
---

<!-- @format -->

Linked Data (LD) is structured data which is linked with other data through the web. It builds upon standard web technologies, such as the %%Resource Description Framework (RDF)|resource-description-framework%% and %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%%, and uses them to share machine-readable data between computers.

LD has four requirements:

1. Uses URIs as names for things​
2. Uses HTTP URIs so people can look up these names​
3. Provides useful information, using open standards such as RDF and %%SPARQL|sparql-protocol-and-rdf-query-language%%, when people look up a URI
4. Includes links to other URIs so people can discover more things​

## Examples

- [Linked Open Data Cloud](https://lod-cloud.net/)

## Further Resources

- [Linked Data (Wikipedia)](https://en.wikipedia.org/wiki/Linked_data)
- W3C (2015) [“Linked Data”](https://www.w3.org/standards/semanticweb/data)
- W3C (2016) [“Linked Data”](https://www.w3.org/wiki/LinkedData)
