---
id: resource-description-framework
title: Resource Description Framework (RDF)
hoverText: A standard for Linked Data (LD) that represents information in a series of three-part “statements” called a triple, which comprises a subject, predicate, and an object in the form subject-predicate-object.
---

<!-- @format -->

The Resource Description Framework (RDF) is a standard for data exchange and the format for %%Linked Data (LD)|linked-data%%. RDF represents information in a series of three-part “statements” called a %%triple|triple%% that comprise a subject, predicate, and an object in the form: `<subject><predicate><object>`. In this way, RDF describes data by defining relationships between data objects. RDF also allows for the use of %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% so that data can be uniquely identified and linked together: multiple triples, and triples from multiple sources, can all point to the same URI to encode that they are all referring to the same %%entity|entity%% or concept. This shared use of %%authorities|authority-file%% is part of what makes the “linked” part of LD possible. RDF data (triples) are stored in a %%triplestore|triplestore%% and can be queried using %%SPARQL|sparql-protocol-and-rdf-query-language%%.

## Examples

- Lincoln (2015) [“Using SPARQL to access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL): The following example shows how you would represent the concept that Rembrandt van Rijn created _The Nightwatch_, following RDF.

  `<Rembrandt van Rijn><created><The Nightwatch> .`

- Lincoln (2015) [“Using SPARQL to access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL): The following example shows how you would represent the concept that Rembrandt van Rijn created _The Nightwatch_, following RDF using URIs.

  ```turtle
  <http://vocab.getty.edu/page/ulan/500011051> <http://purl.org/dc/terms/creator>
  <http://data.rijksmuseum.nl/item/8909812347> .
  ```

## Further Resources

- Fullstack Academy (2017) [“RDF Tutorial—An Introduction to the Resource Description Framework”](https://www.youtube.com/watch?v=zeYfT1cNKQg) [Video]
- [Resource Description Framework (Wikipedia)](https://en.wikipedia.org/wiki/Resource_Description_Framework)
- W3C (2014) _[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)_
