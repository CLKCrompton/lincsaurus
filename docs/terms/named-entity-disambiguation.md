---
id: named-entity-disambiguation
title: Named Entity Disambiguation (NED)
hoverText: To assign a unique identity to an entity in a text to differentiate it from another entity that shares the same name.
---

<!-- @format -->

Named Entity Disambiguation (NED) involves assigning a unique identity to %%entities|entity%% mentioned in text. It is closely related to %%Named Entity Recognition (NER)|named-entity-recognition%%, which involves the process of identifying and categorizing entities mentioned in text, but they produce different outcomes. While NER is interested in what category an entity belongs to (e.g., Regina is a City), disambiguation determines that the instance of “Regina” in the text is indeed a reference to the capital city of Saskatchewan. The instance is linked to an %%authority record|authority-record%% for that entity and not to a different city, the Roman goddess, or a person named Regina.

## Further Resources

- [Entity Linking (Wikipedia)](https://en.wikipedia.org/wiki/Entity_linking)
