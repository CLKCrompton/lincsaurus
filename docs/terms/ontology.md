---
id: ontology
title: Ontology
hoverText: An abstract, machine-readable model of a phenomenon that captures and structures knowledge of entities, properties, and relationships in a domain so a conceptualization can be shared with and reused by others.
---

<!-- @format -->

“An ontology is a formal, explicit specification of a shared conceptualization” ([Studer et al., 1998](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.110.8406&rep=rep1&type=pdf)).

- _Formal_: machine readable
- _Explicit_: the type of concepts used, and the constraints on their use, are explicitly defined
- _Shared_: captures knowledge in a way that is accepted by a group
- _Conceptualization_: an abstract model of some phenomenon in the world by having identified the relevant concepts of that phenomenon

Philosophically, ontology is the study of being: of what pieces fit together to make up the world and how those parts interrelate. In the domain of computer and information sciences, and in relation to %%Linked Data (LD)|linked-data%%, it is the formalization of this concept in a machine-readable way: a model of a domain—the parts that make it up and how they fit together—written in a way that can be understood by computers. An ontology formally captures and structures knowledge of the %%entities|entity%%, %%properties|property%%, and relationships that make up a domain so that this conceptualization can be shared and reused by others. Ontologies are an important element to many computing fields, including object-oriented software system design, information retrieval systems, and a number of tasks in the field of artificial intelligence. Ontologies are also a key part of the %%Semantic Web|semantic-web%%, as they are used to formally define the meanings of used terminology and the relationship of those terms to other concepts and %%vocabularies|vocabulary%%.

|                   | Ontology                                                                                             | Taxonomy                                                                                      | Thesaurus                                                      | Vocabulary                                                            |
| ----------------- | ---------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- | -------------------------------------------------------------- | --------------------------------------------------------------------- |
| **Describes**     | Concepts (content) and the relationships between them (structure), including axioms and restrictions | Hierarchical relationships among concepts, and specifies the term to be used to refer to each | Hierarchical and non-hierarchical relationships among concepts | General term for a collection of concepts (words) to do with a domain |
| **Relationships** | Typed hierarchical and associative                                                                   | Basically hierarchical, but all modeled using same notation                                   | Untyped hierarchical, associative, and equivalence             | Unspecified (abstract concept)                                        |
| **Properties**    | RDFS defines relationship properties and restrictions                                                | None                                                                                          | Can be described in scope notes if required                    | Unspecified (abstract concept)                                        |
| **Structured**    | Network                                                                                              | Tree                                                                                          | Cross-branch tree                                              | Unspecified (abstract concept)                                        |

## Examples

- [BIBFRAME 2](http://id.loc.gov/ontologies/bibframe.html)
- [CIDOC CRM](http://cidoc-crm.org/)

## Further Resources

- Guarino, Oberle, & Staab (2009) [“What Is an Ontology?”](https://iaoa.org/isc2012/docs/Guarino2009_What_is_an_Ontology.pdf)
- Noy & McGuinness (2001) [“Ontology Development 101: A Guide to Creating Your First Ontology”](http://ksl.stanford.edu/people/dlm/papers/ontology-tutorial-noy-mcguinness.pdf)
- [Ontology (Wikipedia)](<https://en.wikipedia.org/wiki/Ontology_(information_science)>)
