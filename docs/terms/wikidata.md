---
id: wikidata
title: Wikidata
hoverText: The largest instance of Wikibase, which acts as a central storage repository for structured data used by Wikipedia, by its sister projects, and by anyone who wants to make use of a large amount of open general-purpose data.
---

<!-- @format -->

Wikidata is the largest instance of %%Wikibase|wikibase%%, a free knowledge base that anyone can edit. It acts as a central storage repository for structured data used by Wikipedia, by its sister projects, and by anyone else who wants to make use of a large amount of open general-purpose data. The content of Wikidata is available under a free license, and can be exported and interlinked to other open datasets on the web ([Mediawiki, 2021](https://www.mediawiki.org/wiki/Wikibase/FAQ#What_is_the_difference_between_Wikibase_and_Wikidata?)).

## Examples

- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Further Resources

- MediaWiki (2021) [“Wikibase/FAQ”](https://www.mediawiki.org/wiki/Wikibase/FAQ#:~:text=General-,What%20is%20the%20difference%20between%20Wikibase%20and%20Wikidata%3F,base%20that%20anyone%20can%20edit.)
- UCLA Library (2017) [“Semantic Web and Linked Data: Wikibase”](https://guides.library.ucla.edu/semantic-web/wikidata)
