---
id: query-variable
title: Query Variable
hoverText: A proxy for the object you are searching for when constructing a SPARQL query.
---

<!-- @format -->

A query variable is a proxy for the object(s) you are searching for when constructing a %%SPARQL query|sparql-protocol-and-rdf-query-language%%. SPARQL queries are composed of %%triples|triple%%: a subject, predicate, and object. In SPARQL queries, each component of the triple needs to be either a query variable or a %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%.

Query variables are indicated with a question mark or dollar sign followed by a word. The word you choose for a variable is arbitrary, but should be human-readable for ease of understanding, and must be used consistently within a query.

## Examples

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL): In the following query, the variable is `?painting`. This variable tells the triplestore to search for all values of `?painting` that properly complete the statement `<has medium><oil on canvas>`. `?painting` stands in for the node(s) that the database will return.

  ```sparql
  SELECT ?painting
  WHERE {
     ?painting <has medium> <oil on canvas> .
   }
  ```
