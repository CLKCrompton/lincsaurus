---
id: simple-knowledge-organization-system
title: Simple Knowledge Organization System (SKOS)
hoverText: A standard that provides a way to represent thesauri, taxonomies, and controlled vocabularies following the Resource Description Framework (RDF).
---

<!-- @format -->

The Simple Knowledge Organization System (SKOS) is a standard that provides a way to represent %%thesauri|thesaurus%%, %%taxonomies|taxonomy%%, and %%controlled vocabularies|controlled-vocabulary%% following the %%Resource Description Framework (RDF)|resource-description-framework%%.

SKOS allows LINCS vocabularies to be connected to other vocabularies and RDF datasets. All vocabulary terms that are declared in SKOS at LINCS are defined as instances of the class E55 type in %%CIDOC CRM|cidoc-crm%% and are given their own %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%.

## Further Resources

- [Simple Knowledge Organization System (Wikipedia)](https://en.wikipedia.org/wiki/Simple_Knowledge_Organization_System)
- W3C (2012) [“Introduction to SKOS”](https://www.w3.org/2004/02/skos/)
- Zaytseva & Durco (2020) [“Controlled Vocabularies and SKOS”](https://campus.dariah.eu/resource/posts/controlled-vocabularies-and-skos)
