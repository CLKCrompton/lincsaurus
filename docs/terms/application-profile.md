---
id: application-profile
title: Application Profile
hoverText: A schema composed of metadata elements drawn from one or more namespaces, as well as policies and guidelines related to their use, prepared for a particular application.
---

<!-- @format -->

An application profile is a schema composed of %%metadata|metadata%% elements drawn from one or more %%namespaces|namespace%%, as well as policies and guidelines related to their use, prepared for a particular application. It reflects a set of recorded decisions about a shared data target for a given community.

For %%Linked Open Data (LOD)|linked-open-data%% projects, an application profile should provide details about the %%ontology|ontology%% (classes and %%properties|property%%), %%vocabularies|vocabulary%%, and data formats. They should document how data is modelled, declare what %%entities|entity%% and %%properties|property%% will be used, and declare the data types for string values, along with scope notes for consistent use of fields and properties. An application profile should clarify the expectations of the data being ingested, processed, and managed, document models and standards, and note where implementation diverges from community standards ([UCLA Library](https://guides.library.ucla.edu/semantic-web/bestpractices)).

## Examples

- [CHIN/DOPHEDA Semantic Paths Specification](https://chin-rcip.github.io/collections-model/en/semantic-paths-specification/current/introduction)
- [Linked Art Documentation](https://linked.art/model/)
- [SARI Documentation](https://docs.swissartresearch.net)

## Further Resources

- [Application Profile (Wikipedia)](https://en.wikipedia.org/wiki/Application_profile)
- Dublin Core Metadata Innovation (2009) [“Guidelines for Dublin Core Application Profiles”](https://www.dublincore.org/specifications/dublin-core/profile-guidelines/)
- Sanderson (2021) [“The Illusion of Grandeur: Trust and Belief in Cultural Heritage Linked Open Data”](https://www.youtube.com/watch?v=o4TzXMz7GBA) [Video]
- Thompson & Sanderson (2021) [“LUX: Illuminating the Collections of Yale’s Museums... From Prototypes to Production”](https://www.youtube.com/watch?v=C4lAJHOs1gY) [Video]
- UCLA Library (2017) [“Semantic Web and Linked Data: Best Practices and Standards”](https://guides.library.ucla.edu/semantic-web/bestpractices)
- W3C (2022) _[Profile Guidance: W3C Editor’s Draft 12 May 2022](https://w3c.github.io/dxwg/profiles/)_
