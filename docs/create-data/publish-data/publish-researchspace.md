---
sidebar_position: 9
title: "Setup in ResearchSpace"
description: "LINCS Conversion Workflows — Setup your Data in ResearchSpace"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


## Introduction

ResearchSpace is the web platform that LINCS uses to publish %%linked open data (LOD)|linked-open-data%%. Once you have finished converting your data, ResearchSpace will allow you to search, browse, visualize, and edit your new linked data which will be stored in the LINCS %%triplestore|triplestore%%. See our [ResearchSpace documentation](/docs/tools/researchspace/) for more details.

We recommend that your team browse existing projects in ResearchSpace to understand the possibilities for how you can present your converted data.

The information on this page applies to data that came out of any of the four conversion workflows because the input for ResearchSpace is LINCS RDF, which each workflow outputs.

## ResearchSpace as a Conversion Step

We use the first ingestion into ResearchSpace as a chance to verify that the conversion workflow was successful. Sometimes we go back and iterate until the data is ready for a final ingestion and publication through ResearchSpace.

This step typically takes 1-4 weeks, cycling through data review and edits with communications between the LINCS teams and the Research Team.

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Ingest Trial Data |  |  |  | ✓ |
| Customize View of Data | ✓ |  |  | ✓ |
| Explore Data and Find Errors | ✓ | ✓ | ✓ | ✓ |
| Fix Errors (GitLab) | ✓ |  | ✓ |  |


### Ingest Trial Data

After the [Validate and Enhance](/docs/create-data/publish-data/publish-validate) step of any conversion workflow, the converted data is handed over to the LINCS **Storage Team** to be uploaded to the LINCS triplestore as a trial.

### Customize View of Data

The ingested trial data is explored by the **Research Team**, **Ontology Team**, **Conversion Team**, and **Storage Team** in [ResearchSpace](/docs/tools/researchspace). 

Without changing the dataset itself, the **Storage Team** can manipulate how ResearchSpace displays the data’s entities and relationships. The **Storage Team** and **Research Team** collaborate to customize the view of the data. 

While the draft dataset is public in ResearchSpace, it is not yet published LOD at this stage, and cannot yet be used in publications.

### Find and Fix Errors (GitLab)

When someone from the **Research Team**, **Ontology Team**, **Conversion Team**, or **Storage Team** spots an error in the dataset, they can report the error via an issue in the LINCS GitLab.

At this stage, the focus is on errors that were introduced through the conversion process, not errors in the data itself. 

For structured data, the easiest way to fix these errors is by going back to the relevant conversion step, changing that step to prevent the error, and rerunning the conversion workflow. The most common errors are incorrect data in the source data file or an incorrectly implemented relationship in the 3M configuration files. 

For the TEI and Natural Language workflows, we would not change the source data or rerun conversion steps because there is typically manual work completed after those steps. We would instead change the final RDF file.

After rerunning the updated workflow, the **Storage Team** replaces the dataset in the LINCS triplestore. We continue to repeat these review and edit steps until the **Research Team** approves their data for publication.

### Finish Data Cleaning and Reconciliation

If data cleaning or entity reconciliation steps were not completed previously, the **Research Team** can continue to do that at this stage before we publish the converted data. LINCS will advise you on if this should be done in GitLab or through ResearchSpace.

Of course, you can continue editing and enhancing your data once it has been officially published.


## ResearchSpace for Published Data

Once all errors introduced during conversion have been removed, the **Research Team** decides if they are happy with the current version of the data. If so, the data can be officially ingested and published.

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Ingest Official Data |  |  |  | ✓ |
| Explore Data and Find Errors | ✓ | ✓ | ✓ | ✓ |
| Fix Errors (ResearchSpace) | ✓ |  |  |  |

### Ingest Official Data

The final version of the dataset is uploaded to the LINCS triplestore by the **Storage Team**. At this stage, the dataset is accessible via the official version of ResearchSpace and is a part of the public LINCS triplestore.

### Explore Data

You can now start exploring and editing your fully converted data in ResearchSpace and connects their data to visualization tool if desired. See our [ResearchSpace documentation](/docs/tools/researchspace/) and [Visualization tools documentation](/docs/explore-data/visualize) for details.

### Find and Fix Errors (ResearchSpace)

Once the dataset is published to ResearchSpace, it is the responsibility of the **Research Team** to make changes to the data directly in ResearchSpace. 

Changes made by the **Research Team** affect the version of the dataset that is in the LINCS triplestore, which means that the conversion workflow does not need to be repeated. The tools to fix errors are accessible through ResearchSpace so GitLab is no longer needed to be used for the dataset.

:::caution

Once the data is published in ResearchSpace, LINCS has committed to keeping the data in Research Space and maintaining the URIs. LINCS is not committed to keeping the source data or to feed back any updates that are made in ResearchSpace to previous versions of the data.

:::
