---
sidebar_position: 1
title: "Learn about Contributing"
description: "Learn What Data Is Eligible for Publication with LINCS"
---

Before you start creating %%Linked Open Data (LOD)|linked-open-data%%, you need to confirm that LOD—and, in particular, publishing with LINCS—is right for you. This section explains who LINCS works with, what kinds of data we can publish, and what support we can provide.

## Is Your Data Eligible?

Anyone can make LOD, but you can only publish data with LINCS if:

* Your dataset falls within the bounds of our Dataset Inclusion Policy (coming soon)
* You own the source dataset or have permission from the source dataset owner
* You have a long-term plan to store the source dataset in an institutional repository, a [Trustworthy Digital Repository](https://en.wikipedia.org/wiki/Trustworthy_Repositories_Audit_%26_Certification) (such as Scholars Portal), or Canada’s [Federated Research Data Repository](https://www.frdr-dfdr.ca/repo/)
* Your dataset contents are publishable under Canadian law
* If your dataset involves people, you have ethics approval from your research institution (if applicable), and you meet the criteria set out in the [Tri-Council Policy Statement: Ethical Conduct for Research Involving Humans](https://ethics.gc.ca/eng/policy-politique_tcps2-eptc2_2018.html)

When you publish with LINCS, you are making your dataset available to—and connecting it with data created by—others. LINCS publishes data under a [Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/). In some cases, we are able to work with data that is not open. We respect data sovereignty and adhere to the [CARE Principles for Indigenous Data Governance](https://www.gida-global.org/care).

Before we can prepare your data for publication, you need to sign the [Data Publication License Agreement](/docs/about-lincs/policies/data-publication-license-agreement) and complete the [Data Publication License Agreement Supplementary Form](https://docs.google.com/document/d/1dA01sGhzetsTytrhoIbrv5I4PXBkRNtXur0aSbGeHaM/edit). We recommend you review these documents before starting to create LOD. For more information, see the Data Publication License Agreement instructions (coming soon).

## Is Your Data Suitable?

The LOD you create will be shaped by what you decide to include and how you structure your data. The LINCS team can help you get the most from your data—and ensure that your converted data connects well with other data published by LINCS.

Before you start converting your data, we invite you to set up a dataset intake interview, where we will discuss your research questions and goals. You can request a dataset intake interview by filling out the Data Publishing Interest Form (coming soon).

In advance of the dataset intake interview, you will be asked to complete a Dataset Intake Questionnaire (coming soon).

## What Support Is Available?

### Learn About LOD

In order to create LOD, you need to have a basic understanding of it:

* Start with [introductory resources](/docs/get-started/) written by the LINCS team
* View our Zotero library (coming soon) for recommended readings, training modules, videos, and tools
* Attend a workshop
  * LINCS often leads workshops at the [DH@Guelph Summer Workshops](https://www.uoguelph.ca/arts/dhguelph), [DHSI](https://dhsi.org/), and [DHSITE](https://dhsite.org/)
  * Check our [events page](/docs/about-lincs/get-involved/events/) for upcoming opportunities
* [Contact us](mailto:lincs@uoguelph.ca) to enquire about additional training opportunities

### Get Expert Help

The LINCS team can work with you as you create LOD:

* Request funding to support a collaboration with LINCS in a grant application
  * Check out our grants menu (coming soon) for the types of support we are able to offer
  * [Contact us](mailto:lincs@uoguelph.ca) to find out more
* Apply for funding from LINCS
  * LINCS has modest funding to support individuals who:
    * Identify as being members of underrepresented groups
    * Are working on datasets that would be part of the Indigenous Knowledges and Resistant Epistemologies [Areas of Inquiry](/docs/about-lincs/research#areas-of-inquiry)
    * Are working on datasets that are more broadly related to underrepresented communities
  * Check out our [Data Mobilization Microgrants](/docs/about-lincs/get-involved/data-mobilization-microgrants) to learn more

### Formalize Your Involvement in LINCS

Alongside publishing your data with LINCS, you can also become a member of the LINCS community:

* Check out the [Membership Roles](/docs/about-lincs/policies/membership-roles) to find out about the different ways to get involved and [Contact us](mailto:lincs@uoguelph.ca) to register your interest
  * All members of the LINCS community are required to sign the [Project Charter](/docs/about-lincs/policies/project-charter)

## Next Steps

To get started:

* Fill out a Data Publishing Interest Form (coming soon), or
* [Contact us](mailto:lincs@uoguelph.ca)

If you are not certain that publishing data with LINCS is right for you, you are still welcome to use LINCS’s many resources and tools to work with your own data or to explore data created by others. You are welcome to contact us at any time in your data journey.
