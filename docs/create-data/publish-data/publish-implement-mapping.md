---
sidebar_position: 7
title: "Implement Conceptual Mapping"
description: "LINCS Conversion Workflows — Implement Conceptual Data Mapping"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


## Introduction

The implement conceptual mapping step is where we finally convert your data from its original structure and ontology into LINCS %%RDF|resource-description-framework%%.

## Resources Needed

For TEI and natural language data, your team will do this step using LINCS tools. Our tools use templates for common relationships so you can get output in a few minutes. Though you may need to spend some time playing around with processing your source data to get the output you want. 

For structured and semi-structured data, we still have tools to help, but our approach is customized to each dataset so the process takes longer. An experienced user could convert a dataset in a few days but we find this step ends up taking a few weeks to a few months for the average project when you consider training, implementation, and troubleshooting. For these workflows, it will be a combined effort between the LINCS **Conversion Team** and your **Research Team**.

## Setting Up Your Data

To proceed with this step, you must have a conceptual mapping developed for your specific data. Ideally this mapping will be final so that you do not need to redo this implementation step later on. With that said, it is fine to have a mapping that only covers certain relationships of interest as a starting point and then to add to that mapping and to this implementation step in phases.

It is best if you have already cleaned your data before this step. However, if your implementation is going to use code or a tool that can be rerun easily then it is fine to start on this step before you have finished data cleaning. You can rerun the implementation step when the final cleaned data is ready.


## Transforming Your Data

Whenever possible, use tools or scripts that let you easily edit and rerun this step. That way if you find errors in your source data or if have more data later on, you can rerun this step to quickly convert it.


<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Pre-Process Scripts | ✓ |  | ✓ |  |
| Convert Using 3M | ✓ | ✓ | ✓ |  |
| Convert Using Custom Scripts | ✓ |  |  |  |
| Consult and Approve |  | ✓ | ✓ |  |

#### Choosing a Tool

Every dataset in this category comes with a unique starting structure and by this point should have its own conceptual mapping. To grab each piece of information from the source data and reconnect it together as CIDOC CRM triples, LINCS prefers to use the [3M mapping tool](/docs/tools/x3ml/). 

The 3M mapping tool takes XML documents as input and, through its graphical user interface, allows users to select data from their source files and map it into custom CIDOC CRM triples. We have found that this is the easiest method to get consistently converted data. LINCS has developed [3M documentation](/docs/tools/x3ml/) to guide you through creating your first mapping file, and the **Ontology Team** and **Conversion Team** can provide support as you get started.

You may choose to use 3M if:

- your data is already in XML or is in a format that can be easily converted to XML (e.g., a spreadsheet or JSON files)
- you do not have a team member with programming experience and need a tool with a graphical user interface
- your data contains many relationships so the reliability of 3M output and its treatment of intermediate nodes will have a large benefit

Alternatively, you may choose to write custom scripts to convert your data instead of using 3M. You may choose to write custom scripts if:

- you have a team member who understands the source data, understands the conceptual mapping, and has sufficient programming experience
- your data only covers a small number of relationships so learning 3M is not worth the time investment
- your data is in a highly normalized relational database and the code needed to transform the relational data into XML would be equivalent to code needed to output triples


#### Pre-Process Scripts

3M requires your data to be input as XML. If your structured data is not in XML, you can convert it following our [Preparing Data for 3M documentation](/docs/tools/x3ml/preparing-data).

The [Preparing Data for 3M documentation](/docs/tools/x3ml/preparing-data) also gives suggestions for ways to edit your XML data to make working in 3M easier.

#### Convert Using 3M or Custom Scripts

The **Conversion Team** and **Ontology Team** use X3ML to write out the conceptual mapping and run the transformation on the data, resulting in LINCS %%RDF|resource-description-framework%%. To make sure that the output from 3M is correct, either the **Research Team** or the **Conversion Team** and **Ontology Team** transform a small sample of the data and vet the results using the built in 3M visualization tools and a manual comparison process. The full dataset is then converted. 


</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Convert Using LEAF-Writer or Custom Methods | ✓ |  |  |  |
| Consult and Approve |  | ✓ | ✓ |  |


For semi-structured data, LINCS recommends that you use [LEAF-Writer](/docs/tools/leaf-writer/), which is a web-based editor that allows you to mark up XML documents, including tagging and reconciling entities. The tool does not require any programming knowledge, but does take manual effort to tag the documents. While this can be time consuming, we have found that for unique semi-structured data a manual approach is worth the resulting quality. With LEAF-Writer, you can continue to slowly mark up your documents and re-extract the output until you are happy with it.

In the future, LINCS will provide tools to move from working in LEAF Writer and the resulting %%web annotations|web-annotation%% data to converting that into CIDOC CRM and publishing the output with LINCS. As that process is being developed, the LINCS **Conversion Team** can work with you through this step.

If LEAF-Writer is not the right fit, you can use other XML processing tools or create custom extraction scripts that execute the conceptual mapping for a given dataset. 

Development of custom scripts and work done in LEAF-Writer would both be completed by your **Research Team**, with the **Conversion Team** available to offer advice.


</TabItem>
<TabItem value="tei" label="TEI Data">

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Convert Using XTriples | ✓ |  |  |  |
| Consult and Approve |  | ✓ | ✓ |  |

For TEI data, you can use the LINCS instance of [XTriples](/docs/tools/xtriples) to select a conversion template and automatically extract CIDOC CRM triples from your TEI files. 

The LINCS XTriples templates expect your source files to conform to the templates in LEAF-Writer. If you are working with files that do not conform to these templates, please transform your TEI using the XSLTs linked from the LINCS XTriples documentation. For details on these conversion templates and XSLTs, see our [XTriples documentation](/docs/tools/xtriples/).

For additional extractions not covered by the XTriples templates, you will need to follow another workflow. If you have fairly regularized XML data then you could follow the structured data workflow, otherwise the semi-structured workflow. If there is significant natural language text contained in the TEI documents, then use the natural language data workflow to extract facts from those textual elements.

</TabItem>
<TabItem value="natural" label="Natural Language Data">

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Convert Using LINCS APIs | ✓ |  |  |  |
| Consult and Approve |  | ✓ | ✓ |  |

:::info
This step of the Natural Language Data workflow is still in progress. Check back over the next few months as we release the tools described here.
:::

#### Background
The task of extracting triples from natural language text in an automated way—without a human manually marking up a document—breaks down into the tasks of named entity recognition (NER)|named-entity-recognition%% and relation extraction (RE). Where we use a computer system to predict which words or phrases in the text represent a named entity and what relationships the text expresses between those entities.

LINCS has developed %%APIs|application-programming-interface%% to make these automated tasks accessible. These APIs take plain text as input and output either:

1. triples where the predicate—or relationship—must be from a list of allowable predicates
2. triples where the predicate can be any word or phrase from the text

The first is the fastest and most reliable way to generate valid linked open data, but limits the number of triples you will get based on the predicates. The second method will give you many more triples to start with and can act as a productive first step in a more manual approach where a human goes through and cleans up the extracted triples. LINCS tools provide both of these options to you.

This level of automation is meant to be a faster, though less precise, conversion method than that of the structured conversion workflow or a manual treatment of natural language texts. If your **Research Team** has the time, then you can put more manual curation into it, using the tools as a starting point.

#### Extraction

These extraction APIs will be part of [NSSI](/docs/tools/nssi/) and made accessible through programming notebooks and eventually through tools such as [LEAF-Writer](/docs/tools/leaf-writer/). In the meantime, LEAF-Writer is a great starting point for creating LOD from natural language texts even without the future relation extraction functionality. It allows you to tag entities in the text, reconcile them against external LOD sources, and then connect the mentions of those entities to the source text using the %%web annotations data model (WADM)|web-annotation-data-model%%. LINCS can then help you transform LEAF-Writer’s output into CIDOC CRM triples ready for publication with LINCS.

One of the systems that we are using behind the scenes of these APIs is made possible through our collaboration with [Diffbot](https://www.diffbot.com/). We have worked with them to tailor their [Natural Language Processing API](https://www.diffbot.com/products/natural-language/) to better account for the unique challenges of processing humanities texts.

:::info
The triples output from these automated systems may change slightly each time you run them if you edit the input text or if the system has updated since your last run. If you plan to run the tools on the same texts multiple times, consider how you will merge all your results after. For example, if you are manually reviewing to remove incorrect facts, keep track of those so that you can automatically remove them from future results.
:::

#### Validation

After extracting triples with these tools, depending on how you choose to balance time vs. data quality, you may want to inspect the results, removing inaccurate extractions and potentially adding missed triples. 

#### Conversion

Finally, LINCS has an additional API to transform triples with predicates from our allowable list into CIDOC CRM triples ready for the [Validate and Enhance step](/docs/create-data/publish-data/publish-validate). 

</TabItem>
</Tabs>


## Expected Outputs

You should now have RDF data that follows LINCS’s ontology and vocabulary standards. Your data may not be quite ready for ingestion into the LINCS knowledge graph yet, but it will be after some final cleanup in the [next step](/docs/create-data/publish-data/publish-validate).
