---
sidebar_position: 5
title: "Publish Data with LINCS"
description: "Publish Data with LINCS"
---


## Introduction

LINCS collaborates with scholars to help convert their research data into linked data and to publish that linked data as part of the LINCS %%Knowledge Graph|knowledge-graph%%. Publishing data with LINCS assumes that you have existing data—though not necessarily linked data—as opposed to [creating linked data from scratch](/docs/create-data/create-linked-data-from-scratch).

The conversion and publication process is a collaborative one, where LINCS provides resources, tools, and advice to guide your team’s work.

## Benefits and Outcomes

- Make your data more accessible to other researchers and the general public
- Gain new perspectives on your data as you interrogate how it should be modeled as %%Linked Open Data (LOD)|linked-open-data%%
- Interlink your data with other projects, enabling you to make exciting new connections in your research
- Explore your data in new ways, visualizing the web of relationships in the graph and drawing conclusions using powerful %%SPARQL|sparql-protocol-and-rdf-query-language%% queries


<!-- ### Examples
*Coming Soon...* -->


## How to Start
### Knowledge Requirements

It is helpful to have a basic understanding of LOD concepts before beginning the conversion process. See [Get Started](/docs/get-started/) for LOD basics, [About LINCS](/docs/about-lincs/) for project details, and the introductory [Create Data](/docs/create-data/) pages to understand the key [Clean](/docs/create-data/clean), [Reconcile](/docs/create-data/reconcile), and [Convert](/docs/create-data/convert) steps.

Once you have the basics down, understand [what it means to contribute to LINCS](/docs/create-data/publish-data/publish-admin). Think about how your project and data will benefit from using LOD technologies and what research questions you seek to answer with your converted data.

Read through the conversion workflow steps documentation starting with [Get Started with Conversion Workflows](/docs/create-data/publish-data/publishing-workflows) to understand the process and how your unique data will fit in.


### Data Requirements
To contribute data to LINCS, you need to come with an existing dataset that fits within the [LINCS Areas of Inquiry](/docs/about-lincs/research#areas-of-inquiry) and one of our [four data categories](/docs/create-data/publish-data/publishing-workflows): structured, semi-structured, TEI, or natural language.

You can still get involved with LOD even if you cannot contribute to LINCS. See our [tools page](/docs/tools/) to find other ways we can help your project or [explore already converted data](/docs/explore-data/).

:::info

Unsure if LINCS is right for your data? [Reach out to us to discuss](mailto:lincs@uoguelph.ca). 

:::




### Team Requirements
The conversion process is led by the data holders, or **Research Team**, with support from the LINCS team. Each Research Team needs to have a Project Lead who is able to make conceptual decisions about the datasets. It is also a good idea to have someone on the Research Team who is able to perform data extraction and manipulation (i.e., convert the data into a particular format).

Many of the steps in the conversion process, especially %%reconcile|reconciliation%%, can be completed by student research assistants who understand the source data and have a basic technical aptitude. These research assistants are generally hired by and work with the Research Team. LINCS will assist with the more difficult tasks, including %%mapping|mapping%% the data to an %%ontology|ontology%%.


### Contact LINCS

**If you want to contribute to LINCS, please get in touch with us early on!** 

To start the process, fill out our Data Contribution Interest Form (Form Coming Soon...).  <!-- TODO -->

As you wait for our response or if you want to start on your own, cleaning and reconciling your data are good places to begin. They tend to be the slowest steps with the most manual work and can benefit your project even if you choose not to continue with creating LOD.

:::info

[Contact LINCS](mailto:lincs@uoguelph.ca) early into your project planning to discuss building support from LINCS into grant applications.

:::


## What to Expect in the Process

### LINCS Support

Throughout the conversion workflow documentation, we specify who needs to be involved at each step. Many steps require familiarity with the contents and domain of the data. These should be completed by the **Research Team**.

LINCS has three teams to help you, depending on the technical needs of the step:
* **Ontology Team**
* **Conversion Team**
* **Storage Team**

For each step, we also outline what communication you are likely to have with LINCS. Most of the time, there will be a check-in with us at the start of a step if your data requires any personalized advice, and at the end of each step to double check that your output is correct.

:::info

Need additional support? See how you can [apply for support from LINCS](/docs/create-data/publish-data/publish-admin#what-support-is-available).
:::


### Timelines
The time needed to complete the full conversion process varies dramatically based on these questions:
- How clean is your source data?
- How many %%entities|entity%% are in your data? How many entities need to be de-duplicated internally or reconciled externally?
- What is the structure of your source data? The TEI and natural language workflows can be faster because they are more automated and less customized than the structured and semi-structured workflows.
- How many unique types of relationships are represented in your data?
- How much time does your team have to dedicate to the process? Is this continuous or will there be pauses based on the academic calendar?
- How many projects is LINCS supporting at the same time as yours?

A small dataset being converted by an experienced team with dedicated time could get through the whole conversion process in a few weeks. However, most projects working with LINCS tend to take between 6 to 12 months, factoring in time to learn tools, busy schedules, and consultation between the Research Team and LINCS. 

We have included time estimates for each step in the conversion workflow documentation. Remember that it does not have to be done all in one go. There is value in completing many of the steps on their own, like cleaning or reconciling your data, and slowly working towards all of the benefits of LOD.
