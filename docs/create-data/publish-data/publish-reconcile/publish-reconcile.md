---
sidebar_position: 5
title: "Reconcile Entities"
description: "LINCS Conversion Workflows — Reconcile Entities"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Introduction

%%Entity reconciliation|reconciliation%%, also called entity linking and %%named entity disambiguation|named-entity-disambiguation%%, is the step where we add unique identifiers in the form of %%URIs|uniform-resource-identifier%% to your data to represent each unique entity. The goal is to use the same identifier every time that the same real-world thing is mention in your data, other LINCS data, and, ideally, linked data elsewhere on the web. By using the same identifier, we can connect all of the statements made about that entity together to create a rich and informative graph about that entity.

This page covers how reconciliation fits into the data conversion workflows and the available tools, while our [Reconciliation Guide](/docs/create-data/publish-data/publish-reconcile/reconcile-guide) gets into the details of how to reconcile entities.

For a list of all reconciliation tools that LINCS uses, see the [Reconcile](/docs/create-data/reconcile) page.

## Resources Needed

### Who reconciles my data?

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Set-Up | ✓ |  | ✓ |  |
| Reconcile | ✓ |  |  |  |
| Merge | ✓ |  | ✓ |  |

This step will always be completed by your **Research Team** because it requires domain knowledge to ensure you are choosing the correct identifiers. This is a great task for undergraduate or graduate research assistants.

The LINCS **Conversion Team** can offer guidance on this step and particularly with how to set up your data for reconciliation and how to merge the URIs back into your data.

:::note

The **Research Team** and **Conversion Team** meet to identify what entities and concepts need to be reconciled and determine a strategy that suits the data.

:::

### How long does reconciliation take?

Reconciliation tends to be the slowest part of the conversion process. It can be sped up with tools that perform automated linking, but this comes at the sacrifice of accuracy. The loss in accuracy is worsened for the type of data coming into LINCS because it references more obscure or historically overlooked entities that are not well represented in existing linked data sources.

LINCS’s approach is to mix automation with manual review:

1. Start with tools that automatically suggest candidate matches for entities
2. If possible, apply filtering based on the context for each entity in your data and the authority data to separate trustworthy suggestions from ones that need review 
3. Have students manually review the uncertain candidates

With that mixed approach, you can estimate the time needed by assuming that each entity in your data will need 1-5 minutes for a human to reconcile it. This range depends on how familiar the person is with the data and whether they will need to spend time researching the entities to confirm a match.


### When should reconciliation take place?

Given that this process is so time consuming, we recommend starting to reconcile our data as early as possible. You can follow our guide here to start reconciling even before you have committed to the rest of the LINCS conversion process.

Reconciliation can be completed in tandem with the other steps in the conversion workflow, as placeholder values can be used for the conversion until your **Research Team** has finished reconciling. Once URIs have been found, they can be added to either the source data or to the converted data to replace the placeholder values. The dataset, however, is not be published by LINCS until either the **Research Team** finishes their reconciliation or LINCS and the **Research Team** come to an agreement that no more reconciliation can take place, and new URIs need to be %%minted|uniform-resource-identifier-minting%% for the remaining entities. Note that once the data is published, the **Research Team** can continue to enhance it, including further reconciliation.

### What if reconciliation cannot be completed?

For large datasets, it is not always feasible to carefully reconcile all entities. Our strategy for this has been:

1. Reconcile as much as is feasible.
2. When the data is ready for publication, other than reconciliation not being completed, your team can discuss with LINCS at what point you would like to call it and mint URIs for the remaining unreconciled entities.
3. Once the data is published, you can slowly continue to add URIs for the unreconciled entities in ResearchSpace.


## Set-Up your Data

For each workflow there are typically two options for setting up your data:
- Use a tool that takes your data in its original format and allows you to add URIs to the source data. For example, [LEAF Writer](/docs/tools/leaf-writer/) lets you annotate XML, TEI, and natural language data with entity types and URIs.
  + In this case, you should not need to do any setup beyond the typical [data cleaning step](/docs/create-data/publish-data/publish-clean/).
- Use a script or query tool to pull entities out of your data, along with contextual information about those entities. Then use a tool such as [VERSD](/docs/tools/versd) or [OpenRefine](/docs/tools/openrefine) to find URIs for the entities. Finally, use another script or query tool to insert those URIs back into either the source data or the converted data.
  + This will typically result in one or more a spreadsheets where each row represents one entity and the columns contain contextual details about the entity. For example, you may have an internal unique identifier for row to represent a person, and then columns for their name, birth date, death date so that you can quickly check if candidate URIs are correct.
  + LINCS typically uses custom scripts to complete this step. The **Conversion Team** can offer advice and sample scripts from previous conversions.

:::caution
   Consider enhancing your source data with internal unique identifiers for each entity. These can be temporary identifiers that will be replaced before your data is published. The benefit is that if you extract entities from your text, reconcile them, and put the new URIs into the source or converted data then you will be able to easily put the new URIs in the correct locations.
:::


## Reconcile your Entities

<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>

For structured data, extract entities and their context from your source data. That may require a custom script, but often it will be as easy as using a simplified version of your source spreadsheets or using an online tool to convert structured data into a spreadsheet. To find and confirm URIs, use [VERSD](/docs/tools/versd) if your data is bibliographic and [OpenRefine](/docs/tools/openrefine) otherwise. Note that OpenRefine accepts a broad range of starting file types so you may be able to skip the initial extraction step.

Particularly for small datasets, you may find it sufficient to manually lookup URIs and add them directly to your source data or wait and add them to the converted data.

</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">

As usual your options for semi-structured data depend on your specific data:

- Use [LEAF Writer](/docs/tools/leaf-writer/) to review and add URIs directly to the source document.
- Create a custom script to extract entities and their context. To find and confirm URIs, use [VERSD](/docs/tools/versd) if your data is bibliographic and [OpenRefine](/docs/tools/openrefine) otherwise.
- Manually lookup URIs and add them directly to your source data or wait and add them to the converted data.


</TabItem>
<TabItem value="tei" label="TEI Data">

To take advantage of the automated triples extraction possible with the TEI workflow, it is best that you insert entity URIs directly into your source TEI files. [LEAF Writer](/docs/tools/leaf-writer/) is our preferred tool for this step.

Alternatively, you may choose to make manual changes or changes using custom scripts to the source TEI files. You do still have the option to add additional URIs to the converted data at the end.

</TabItem>
<TabItem value="natural" label="Natural Language Data">

:::info
This step of the Natural Language Data workflow is still in progress. Check back over the next few months as we release the tools described here.
:::

The APIs that we use to automatically extract triples from natural language data combine the tasks of identifying entities, choosing a URIs for them, and extracting the relationships between entities. If you are using this approach, then you do not need to do anything for this step. Though, you may want to review the URIs that the tool suggests for your data and double check any that have conflicting information. You should also do additional look-ups to find URIs for entities that our tools did not reconcile, as these automated tools do not search through all authority files. Refer to the [documentation](/docs/tools/) for the tool you are using to confirm which authority files it connects to.

</TabItem>
</Tabs>


## Merge Reconciled Data

Your **Research Team** and **Conversion Team** use a custom script or the [Linked Data Enhancement API](/docs/tools/linked-data-enhancement-api) to merge the new URIs with either the cleaned version of the source data or the converted data.

The **Conversion Team** will %%mint|uniform-resource-identifier-minting%% new URIs for anything that could not be reconciled.

## Vocabularies

Similarly to reconciling entities, you also need to choose vocabulary terms to use in your data and include their URIs. These vocabulary terms are often used to add fine-grained types to entities and relationships, compared to the broad types that CIDOC CRM uses. Choosing the appropriate vocabulary terms for your project will require you to explore the terms’ definitions to find ones that fit. When possible, prioritize using terms that are already frequently used within LINCS data to increase the connections and potential for interesting queries between your data and other LINCS data.

See our [Vocabularies documentation](/docs/get-started/linked-open-data-basics/concepts-vocabularies) for additional background and the [LINCS Vocabulary Browser](/docs/tools/lincs-vocabulary-browser/) to find vocabulary terms created by or used in LINCS.

Check back soon for details on how you can create and edit vocabularies with LINCS.

<!-- - Not specific to any data type. Info about vocab usage or link to other documentation about LINCS vocab usage
- [unless this is included in the general page about reconciliation. But we need to instruct people on how to use vocabs in their data and how they would go about creating vocab terms.]
  - [Vocabulary Policy document](https://docs.google.com/document/d/1Lv1qOglW0SM2pi4hRu3asTrACQ3OW9RflCOqa90k_Do/edit?usp=sharing) (in progress)
  - [Vocabulary Workflow document](https://docs.google.com/document/d/1W_sbnIPAMOE2Bqr12P5v0YBei83dzyE2xY_UQ5xLvRQ/edit?usp=sharing) (in progress)
  - Requirements for creating vocab terms (in progress) -->
