---
sidebar_position: 6
title: "Develop Conceptual Mapping"
description: "LINCS Conversion Workflows — Develop Conceptual Data Mapping"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


## Introduction

Every incoming dataset starts with a unique structure and use of terms. To get all of this unique data to connect as %%Linked Open Data (LOD)|linked-open-data%%, each dataset needs to use the same %%ontology|ontology%%. In the Develop Conceptual Mapping step, LINCS develops a mapping that basically gives us instructions on how each relationship in the original data should look as %%RDF|resource-description-framework%% data.

LINCS has adopted %%CIDOC CRM|cidoc-crm%% as its base ontology. CIDOC CRM can be tailored to a specific dataset using existing CIDOC CRM extensions or domain specific %%vocabularies|vocabulary%%. LINCS’s use of CIDOC CRM is documented in the LINCS %%application profile|application-profile%%, with project specific conversion details available in the project application profiles. Check back soon for links to these application profiles.

For more information, see our [ontology documentation](/docs/get-started/linked-open-data-basics/concepts-ontologies).


## Resources Needed

This step can be a challenging one as **Research Teams** are often new to CIDOC CRM and many of the ontology concepts introduced. LINCS knows this and are here to help make the step as simple as possible. 

For natural language and TEI data, we have tools that let you skip this step and extract RDF directly from your data using templates for common relationships. 

For datasets that need custom mappings, we have application profiles that you can follow. These application profiles show the patterns we have used in mappings for previously converted LINCS data. When that is not enough, the **LINCS Ontology Team** can—with your consultation—develop custom mappings for you. This process typically takes 2-4 weeks from the time that LINCS receives a copy of your source data and enough documentation to understand the relationships it contains.

:::info
We encourage your team to take an active role through the mapping process, even if LINCS is developing a custom mapping for you.

By interrogating the ways in which your data is mapped to CIDOC CRM and the vocabulary terms introduced during this step, Research Teams often learn new things about their data and reevaluate the best ways to express concepts.

The mapping process has inspired past LINCS projects to go back and enhance their source data once they understood how expressive CIDOC CRM and RDF data can be.
:::

Regardless of the workflow you are following, your team will benefit from reviewing our [ontology documentation](/docs/get-started/linked-open-data-basics/concepts-ontologies) to understand the goals of this step and the basics of CIDOC CRM. Once your data is converted, understanding some CIDOC CRM will also help you navigate your data and take advantage of its new structure. 


## Developing a Mapping

<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>


|                     | Research Team | Ontology Team | Conversion Team | Storage Team |
| ------------------- | ------------- | ------------- | --------------- | ------------ |
| Use LINCS Application Profile to Create Mapping    | ✓              |              |                 |              |
| Develop Custom Mapping     |               | ✓             |                 |              |
| Consult and Approve | ✓             | ✓             |                 |              |


Most incoming datasets express some relationships that are common to already converted LINCS datasets. Basic biographic information about people or bibliographic details of written works are common examples. In these cases, your team can, with the support of the LINCS <b>Ontology Team</b>, use the LINCS application profile to start mapping those components. As more datasets are added to the LINCS %%triplestore|triplestore%%, more mappings will be available to draw upon.

When there are no existing datasets in LINCS that have similar structure and content, The **Ontology Team** either drafts a new conceptual mapping or adapts an existing mapping. 

:::note

The **Research Team** and **Ontology Team** meet to determine if there is meaning in the dataset that has been missed or misrepresented in the mapping.

:::

The **Ontology Team** iterates the mapping process until a conceptual model has been created that accurately captures and perhaps even enhances the meaning of the original data. The mapping is approved by the **Research Team**.


</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">

|                     | Research Team | Ontology Team | Conversion Team | Storage Team |
| ------------------- | ------------- | ------------- | --------------- | ------------ |
| Assess Current Data for Extraction Needs and Relationships of Interest     | ✓              |              |                 |              |
| Use LINCS Application Profile to Create Mapping     | ✓              |              |                 |              |
| Consult and Approve | ✓             | ✓             |                 |              |

<p>Data in this category is unique to each project and, compared to structured data, there is not necessarily a clear structure of entities and the important relationships between them.</p>

The basics of this step typically look like this:
<ol>
<li>Your team will identify what from the existing data should be expressed by the new mappings. An important consideration here is how that information can be extracted from the data. Is the data standardized and annotated enough that a script could be written to extract it or will it need to be manually extracted by a human?</li>
<li>Your team should review our <a href="/docs/get-started/linked-open-data-basics/concepts-ontologies">ontologies documentation</a> to gain the relevant background knowledge. Next, review the LINCS application profile to understand how basic information from your data can be mapped to CIDOC CRM. If your data is similar to existing LINCS data, then try to follow the application profile to map your data.</li>
<li> If you need support or you have data that does not fit within existing LINCS mapping in the application profile, the <b>LINCS Ontology Team</b> can help with creating custom mappings for your project. Note that the amount of support we can provide varies depending on the other projects we are supporting at that time.</li>
</ol>


</TabItem>
<TabItem value="tei" label="TEI Data">

|                     | Research Team | Ontology Team | Conversion Team | Storage Team |
| ------------------- | ------------- | ------------- | --------------- | ------------ |
| Use LINCS Tools with Built-in Mapping     | ✓              |              |                 |              |
| Use LINCS Application Profile to Create Mapping Not Covered by LINCS Tools     | ✓              |              |                 |              |
| Consult and Approve | ✓             | ✓             |                 |              |

<p>LINCS has developed tools that include pre-set templates for you to choose from that will extract information from your TEI documents and output CIDOC CRM RDF data. For details on these tools, continue to the <a href="/docs/create-data/publish-data/publish-implement-mapping/">Implement Conceptual Mapping</a> step.</p>

<!--[XTriples info... link to details of what relationships will be covered. Or include that in the Implement Conceptual Mapping step instead of here. For natural language I'm staying vague on this page so all the info can be on the next one.] -->

<p>If you need additional custom mappings for TEI fields that do not fit in the templates but that fall within our definition of structured data, then you should follow the structured data workflow for those fields or use the conversion XSLTs provided in the XTriples documentation.</p>

<p>If you need additional mappings for TEI fields that do not fit in the templates, refer to the LINCS application profile to map them into CIDOC CRM or consult with LINCS if custom mappings need to be created. Refer to the <a href="/docs/create-data/publish-data/publish-develop-mapping?workflow=structured#developing-a-mapping">Structured Data tab</a> for details on the process of creating custom mappings.</p>

<p>If you want to handle natural language text fields embedded in the TEI documents then follow the natural language workflow for that part of the data.</p>


</TabItem>
<TabItem value="natural" label="Natural Language Data">

|                     | Research Team | Ontology Team | Conversion Team | Storage Team |
| ------------------- | ------------- | ------------- | --------------- | ------------ |
| Use LINCS Tools with Built-in Mapping     | ✓              |              |                 |              |
| Use LINCS Application Profile to Create Mapping Not Covered by LINCS Tools     | ✓              |              |                 |              |
| Consult and Approve | ✓             | ✓             |                 |              |


<p>There are two ways LINCS extracts facts from natural language texts.</p>

<p>The preferred method is to use LINCS natural language processing tools which use a preset list of relationships that they can extract. For these extracted facts, you can use LINCS tools to convert the facts into CIDOC CRM. You do not need to create any custom mappings. For details on these tools, continue to the <a href="/docs/create-data/publish-data/publish-implement-mapping/">Implement Conceptual Mapping</a> step. Check back soon for the application profile for these tools, which shows what relationships are covered and how they are expressed in CIDOC CRM.</p>

<p>If you would like to extract additional facts from your text that were either missed by our tools or that fall into relationships not covered by our tools, then you can manually extract additional facts. This could be by hand or using other relation extraction systems. For these facts, refer to the LINCS application profile to map them into CIDOC CRM or consult with LINCS if custom mappings need to be created. Refer to the <a href="/docs/create-data/publish-data/publish-develop-mapping?workflow=structured#developing-a-mapping">Structured Data tab</a> for details on the process of creating custom mappings.</p>


</TabItem>
</Tabs>

## Expected Outputs

If you are using LINCS natural language or TEI conversion tools then you will not have output from this step and will move on to the [implement conceptual mapping step](/docs/create-data/publish-data/publish-implement-mapping/) to generate your converted data.

If you need any additional mappings created, then at the end of this step you should have mappings defined that you will implement in the [next step](/docs/create-data/publish-data/publish-implement-mapping/).