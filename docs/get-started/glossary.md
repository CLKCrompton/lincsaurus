---
id: glossary
title: Glossary
sidebar_position: 3
description: "Definitions, examples, and resources"
---

### [Application Profile](/docs/terms/application-profile) 
A schema composed of metadata elements drawn from one or more namespaces, as well as policies and guidelines related to their use, prepared for a particular application.


### [Application Programming Interface (API)](/docs/terms/application-programming-interface) 
A code library that enables third-party applications to communicate with a web service platform.


### [Argument](/docs/terms/argument) 
A series of reasons, statements, or facts in a metadata schema intended to support or establish a point of view, rather than a neutral statement, that describes a person, event, or object.


### [Authority Record](/docs/terms/authority-record) 
A stable, persistent Uniform Resource Identifier (URI) for a concept in the Linked Data (LD) ecosystem.


### [Blank Node](/docs/terms/blank-node) 
A subject or object in a Resource Description Framework (RDF) graph for which a Uniform Resource Identifier (URI) or literal is not given.


### [CIDOC CRM](/docs/terms/cidoc-crm) 
A suite of event-centric ontologies for describing data in the cultural heritage domain, developed to link together heterogeneous sets of data managed by museums, galleries, and other heritage institutions.


### [Classing](/docs/terms/classing) 
To declare an entity to be an instance of a class using rdf:type within the chosen ontology for the dataset.


### [Controlled Vocabulary](/docs/terms/controlled-vocabulary) 
A standardized and organized arrangement of words and phrases, which provides a consistent way to describe data.


### [Conversion](/docs/terms/conversion) 
The process of changing data from one format to another.


### [Crosswalking](/docs/terms/crosswalking) 
The conceptual process of mapping data between data models.


### [Cypher](/docs/terms/cypher) 
A query language for graph databases that reflects the semantic nature of triples but does so with its own unique syntax and formatting.


### [DBPedia](/docs/terms/dbpedia) 
A project that creates publicly available structured data for the Linked Open Data (LOD) Cloud.


### [Dereferenceable](/docs/terms/dereferenceable) 
An adjective used in relation to Uniform Resource Identifiers (URIs) that can turn from an abstract reference into something more concrete, namely a web resource.


### [Domain](/docs/terms/domain) 
One of two entities in a triple, representing the subject in a subject-predicate-object relationship.


### [Edge](/docs/terms/edge) 
A line that connects one node to another in a graph database, representing a relationship between the nodes.


### [Entity](/docs/terms/entity) 
A discrete thing, often described as the subject and object (or the domain and range) of a triple (subject-predicate-object).


### [Event-Oriented Ontology](/docs/terms/event-oriented-ontology) 
An ontology that uses events to connect things, concepts, people, time, and place.


### [Federated SPARQL Search](/docs/terms/federated-sparql-search) 
A single entry point to access remote SPARQL Endpoints so that a query service can retrieve information from several data sources.


### [Graph Database](/docs/terms/graph-database) 
A database that structures information as a graph or network, where a set of resources, or nodes, are connected together by edges that describe the relationships between each resource.


### [Inferencing](/docs/terms/inferencing) 
The automated discovery of new facts generated from existing triples.


### [Ingestion](/docs/terms/ingestion) 
The process by which data is moved from one or more sources to a new destination where it can be stored and further analyzed.


### [International Image Interoperability Framework (IIIF)](/docs/terms/international-image-interoperability-framework) 
A set of tools and standards that make digital images interoperable, providing a standardized method of describing and delivering images online.


### [Internationalized Resource Identifier (IRI)](/docs/terms/internationalized-resource-identifier) 
An identifier that builds upon the Universal Resource Identifier (URI) protocol by expanding the set of permitted characters to include most of the Universal Character Set.


### [Knowledge Graph](/docs/terms/knowledge-graph) 
A representation of a set of linked triples that illustrates the relationships between them.


### [Knowledge Map (ResearchSpace)](/docs/terms/knowledge-map) 
A visualization tool within the ResearchSpace environment that displays the different data entities in the triplestore and how they are connected to other data entities.


### [Knowledge Pattern (ResearchSpace)](/docs/terms/knowledge-pattern) 
Predefined graph paths that abstract real-world activities using classes and properties within the ResearchSpace environment.


### [Linked Data](/docs/terms/linked-data) 
Structured data that is linked with other data through the web and builds upon standard web technologies to share machine-readable data between computers.


### [Linked Open Data](/docs/terms/linked-open-data) 
Data that is linked and uses open sources.


### [Literal](/docs/terms/literal) 
An object in a triple that does not refer to a resource with a Uniform Resource Identifier (URI), but instead conveys a value, such as text, a number, or a date.


### [Named Entity Disambiguation (NED)](/docs/terms/named-entity-disambiguation) 
To assign a unique identity to an entity in a text to differentiate it from another entity that shares the same name.


### [Named Entity Recognition (NER)](/docs/terms/named-entity-recognition) 
The process of identifying and categorizing entities—a word or set of words that refer to the same thing—in text.


### [Named Graph](/docs/terms/named-graph) 
An extension of the Resource Description Framework (RDF) data model in which an RDF graph is identified using a Uniform Resource Identifier (URI), thus allowing for the publishing and presentation of metadata about that graph as a whole.


### [Namespace](/docs/terms/namespace) 
A directory of concepts that are used to identify and refer to entities within a dataset.


### [Natural Language Processing (NLP)](/docs/terms/natural-language-processing) 
A branch of artificial intelligence that involves automatic processing and/or manipulation of speech, text, and other unstructured data forms that represent how humans communicate with each other.


### [Node](/docs/terms/node) 
A representation of an entity or instance to be tracked in a graph database or triplestore, such as a person, object, organization, or event.


### [NoSQL Database](/docs/terms/nosql-database) 
A database that is modeled in a way that is different to the tabular relations used in a relational database, such as a document database, key-value store, wide-column store, or graph database.


### [Ontology](/docs/terms/ontology) 
An abstract, machine-readable model of a phenomenon that captures and structures knowledge of entities, properties, and relationships in a domain so a conceptualization can be shared with and reused by others.


### [Open Data](/docs/terms/open-data) 
Data that can be accessed, used, and reused by anyone, based on the idea that data should be freely available to everyone to see and make use of, without copyright restrictions.


### [Optical Character Recognition (OCR)](/docs/terms/optical-character-recognition) 
The automatic conversion of images of words to a text file that users can then search and edit.


### [Property](/docs/terms/property) 
A specified relationship between two classes or entities, such as the predicate in a triple (subject-predicate-object).


### [Property Graph](/docs/terms/property-graph) 
A graph where relationships (properties) between entities are named and carry some defined properties of their own, extending the basic graph database of linked triples to show complex connections that describe how different types of metadata relate.


### [Provenance](/docs/terms/provenance) 
The history of ownership, custody, or location of an object being described or the data describing that object.


### [Quad](/docs/terms/quad) 
An extension of a triple to include a fourth section that provides context for the triple, such as the Uniform Resource Identifier (URI) of the graph as a whole (subject-predicate-object-context).


### [Range](/docs/terms/range) 
One of two entities in a triple, representing the object in a subject-predicate-object relationship.


### [Reconciliation](/docs/terms/reconciliation) 
The process of ensuring that an entity in a dataset refers to a stable Uniform Resource Identifier (URI), ideally from a stable namespace, to make data more accessible, interoperable, and efficient when searching, storing, and retrieving.


### [Regular Expression (Regex)](/docs/terms/regular-expression) 
A syntax that can be used in programming languages to find, manipulate, or replace patterns in texts.


### [Reification](/docs/terms/reification) 
The process of making an abstract concept concrete, such as taking the notion of a relationship and viewing it as an entity or expressing something using a programming language, so it can be programmatically manipulated.


### [Relational Database](/docs/terms/relational-database) 
A database that stores data in tabular form (columns and rows), where columns hold attributes of data (such as datatypes), rows hold “records,” and relationships are defined between tables using rules.


### [Resource Description Framework (RDF)](/docs/terms/resource-description-framework) 
A standard for Linked Data (LD) that represents information in a series of three-part “statements” called a triple, which comprises a subject, predicate, and an object in the form subject-predicate-object.


### [Resource Description Framework (RDF) Serialization](/docs/terms/resource-description-framework-serialization) 
A syntax that can be used to write triples, including Turtle (TTL), XML (XML-RDF), and JSON.


### [Resource Description Framework Schema (RDFS)](/docs/terms/resource-description-framework-schema) 
An extension of the basic Resource Description Framework (RDF) vocabulary that can be used to define the vocabulary (terms) to be used in an RDF graph.


### [Semantic Narrative (ResearchSpace)](/docs/terms/semantic-narrative) 
An interactive document within the ResearchSpace environment that combines textual narrative and Linked Data (LD) to communicate ideas about people, places, and events.


### [Semantic Web](/docs/terms/semantic-web) 
The idea of extending the World Wide Web by including additional data descriptors to web-published content so computers can make meaningful interpretations of the published data.


### [Shape Expressions (ShEx)](/docs/terms/shape-expressions) 
A language for validating and describing Resource Description Framework (RDF) graph structures.


### [Shapes Constraint Language (SHACL)](/docs/terms/shapes-constraint-language) 
A standard for describing Resource Description Framework (RDF) graphs and validating them against a set of conditions.


### [Simple Knowledge Organization System (SKOS)](/docs/terms/simple-knowledge-organization-system) 
A standard that provides a way to represent thesauri, taxonomies, and controlled vocabularies following the Resource Description Framework (RDF).


### [SPARQL Endpoint](/docs/terms/sparql-endpoint) 
A location on the internet that is identified by a Uniform Resource Locator (URL) and is capable of receiving and processing SPARQL queries, allowing users to access a collection of triples.


### [SPARQL Protocol and RDF Query Language (SPARQL)](/docs/terms/sparql-protocol-and-rdf-query-language) 
A query language for triplestores that translates graph data into normalized, tabular data with rows and columns.


### [Structured Query Language (SQL)](/docs/terms/structured-query-language) 
A query language for relational databases that allows users to specify what data to return, what tables to look in, what relations to follow, and how to order the data that meets these set conditions.


### [Taxonomy](/docs/terms/taxonomy) 
A system that identifies hierarchical relationships among concepts within a domain.


### [Text Encoding Initiative (TEI)](/docs/terms/text-encoding-initiative) 
An encoding language that supports detailed encoding of complex documents and is used extensively by a number of different Digital Humanities (DH) projects.


### [Thesaurus](/docs/terms/thesaurus) 
A structured vocabulary that shows hierarchical, associative, and equivalence relationships between concepts, so users not only will find terms that are broader and narrower than others, but also terms that are synonymous, antonymous, or otherwise related (associated) in a defined manner.


### [Triple](/docs/terms/triple) 
A statement in the form of subject-predicate-object that follows the Resource Description Framework (RDF).


### [Triplestore](/docs/terms/triplestore) 
An NoSQL database that stores triples.


### [Typing](/docs/terms/typing) 
To use a standardized path to link any entity to an external vocabulary, thesauri, or ontology within CIDOC CRM.


### [Uniform Resource Identifier (URI)](/docs/terms/uniform-resource-identifier) 
A reliable and usable way to identify a unique entity so multiple datasets from various sources can communicate that they are all referring to the same thing.


### [Uniform Resource Identifier (URI) Minting](/docs/terms/uniform-resource-identifier-minting) 
The process of creating a new Uniform Resource Identifier (URI) to represent an entity.


### [Uniform Resource Locator (URL)](/docs/terms/uniform-resource-locator) 
A statement that describes the location of something on the web specifically for locating online assets.


### [Vocabulary](/docs/terms/vocabulary) 
A collection of terms that could be concretely described in an ontology, taxonomy, or thesaurus.


### [Web Ontology Language (OWL)](/docs/terms/web-ontology-language) 
A knowledge representation language for ontologies that explicitly represents the meaning of terms in vocabularies and the relationships between those terms, as well as between groups of terms.


### [WEMI](/docs/terms/wemi) 
An acronym standing for Work, Expression, Manifestation, and Item—terms deriving from Functional Requirements for Bibliographic Records (FRBR), which is the primary means of describing bibliographic records.


### [Wikibase](/docs/terms/wikibase) 
A suite of free and open-source knowledge-base software for storing, managing, and accessing Linked Open Data (LOD), written for and used by the Wikidata project.


### [Wikidata](/docs/terms/wikidata) 
The largest instance of Wikibase, which acts as a central storage repository for structured data used by Wikipedia, by its sister projects, and by anyone who wants to make use of a large amount of open general-purpose data.


### [Wikimedia Commons](/docs/terms/wikimedia-commons) 
A media file repository (images, sounds, and video clips) that makes public domain and freely licensed media content available, and which acts as the digital asset manager for all projects of the Wikimedia Foundation.


### [Wikimedia Foundation](/docs/terms/wikimedia-foundation) 
The umbrella organization that manages Wikipedia, Wikibase, Media Wiki, Wiktionary, and other Wiki projects and chapters.


### [XML](/docs/terms/xml) 
A human- and machine-readable markup language that allows users to create their own tags to describe documents.
