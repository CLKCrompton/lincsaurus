---
sidebar_position: 3
title: "Ontologies"
description: "Introduction to mapping, CIDOC CRM, WADM, and OWL"
---

<!-- @format -->

import ZoomableImage from "@site/src/components/imageGallery/ZoomableImage";

In order to make %%Linked Open Data (LOD)|linked-open-data%% as consistent and precise as possible, it must conform to an ontology. An ontology creates rules for your data, setting out exactly what types of %%entities|entity%% and relationships can exist in your data. Ontologies make the links between your data points semantic, which means that a computer can read and understand the specific meaning of the relationships.

## Ontologies as Worldviews

In metaphysics, **ontology** is the philosophical study of being, such as what pieces fit together to make up the world and how these pieces interrelate. In the information sciences, ontologies encode a conceptual model—an abstracted way of thinking—in a machine-readable way. More specifically, for LOD, a formal ontology is a set of classifications that define which subjects, objects, and predicates can be used to describe a given dataset.

It is important to understand that all ontologies present a worldview. By specifying what is allowed to belong in a %%domain|domain%% and %%range|range%%, an ontology determines what a category is and how different entities can be related to each other. Therefore, ontologies can be said to create just as much as they describe. Our data, and the decisions we make around how we choose to model and represent it, are connected to systems of power, agency, and control. LINCS is just as interested in how the structures we use bring meaning to the data as we are in linking data together.

LINCS aims to adopt ontologies that:

* Support nuance, difference, and diversity, including diverse ways of knowing
* Reflect an understanding of identity categories and social classification as culturally produced, intersecting, and discursive
* Provide context and provenance of assertions
* Mitigate risk of (re-)coding cissexism, racism, colonialism, heteronormativity, ableism, classism, and other forms of discrimination into data

For more about how LINCS uses ontologies, see the [Ontologies Adoption and Development Policy](/docs/about-lincs/policies/ontologies-adoption-and-development.md).

## Mapping

Fitting a dataset to an ontology is called %%mapping|mapping%%. Mapping forms the data into a structure that allows for consistent representation of all the things described by the dataset and the relationships between them. Some ontologies are meant to describe a general discipline or area (e.g., %%CIDOC CRM|cidoc-crm%% covers cultural heritage data). Others, often called **domain ontologies**, are custom-built for more specific subject matter.

Mapping can be a complicated process. When LINCS converts datasets into LOD, the Ontology Team drafts a conceptual mapping based on the structure of the dataset being converted. If possible, LINCS will adapt an existing mapping if there is a dataset in the %%triplestore|triplestore%% that has already been converted and shares a similar structure and content to the new dataset. Datasets that adhere to an ontology tend to be more consistent and accurate in their description of their data than datasets that have not yet been mapped. This consistency allows for the creation of richer and more useful LOD, especially when a widely adopted ontology is used.

## Orientations

Most ontologies are either oriented towards objects or towards events. %%Object-driven ontologies|object-oriented-ontology%% place objects at the centre of their structure and connect together pieces of information that are attributes of those objects. For example, a book could be modelled as a central object, with its author and publication date as connected attributes:

![alt="The book Indian Horse has the properties of having the author Richard Wagamese and having the publication date 2012"](/img/documentation/ontologies-object-driven-ontology-(c-LINCS).jpg)

%%Event-driven ontologies|event-oriented-ontology%% place events at the centre of their structure and describe the results or outputs of these events. For example, a book could be modelled as a product of a creation event, with the author and publication date as aspects of that event:

![alt="The book Indian Horse is a product of a creation event. The author Richard Wagamese and publication date 2012 are aspects of that event."](/img/documentation/ontologies-event-driven-ontology-(c-LINCS).jpg)

## CIDOC CRM

LINCS uses CIDOC CRM and its extensions as its core ontology. CIDOC CRM is the Conceptual Reference Model (CRM) of the International Council of Museums Committee for Documentation. CIDOC CRM is an event-driven ontology designed to model cultural heritage data and was originally developed for use by museums and archives.

CIDOC CRM contains a list of entities (i.e., subjects) and %%properties|property%% (i.e., predicates). Entities are identified by the “E” prefix and properties by the “P” prefix. The core CIDOC CRM ontology consists of approximately eighty entities that can be connected through roughly two hundred properties.

Entities and properties are organized in a hierarchy, with each subentity or subproperty being more specific than its superentity or superproperty. The following diagram describes an artwork within CIDOC CRM’s event-driven ontology. The ontology places the event of the artwork’s production at the centre (E12). E22 and E57 are subentities of E12. P45 is a subproperty of P106.

![alt="The production of a piece of art, for example, has properties such as has produced a human-made object and subproperties such as consists of the materials paper and ink "](/img/documentation/ontologies-classes-and-properties-hierarchy-(c-LINCS).jpg)

As an event-centric ontology, in CIDOC CRM, all knowledge comes from events. The following is a high-level diagram of the ontology.

<ZoomableImage path="/img/documentation/ontologies-CIDOC-CRM-(c-LINCS).jpg" title="" altlabel="Simplified overview of the CIDOC CRM ontology shows that everything starts with an event" caption=""/>

This diagram depicts the following:

* Actors (E39 Actor) can participate in events (E2 Temporal Entity).
* Events happen at a time (E52 Time-Span) and at a place (E53 Place).
* Events can affect things.
* Things can be physical (E24 Physical Human-Made Thing) or conceptual (E28 Conceptual Object).
* Things can have locations (E53 Place).
* Things can be categorized in many different groups (E55 Type).
* Things can have any number of identifiers (E41 Appellation).

Using an ontology like CIDOC CRM to organize your data makes it more compatible with other data. Compatibility makes your data more meaningful, reusable, and sustainable.

## Extensions

CIDOC CRM’s extensions allow the ontology to cover many more specific knowledge domains, when needed. The adaptability provided by the extensions is helpful for LINCS, since LINCS datasets, while all belonging under the umbrella of cultural heritage, come from a range of disciplines:

* [CRMarchaeo](https://cidoc-crm.org/crmarchaeo/): Archaeological excavation
* [CRMba](https://cidoc-crm.org/crmba/): Archaeological buildings
* [CRMdig](https://cidoc-crm.org/crmdig/): Provenance information
* [CRMgeo](https://cidoc-crm.org/crmgeo/): Spatiotemporal properties
* [CRMinf](https://cidoc-crm.org/crminf/): Argumentation
* [CRMsci](https://cidoc-crm.org/crmsci/): Scientific observation
* [CRMsoc](https://cidoc-crm.org/crmsoc/): Social phenomena
* [CRMtex](https://cidoc-crm.org/crmtex/): Ancient texts
* [FRBRoo/LRMoo](https://cidoc-crm.org/frbroo/): Bibliographic information
* [PRESSoo](https://cidoc-crm.org/pressoo/): Journals and periodicals
* [DoReMus](https://www.doremus.org/): Music

## Web Annotation Model (WADM)

LINCS sometimes uses the %%Web Annotation Data Model (WADM)|web-annotation-data-model%% in conjunction with CIDOC CRM. WADM is a standard for formatting and structuring %%web annotations|web-annotation%%. It was designed to make descriptions of web annotations consistent and therefore more shareable and reusable across different sources. LINCS uses WADM to describe the sources used in the datasets that are being converted.

WADM is not an extension of CIDOC CRM, nor is it event-centric by nature. While there is no pre-existing alignment between WADM and CIDOC CRM, ontologists are working to make WADM and CIDOC CRM work together. For example, at LINCS, annotations are classified as a crm:E33_Linguistic_Object in CIDOC CRM.

## Web Ontology Language (OWL)

At LINCS, we use %%Web Ontology Language (OWL)|web-ontology-language%% to express our ontologies so they can be understood by computers. OWL is like a language (or perhaps better, grammar) for ontologies. It is used to represent complex relationships between categories of things and their properties.

In the following diagram, OWL can be used to express:

* “P1 is identified by” is a property.
* The domain of “P1 identified by” is “E21 Person.”
* The range of “P1 is identified by” is “E41 Appellation.”
* The opposite (flipped) relationship of “P1 is identified by” is “P1i identifies.”
* The English label for this property is “is identified by.”

![alt="The domain Person is identified by the range Appellation"](/img/documentation/ontologies-OWL-(c-LINCS).jpg)

Below is what OWL encoding actually looks like:

```xml
<owl:ObjectProperty rdf:about="http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by">
    <rdfs:domain rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E21_CRM_Entity"/>
    <rdfs:range rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E41_Appellation"/>
    <owl:inverseOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/P1i_identifies"/>
    <rdfs:label xml:lang="en">is identified by</rdfs:label>
</owl:ObjectProperty>
```

## Summary

* In information sciences, ontologies encode a conceptual model in a machine-readable way.
* All ontologies present a worldview.
* Mapping forms the data into a structure that allows for consistent representation of all things and the relationships between them.
* Most ontologies are either object-driven or event-driven.
* CIDOC CRM is an event-driven ontology designed to model cultural heritage data.
* CIDOC CRM’s extensions allow it to cover many more knowledge domains.
* Web Annotation Data Model (WADM) is a standard for the formatting and structuring of web annotations.
* Web Ontology Language (OWL) is a grammar that is used to express ontologies.

## Resources

To learn more about ontologies, see the following resources:

* Bruseker (2019) [“Learning Ontology & CIDOC CRM”](https://www.archesproject.org/wp-content/uploads/2020/04/Learning-Ontology-CRM.pdf)
* Bruseker & Guillem (2021) [“CIDOC CRM Game”](http://www.cidoc-crm-game.org/)
* Bruseker (2022) [“Formal Ontologies: A Complete Novice’s Guide"](https://training.parthenos-project.eu/sample-page/formal-ontologies-a-complete-novices-guide/) 
* CIDOC CRM (2022) [“Compatible Models & Collaborations”](https://cidoc-crm.org/collaboration_resources)
* CIDOC CRM (2022) [“What is the CIDOC CRM?”](https://cidoc-crm.org/)
* Guarino, Oberle, & Staab (2009) [“What Is an Ontology?”](https://iaoa.org/isc2012/docs/Guarino2009_What_is_an_Ontology.pdf)
* Noy & McGuinness (2001) [“Ontology Development 101: A Guide to Creating Your First Ontology”](https://protege.stanford.edu/publications/ontology_development/ontology101-noy-mcguinness.html)
