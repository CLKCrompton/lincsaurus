---
title: "Get Started"
sidebar_class_name: "landing-page"
---

<!-- @format -->

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

LINCS develops infrastructure and tools so scholars and students of the humanities can create and access %%Linked Open Data (LOD)|linked-open-data%%. The promise and potential of LOD is at the heart of LINCS.

While the term **data** is not commonly used to describe the scholarly output of humanities researchers, many humanities researchers produce data. Humanities data can take many forms, such as:

- Image files (e.g., pictures, paintings, photographs)
- Text files (e.g., poems, manuscripts, letters, newspapers, journals, short stories, social media posts)
- Maps and geospatial coordinates
- Audio recordings (e.g., music, oral histories)
- %%Metadata|metadata%% files

These items can all be transformed into standard formats that both humans and computers can read. When data is made openly available on the web, scholars—and everyone!—can discover data, link to data, and advance humanities scholarship.

When research materials are converted into LOD, anyone can:

- Discover unexpected connections and influences among creative works, people, places, events, and organizations  

> _What are the social connections among people and networks that created the artwork and literature in these fin-de-siècle magazines?_  

- Ask new questions by searching across multiple diverse datasets  

> _Who (demographic data) lived on these streets (location data) in early modern London?_

- Query the data using multiple filters  

> _Show me all women authors who were published by independent presses and whose fathers were members of the clergy._  

- Reveal relationships by viewing data graphically as a timeline, word cloud, network, or chart

## Linked Open Data Basics

Learn the foundational concepts of LOD. For a full list of terms, consult the [Glossary](/docs/get-started/glossary).

:::tip

<!-- - Check out our recommended readings (link to Zotero coming soon) -->
- [Watch videos](https://www.youtube.com/@lincsproject/featured) on LOD topics and LINCS tools
- Look for upcoming [LINCS workshops and events](/docs/about-lincs/get-involved/events)

:::

<DocCardList items={useCurrentSidebarCategory().items.slice(0,2)}/>
