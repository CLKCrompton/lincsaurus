---
title: "ResearchSpace Documentation"
sidebar_position: 2
---

<!-- @format -->

## Overview

ResearchSpace lets you browse and search the wide variety of humanities data curated by LINCS and available in the LINCS %%triplestore|triplestore%%.  

The data includes people, places, groups, sources, events, and creative works from several different datasets. A single data point (a person, group, source, event, or creative work) is called an %%**entity**|entity%%. ResearchSpace lets you explore the details about a single entity and the connections between entities.

ResearchSpace has two special features: the %%**Knowledge Map**|knowledge-map%% and %%**Semantic Narrative**|semantic-narrative%%. The **Knowledge Map** is a visualization tool that lets you create your own diagram of entities and their connections. The **Semantic Narrative** lets you write a document which incorporate entities, and Knowledge Maps you have created.  

:::note
- ResearchSpace works best on larger screens. It is not optimized for mobile devices.
- ResearchSpace can be slow to load. If your page doesn't load, you may need to refresh the page and start again.
- ResearchSpace uses tabs (called  **Frames** in ResearchSpace). Many actions in ResearchSpace will cause a new Frame to open. Best practice is to close Frames you no longer need.
- Do not use your browser’s back button. You will lose your work.
- There is no undo function.
- ResearchSpace does not use persistent links. You cannot return to the same page in ResearchSpace by saving the page’s URL.
:::

## Researchers with Data in LINCS

LINCS maintains a distinct ResearchSpace environment for researchers with data in LINCS. In the **ResearchSpace (Review)** environment, you can edit your data and make changes to the metadata about your research project.  [Learn how to use the editing features available in ResearchSpace (Review)](/docs/tools/researchspace/researchspace-review-documentation).

## Create an Account

1. Click **Login** in the top navigation bar of ResearchSpace.
2. Click **Register** to create a new account or **Sign in with GitHub**. 
3. (Optional) If you are a researcher with data in LINCS [contact us](mailto:lincs.project@gmail.com) to obtain editing permissions.

:::note
You may explore ResearchSpace anonymously without creating an account. However, you will need to create an account to save your work. ResearchSpace uses Keycloak for authentication.
:::

## Browse LINCS Data

Discover the breadth of humanities data published by LINCS by selecting one of the browsing options available on the homepage: Browse by Category, Browse All Entities, or Browse Datasets. Your results will open in a new Frame.
![Browse](/img/documentation/researchspace-browse-(c-LINCS).png)

### Refine Your Results

You can refine your results using **Filters** and **Search within results**.  

**Refine your results using Filters:**    
Example:   
1. On the homepage, Browse by Category **Sources**.
2. On the results page, select one of the filters on the left side of the page.


![Filters](/img/documentation/researchspace-filters-(c-LINCS).png)

**Refine your results using search within the results**:  
Example: 
1. On the homepage, Browse by Category **People**.
2. On the results page, refine your results by searching for a person in the search box. 


![Search-within](/img/documentation/researchspace-search-within-(c-LINCS).png)

:::note
ResearchSpace treats search as if there were an **"OR"** between each search term. For example, **Florence Nightingale** will retrieve matches on **Florence** and matches on **Nightingale**. Phrase searching (**"Florence Nightingale"**) is not available.

:::

### View Results

By default, ResearchSpace displays the data as a grid of **Entity Cards**. The Entity Card displays the name of the entity and the %%CIDOC CRM|cidoc-crm%% class to which it belongs. In this example, the entity is **Florence Nightingale**, and she belongs to the CIDOC CRM class **Person**.   

![entity-card](/img/documentation/researchspace-entity-card-(c-LINCS).png)

### Entity Card Features

**View (eye icon)**: View brief information about the entity.  
**Explore**: Open the [Entity Summary Page](#understand-the-entity-summary-page) for a list of all incoming and outgoing statements connected with the entity.  
**Edit**: Edit information about the entity. This feature is only available in ResearchSpace (Review) for researchers with data in LINCS. See [documentation for ResearchSpace (Review)](/docs/tools/researchspace/researchspace-review-documentation).   
**Map**: Open the entity on a [Knowledge Map](#create-a-knowledge-map) canvas.  


In addition to a grid, you can also display the data as a map, table, chart, or timeline by clicking on one of these options. Depending on the data, these views may or may not be useful to you.

![view-options](/img/documentation/researchspace-view-options-(c-LINCS).png)

### Download Results

Click either the **Download csv** button or **Download json** button at the bottom of the screen. 


### Save Data to the Clipboard

 The Clipboard is a place to pin entities, sets of entities, and Knowledge Maps. You must be logged in if you want to save items on your Clipboard for future sessions.The Clipboard is a tab near the top right of the screen. CLick the tab to open and close the Clipboard.

![clipboard](/img/documentation/researchspace-clipboard-(c-LINCS).png)

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
  <TabItem value="individual" label="Save Individual Entities" default>  

1. Click the Clipboard tab to open it.
2. Drag individual Entity Cards onto the Clipboard.  


  </TabItem>
  <TabItem value="set" label="Save a Set of Entities">


1. Click the Clipboard tab to open it.
2. Click the checkbox on top left of as many Entity Cards as you want in your set. 
3. Click the **With Selected** box.
4. Click **Create New Set**. 
5. Name your set. Click **Save**.  


  </TabItem>
  <TabItem value="all" label="Save All Entities">

1. Click the Clipboard tab to open it.
2. Click **Save as Set** button at the bottom of the screen. If there are multiple pages of results, this will select and save all of them.
3. Name your set. 
4. Click **Save**.   


  </TabItem>
  <TabItem value="search" label="Save As Search">


1. **Save As Search** saves the %%SPARQL|sparql-protocol-and-rdf-query-language%% query that generated your search results.
2. Click the Clipboard tab to open it.
3. Click **Save as Search** button at the bottom of the screen. 
    


  </TabItem>
</Tabs>

### Clipboard Features

- **Search**: If you have a lot of items on your Clipboard, you can use the search box at the top of the Clipboard to find them quickly. The three vertical dots next to the search bar opens a search filter to aid your search. 
- **View Entities in a Set**: To open and set and view its contents, click on the name of the set.  

![clipboard-view-set-content](/img/documentation/researchspace-clipboard-view-set-content-(c-LINCS).png)  

These features are found at the bottom of the Clipboard. 

![clipboard-bottom-actions](/img/documentation/researchspace-clipboard-bottom-actions-(c-LINCS).png)  

From left to right these icons are:  

- **Grid View**: View entities as a Grid. 
- **List View**: View entities as a List. 
- **Reorder Items**: Drag items on your Clipboard to reorder them. When finished, click **Save changes**.
- **Create New Set**: Create a new (empty) set in your Clipboard. You can move entities into this set. 


- **Remove an Entity from the Clipboard**: Switch to List View using the List View icon at the bottom of the Clipboard. Then you can click the caret icon next to the entity you want to delete and select **Remove**

- **View Entity on a Knowledge Map**: If you're in Grid View, click the Map icon on the Entity Card. If you're in List View, click the caret and select Knowledge Map.

- **View a Set of Entities on a Knowledge Map**: Click the caret icon on the set and select **Explore Set**.
- **Rename Set**: Click the caret icon on the set and select **Rename Set**. Give the set a new name.
- **Delete Set**: Click the caret icon on the set and select **Remove Set**. Are you sure? **Yes**.

## Search LINCS Data

Start typing into the **Quick Search** box on the homepage to see a list of LINCS entities that match your search. Select an entity. It will open in a new Frame displaying the [**Entity Summary Page**](#understand-the-entity-summary-page).

You can also access the same search function in the top navigation bar. (This may not be visible if you are using a smaller screen). 

### Understand the Entity Summary Page

![clipboard-actions](/img/documentation/researchspace-entity-summary-(c-LINCS).png)

The Entity Summary Page provides details about an entity, including: 

- **Image of the Entity**: If an image exists, you will see it on the page. Click **Explore Media** (expand image button at the top left of the image) for a larger view of the image.
- **Entity Type**: The class according to the %%CIDOC CRM|cidoc-crm%% ontology.
- **Resource Identifier**: The entity's unique %%IRI|internationalized-resource-identifier%%.
- **Dataset(s)**: The name of the LINCS dataset(s) the entity is from.
- **Outgoing Statements (Entity as Subject)**: A list of statements where the entity is the subject of the %%triple|triple%%. Example: 

![outgoing-statement](/img/documentation/researchspace-outgoing-statement-(c-LINCS).png)

- **Incoming Statements (Entity as Object)**: A list of statements where the entity is the object of the %%triple|triple%%. Example: 

![incoming-statement](/img/documentation/researchspace-incoming-statement-(c-LINCS).png)

:::note
To learn more about triples, see [Linked Data](/docs/get-started/linked-open-data-basics/concepts-linked-data). 

:::

**Entity Summary Page Features**  

- **View in Knowledge Map**: Open the entity on a [Knowledge Map](#create-a-knowledge-map) canvas.
- **Save to Clipboard**: Save the entity to your [Clipboard](#save-data-to-the-clipboard). You must be logged in if you want to save your work for future sessions.   
- **Copy Link to this Entity Page**: Share a link to the Entity Summary page.

## SPARQL Search

Query the LINCS %%triplestore|triplestore%% using the  %%SPARQL endpoint|sparql-endpoint%% on the homepage. A separate login is required for this. [Contact us](mailto:lincs.project@gmail.com) to obtain these login credentials.

![SPARQL](/img/documentation/researchspace-SPARQL-(c-LINCS).png)

1. Write your query.
2. Click **Save**. 
3. Your query will save as a **Global query.**


## Create a Knowledge Map

A **Knowledge Map** is a visualization tool that lets you create your own diagram of entities and their connections. Knowledge Maps can be saved as images to use in publications and presentations. They can also be inserted into [Semantic Narratives](#semantic-narrative). 

Example: This Knowledge Map shows members of the Royal Society for the Prevention of Cruelty to Animals (from the Orlando dataset).

![knowledge-map](/img/documentation/researchspace-knowledge-map-(c-LINCS).png)

1. You can start a Knowledge Map with a **blank canvas** or by linking from an [**Entity Card**](#entity-card-features) or [**Entity Summary page**](#understand-the-entity-summary-page).  

  i. Start a new Knowledge Map **as a blank canvas** in one of the folowing ways:
- Click **New Knowledge Map** on the homepage
- Open a new Frame, then select **Knowledge Maps**

:::note

If you start your Knowledge Map with a blank canvas, you will first need to choose an entity to put on your Knowledge Map. You can add entities from your [Clipboard](#save-data-to-the-clipboard) or search all LINCS data. (See the section [Add Additional LINCS Data to Your Knowledge Map](#add-additional-lincs-data-to-your-knowledge-map)).

:::

  ii. Start a new Knowledge Map by **linking from an Entity Card** or **Entity Summary Page** in one of the following ways:
- The **Map** icon on an [Entity Card](#view-results)
- The **View in Knowledge Map** button on an [Entity Summary Page](#understand-the-entity-summary-page)
- **Explore Set** in the [Clipboard](#save-data-to-the-clipboard)


2. The Knowledge Map canvas will open displaying the entity and its **Connections Box**. The Connections Box lists all the incoming and outgoing statements related to the entity. (See [Understand the Entity Summary Page](#understand-the-entity-summary-page) for an explanation of incoming and outgoing statements). 

:::note

If you don't see a Connections Box next to the Entity Card, click on the **Entity Card**, then click the **compass** icon to the right of the card to open the Connections Box.  

:::


Example: This Connections box shows some of the _incoming statements_ (refers to, was death of) and _outgoing statements_ (has representation, is subject of, Same As, type) for the person Florence Nightingale. Take note of the icons that represent incoming and outgoing statements.

![knowledge-map-connections](/img/documentation/researchspace-knowledge-map-connections-box-(c-LINCS).png)

3. Select one of the statements. To back to the main list of connections, click **Connections** at the top of the Connections Box.
4. Select one or more connections and click **Add Selected**. To select multiple connections, press and hold the Control (Ctrl) key (or Command key on a Mac) then click the items.
5. The connections appear on the Knowledge Map and the link between them indicates the direction of the connection. 
6. Add as many more connections as you want. Once an entity is on the Knowledge Map canvas it will appear greyed out in the Connections Box. If the Connections Box is not visible, click on the Entity Card, then click the compass to the right of the card to re-open the Connections Box.
7. When you are finished, click **Save Knowledge Map** button at the top of the canvas. 
8. Click **Save Knowledge Map As...** and type in a name. Do not use quotation marks in the name.
9. Click **Save**. Your created Knowledge Map will save to the Clipboard. ResearchSpace does not have an autosave feature. If you make changes to your Knowledge Map, you must re-save using the Save Knowledge Map button.

:::tip
Each statement in the Connections Box displays a **Filter** icon.  The Filter gives you an alternative way to view the connections.   
1. Click the **>** arrow on the left side of the screen to reveal the **Classes** and **Instances** panel.    
2. Click the **Filter** icon next to any of the statements in the Connections Box. The connections will appear in the bottom half of the panel under **Instances**.
3. Drag and drop entities from the **Instances panel** onto your canvas. Entities that are already on your canvas will appear greyed out.
:::

### Knowledge Map Features

On the **top** of the Knowledge Map canvas you will find several buttons and icons:
- **Save Knowledge Map** button: Saves your Knowledge Map to your Clipboard.
- **Force layout** (snowflake icon): Arranges Entity Cards in a default layout.
- **Clear all**: Removes all Entity Cards from the canvas. You cannot undo this function.
- **Zoom in** and **Zoom out**: Zooms in or out of the canvas. 
- **Fit to screen**: Brings the Entity Cards back into the centre of your screen.
- **Export diagram as PNG**, **Export diagram as SVG**, **Export diagram as PDF**, and **Print diagram** (printer icon): Downloads your Knowledge Map in one of these formats or prints your Knowledge Map.  

On the **bottom** of the Knowledge Map canvas, you will find the **Expand Navigator** button that lets you move you entire Knowledge Map diagram around the canvas.

On the **left side** of the Knowledge Map canvas, you will find two side panels: The **Classes** panel and the **Instances** panel. To open these panels, click **>** (arrow icon) on the left side of the Knowledge Map canvas.  
- The **Classes** panel lists the CIDOC CRM ontology and extensions. Use this to [create a new entity](#create-an-new-entity-on-your-knowledge-map) on your Knowledge Map. 
- The **Instances** panel lets you search all LINCS data to [add LINCS entities to your Knowledge Map](#add-additional-lincs-data-to-your-knowledge-map). 

On the **right side** of the Knowledge Map canvas, you will find the **Connections** panel. To open this panel, click the **<** (arrow icon) on the right side of the Knowledge Map canvas. This panel lists all connections that exist for a specific entity on the Knowledge Map canvas. Use this panel to [change the appearance of the connecting links on your Knowledge Map](#edit-the-appearance-of-your-knowledge-map).

### View Additional Properties of an Entity in a Knowledge Map

1. Click **v** at the bottom of an Entity Card on the Knowledge Map canvas to reveal a note about the entity. Many entities do not have a note attached.
2. Click **^** to hide the note.

### Add Saved Data to Your Knowledge Map

1. Open the [**Clipboard**](#save-data-to-the-clipboard) panel on the right side of the Knowledge Map canvas.
2. Drag entities from your Clipboard onto the Knowledge Map.

### Add Additional LINCS Data to Your Knowledge Map

1. Click **>** (arrow icon) on the left side of the Knowledge Map canvas to open the Classes and Instances panels.
2. The lower half of the side panel is labelled **Instances**. Search all LINCS data by typing in the **Instances** search box.
- You can limit your results to a particular class by first selecting a class from one of the ontologies in the Classes panel.  
3. Drag the entity you want onto the canvas.

### Remove an Entity from Your Knowledge Map

1. Click on the entity you wish to remove.
2. Click the **x** which appears above the top right corner of the Entity Card. 

### Edit the Appearance of Your Knowledge Map

Once you’ve made connections on a Knowledge Map, you can edit how the links are displayed. Links and/or their corresponding labels can be displayed or hidden. None of these actions affects the source data.  

You can delete or re-label individual connections directly on your Knowledge Map canvas.  

Example: The connection "has current or former member" between the Royal Society and Florence Nightingale has been clicked to reveal icons for editing or deleting this link.   

![edit-link](/img/documentation/researchspace-edit-link-(c-LINCS).png)

You can remove the links and/or the link labels in a more methodical way: 
1. Click the **<** (arrow icon) on the right side of the Knowledge Map canvas to open the **Connections Panel**
2. Click on an Entity Card in your Knowledge Map. The Connections Panel lists all connections that exist for that specific entity on the canvas.
3. Change the appearance of the links connected to that Entity by performing one of the following actions:
- **Hide links and labels**: Click the **X** icon.
- **Show links without labels**: Click the **double-headed arrow** icon.
- **Show links with labels**: Click the **T** (text icon).  

You can make a global change to the appearance of all the links on your Knowledge Map by clicking the **X**, **double-headed arrow**, or **T** at the top of the Connections Panel.

![connections-panel](/img/documentation/researchspace-connections-panel-(c-LINCS).png)

## Create a Semantic Narrative

Semantic Narrative is a unique feature of ResearchSpace that lets you compose a document and embed dynamic data and Knowledge Maps into it. Whatever changes you make to your data or Knowledge Map will update in the Semantic Narrative. Your Semantic Narrative will be viewable by all ResearchSpace users.

Example:
  
![semantic-narrative](/img/documentation/researchspace-semantic-narrative-(c-LINCS).png)

1. To start a Semantic Narrative, click **New Semantic Narrative** on the homepage or open a new Frame and click **Semantic Narrative**.
2. Begin typing text into the blank page.
3. Insert data or Knowledge Maps from your [Clipboard](#save-data-to-the-clipboard) by dragging them onto the page.
4. When you are finished, save your work with the **Save** button at the top of the screen.  

 Clic the **[Activity](#activity)** link in the top navigation bar to access saved Semantic Narratives.

## Activity 

The **Activity** link in the top navigation bar is where you will find all ResearchSpace users' Semantic Narratives and Knowledge Maps. 
To delete an activity, select **Knowledge Maps** or **Semantic Narratives** and under **Actions** click on the garbage can icon next to the item.

![activity-delete](/img/documentation/researchspace-activity-delete-(c-LINCS).png)

## Acknowledgements

Lehmann, M. (n.d.) “[Introduction to the Knowledge Map](https://amara-west.researchspace.org/resource/?uri=http%3A%2F%2Fwww.researchspace.org%2Finstances%2Fnarratives%2FIntroduction_to_the_Knowledge_Map.html)” 

Lehmann, M. (n.d.) "[Introduction to the Semantic Narrative](https://amara-west.researchspace.org/resource/?uri=http%3A%2F%2Fwww.researchspace.org%2Finstances%2Fnarratives%2FAmara_West_ResearchSpace_Tutorial.html)"