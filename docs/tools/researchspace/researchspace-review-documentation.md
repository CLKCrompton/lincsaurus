---
title: "ResearchSpace (Review) Documentation"
sidebar_position: 3
---

<!-- @format -->

## Overview

LINCS maintains _**ResearchSpace (Review)**_ as a distinct ResearchSpace environment for researchers with data in LINCS.  

_**ResearchSpace (Review)**_ gives you the same features as the ResearchSpace environment with some additional functionality:
- View and [edit your own pre-published data](#edit-an-entity)
- Edit the [metadata about your dataset  ](#edit-metadata-about-your-dataset)

Consult the documentation for ResearchSpace to learn how to:
- [Create an account](/docs/tools/researchspace/researchspace-documentation#create-an-account)
- [Browse LINCS data](/docs/tools/researchspace/researchspace-documentation#browse-lincs-data)
- [Search LINCS data](/docs/tools/researchspace/researchspace-documentation#search-lincs-data)
- [SPARQL Search](/docs/tools/researchspace/researchspace-documentation#sparql-search)
- [Create a Knowledge Map](/docs/tools/researchspace/researchspace-documentation#create-a-knowledge-map)
- [Create a Semantic Narrative](/docs/tools/researchspace/researchspace-documentation#create-a-semantic-narrative)
- [Access all Knowledge Maps and Semantic Narratives](/docs/tools/researchspace/researchspace-documentation#activity)

:::note

Knowledge Maps and Semantic Narratives should be created in ResearchSpace, _**not**_ in ResearchSpace (Review). In the Review environment, you won't be able to share your work. If your Knowledge Map contains unpublished data, the data may change, affecting the work.

:::
 
## Edit an Entity

1.	Use **Search** to find entity you wish to edit.
2.	On the [Entity Summary Page](/docs/tools/researchspace/researchspace-documentation#understand-the-entity-summary-page), click **Edit this Entity**. You can also access a link to edit an entity by clicking on **Edit** (pencil icon) on an [Entity Card](/docs/tools/researchspace/researchspace-documentation#view-results).
3.	Edit the entity as needed.
4.	Click **Save** at the bottom of the form. Changes will be saved to the %%triplestore|triplestore%%.


:::tip

Use the **Same As** field in the entity editing form to reconcile an entity. You can enter a LINCS URI or a URI from an external authority like Wikidata or Getty.

:::

## Delete an Entity

1.	Use **Search** to find entity you wish to edit.
2.	On the [Entity Summary Page](/docs/tools/researchspace/researchspace-documentation#understand-the-entity-summary-page), click **Edit this Entity**. You can also access a link to edit an entity by clicking on **Edit** (pencil icon) on an [Entity Card](/docs/tools/researchspace/researchspace-documentation#view-results).
3.	Click **Delete** at the bottom of the form. This removes the entity from the %%triplestore|triplestore%%


## Create a New Entity

In rare cases you may wish to create a new entity from scratch. This requires sufficient understanding of CIDOC CRM.   
1. On the homepage, find **Create/Edit Entities** in the **Manage Data** box.
2. Find the entity type you wish to create.
3. Click **Create a New Entity** and fill in the Entity Editor Form.
4. Click **Save**.

## Edit Metadata about Your Dataset 

1.	Click **View Metadata** on the homepage.
2.	Click **Edit** (pencil icon) on your dataset card.
3.	Next to Metadata (Project/Dataset) Form click **Use**.
4.	Edit the Metadata form as needed.
5.	Click Save.
