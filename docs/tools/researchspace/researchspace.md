---
description: "Explore and edit LINCS datasets."
title: "ResearchSpace"
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

<div className="function-button-row">
  <functionbutton buttonName="Reconcile"></functionbutton>
  <functionbutton buttonName="Browse"></functionbutton>
  <functionbutton buttonName="Search"></functionbutton>
  <functionbutton buttonName="Visualize"></functionbutton>
</div>

ResearchSpace is a web-based tool for the storage, review, publication, and discovery of %%Linked Open Data (LOD)|linked-open-data%%. It allows users to search, browse, visualize, and edit existing LOD stored in the LINCS %%triplestore|triplestore%%.

<div className="banner">
  <img src="/img/logos/researchspace_logo_transparent.png" alt="" />
</div>

<div className="primary-button-row">
  <primarybutton
    link="https://rs.lincsproject.ca/"
    buttonName="To the Tool"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="/docs/tools/researchspace/researchspace-documentation"
    buttonName="To the Documentation"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="https://gitlab.com/calincs/infrastructure/lincs-rs-custom-app"
    buttonName="To GitLab"
    icon={faUpRightFromSquare}></primarybutton>
</div>

## ResearchSpace and LINCS

LINCS uses ResearchSpace as the publishing platform for its datasets. ReseachSpace supports two main types of users:

* Users who want to explore LINCS’s datasets
* Researchers whose data is published with LINCS

To cater to both types of users, LINCS provides two environments:

* **ResearchSpace**: Provides access to published LINCS data and all the features of the tool with the exception of data creation and modification
* **ResearchSpace (Review)**: Provides access to published and pre-published LINCS data, and lets users with a researcher account make changes to their own data and curate project metadata.

In both environments, you can search for data across the entire LINCS triplestore, across specific knowledge domains (e.g., art, bibliographic sources, people, places), or search within specific %%Knowledge Graphs|knowledge-graph%%, each representing a single research dataset.

You can create dynamic graph visualizations using a tool called a %%Knowledge Map|knowledge-map%% and export your Knowledge Map image in common digital formats for use in research outputs.

## Prerequisites

Users of ResearchSpace:

* Some understanding of [Linked Open Data](/docs/get-started/linked-open-data-basics) and %%CIDOC CRM|cidoc-crm%% will be helpful.  
* You will need to [create an account](/docs/tools/researchspace/researchspace-documentation#create-an-account) to save your work.

:::info

ResearchSpace comes preloaded with data from the LINCS triplestore. If you are interested in ingesting your data into the triplestore, see our documentation on [conversion workflows](/docs/create-data/publish-data/publishing-workflows) for more information.

:::

## Resources

To learn more about ResearchSpace, see the following resources:

* British Museum (2021) [“ResearchSpace: Semantic Tools”](https://researchspace.org/semantic-tools/)
* Oldman & Tanase (2018) [“Reshaping the Knowledge Graph by Connecting Researchers, Data and Practices in ResearchSpace”](https://pdfs.semanticscholar.org/9bc8/63036314d24f2d9851b3dc6ae727a55b8b9e.pdf)
* Oldman, Tanase, & Satschi (2019) [“The Problem of Distance in Digital Art History: A ResearchSpace Case Study on * Sequencing Hokusai Print Impressions to Form a Human Curated Network of Knowledge”](https://www.zora.uzh.ch/id/eprint/197623/1/Oldman%2C_Dominic%2C_Diana_Tanase%2C_and_Stephanie_Santschi._2019._The_Problem_of_Distance_in_Digital_Art_History.pdf)

LINCS ResearchSpace is an instance of the ResearchSpace platform developed by the British Museum. Information about the team that developed [LINCS ResearchSpace](/docs/about-lincs/tools-credits#researchspace) is available on the Tool Credits page.
