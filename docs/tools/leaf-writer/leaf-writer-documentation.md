---
title: "LEAF-Writer Documentation"
---

<!-- @format -->

## Prerequisites

### Create an Account

You need a GitHub account or GitLab account to access LEAF-Writer.

- Sign up for a [GitLab account](https://gitlab.com/)
- Sign up for a [GitHub account](<https://github.com/join?source=header-home](https://github.com/join?source=header-home)>)

You can log in directly via GitLab or GitHub or use the Keycloak portal to log in.

:::note

You will need to authorize LEAF-Writer after you log in. You will only need to complete this action once.

:::

## Open a Document

When you open LEAF-Writer, you will be prompted to choose a template or to open a document from the Cloud or your computer.

If you would like to open a document, it must be LEAF-Writer–compatible: an XML document that is well-formed and references a schema supported by LEAF-Writer. Supported schemas include the Orlando and CWRC bespoke schemas, as well as the [customizations](https://tei-c.org/guidelines/customization/) provided by the [Text Encoding Initiative (TEI)](https://tei-c.org/).

### Open a Template

There are four available templates: **Blank**, **Letter**, **Poem**, and **Prose**.

Templates come with pre-existing text. To add your own text into a template:

1. Right-click on the tag that contains the text you would like to delete.
2. Choose **Remove Content Only**. This action will remove the content from the tag, but will leave the tag in the document.
3. [Optional] Choose **Remove All** to delete the text and the tag.

For more information on tags, see the instructions for the [Markup Panel](#markup-panel).

### Open From the Cloud

You can search for and load public LEAF-Writer–enabled documents or repositories from GitLab or GitHub.

### Open From Your Computer

You can open a LEAF-Writer–compatible document (i.e., a document that has previously been opened in LEAF-Writer) from your computer by clicking **From Your Computer** and dragging and dropping a file into the popup that appears.

### Open XML

You can paste XML directly into the box provided to create a LEAF-Writer compatible document. If the document is not well-formed or uses a schema that is not supported by LEAF-Writer, it will not open.

## Save Your Work

There are three ways to save your work:

- Click **Save** in the top-left hamburger menu
- Click **Click to Sync** (cloud icon) next to the document’s title (a small blue dot will show up next to the icon when changes are made to remind you to save)
- Use a keyboard shortcut (**Ctrl+S** or **⌘+S**)

LEAF-Writer saves to the default source of your file. For example, if you open a document from GitHub, it will save in GitHub. If you open a document from your computer, it will save to whatever repository you used to log in (GitLab or GitHub).

If you want to change where your file saves, navigate to your LEAF-Writer identity (your GitLab or GitHub profile picture) in the top right. Here you can select if you would like LEAF-Writer to store your files in either GitLab or GitHub.

To save changes to a repository you do not own, the repository owner needs to have added you as a collaborator from the GitHub or GitLab repository settings. If you are not a collaborator on the external repository, you can still save a version of the document with the changes you have made as a new repository that you own.

## Navigation Panels

There are two different tabs on the left-hand side of LEAF-Writer.

### Markup Panel

The **Markup Panel** displays the document’s XML tags. From the **Markup Panel** you can add, delete, edit, and copy and paste tags in the document.

There are two ways to select a tag in the **Markup Panel**:

- Click the tag (so it becomes bolded and italicized) to select the contents of the tag.
- Double-click the tag (so it becomes bolded) to select both the contents of the tag and the tag itself.

### Entities Panel

The **Entities Panel** lets you see the entities tagged in the document. You may view the items in this tab sequentially, alphabetically, or categorically.

Click **Edit** (pencil icon) to bring up a popup box where you can edit the attributes of an entity. Click **Remove** (circle icon) to remove the tag.

## Editor Modes

On the bottom left of LEAF-Writer, you can change the editor mode of your document. The Editor Mode you choose is based on what you would like to do with the output.

There are four editor modes in LEAF-Writer:

|        Mode         | Allows overlapping XML entities? |
| :-----------------: | :------------------------------: |
|         XML         |                ✗                 |
|      XML + RDF      |                ✗                 |
| XML + RDF (Overlap) |                ✔                 |
|         RDF         |                ✔                 |

The default mode is XML + RDF. While you can change the editor mode at any time, if you have overlapping entities in your document and switch to a mode that prohibits them, the overlapping entities will be discarded.

## Toolbar

LEAF-Writer has a toolbar that can be used to add, edit, and delete tags.

|                                      Icon                                      |                                                                           Action                                                                           |
| :----------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------: |
|             ![alt="Tags"](/img/documentation/leaf-writer-tags-(c-owner).jpg)             |                                               Displays a list of eligible tags you can add to your document.                                               |
|       ![alt="Tag Person"](/img/documentation/leaf-writer-tag-person-(c-owner).jpg)       |                                                   Opens a popup that allows you to tag a person by name.                                                   |
|        ![alt="Tag Place"](/img/documentation/leaf-writer-tag-place-(c-owner).jpg)        |                                                       Opens a popup that allows you to tag a place.                                                        |
| ![alt="Tag Organization"](/img/documentation/leaf-writer-tag-organization-(c-owner).jpg) |                                              Opens a popup that allows you to tag an organization or company.                                              |
|      ![alt="Tag Text/Title"](/img/documentation/leaf-writer-tag-text-(c-owner).jpg)      |                                       Opens a popup that allows you to tag a title or the text of any type of work.                                        |
| ![alt="Tag Referencing String"](/img/documentation/leaf-writer-tag-string-(c-owner).jpg) |                                      Opens a popup that allows you to tag a general purpose name or referring string.                                      |
|     ![alt="Tag Citation"](/img/documentation/leaf-writer-tag-citation-(c-owner).jpg)     |                                                      Opens a popup that allows you to tag a citation.                                                      |
|         ![alt="Tag Note"](/img/documentation/leaf-writer-tag-note-(c-owner).jpg)         |                                                 Opens a popup that allows you to tag a note or annotation.                                                 |
|         ![alt="Tag Date"](/img/documentation/leaf-writer-tag-date-(c-owner).jpg)         |                                                        Opens a popup that allows you to tag a date.                                                        |
|   ![alt="Tag Correction"](/img/documentation/leaf-writer-tag-correction-(c-owner).jpg)   |                                   Opens a popup that allows you to tag a correction you have made to the original text.                                    |
|      ![alt="Tag Keyword"](/img/documentation/leaf-writer-tag-keyword-(c-owner).jpg)      |                                                      Opens a popup that allows you to tag a keyword.                                                       |
|         ![alt="Tag Link"](/img/documentation/leaf-writer-tag-link-(c-owner).jpg)         |                                                    Opens a popup that allows you to tag a link or URL.                                                     |
|     ![alt="Add Translation"](/img/documentation/leaf-writer-translate-(c-owner).jpg)     | Opens a popup that allows you to add a translation for the text in a `div`. The translation appears in a new div that is added beneath the translated one. |
|      ![alt="Toggle Tags"](/img/documentation/leaf-writer-toggle-tags-(c-owner).jpg)      |                                                      Shows or hides XML tags in the **Editing Pane**.                                                      |
|       ![alt="Show Raw XML"](/img/documentation/leaf-writer-xml-show-(c-owner).jpg)       |                                                  Opens a left sidebar that shows the document’s raw XML.                                                   |
|       ![alt="Edit Raw XML"](/img/documentation/leaf-writer-xml-edit-(c-owner).jpg)       |                                           Opens a popup that allows you to directly edit the document’s raw XML.                                           |
|         ![alt="Validate"](/img/documentation/leaf-writer-validate-(c-owner).jpg)         |                                                 Validates the XML document against the associated schema.                                                  |
|         ![alt="Settings"](/img/documentation/leaf-writer-settings-(c-owner).jpg)         |                                                               Opens the **Settings Panel**.                                                                |
|       ![alt="Fullscreen"](/img/documentation/leaf-writer-fullscreen-(c-owner).jpg)       |                                                                Toggles to fullscreen view.                                                                 |

### Settings Panel

The **Settings Panel** is located in the toolbar. Use the **Settings Panel** to:

- Modify the appearance of the interface (light mode, automatic mode, dark mode)
- Change the language (English or French)
- Change the size of the text in the **Editor Area**
- Change how entities are displayed in the **Editor Area** (as coloured text or highlighted)
- Reorganize the priority of the entity sources (see [Tag Entities](#tag-entities) for more information)

### Validate

LEAF-Writer has continuous validation, which means you will not need to validate your document manually as you work. If your document becomes invaild, a list of the errors will appear in the right-hand panel.

If you would like to manually validate your XML document, click **Validate** in the toolbar. Click an error message to highlight the error in the document. You can then correct the invalid structure and the error message will disappear.

:::warning

If you try to save a document that is not well-formed, a popup will appear asking you to confirm if you want to proceed. You should not save documents that are not well-formed. It is important to validate your document often and fix your errors as soon as they appear.

:::

## Right Click Menus

LEAF-Writer has two right-click menus that can be used to add, edit, and delete tags.

### Editor Area Menu

Right-click the **Editor Area** to see a menu with nine options.

|          Option           |                              Action                               |
| :-----------------------: | :---------------------------------------------------------------: |
|        **Add Tag**        |  Displays a list of eligible tags you can add to your document.   |
| **Add Entity Annotation** |            Opens a popup that allows you to add a tag.            |
|       **Edit Tag**        |  Opens a popup that allows you to edit the attributes of a tag.   |
|      **Change Tag**       | Opens a popup that allows you to change a tag to a different tag. |
| **Copy Tag and Contents** |              Copies a tag and the text it contains.               |
|       **Split Tag**       |                     Splits one tag into two.                      |
|      **Remove Tag**       |               Removes a tag, but keeps the content.               |
|  **Remove Content Only**  |        Removes the content from a tag, but keeps the tag.         |
|      **Remove All**       |                 Removes the tag and its content.                  |

### Markup Panel Menu

Right-click the **Markup Panel** to see a menu with ten options.

|          Option           |                                              Action                                               |
| :-----------------------: | :-----------------------------------------------------------------------------------------------: |
|       **Edit Tag**        |                  Opens a popup that allows you to edit the attributes of a tag.                   |
|      **Change Tag**       |                 Opens a popup that allows you to change a tag to a different tag.                 |
| **Copy Tag and Contents** |                              Copies a tag and the text it contains.                               |
|      **Remove Tag**       |                               Removes a tag, but keeps the content.                               |
|  **Remove Content Only**  |                        Removes the content from a tag, but keeps the tag.                         |
|      **Remove All**       |                                 Removes the tag and its content.                                  |
|    **Add Tag Before**     |                       Adds a tag before the tag you have right-clicked on.                        |
|     **Add Tag After**     | Adds a tag after the tag you have right-clicked on, skipping over any nested tags within the tag. |
|    **Add Tag Around**     |                    Adds a tag that will surround the originally selected tag.                     |
|    **Add Tag Inside**     |                   Adds a new tag nested within the initial tag you clicked on.                    |

## Create Structure

Copying and pasting is the quickest way to build the structure of your document:

1. Click on a tag you would like to copy in the **Markup Panel** so it becomes bolded and italicized.
2. Right-click the tag and select **Copy Tag and Contents** in the popup.
3. Select the parent tag in the **Markup Panel** in which you would like to paste your copied tag so it becomes bolded and italicized.
4. Right-click the parent tag and select **Paste Tag** in the popup. The copied tag will be pasted within the parent tag after the parent tag’s other content.

:::note

Once you paste a tag, it leaves your clipboard. You will have to complete the above steps every time you would like to copy and paste a tag.

:::

:::warning

Copy and pasting tags can create structural problems in your document if you are not careful. Make sure to fix validation errors often when copying and pasting tags.

:::

## Add Text

Place your cursor in the **Editor Area** and type to add text to your document. You can also copy and paste text directly into the **Editor Area**:

1. Copy the information that you would like to paste. If you copy text from another XML document, all existing tags will be stripped out when the text is pasted into LEAF-Writer.
2. Activate the field you want to paste into by clicking the tag in the **Markup Panel**. You cannot paste into multiple fields at once.
3. Place your cursor in the highlighted area in the **Editor Area**.
4. Paste the text using keyboard shortcuts (**Ctrl+V** or **⌘+V**).

The **Markup Panel** tags will not be affected unless you have pasted over preexisting tags.

## Tag Entities

LEAF-Writer is connected to the LEAF entity system, which allows you to reconcile entities in your text. See [Reconciliation](/docs/create-data/reconcile) for more information.

Named entities can be searched for from within LEAF-Writer using authoritative %%Linked Data (LD)|linked-data%% sources such as DBPedia, Getty ULAN, GeoNames, VIAF, and Wikidata. Each source is best suited to certain entity types:

|                  | [DBPedia](https://www.dbpedia.org/) | [Getty ULAN](https://www.getty.edu/research/tools/vocabularies/ulan) | [GeoNames](http://www.geonames.org/) | [VIAF](http://viaf.org/) | [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) |
| :--------------: | :---------------------------------: | :------------------------------------------------------------------: | :----------------------------------: | :----------------------: | :----------------------------------------------------------: |
|    **Person**    |                  ✓                  |                                  ✓                                   |                                      |            ✓             |                              ✓                               |
|    **Place**     |                  ✓                  |                                                                      |                  ✓                   |            ✓             |                              ✓                               |
| **Organization** |                  ✓                  |                                                                      |                                      |            ✓             |                              ✓                               |
|  **Text/Title**  |                                     |                                                                      |                                      |            ✓             |                                                              |
|   **Citation**   |                                     |                                                                      |                                      |            ✓             |                                                              |

LEAF-Writer allows you to choose the sources you would like to use and order them by preference:

1. Click **Settings** in the Toolbar.
2. Drag to reorder the sources under **Entity Lookup Sources**.

:::note

If you are unable to find an appropriate entity using the LEAF entity system, you can manually add a URI to your document when tagging. While the URI will be added to your document, it will not be saved to the LEAF entity system.

:::

The processes to tag a person, place, organization, text/title, referencing string, and citation are similar:

1. Highlight the text that you would like to tag.
2. Click the appropriate button in the toolbar for your entity (**Tag Person**, **Tag Place**, **Tag Organization**, **Tag Text/Title**, **Tag Referencing String**, or **Tag Citation**). A list of potential matches will appear in a popup.
3. Choose the match that corresponds to the entity you wish to tag.
4. [Optional] If none of the matches are correct, manually add a URI under **Other / Manual Input**.
5. Click **Select**.
6. Fill in the required information in the popup. The information required varies slightly depending on what type of entity you are tagging.
7. Click **OK**.

The processes to tag a note, date entity, correction, keyword, or link are similar:

1. Highlight the text that you would like to tag.
2. Click the appropriate button in the toolbar for your entity (**Tag Note**, **Tag Date**, **Tag Correction**, **Tag Keyword**, **Tag Link**).
3. Fill in the required information in the popup. The information required varies slightly depending on what type of entity you are tagging.
4. Click **OK**.
