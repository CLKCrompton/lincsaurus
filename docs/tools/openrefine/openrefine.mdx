---
description: "Clean and transform structured data."
title: "OpenRefine"
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

<div className="function-button-row">
  <functionbutton buttonName="Clean"></functionbutton>
  <functionbutton buttonName="Reconcile"></functionbutton>
</div>

OpenRefine is a data processing application that allows you to clean up and transform structured data. It has several functionalities suited for creating %%Linked Open Data (LOD)|linked-open-data%%, such as %%reconciliation|reconciliation%%, format translation, %%Resource Description Framework (RDF)|resource-description-framework%% %%mapping|mapping%%, and export options.

<div className="banner">
  <img src="/img/documentation/openrefine-overview-logo-(c-owner).png" alt="" />
</div>

<div className="primary-button-row">
  <primarybutton
    link="https://openrefine.org/"
    buttonName="To the Tool"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="https://docs.openrefine.org/"
    buttonName="To the Documentation"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="https://github.com/OpenRefine/OpenRefine"
    buttonName="To GitHub"
    icon={faUpRightFromSquare}></primarybutton>
</div>

## OpenRefine and LINCS

Within the LINCS project, OpenRefine is used for [data cleaning](/docs/create-data/clean) and [reconciliation](/docs/create-data/reconcile). It is primarily used by researchers bringing their own datasets to the project. OpenRefine allows for these domain experts to have full control over the changes made to their data.

OpenRefine is best suited for structured data, since it will represent the data in a format similar to a spreadsheet or table. Any file type that follows a similar system, such as comma separated values (CSV), is best, though it is also compatible with other file types like %%XML|xml%%, %%JSON|json%%, and RDF. If a researcher’s data falls within a certain domain or is unstructured, a different tool may be more appropriate:

- Use [NSSI](/docs/tools/nssi) or [NERVE](/docs/tools/nerve) for an unstructured dataset.
- Use [VERSD](/docs/tools/versd) to reconcile an entirely bibliographic dataset.

The software can be downloaded from [OpenRefine’s website](https://openrefine.org/). When launched, the application will open in a browser tab that runs locally on your computer.

Though this tool can be useful for researchers and data specialists outside of LINCS, it is important for those who are in the process of getting their data into the LINCS system to begin cleaning and reconciling it in OpenRefine early in the data preparation process.

:::info

### Contribute to LINCS

Interested in creating Linked Open Data and publishing it in the LINCS triplestore? See [Publish Data with LINCS](/docs/create-data/publish-data) for more information.

:::

### Reconciling against LINCS

Check out the [LINCS Authority Service](/docs/tools/lincs-authority-service) to reconcile your data against the LINCS Knowledge Graph from within OpenRefine.

## Prerequisites

Users of OpenRefine:

- Need to come with their own dataset
- Need a basic understanding of [reconciliation](/docs/create-data/reconcile) and [data cleaning](/docs/create-data/clean)
- Do not need to create a user account

OpenRefine supports the following inputs and outputs:

- **Input:** CSV, TSV, XLS, XLSX, JSON, XML, RDF, plain text, and more
- **Output:** CSV, TSV, XLS, XLSX, HTML-formatted tables, and more

## Resources

To learn more about OpenRefine, see the following resources:

**Clean Data:**

- [OpenRefine User Manual](https://docs.openrefine.org/)
- Rue & Hernandez (2019) [“Using OpenRefine to Clean Your Data”](https://multimedia.journalism.berkeley.edu/tutorials/openrefine/)
- Hervieux (2020) [“OpenRefine Activity”](https://docs.google.com/presentation/d/1RVvRpLRP-xUONOAf502ZfEHuJPSlD32eQYq2EgovdvI/edit#slide=id.g132ca824ecf_0_413) [PowerPoint]
- van Hooland, Verborgh, & De Wilde (2021) [“Cleaning Data with OpenRefine”](https://programminghistorian.org/en/lessons/cleaning-data-with-openrefine)

**Reconcile Entities:**

- [OpenRefine User Manual—Reconciling](https://docs.openrefine.org/manual/reconciling)
- Getty Digital (2020) [“Getty Vocabularies OpenRefine Tutorial and Tips for Advanced Users”](https://www.getty.edu/research/tools/vocabularies/obtain/getty_vocabularies_openrefine_tutorial.pdf)
