---
description: "Map and convert XML data."
title: "X3ML (3M)"
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

<div className="function-button-row">
  <functionbutton buttonName="Convert"></functionbutton>
</div>

X3ML is a toolkit that includes 3M, the Mapping Memory Manager, an open-source schema %%mapping|mapping%% tool that can be used to map and convert data from one format or structure to another. It allows data experts to set, edit, and save their mapping rules for easy data conversion.

<div className="banner">
  <img src="/img/documentation/x3ml-overview-logo-(c-owner).png" alt="" />
</div>

<div className="primary-button-row">
  <primarybutton
    link="https://3m.lincsproject.ca/3M/"
    buttonName="To the Tool"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="https://github.com/isl/Mapping-Memory-Manager"
    buttonName="To GitHub"
    icon={faUpRightFromSquare}></primarybutton>
</div>

## X3ML and LINCS

Within the LINCS project, X3ML is used to convert data from a variety of sources to our selected %%ontologies|ontology%%, including %%CIDOC CRM|cidoc-crm%% and %%Web Annotation Data Model (WADM)|web-annotation-data-model%%.

The LINCS project hosts its own instance of 3M that can be accessed via a [log in](https://3m.lincsproject.ca/3M/). Anyone can create an account to use this instance of 3M. Uploading your data to this instance of 3M, however, does not mean that you are contributing your data to LINCS because 3M is a tool for data conversion and not our %%triplestore|triplestore%%. While you can use this instance of 3M to convert your data and download your files, LINCS will not keep any of the uploaded files in long-term storage.

Though 3M can be useful for data experts outside of LINCS, it is important for those who are in the process of preparing their data for contribution to the LINCS triplestore to liaise with the LINCS team before mapping their data in 3M.

:::info

### Contribute to LINCS

Interested in creating Linked Open Data and publishing it in the LINCS triplestore? See [Publish Data with LINCS](/docs/create-data/publish-data) for more information.

:::

## Prerequisites

Users of X3ML:

- Need to come with their own dataset
- Need to create a [user account](https://3m.lincsproject.ca/3M/SignUp?lang=en)
- Need an advanced understanding of [ontologies](/docs/get-started/linked-open-data-basics/concepts-ontologies)

X3ML supports the following inputs and outputs:

- **Input:** XML
- **Output:** RDF/XML, %%TTL|turtle%%, N-Triples

## Resources

To learn more about X3ML, see the following resources:

**Technical Articles and Background Information:**

- Marketakis et al. (2017) [“X3ML Mapping Framework for Information Integration in Cultural Heritage and Beyond”](https://dl.acm.org/doi/10.1007/s00799-016-0179-1)
- Minadakis et al. (2015) [“X3ML Framework: An Effective Suite for Supporting Data Mappings”](http://users.ics.forth.gr/~fgeo/files/X3ML15.pdf)

**Approachable Introductions and Tutorials:**

- Bruseker (2018) [“General Introduction to the Use of X3ML Toolkit”](https://cidoc.mini.icom.museum/wp-content/uploads/sites/6/2018/12/25.09_WS_CRM.5_Bruseker_X3MLToolkitTutorial.pdf) [PowerPoint]
- Kräutli (2018) [“CIDOC-CRM by Practice”](https://dh-tech.github.io/workshops/2018-10-15-CIDOC-CRMbyPractice/#/) [Video]
- Theodoridou (2021) [“Mapping & Transforming Data for Semantic Integration: The X3ML Toolkit”](https://www.rd-alliance.org/system/files/documents/4_3M-X3ML-RDA%2017VPM%20%5B2021-04-20%5D.pptx) [PowerPoint]
- Theodoridou (2018) [“The X3ML Toolkit”](https://projects.ics.forth.gr/isl/cci/demos/brochures/CIDOC2018-X3ML-Tutorial.pdf) [PowerPoint]

**Official Technical Manuals:**

- Oldman, Theodoridou, & Samaritakis (2010) [“Using Mapping Memory Manager (3M) with CIDOC CRM”](http://83.212.168.219/DariahCrete/sites/default/files/mapping_manual_version_4g.pdf)
- Theodoridou et al. (2010) [“Mapping Memory Manager (3M) Instance and Label Generator Rule Builder”](https://mapping.d4science.org/3M/Manuals/en/X3ML_Generators_Manual.pdf)

**Case Study:**

- Theodoridou (2014) [“Mapping Cultural Heritage Information to CIDOC-CRM”](https://www.slideshare.net/MariaTheodoridou/london-meetup2014-mappingchi2cidoccrm) [PowerPoint]
