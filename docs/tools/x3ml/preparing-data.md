---
sidebar_position: 2 # REPLACE NUMBER
title: "Preparing Data for 3M" # REPLACE TITLE
description: "Tips for preparing data for 3M conversion" # REPLACE with DESCRIPTION of page contents (this will appear in cards and social links and metadata about the page, keep it short)
---

<!-- @format -->

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **This page is under construction.** <i className="fa-solid fa-person-digging"></i>

:::note

The LINCS data conversion workflow documentation is not yet online.  

If you are interested in converting data with LINCS, [contact us](mailto:lincs@uoguelph.ca) to find out more and to access our full suite of data conversion resources.

:::
