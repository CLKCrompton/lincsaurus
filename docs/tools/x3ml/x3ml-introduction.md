---
title: "X3ML Introduction"
---

<!-- @format -->

LINCS 3M documentation focuses on creating data for the LINCS %%Knowledge Graph (KG)|knowledge-graph%%. We recommend you review the [official 3M manual](https://gitlab.isl.ics.forth.gr/cci/3m-docker/-/blob/master/manual.pdf) for a general introduction to 3M and for in-depth information that is outside the scope of LINCS’s documentation. [Community-made resources](/docs/tools/x3ml/#resources) are also available.

The [LINCS 3M instance](https://mapper.lincsproject.ca/3m/) comes preloaded with the %%ontology|ontology%% and generator files used for LINCS data conversion. To use the LINCS 3M instance, [register for an account](https://mapper.lincsproject.ca/3m/Registration) and wait for account approval from LINCS.

LINCS uses the [2022 version of 3M](https://gitlab.isl.ics.forth.gr/cci/3m-docker), and all screenshots are from this version. Although some external documentation contains screenshots from previous 3M versions, the information provided is still helpful.

## 3M Pages

This section gives an overview of the 3M pages you will use to convert data for publication with LINCS. It is not a comprehensive list of all the functions in 3M. More detailed instructions for 3M are available:

* The [official 3M manual](https://gitlab.isl.ics.forth.gr/cci/3m-docker/-/blob/master/manual.pdf)
* Creating a Mapping Project in 3M<!-- ADD LINK -->
* The Interface of a Mapping Project<!-- ADD LINK -->

## Project Mappings

| ![alt=""](/img/documentation/x3ml-intro-projects-(c-LINCS).png)|
|:--:|
| The **Project Mappings** page with annotations.|

The **Project Mappings** page is the screen that loads after logging in. This is where you manage your conversion projects. We refer to the conversion rules you set up for each project as a %%mapping|mapping%%.

On the **Project Mappings** page, you can:

1. See all projects in the LINCS 3M instance
2. Organize your projects
3. Open your projects
4. Make new projects by
    1. Copying existing projects
    2. Creating new projects
    3. Uploading exported 3M ZIP files

:::caution

When using the LINCS 3M instance, your files will be stored on LINCS servers. We recommend that you regularly export your 3M projects and store them locally, as LINCS does not guarantee long-term storage for 3M projects. Exported projects can be re-uploaded to 3M. More details are available in [section of Creating a Mapping Project in 3M with this info].<!-- ADD LINK -->

:::

## Files Metadata Management

| ![alt=""](/img/documentation/x3ml-intro-metadata-(c-LINCS).png)|
|:--:|
| **Files Metadata Management** window with annotations.|

The **Files Metadata Management** window loads when you open a project. You can use this window to specify what data you want to convert and what ontologies the converted data should follow.

In the **Files Metadata Management** window you can:

1. Give a title to your source data input file (your %%XML|xml%% file)
2. Change your source data input file
3. Add and remove %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%% files for your chosen target schemas (ontologies)

## Mappings Overview

| ![alt=""](/img/documentation/x3ml-intro-hrid-(c-LINCS).png)|
|:--:|
| **Mappings Overview** page with annotations. |

The **Mappings Overview** page loads when you close the **Files Metadata Management** window. You can use the Mappings Overview page to set up the rules for converting your data.

On the **Mappings Overview** page you can:

1. Change the project
    1. Title
    2. Description
2. Change the view
    1. Expand or collapse all mappings
    2. Expand or collapse a single mapping
3. Import from an X3ML file
4. Export all mapping files as a ZIP file

| ![alt=""](/img/documentation/x3ml-intro-mapping-(c-LINCS).png)|
|:--:|
| **Mappings Overview** page with annotations. |

For each mapping, you can also edit the:

1. %%Domain|domain%%
2. Link
3. Source %%Nodes|node%%
4. Generators
5. Labels
6. Variables

## Generator Definitions Manager

| ![alt=""](/img/documentation/x3ml-intro-open-(c-LINCS).png)|
|:--:|
| **Mappings Overview** page screen with **Open Generator Definition Manager** outlined. |

You can access the **Generator Definitions Manager** by clicking **Open Generator Definition Manager** (link icon) in the top-right corner.

| ![alt=""](/img/documentation/x3ml-intro-generator-(c-LINCS).png)|
|:--:|
| **Generator Definitions Manager Dialog** window with annotations. |

The **Generator Definitions Manager Dialog** window lets you specify how you want to create %%entity|entity%% labels and identifiers.

In the **Generator Definitions Manager Dialog** window you can:

1. Export definitions to a XML file
2. Use an existing generator file
3. Add new definitions to the project directly in the definitions manager
4. Delete existing definitions from your project

You can also edit generator definitions. For more information see [link to generators part of the walkthrough].<!-- ADD LINK -->

## “Produce RDF” (Transformed Output)

| ![alt=""](/img/documentation/x3ml-intro-rdf-(c-LINCS).png)|
|:--:|
| **Mappings Overview** page with **Produce RDF** outlined.|

| ![alt=""](/img/documentation/x3ml-intro-output-(c-LINCS).png)|
|:--:|
| **Transformed Output** window with annotations.|

At the bottom of the **Mappings Overview** page is the **Produce RDF** button. The mapping project is automatically saved in the LINCS 3M instance. To get your converted data out of 3M, you must produce RDF data.

On the **Mappings Overview** page and the **Transformed Output** window you can:

1. Change the %%RDF serialization|resource-description-framework-serialization%% (format) of the output
2. Download the output in your chosen format (RDF/XML, %%TTL|turtle%%, N-Triples, TriG)
