---
description: "Finalize converted data for ingestion into ResearchSpace."
title: "Linked Data Enhancement API"
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

<div className="function-button-row">
  <functionbutton buttonName="Clean"></functionbutton>
  <functionbutton buttonName="Convert"></functionbutton>
</div>

The Linked Data Enhancement API is a collection of tools used to finalize data that has been converted to %%Resource Description Framework (RDF)|resource-description-framework%% data according to LINCS %%ontological|ontology%% standards for %%ingestion|ingestion%% into [ResearchSpace](/docs/tools/researchspace).

<div className="banner">
  <img src= "/img/documentation/lincs-api-logo-dark-(c-LINCS).png" alt="" />
</div>

<div className="primary-button-row">
  <primarybutton
    link="https://gitlab.com/calincs/conversion/post-processing-service"
    buttonName="To the Tool"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/home"
    buttonName="To GitLab"
    icon={faUpRightFromSquare}></primarybutton>
</div>

## Basics

At LINCS, the Linked Data Enhancement API can be used to:

- Enhance RDF data with external rdfs:label values from %%authorities|authority-file%% such as [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page), [Getty](https://www.getty.edu/research/tools/vocabularies/), [Library of Congress (LOC)](https://authorities.loc.gov/), [Virtual International Authority File (VIAF)](https://viaf.org/), and [DBpedia](https://www.dbpedia.org/)
- Make batch replacements in the data
- Combine or split RDF files
- Clean RDF-formatted data
- Validate data by checking for invalid prefixes, checking that the file is valid RDF, and finding %%entities|entity%% that are missing rdfs:label or rdf:type values
- Convert a batch of data using an already developed [X3ML](/docs/tools/x3ml) %%mapping|mapping%% without opening the X3ML interface
- Suggest matching entities from the LINCS %%knowledge graph|knowledge-graph%% using owl:sameAs relationships
- Add temporary LINCS %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% to data to help speed up the %%entity-minting|uniform-resource-identifier-minting%% process (this only applies to datasets being prepared for LINCS)

Though some of the tools within the API may be useful for enhancing and validating data that do not adhere to LINCS ontologies, such data cannot be added to the LINCS knowledge graphs.

Users of the Linked Data Enhancement API will need to be familiar with using APIs. Users must be able to follow LINCS’s instructions to send a request to the API and run it locally. Requests can be sent by downloading the example file and the program [Postman](https://www.postman.com/), or using LINCS’s Python notebook in either Jupyter notebooks or GoogleColab. For more instructions on running and using the API, see the [GitLab wiki](https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/home).

## Prerequisites

Users of the Linked Data Enhancement API:

- Do not need a user account
- Need to come with their own RDF-formatted dataset
  - If this data is being prepared for [ResearchSpace](/docs/tools/researchspace), it must also follow LINCS ontological standards.
- Need an understanding of [Linked Data](/docs/get-started/linked-open-data-basics/concepts-linked-data)
- Need an understanding of how to use an %%Application Programming Interface (API)|application-programming-interface%%

The Linked Data Enhancement API supports the following file types:

- **Input:** RDF
- **Output:** RDF (either %%JSON|json%% or %%TTL|turtle%% depending on the endpoint used)

## Resources

To learn more about the Linked Data Enhancement API, see the following resources:

- [Linked Data Enhancement API Wiki](https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/home)

Information about the team that developed the [Linked Data Enhancement API](/docs/about-lincs/tools-credits#linked-data-enhancement-api) is available on the Tool Credits page.