---
description: "PLACEHOLDER"
title: "CWRC 2.0"
---

<!-- @format -->

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **This page is under construction.** <i className="fa-solid fa-person-digging"></i>

Information about the team that developed [CWRC 2.0](/docs/about-lincs/tools-credits#cwrc-2.0) is available on the Tool Credits page.
