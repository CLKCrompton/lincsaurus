---
description: "PLACEHOLDER"
title: "Context Plugin"
---

<!-- @format -->

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **This page is under construction.** <i className="fa-solid fa-person-digging"></i>

Information about the team that developed the [Context Plugin](/docs/about-lincs/tools-credits#context-plugin) is available on the Tool Credits page.
