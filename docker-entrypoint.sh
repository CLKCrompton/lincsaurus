#!/bin/sh

# Run a Typesense service first
./typesense-server --data-dir=$PWD/typesense-data --api-key=$TYPESENSE_API_KEY --enable-cors &

exec "$@"