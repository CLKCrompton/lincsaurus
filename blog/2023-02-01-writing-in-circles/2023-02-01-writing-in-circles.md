---
slug: writing-in-circles
title: Writing in Circles - From Beginner to Expert and Back Again
authors:
  name: Sam Peacock
  title: LINCS Undergraduate Research Assistant
tags: [digital humanities, research]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./writing-in-circles-unsplash-(cc0).png)

As an English student, I was always told to keep my writing concise. Doing so was often easy because I could assume that the person reading my work would be an English scholar, so their expectations of my writing and the knowledge they brought to it would sit within a very specific range. In this way, I trained myself to default to an academic tone and level of complexity and to produce writing that was comparative and analytical.

This summer, however, as a member of the LINCS documentation team, I worked on an entirely different kind of writing: user guides and instruction manuals for the many software tools developed or employed by the project. To begin writing documentation, I first had to relearn how to write...<!--truncate-->

Writing instructions begins with simplicity. I thought that since I was a concise writer, I would be able to write simply and clearly, but when the first draft of step-by-step instructions for [maintaining the LINCS documentation website](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/maintenance-guide) was edited, I saw that my word count had been cut nearly in half. As I scrolled through pages of my writing, I saw that the entire intention had been flipped—not just a matter of simply swapping in synonyms, but a wholesale reworking of voice and perspective.

Explaining the process of creating a merge request, for example, I first wrote this wordy and personal paragraph:

_To work on an issue, you will need to create a merge request. This will create a new “branch” on our GitLab repository, which will allow you to make and track any changes without affecting the main branch of the repository (and in turn, the website itself). Later, once you are happy with your changes, they can be added to the main branch so that they can be published to the documentation website._

Working with the documentation team to revise my first attempt, we started by recasting my draft to have an active tone. We removed excess statements like “make and track” and “in turn, the website itself,” making the whole section much shorter without compromising the quality of the information:

_Create a merge request to work on an issue. This request will make a new branch in GitLab, allowing you to make changes without affecting the main repository. When your changes are complete and checked, they can be added to the main branch to be published on the documentation website._

We developed a [style guide](https://gitlab.com/calincs/admin/lincsaurus/-/wikis/style-guide) to help direct our choices, and while it helped to keep me within the lines for the most part, I still found it hard to suppress the English-student essay writer who had made his home deep in my brain. In a way, I found writing instructions far more challenging than writing an essay. I could no longer hide any lack of clear vision or shortcomings of understanding behind words that just sounded good. If there was no clear meaning, my writing fell apart.

When I began breaking the instructions down into sections and the sections into individual steps, I saw that it was easier to write simply. Yet, my instincts were still not quite right. I was still looking for ways to make the writing flow between sections. I imagined an audience who was following me from step to step, travelling through the tasks in the same order that I was: starting at the same place and ending with the same goal. What our style guide reminded me, however, was to view the writing from the audience’s perspective. Like me, my reader could be a beginner who was new to using a platform like GitLab, and who needed to know every step along the way. But equally, my reader could be a veteran user who forgot only one step and skipped straight to a specific subheading. Each tiny section therefore needed to function as an independent piece of documentation. I couldn’t assume that my reader would have read all of the preceding sections. In adhering to this principle, I found myself having to sometimes break one of my personal golden rules of writing: avoid repetition! This process—though it created writing that repeated information that had appeared in previous sections—was helping to make these tools usable to as many people as possible.

This repetition also signalled the variation in readers we needed to write for. From the earliest days of planning our documentation strategy, we sought to identify the imaginary audience for whom we were tailoring the complexity and technicality of each piece. As a field, %%Digital Humanities (DH)|digital-humanities%% welcomes participation from those with a broad range of academic backgrounds and levels of technical expertise. We understood that we would need to cater to everyone from undergraduate arts students to experienced software developers, which meant assuming a basic level of technical ability but no more.

For my process as a writer, trying to meet the needs of such a diverse range of users meant taking another circular approach. I came to the tools I was documenting as a beginner, and as I familiarized myself with them to learn what needed to be documented, I became an expert. Then, I had to return to the role of the beginner (to the beginning of the circle), writing for someone who was new to the tool. I had to learn to use tools with which I was previously completely unfamiliar to the point of being able to write their instructions, and then immediately assume the position of my former self, who didn’t know the tools at all.

Thanks to my experience writing documentation, I now have not just a commitment to concision and simplicity, but an entirely different approach to writing. Writing documentation has pushed me to consider the audience’s point of view and to not be afraid to break some of my own writing rules. Writing in circles has taught me how to craft texts that are more accessible for my readers.
