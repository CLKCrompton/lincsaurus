---
slug: stem-vs-arts-dh
title: Balancing Your Brain - STEM vs Arts in Digital Humanities
authors:
  name: Kathleen McCulloch-Cop
  title: LINCS Co-op
tags: [digital humanities]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash.](./stem-vs-arts-dh-unsplash-(cc0).jpg)

Pop psychology has a concept most of us are familiar with: the linear, calculating left brain vs. the intuitive, creative right brain. The theory is that you either use one or the other, you’re either left-brained dominant or you’re right-brained dominant, but you can’t be both. This myth has been debunked many times over, but still you can find countless Facebook articles and Pinterest links with quizzes to tell you which side of your brain is the one in the driver’s seat. Even if you don’t buy into the left and right brain theory, we’ve still managed to try to separate logic from creativity, arts from sciences.

For me, this has always seemed a little off...<!--truncate--> it could be because I have a background in the literary arts but chose to pursue a degree in Software Engineering. I didn’t want to give up reading and writing, so I declared a minor in English. And then I found that I missed my calculator, so I declared a second minor in Mathematics. When I tell people that I love coding and calculus as much as I love literary criticism and creative writing, it’s always a bit of a shock. “Well, what are you going to do with that?” they ask. I plan to teach, but that’s not the issue here. It’s that people can’t seem to conceive of a place where both skills would be necessary.

The field of %%Digital Humanities (DH)|digital-humanities%% is one such place. It sits at the intersection between technology and the humanities. DH is not new, by any means. It has its roots way back in the 1940s and 50s, when Roberto Busa, an Italian priest, collaborated with IBM to create the Index Thomisticus, a lemmatization of the works of Saint Thomas Aquinas. A lemmatization is the process of grouping together inflected forms of a word, analyzing its meaning and part of speech not only in the sentence it appears in, but in the larger context of the surrounding sentences as well. They did this for approximately 11 million words, published in 56 volumes, allowing for unprecedented searching power throughout Aquinas’s works.

![IBM Punch Card](./-stem-vs-arts-dh-card-(cc0).jpg)

_Mid-20th century IBM punch card from [Electronic Data Processing](https://en.wikipedia.org/wiki/Electronic_data_processing) on Wikipedia._

I, however, am very new to DH. Working with the LINCS project and in the (virtual) %%THINC Lab|thinc-lab%% is my first brush with a project focused on DH. But the more I work on the project and learn what LINCS is about, the more I realize just how wrong we are to try to separate STEM from arts. I’ve always felt like they complement one another, and now I’m seeing for the first time the actual act of putting technology into work with humanities content. And honestly, as overwhelming as it seems like it would be to try to translate human language into dynamic information that is both human and machine readable, it makes sense. Why wouldn’t we be putting these incredible, rocket-science capable machines (that almost all of us have in our own homes) to work in making our lives easier and our information accessible for all fields? Why would we ever think that the technological capability for humanities research and materials stopped at online journals and downloadable PDFs of articles?

Not everyone is attempting to separate STEM from arts—there’s actually a push for students to focus on STEAM. This is STEM, inclusive of the Arts, and the focus is on inquiry and problem-based learning that you often experience when studying the arts, used in conjunction with STEM concepts. STEAM feels to me like a way to apply the foundations of DH to even more fields of scientific study, to introduce the concepts even earlier, and that is so incredibly exciting to see happen.

It is quite honestly a joyous feeling to be to work with “both sides” of my brain. To apply a computer-science structure to human-created content, to work with materials that were made for humans and turn them into something machines can read, manipulate, and analyze. It’s a testament to the possibilities that are out there for humanities researchers, to what they can do and create and work with given the right technical infrastructure and platform. It’s truly exciting work, and it requires using scientific thought and logical approaches to content that is highly creative as well as ambiguous.

Doing DH work has shown me that the separation of arts and sciences isn’t actually real, and it can’t be if we want to keep creating groundbreaking work and changing the way that we use and create content in the humanities. Having the capabilities for both arts and science is integral to both fields. Technology allows us to radically change how we interact with the humanities, and the challenge of doing so drives innovation in tech. Creating the Index Thomisticus would likely have been near impossible without the technological infrastructure—and equally impossible if approached solely from a computer science standpoint.

It was IBM’s first instance of using a computer for linguistic and lexicographic analysis, and the published work has been heralded as a complete collection of Aquinas’s knowledge. If we want to create projects as groundbreaking as Busa’s, we need to use both “sides” of our brains.

When rounding out my skills and learning to work with tech and humanities alongside each other, I’ll probably still be asked, “Well, what are you going to do with that?” But as I’m finding out more and more everyday, there isn’t a wrong answer to that question. In fact, DH can open up so many possibilities that it’s hard to pick just one answer.
