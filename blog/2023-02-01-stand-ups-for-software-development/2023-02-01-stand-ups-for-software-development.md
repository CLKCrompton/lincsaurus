---
slug: stand-ups-for-software-development
title: Stand-ups for Software Development
authors:
  name: Eason Liang
  title: LINCS Software Development Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./stand-ups-for-software-development-unsplash-(cc0).jpg)

When I began my first co-op placement, I had no idea how software development worked in a professional environment. Previously, my experience had only been in the classroom, where  my classmates and I developed software for assignments. When I moved into a professional context, I was exposed to new ways of collaborating, among them stand-up meetings, or stand-ups...<!--truncate-->

What are stand-up meetings? Stand-ups are short, daily meetings where a team meets to discuss different tasks about a project. Nowadays, stand-ups are core to lots of software development projects, but they were originally popularized in agile development, which is a well known methodology in the industry. Software development projects can be complicated, so agile development breaks complex problems down into manageable, achievable components.

Meeting daily is an excellent way to build a large project. It encourages the team to set goals that can be met in one day, and then to follow up the next day to identify problems, address issues, and sort out confusions quickly. Stand-up meetings also boost communication. They help the team become comfortable with each other through frequent communication. Stand-ups make everyone feel included.

Stand-ups keep everyone on the team productive by giving each member a daily reminder of the goals that they need to work on and how these goals fit together with the project as a whole. Although software development is often an individual effort, getting feedback from a supervisor or a peer as problems arise is an effective way to not only reduce roadblocks but also expand the team’s knowledge. Stand-ups are a great way to achieve transparency, as they encourage people to share problems and solutions.

When I started working at LINCS, I didn’t understand the value of stand-ups. But I found that the more stand-up meetings I did, the more easily I was able to look for help and the more comfortable I felt asking questions. In turn, I discovered that I was able to quickly adapt my thinking and resolve the issues I had. Stand-ups helped me to focus on the tasks at hand, and they provided me with the structure I needed to work effectively and efficiently.

Because stand-ups take place frequently, they are also meant to be very short. Thanks to the limited time that we set aside for the meetings, I learned to ask my questions concisely and to get straight to the point. It became clear to me that I find it more effective to have smaller and shorter meetings because they help me address small issues before they become big problems. I have always been a shy person, so having stand-up meetings often helped me grow more comfortable talking to my colleagues.

I thought that my co-op would teach me about software development. I did not expect to also learn so much about collaboration, teamwork, or about myself. Stand-ups have provided me with an excellent tool that I will use throughout my career.
