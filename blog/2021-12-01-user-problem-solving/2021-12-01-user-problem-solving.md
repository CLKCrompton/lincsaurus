---
slug: user-problem-solving
title: User-Centric Problem Solving
authors:
  name: Sana Javeed
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./user-problem-solving-unsplash-(cc0).jpg)

Before becoming a UX Design student at University of Toronto, I used to solve a problem just by identifying what the problem was and then coming up with a band-aid solution. However, over a period of time, I realized that by using such an approach—opting for the easiest solution—I was often actively ignoring the real problem and sometimes even allowing it to worsen...<!--truncate-->

If you popped in a painkiller every time you had a headache, you might get rid of the pain momentarily, but the headaches would continue to crop up. Is this the right approach for recurring headaches? Well, no! If you really want to get rid of your headaches, you need to find what is causing them. Are they due to vision problems? Grinding your teeth? Staring at the screen for long hours? In order to find the root cause, you need to analyze the issue by considering various angles. Only once you have done that can you begin to search for the right solution. This is how to problem solve effectively.

With no background in the humanities, my experiences at LINCS have left me in awe of the role scholars play in developing digital technologies. Scholars have collaborated on digital technologies like Voyant, ResearchSpace, HuViz, and many others to enrich research data. These tools were designed—often by humanities researchers—to help them and others collect, connect, and interpret information.

So, what exactly is the problem to solve here? LINCS’s aim is to make information even more accessible. LINCS is making tools that are open source and easily accessible for researchers, providing researchers with a starting point for their goal-specific information-seeking journeys. The problem is how to design and implement a platform that can not only cater to a diverse range of user-specific goals, but also enhance the researcher’s experience? Before jumping into brainstorming some solutions, I needed to understand the domain and where UX fits in.

![Design Thinking Illustration](./designthinking_illustration-(c-Nielsen-Norman-Group).png)

_Gibbons, S. (2016, July 31). Design Thinking 101. Retrieved March 27, 2020, from [https://www.nngroup.com/articles/design-thinking](https://www.nngroup.com/articles/design-thinking)_

When undertaking UX design, the [Nielsen Norman](https://www.nngroup.com/) design thinking model encourages designers to _empathize_ and _define_ in order to see the situation from the perspective of the users and understand the problem to be solved. This understanding can be reached by determining the _what_, _how_, and _who_. How is this done? Imagine yourself trying to buy a toy for a kid. If you are like me, then you will examine this toy to determine what precisely it is and who it caters to. To learn how the toy works and what the experience of playing with it is like, you would observe the user (a kid) to see _how_ they interact with it: What steps do they take to perform a task? How do they overcome frustration when the toy doesn’t do what they want it to? From this, you would learn _what_ the toy is, _who_ the toy is intended for, and _how_ it works for the intended audience.

I used the Nielsen Norman model to analyze the LINCS access interface. First, I learned that the who were humanities researchers, research assistants, and students. The next step was to determine the _what_ by asking the right questions:

1. What are the user goals, needs, and motivations in using a specific tool?
2. What knowledge do the users currently have, and what knowledge do they need to acquire to achieve their goal?

To answer the questions above, I first broadly categorized three different types of users: the Explorer (beginner with little to no knowledge of %%Linked Open Data|linked-open-data%%), the Contributor (curator of Linked Open Data), and finally the Coder (with basic knowledge of Linked Open Data and coding experience with JavaScript or Python).

These aren’t exact definitions, as each group’s users actually have a wide range of skills and goals. These range from just wanting to explore the dataset all the way to wanting to manipulate or even create bibliographic data.

Finally, the biggest step in problem solving is determining the _how_. Since this stage is complex and time consuming, I am still working on it. So far, I have discovered that the best approach is to listen to what the users have to say, and to study their behaviour and environment to determine their purpose. Empathizing with the users as I watch them use the tools helps me understand their struggles, determine what their problems were, and what brought them to the pain point. Listening to the user is key to finding the root problem. It is only once you have identified the problem piece of the puzzle that you can put your thinking hat on! Ideate, prototype, test, and repeat.

So now you see how the user-centric approach helps define a problem through empathy. In my experience, compassion is the best way to identify the root cause, as it means understanding and therefore treating the problem (the vision problems, the teeth grinding, the screen use) rather than its symptom (the headache). An empathetic, user-centric approach might take more time than a band-aid solution, but it also ensures that your solutions will be best suited to users and their underlying issues. This compassionate approach can be used across domains. Whether you are a doctor, engineer, teacher, customer service representative, manager, or a grocery store cashier—whatever role you play, you can solve your problems with a user-centric approach.

---

### Works Cited

Gibbons, Sarah. “Design Thinking 101.” _Nielsen Norman Group_. July 31, 2016. [https://www.nngroup.com/articles/design-thinking/](https://www.nngroup.com/articles/design-thinking/).
