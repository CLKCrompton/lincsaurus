---
slug: invisible-design
title: Invisible Design
authors:
  name: Amardeep Singh
  title: LINCS Computer Science Co-op
tags: [UX, technical]
hide_table_of_contents: true
---

<!-- @format -->

![Art Wireframe](./invisible-design-(cc0).jpeg)

“Most people make the mistake of thinking design is what it looks like. People think it’s this veneer—that the designers are handed this box and told, ‘Make it look good!’ That’s not what we think design is. It’s not just what it looks like and feels like. Design is how it works.” - Steve Jobs

If you are a developer, designer, or a creative individual interested in design, you are probably familiar with the phrase “good design is invisible.” Everything is designed—there is thought behind every project or product—but only a few things are designed well. When design is done poorly everyone tends to notice its flaws, but when design is done well it usually goes unnoticed...<!--truncate--> good design is invisible because most people don’t notice when something is easy to use, especially when it fulfils its purpose in a way that is familiar or intuitive. For example, file manager applications such as Dropbox and Google Drive benefit from the fact that most users are familiar with how file system interfaces work. They make the already-understood processes of uploading and storing files so simple that the user doesn’t think about the process behind the drag and drop functionality. Instead, it just flows naturally.

Jared Spool, the founding principal of User Interface Engineering, stated, “Good design, when it’s done well, becomes invisible. It’s only when it’s done poorly that we notice it.” I believe this statement also applies to how good design finds the perfect balance between form and function. The goal of good design should always be to make interactions with the user flow better. Therefore, a lot more than aesthetics needs to be considered when developing a good design.

#### User Flow

Another important design aspect that tends to go unnoticed is user flow. Lindsay Munro, a social strategist at Adobe, explains, “the term ‘flow’ depicts movement. Product creators need to consider the path in which the user will navigate through the website or app, prompting the creation of user flow. This path is divided into a series of steps that the user takes from the entry point through conversion funnels towards the final action” (Munro, 2020). What’s the most straightforward way for users to accomplish their intended task? This is the question designers and developers need to answer to find their user flow. When designing an interface, it is important to think about how the intended users will make the best use of what is on their screen. User flow plays an important role in guiding users towards their desired outcome. When user flow is well thought out, it is essentially invisible to the user since it creates a seamless user experience.

Therefore, developers and designers can benefit from using tools such as user flow diagrams to help guide their designs. Not only is the user flow beneficial for the end-user, but it also acts as a guide for the developers and designers when developing a product. User flow diagrams are a great way of ensuring that all the aspects of a project’s design are understood by each member of the team. With the project’s requirements being presented visually, developers no longer need to create their own mental image of the project. Instead, they have a user flow diagram that allows them to step through the design of the project, making sure they have a strong understanding of their place within the project as a whole. In addition, when visualizing a project with a user flow diagram, edge cases and missing requirements are easier to notice and solutions are easier to implement, as they can be introduced before the actual project commences.

For me, the importance of user flow diagrams became evident when I was working on the front-end of Vetting Environment and Reconciliation for Structured Data (VERSD). VERSD is a graphical interface that allows for quick manual vetting during the conversion process for structured data. The back-end of VERSD connects to the %%Application Programming Interface (API)|application-programming-interface%% for %%reconciliation|reconciliation%% but it takes inputs from the front-end interface which I helped develop. When starting out with this task, I was given an in-depth wireframe that helped me think of a user flow to move the user through the steps required to process their data. By analyzing the wireframe, I was able to understand what I needed to build and the components I would be using. The process felt seamless. The wireframe became a reference point for me and it prevented me from having to reimplement requirements.

#### Form Validation

Another design aspect that is usually invisible until it is necessary is form validation. Designing forms can be very difficult as many form components tend to work dynamically and rely on the inputs and outputs of each other. Form validation guides users with filling out form data. It is especially important for situations where the user may not be required to fill out a form—or may not be fully invested in the process. Forms such as job applications will probably have users who are mindful of the information that they are filling out, feedback surveys might have users who aren’t as engaged. In these latter contexts, features such as live feedback and hints are very useful because they help the user navigate the form. In particular, proper feedback is essential. If users don’t know what they are doing wrong—and they become stuck in the process of filling a form out—they will become frustrated and eventually leave an incomplete form behind. Feedback with proper timing and placement enhances the user experience.

My work developing interfaces for VERSD has given me first-hand experience with form validation. For VERSD, I have been developing a large form into which users enter various data, but I have made sure to keep form validation as a core concept in my designs. For example, many of my form’s components are only visible when they need to be, which helps guide the user to the next step without overwhelming them by asking for information that they may not be required—or indeed even able—to provide.

Design is more than meets the eye. It is a language that revolves around the idea of making the best possible user experience, and is not limited to only the aesthetics of an application. Invisible design may be a tough concept to grasp initially, however, once a good understanding is established it makes the process of bringing an application from an idea through to reality flawless. I have been applying the concepts of invisible design to my work for the VERSD interface, and I have also been making an effort to notice where they have been implemented in the interfaces I use to help me advance as a software developer who wants to perfect the art of creating intact user experiences.

---

### Works Cited

Munro, Lindsay. “Understanding User Journey vs. User Flow.” _Xd Ideas_. January 12, 2020. [https://xd.adobe.com/ideas/process/user-research/user-journey-vs-user-flow/](https://xd.adobe.com/ideas/process/user-research/user-journey-vs-user-flow/).
