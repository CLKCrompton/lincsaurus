---
slug: development-in-steps
title: Development in Steps - Learning to Collaborate on a Technical Project
authors:
  name: Basil Yusef
  title: LINCS Computer Science Undergraduate Research Assistant
tags: [technical]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./development-in-steps-unsplash-(cc0).jpg)

LINCS is using ResearchSpace as a platform for exploring relationships in interlinked cultural datasets. With ResearchSpace, researchers can browse, search, and visualize data in the LINCS datastore. In summer 2022, I was part of a team that was developing new features for LINCS’s version of ResearchSpace.

Our team was a collaboration between contributors with a background in user experience (UX) and contributors with a background in software development. We worked in tandem: the UX group recommended features to improve the experience of using the web application. These recommendations were turned into tickets in [GitLab](https://gitlab.com/calincs). The tickets described what the desired end product would be, and it was up to the developers to determine how to achieve this goal. The development lead shared tickets out among the software developers, dividing them up based on what would be required to build the suggested features...<!--truncate-->

Most of the tickets I was assigned were for components that required React.js. When I started with LINCS, I did not have much experience with React. While I was already familiar with React Hooks, I needed to learn the class-based React that was utilized in ResearchSpace. Often, I needed to know more than just what my ticket asked me to do. I had to understand the project’s sizable codebase and how it operated while also extending my skills. As a result, I developed expertise in TypeScript and React and I learned how Docker and Docker containers function. These things may have been confusing at the start, but now they are familiar. For example, I now have a good understanding of the functioning of Docker Compose files and I have lots of experience using Handlebar templates for bug fixes.

I learned a lot more than just technical skills. I have a lot of gratitude towards my team members. Zach Schoenberger, who supervised my work, helped me develop the crucial skill of how to explain technical concepts to non-technical people. Dawson MacPhee, a senior student developer, taught me how to approach my work. From him, I learned to start making improvements gradually, to do several tests, and to divide my objectives into manageable pieces. Dawson taught me to refer to the documentation whenever necessary and encouraged me to ask questions as I went.

Making modifications without upsetting what had previously been established was challenging, but with the skills I learned from Zach and Dawson, I was able to successfully make gradual changes. ResearchSpace has many interconnected React components. While at first I found it hard to make changes without breaking other things, over time I developed both my technical and non-technical skills, which meant I was able to turn the UX requests into functional components.
