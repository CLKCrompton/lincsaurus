---
slug: music-ontologies
title: Searching for Harmony - Questions in Ontologies for Music Data
authors:
  name: Sam Peacock
  title: LINCS Undergraduate Research Assistant
tags: [digital humanities, ontologies, research]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./music-ontologies-unsplash-(cc0).jpg)

When I began working with %%ontologies|ontology%% at the LINCS project this summer, my colleagues and I quickly found ourselves asking exasperating questions like “How do you explain the visual concepts present in an artwork to a database?” Even more broad (and maybe ultimately unanswerable) questions like “what is a thing?” also began to arise.

Soon I was assigned to music data... <!--truncate-->

As a way to begin thinking about the ontological questions present in the field, I read “A Scenes Approach to Metadata Models for Music” by Stacy Allison-Cassin. In this paper, Allison-Cassin poses further, even more complicated questions, asking whether or not the production of music should really be centered on a single “work,” as in many existing data models for music. Her paper offers an alternate model wherein music is the product of a “scene”: a complex network of interwoven influences, actors, places, history, and culture, positing that this model produces a more truthful expression of music. A compelling question for music ontologists then becomes: “How do we reflect scene in our data?”

In the same paper, Allison-Cassin mentions that the process of using %%Linked Data|linked-data%% to describe highly ephemeral and intangible concepts such as music has been continually frustrated by the lingering influence of book-based cataloguing standards. This summer, I have worked with LINCS to evaluate ontologies like %%CIDOC CRM|cidoc-crm%% and IFLA’s Functional Requirements for Bibliographic Records (FRBR) (as well as it’s object-oriented counterpart, [FRBRoo](https://cidoc-crm.org/frbroo/)) for use with music. These ontologies are largely entrenched in a museum or library standpoint, making their models often dependent on a physical “artifact” or book. Though aspects of music data such as audio recordings, their physical carriers (CDs, for example), posters, and programs can easily fit into these frameworks, what room is there for the musical performance itself, or even live improvisation? In their widely referenced paper “‘Such Stuff as Dreams Are Made On’: How Does FRBR Fit Performing Arts?” David Miller and Patrick LeBoeuf analyze these issues, attempting to find solutions within the FRBR framework. They mention that the problem of representing live art through past modeling standards is that performances are “characterized by their lack of enduring embodiment,” and as soon as one attempts to center their model through a video recording, for example, the performance is transformed from its truest original form into something “fixed,” and not at all ephemeral. This serves to “alter its very nature, turning it into a cinematographic work,” rather than a musical one (Miller & LeBoeuf, 2005, 153).

![WEMI Definition](./music-ontologies-wemi-(fair-dealing).png)

_Fig. 1. From Ore, Christian-Emil. “FRBRoo and Performing Arts,” 2015._

Of the numrous papers and projects that have attempted to model musical performance with semantic accuracy, many have turned to variations of FRBR’s Work, Expression, Manifestation, and Item (%%WEMI|wemi%%) framework for solutions. As shown in fig. 1, the Work represents an original, amorphous idea or concept, which can then be realized in an Expression—essentially the Work actualized (perhaps as a script, performance, etc.). Manifestations are then the physical creations (or set of physical objects) that embody the Expression, and an Item becomes a single physical object that represents or carries a Manifestation. Miller and LeBoeuf proposed that a performance should not be a direct Expression of a musical Work, as many had previously thought. Instead, it should be an Expression of the more specific, spatiotemporal work of a “director” (could be a stage director, conductor, etc.), which itself is based on the original Work. Though this can create a more semantically nuanced and accurate music model than had been previously possible, it is still unable to account for musical instances such as an improvisation that are characterized specifically by their lack of any direction. Allison-Cassin feels that representing these unscripted aspects of music is crucial to creating a data model that truly expresses our relationship with performance in the real world. Therefore, even advanced models such as Miller and LeBoeuf’s fall short. This is the very kind of conundrum that makes music data so difficult, yet so ontologically interesting.

![DoReMus Coltrane](./music-ontologies-doremus-(fair-dealing).png)

_Fig. 2. From Lisena, Achichi, Choffe, Cecconi, Todorov, Jacquemin, and Troncy. “Improving (Re-) Usability of Musical Datasets,” 2018._

More recent efforts have led to the development of music-specific ontologies and %%vocabularies|vocabulary%% designed to tackle these problems. [DoReMus](https://data.doremus.org/ontology/) (DOing REusable MUSical data) is one of these, built as an extension of FRBR’s object-oriented version, FRBRoo. As evidenced by Pasquale Lisena, Manel Achichi, Pierre Choffe, Cécile Cecconi, Konstantin Todorov, Bernard Jacquemin, and Raphaël Troncy in their [2018 research paper](https://www.degruyter.com/document/doi/10.1515/bfp-2018-0023/html), DoReMus has created a class called a “Performed Expression” that is distinct from a regular WEMI Expression. This class allows the data model to specifically articulate differences between the original and performed versions of a musical work, while also differentiating the performance from any recordings or direction that may have been involved in the impermanent event of its presentation. With this, DoReMus is able to create a model of musical performance that can express a live musical improvisation with relative accuracy and fullness. Crucially, the musical performance is no longer “fixed” by an origin in a bibliographic record—it is independent and free to be recorded as a purely ephemeral event. Fig. 2 shows that model, which displays a jazz performance by John Coltrane in New York, 1962.

DoReMus is by no means a perfect music ontology, and many others, such as Stanford University’s [Performed Music Ontology](http://performedmusicontology.org/), the [Swiss Performing Arts Archive](https://www.performing-arts.ch/resource/sapa:Search?semanticSearch-mainSearch=N4IgziBcoNZQTAGhANyiEyAmB7dAzAJwCNCcdIAxeAVgH0AFAU0Px0IFsBDAOwGMmjADa8QAX2T4oAbQC6yQkwjRwTbjwAuASz4BaMEy6E%2BAC10weOAO5CmWAOZNdAB15MhUYGO9A), and others provide alternate models with varying structures. Regardless of the ontology, they all continue to be based in established library or archival structures. Even the most semantically complete ontologies seem to focus on single works as the origin of their music. They are then unable to fulfill Allison-Cassin’s vision for music data that incorporates networks of influences and the scene, which have been so crucial to the creation of musical works throughout history. The question is then: Can legacy methods of data modeling be sufficiently improved through new ontologies like DoReMus or improvements to older ones like FRBRoo? Or, must we move these Linked Data standards entirely beyond their roots? Structures for music data and the researchers who build them continue to push boundaries in the field, creating exciting opportunities to branch out into structures that could even defy the static nature of books and bibliographic records.

---

### Works Cited

Allison-Cassin, Stacey. “A Scenes Approach to Metadata Models for Music.” _Journal of Library Metadata_ 16, no. 3-4 (2016): 181-201. [https://doi.org/10.1080/19386389.2016.1258891](https://doi.org/10.1080/19386389.2016.1258891).

Lisena, Pasquale, Manel Achichi, Pierre Choffe, Cécile Cecconi, Konstantin Todorov, Bernard Jacquemin, and Raphaël Troncy. “Improving (Re-) Usability of Musical Datasets: An Overview of the DOREMUS Project.” _BIBLIOTHEK Forschung und Praxis_ 42, no. 2 (2018). [https://doi.org/10.1515/bfp-2018-0023](https://doi.org/10.1515/bfp-2018-0023).

Miller, David and Patrick Le Boeuf. “‘Such Stuff as Dreams Are Made On’: How Does FRBR Fit Performing Arts?” _Cataloging & Classification Quarterly_ 39, no. 3-4 (2005): 151-178. [https://doi.org/10.1300/J104v39n03_10](https://doi.org/10.1300/J104v39n03_10).

Ore, Christian-Emil. “FRBRoo and Performing Arts.” _CIDOC Conference_. September 6, 2015. [https://slideplayer.com/slide/13579103/](https://slideplayer.com/slide/13579103/).
