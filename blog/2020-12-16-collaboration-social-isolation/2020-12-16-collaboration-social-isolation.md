---
slug: collabortation-social-isolation
title: Collaboration in Times of Social Isolation
authors:
  name: Thomas Smith
  title: LINCS Undergraduate Research Assistant
tags: [CWRC, digital humanities]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./collaboration-social-isolation-unsplash-(cc0).jpg)

To avoid the COVID-19 outbreak, I moved back home from Guelph to the town of Bowmanville, a community with a population of around 40,000 people. My family sold our home of twenty years just before the pandemic, buying a quaint little house in Minden, Ontario, a town with only 4,000 residents. I consider myself lucky for moving back with my family when the outbreak began to worsen, even if I have gone from high-speed internet on the University of Guelph’s campus to fighting for bandwidth with the neighbours. Even if I also need to check for bears when leaving the house...<!--truncate-->

For the past two years I have been an Undergraduate Research Assistant in the %%THINC Lab|thinc-lab%%. The THINC Lab is a room situated on the second floor of the Mclaughlin Library at the University of Guelph. There, I have met dozens of scholars from around the globe who have come to THINC Lab in search of collaboration, to assist in fellows’ research, and to give talks in the great glass box we call the THINC Lab. The THINC Lab provides researchers a space for collaboration, development of digital tools, and the training in using digital tools. It also hosts the DH@Guelph series and partner events.

Despite my being in the middle of the woods, the camaraderie of the THINC Lab remains. Through Google Meet, Slack channels, and photos of our [THINC Lab mugs](https://twitter.com/search?q=%23showusyourMUG%20%40dhatguelph&src=typed_query) shared on social media, we remain connected as a big academic family. With scholars associated with the lab having diverse interests, one day you can talk to a Computer Science professor about your favourite hacker TV shows, whereas another you could talk to an English professor about the history of bookbinding. Everyone in the THINC Lab shares their knowledge to foster community and to teach others. One description I feel encapsulates the THINC Lab is on its website, where it is described as a safe space to “take risks, fail, and so to learn.”

For the past few weeks, the other Undergraduate Research Assistants and I have been trained on using the tools of the [Canadian Writing Research Collaboratory (CWRC)](https://cwrc.ca/). What would have been meetings around a large table in the THINC Lab have become virtual sessions in Google Meet. Asking coworkers questions requires waiting for a Slack message reply, and instead of being able to gather around a computer, we now must set up screen sharing sessions. Although our methods of collaboration have changed, the spirit of the THINC Lab still feels the same. I can talk to my fellow co-workers just as if I would be talking to them in the lab. In these budding phases of my academic career, I still feel a sense of community in which I can share my ideas with fellow researchers, even if I am 260km away.

The same collaborative environment I have experienced in the THINC Lab has been created online through CWRC. The diversity of projects is shown on the [Projects page](https://cwrc.ca/projects). In what other space could you learn about a cabaret the same time you learn about Canada’s involvement in the Spanish Civil War? In the same way that anyone could come to the THINC Lab to hear about other people’s projects and become involved, anyone can message a researcher of a CWRC project to express interest in collaborating. The transition from a physical space to a digital one has been fully embraced by those associated with the THINC Lab.

Even though we cannot see each other in person, we remain in a very connected world. You never know the situations that your colleagues are facing these days. Maybe take a minute to contact your work friends, colleagues, or other people that have been important in your life or research. I would like to extend the THINC Lab’s model of inclusivity into people’s temporary workspaces, as well as remind everyone to welcome everyone’s differences as a source of intellectual and cultural strength.
