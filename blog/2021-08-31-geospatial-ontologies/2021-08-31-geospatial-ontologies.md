---
slug: geospatial-ontologies
title: The Shifting Landscape of Geospatial Ontologies
authors:
  name: Thomas Smith
  title: LINCS Undergraduate Research Assistant
tags: [CWRC, ontologies]
hide_table_of_contents: true
---

<!-- @format -->

![Pin Map](./geospatial-ontologies-unsplash-(cc0).jpeg)

Over the many years I have worked on the LINCS project, I encountered many new terms. Sitting at my desk in %%THINC Lab|thinc-lab%%, I used to hear people discussing something called %%ontologies|ontology%%. I would listen in to meetings about the creation of the [CWRC ontology](https://sparql.cwrc.ca/ontologies/cwrc.html), and eventually I was asked to contribute by reading over the definitions proposed for it. Thinking that this would be the entirety of my ontology experience, I did my job but did not dig into any of the underlying technical concepts...<!--truncate-->

My ignorance about the broader concepts related to ontologies came to an end recently, when I took up a role with the LINCS ontology team in summer 2021. My minor is in Geographic Information Systems and Environmental Analysis, so I was asked to investigate ontologies to support LINCS’s geospatial data.

This was a great opportunity for me. I was fortunate to be able to connect something new with geospatial data, which I already knew a lot about. I need not have been so worried about what I did not know, though: it turns out that there is a very active spatial history community, with plentiful documentation written on the subject to guide those like me who are new to the field.

In joining the conversation, my interest was caught by one of the biggest issues facing ontologies: how do we represent changing places over time? I say _places_ specifically, as there is a push in this field to separate the concept of _place_ from that of _space_—the most common explanation being that all places consist of space, but must be named and inhabited by humans to be a place.

The idea that places change over time can be shown by looking at the city of Helsinki, Finland, which has changed hands many times throughout its history. It was under the control of the Kingdom of Sweden before 1809, then Russia, and then the Finnish Socialist Workers’ Republic for a brief period in 1918 during the Finnish revolution. How can historians map a city with such an inconsistent history? Although gazetteers are great sources of historical information, they are static and do not convey change over time.

A similar problem exists when trying to represent places that exist within a continuously changing surface area, such as [the death of Lord Nelson at sea](https://en.wikipedia.org/wiki/Horatio_Nelson,_1st_Viscount_Nelson#Wounding_and_death). The location in which Lord Nelson died is historically significant, but it is difficult to determine the geographical location at which his death occurred. The ship he was on was in constant motion, so would the point of his death be a spot on the surface of the water? Or is the location the seafloor directly below the ship?

Solutions to this problem are offered by a variety of geospatial ontologies, but I believe that %%CIDOC CRM|cidoc-crm%%’s [CRMgeo](https://cidoc-crm.org/crmgeo/home-5) captures all the desired aspects of spatial history; it is also the most commonly accepted method of representing changing places over history. With Lord Nelson’s ship, the different events that make up the battle in which he died are divided into subclasses, such as the identifier [crm:E93_Presence](https://cidoc-crm.org/Entity/E93-Presence/version-7.1.1) showing the presence of specific ships at crucial moments in the battle, or a [crm:E5_Event](https://cidoc-crm.org/entity/e5-event/version-7.1) representing the sinking of Lord Nelson’s ship. This representation of the fight is human-understandable, and its temporal and spatial presence can be represented separately. Phenomenal places of the ship’s current, modern-day location may be designated, while the declarative places represent the approximated location found on enemy sea charts. More declarative places can be represented in the form of a polygon, such as the area that contains the ship’s sunken remains; [geo:SP7_Declarative spacetime volume](https://ontome.net/class/351) introduced in CRMgeo allows objects to have identities in relation to their space and time on Earth, and [geo:SP1_Phenomenal spacetime volume](https://ontome.net/class/345/namespace/9) allows events and other persistent things for which an an existence is fuzzy compared to other phenomena.

While I am still very new to ontologies, I have really enjoyed researching how people represent spaces—and places—important to them. The examples above fascinate me, and I am intrigued by the advancement in humanity’s understanding of the relationship between space and time. I am still not able to keep up with the technical jargon of some articles, but I am grateful that the LINCS team allowed me to explore this evolving field.
