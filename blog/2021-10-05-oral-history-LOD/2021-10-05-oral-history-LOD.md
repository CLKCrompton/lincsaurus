---
slug: oral-history-LOD
title: Connections are Important! Linked Open Data and Oral History
authors:
  name: Gracy Go
  title: LINCS Undergraduate Research Assistant
tags: [digital humanities, linked open data, research]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./oral-history-LOD-unsplash-(cc0).jpg)

History has always been something I’ve been passionate about, and as an undergraduate student approaching graduation, I’ve become more eager to find ways to preserve primary sources. From my experience, having access to primary sources makes the researching process a lot easier, and these sources would not exist if there weren’t proper measures to preserve them.

If you’ve ever taken a history class, you probably know about the distinction between primary and secondary sources, and how primary sources are integral to the reliability of any history paper or assignment. Among primary sources in history, one type that goes largely unnoticed and can actually be hard to define is oral history...<!--truncate-->

The best description I’ve been able to find for oral history is from the [Oral History Association](https://www.oralhistory.org/about/do-oral-history/) website: “... a field of study and a method of gathering, preserving, and interpreting the voices and memories of people, communities, and participants in past events.”

In many cases, oral histories offer a “history from below” perspective. “History from below” refers to history as told by your everyday person who has firsthand experience of events. This is my favourite type of history since they oftentimes touch on social themes and have the potential to educate others on the perspectives of marginalized communities. I’ll touch on an example of this later on in this post.

I’ve worked with all sorts of archival material, from audiovisual sources such as oral interviews to scans of handwritten documents from the late 19th to 20th centuries. What I have never looked at in my four years of undergraduate study is the topic of %%Linked Open Data (LOD)|linked-open-data%%. What I have been realizing these past few months, and have been increasingly amazed by, is the way these two things can benefit one another. In my first week of working with LINCS, I learned that LOD exists pretty much everywhere on the Internet. Like many university students, I often go down various Wikipedia rabbit holes. It was only very recently that I found out that behind the scenes there is a plethora of metadata underpinning these articles. Websites such as the %%Wikimedia Foundation|wikimedia-foundation%% and %%Wikidata|wikidata%% provide information to create linkable metadata in archival collections or repositories.

A simple Google search will provide you with an example of what LOD can do. For example:

![Sylvia Plath](./oral-history-LOD-plath-(c-LINCS).jpg)

This side bar or Google Knowledge Panel that comes up when you search for a person, a band, a movie, and many other things is LOD! I used to think it was just a convenient way of finding quick tidbits on whatever I was researching, which is definitely something I took for granted prior to learning LOD and how it could help to contextualize oral histories. I never considered the work that goes on behind the scenes to make links to related terms until I started working with LOD myself, where I experienced how tedious of a process it actually was.

What I think is fascinating is how LOD can provide so much information—there is a seemingly unlimited amount of information on the Web. The accessibility to a wealth of knowledge and how these connections work with one another is one of the most important things with the kind of work I want to do as an aspiring curator or archivist.

So, how does LOD link with oral histories and other primary sources?

Well, it provides a way to link similar concepts to one another, such as places, dates, and people. As you can see in the example above, Sylvia Plath is linked to relevant information such as where and when she was born, when she died, and the works she is known for.

This same thing can be applied to oral histories. As an example, I’m going to refer you to a large repository of archival material that encompasses all of Europe, the [European Holocaust Research Infrastructure](https://www.ehri-project.eu/) (EHRI). EHRI collects historical and archival material related to the Holocaust. Their goal is to connect Holocaust material that exists in European countries to produce one large repository for containing this material. As you would imagine, this is a lot of material, but any piece within the EHRI collection is just as important as every other and should be included to provide researchers with the greatest amount of information and fullest context. The EHRI could benefit greatly from the use of LOD, as it would allow researchers to explore material across the breadth and depth of the Holocaust.

Nowadays, many repositories and museum collections are digitized and readily available online. LOD is particularly useful in these cases because it makes it possible for the user to obtain as much information—or as specific information—as they want. But in order to get the most out of LOD, collections must be interoperable—meaning collections across institutions must use the same standards to allow for shared searchability, cataloguing, interfacing, and many other aspects.

I mention the EHRI repository specifically because the Holocaust shrouds all of Europe, and there must be many survivors and relatives who wish to find information on what they or their parents, grandparents, and great-grandparents lived through. Knowing that this information is out there, and not entirely accessible to those who are seeking it, means that there is a long way to go to help people find parts of their past. When I mention accessibility, I don’t mean just for researchers or other scholars, I mean for the people that have firsthand experience of these events who are looking for this information. And those people, these events, the material in the EHRI is to help serve researchers AND the oppressed whose histories are histories from below.

Information is out there, and collections like the EHRI strive to put it in one place—but how can things be found if they’re not connected? Linked Open Data can be one way of solving it.
