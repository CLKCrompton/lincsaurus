---
slug: design-deep-dive
title: The Design Deep Dive
authors:
  name: Farhad Omarzad
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./design-deep-dive-(cc0).jpg)

During my time at LINCS, the UX team has laid the groundwork for implementing an intuitive and effortless user flow for ResearchSpace. To accomplish this, we have conducted numerous user and usability tests, interviews, and card sorts.

Working on ResearchSpace was my first real experience in the world of UI/UX. It was also my first time working on a design team, and I was blown away by the talent of my peers...<!--truncate--> I enjoyed the challenge of learning new UX concepts and learning about the team’s design and user testing process. [Evan Rees](/blog/2022-03-23-tube-map-diverged/2022-03-23-tube-map-diverged.md), for one, had amazing skill when it came to answering and asking questions during a user test without pushing any sort of bias or leading. For instance, instead of asking a user, “Does this make you feel frustrated?” when they were clearly frustrated, Evan would ask them, “How does this make you feel?” instead. Evan’s carefully worded question allowed the user to answer honestly without being pushed in a certain direction. Jordan Lum and [Nem Brunell](/blog/2022-04-12-speaking-in-different-languages/2022-04-12-speaking-in-different-languages.md) both taught me a lot about how to use Figma to organize my work and create components.

My previous experience in design had only taught me about creating something that would be visually pleasing. Working as a UX designer changed my perspective drastically. I learned to put myself in the shoes of the user—to try to understand what they would want out of a design. Adding in the new elements of usability and practicality elevated my design thinking skills drastically. For example, when I create designs now, I determine the mood I want the viewer to feel and then I choose colours, typography, and so on accordingly. Even as I go back to traditional design work after this work term, I will continue to think about how typography, colour choices, and text placement will be perceived by the viewer.

One challenge I faced while working as part of a UX and broader development team was learning to accept that my ideas may not always be practical enough for implementation. There were instances of me thinking I had come up with a design that would solve a user’s problems, only for it to be shot down in a second. For example, when the UX team was trying to solve the problem of hover-dependent icons on ResearchSpace cards for tablet users, I proposed a dropdown menu built into the entity card as a solution. However, if a dropdown menu had been implemented, all the cards on the screen would have had to move down the screen to make room, which would have been difficult to implement. The UX team ended up adding the icons on the bottom of the card without altering the card dimensions—a solution that could be implemented more practically! I have never taken these critiques personally and am happy to say that these experiences helped me develop my creative problem solving skills.

Working on the UX Design team was a rewarding experience that has already helped me land a design job for the upcoming fall and winter semester at the University of Guelph. I have had the pleasure of meeting so many talented people and I am beyond grateful that I had the opportunity to work at LINCS!
