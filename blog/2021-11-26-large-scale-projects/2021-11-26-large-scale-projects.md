---
slug: large-scale-projects
title: Diving into Large-Scale Projects as a Complete Novice
authors:
  name: Dawson MacPhee
  title: LINCS Computer Science Co-op
tags: [technical]
hide_table_of_contents: true
---

<!-- @format -->

![Roozebeh Eslami Unsplash](./large-scale-projects-unsplash-(cc0).jpg)

When beginning my very first co-op job search, I had no idea what employers would expect from me. It was a daunting task looking through the job postings and deciding what I thought I’d be (somewhat) qualified for. After a few job interviews, I applied to join the LINCS project, feeling confident that my skills aligned with the job requirements. Eventually, I was offered and accepted a job from LINCS, beginning a journey that has challenged my technical abilities every day and has expanded far past what I originally thought I’d be doing...<!--truncate-->

My contributions to LINCS have been very diverse in scope. I’m proud to say that I’ve worked on many different aspects of the project, and in doing so have drawn on many different skills: I’ve made use of three different programming languages and countless libraries, frameworks, %%Application Programming Interfaces (APIs)|application-programming-interface%%, and software. Through this experience, I’ve gained some insights into what it’s like to use and contribute to large-scale software projects that are mid-development. I’m sharing these insights here for other developers following in my footsteps—both at LINCS and as contributors to other large-scale projects.

When joining a project that’s already started, always begin by reading documentation, and if possible, talking with someone already familiar with the project. You shouldn’t be studying documentation like it’s a textbook and trying to remember every little thing either. Your first goal in contributing to or using a project is to gain a high-level overview and learn about its structure. You can always reference specific parts of the documentation later, but to start all you want to do is learn (generally) where everything is and what it does.

From there, the next step should be to get the project running locally and to set up your dev environment (or to just practice using the software if it is hosted elsewhere). A lot of times, just learning how to run a project, view its logs, and mess around with the product can contribute greatly to your overall understanding. Depending on the project, it may be a challenge to get things running locally, as there may be dependencies and other requirements for you to deal with. This could be the first of a lot of troubleshooting you will have to do, which will lead to further dives into the documentation—and eventually a greater understanding of the project.

The first two steps could be a slow process. Honestly, the initial work can be boring, and at times it is very tempting to just start making changes. Yet, it is important to take your time and fully understand how the pre existing code base relates and interacts with your goals.

What you do next is where things get malleable. If you plan on contributing, start making changes slowly, running lots of tests, and breaking your goals down into small chunks. Reference documentation as needed, and if possible, ask questions to someone knowledgeable in the project as you go. Your challenge is to make changes without breaking what’s already established.

It sounds strange, but as a developer, one of your main goals is to make as few changes as possible on your way to creating the desired outcome. Reuse or repurpose what’s already there as much as you possibly can. You don’t want to create new assets or functions/methods that already exist elsewhere, because the practice of duplicating work can get out of hand very quickly if not kept in check. Not to mention that if you do, most likely you will end up being asked to remove any work that repeats what exists elsewhere, wasting your time and the team’s.

These are the steps that I use every time I have to learn about or contribute to a new project: reading documentation, talking with and asking questions to previous developers, setting up a local dev environment, and then incrementally making changes while being mindful of the scope of your task. This can be a slow process, but rushing things without fully understanding the project can lead to headaches in refactoring or repurposing later on. Take the time required up front to gain the confidence and knowledge, so that you can use or change the software down the road as efficiently as possible. Following and practising these steps can boost your efficiency greatly. I have found that learning how to approach new projects is an important skill, and is definitely one that improves with practice.
