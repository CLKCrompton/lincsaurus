---
slug: speaking-in-different-languages
title: Speaking in Different Languages - Working Across Groups and Disciplines
authors:
  name: Nem Brunell
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Windows Unsplash](./speaking-in-different-languages-unsplash-(cc0).jpeg)

The situation is this: you’ve been asked to design a way for a researcher to easily move between two tools. You familiarize yourself with both tools, learn about the known issues, and read about what’s already been tried. You spend a few days deciding on the best way to get from point A to point B. You spend a few more days designing how it will look. Finally, you unveil your prototype in a meeting with the development team ... only to find out that moving between the tools is not technically possible. Or that it would take too long to build your design. Or that moving between tools opens up new issues on the back end.

How do you go forward? ...<!--truncate-->

LINCS’s User Experience (UX) team is a collection of designers and developers who work in tandem to make using LINCS tools smoother for users, working on wide-ranging topics like [system structures](/blog/tube-map-diverged), [improving access](/blog/user-problem-solving), or [usability testing](/blog/design-frolics). The team meets twice weekly to prevent situations like the above. By connecting often, the team is able to keep up to date on what everyone is working on and can then weigh in on the positives and potential challenges of each others’ projects.

Robin Bergart, UX Librarian at the University of Guelph, has been with LINCS since April 2021. She considers the UX meetings essential. “Different people with different skills will bring different observations. It’s surprising when they see things you haven’t.” When developers and designers share their plans and are generous with their skills, they’re able to make sure that changes are possible (and desired!) before any one team member spends hours on something that is not likely to work.

[Dawson MacPhee](/blog/large-scale-projects), who has been working as a developer with LINCS since May 2021, enjoys using his technical knowledge to help the UX team make informed design choices. Being involved in design also keeps him engaged: “If I were building an interface and wasn’t allowed to contribute to the design, it’d be harder to find interest and motivation.”

UX meetings are not without their challenges, however. The gap between what designers may want to do and what developers are able to do can be difficult to bridge. Justifying why they are saying no to a proposed design can be hard for developers: “It’s a challenge to convince the group that something wouldn’t be worth our time,” says Dawson. “It can be a challenge to find the right words for things in non-technical terms.”

A project of any scale benefits when its participants come together to share their knowledge and resources. “We’re able to have more thoughtful interactions ... because we have a shared context,” says Alliyya Mo, a developer with LINCS since May 2020. “Still, missing context sometimes leads to misunderstandings. But by getting to a shared consensus with the language we use, we can better communicate ideas, leading to a better product.”

The UX team brings its diverse strengths to work on many parts of a complex whole—meeting as a group not only allows us to combine our skills to create better tools for LINCS, but it also allows us to bring what we’ve learned back to our individual work.

By working together, the situation above turns from a design failure to a collaborative opportunity. Bringing the project to the team at an earlier stage opens the discussion up to learning about the limitations of a design so we can learn about new options, not stop in our tracks. We are able to make stronger, more informed design choices by recognizing the process as something we all are a part of. By having a space to share with and learn from each other, we are able to make sure that everyone has a hand in the work and the understanding needed to develop something meaningful.
