/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  // By default, Docusaurus generates a sidebar from the docs folder structure
  // tutorialSidebar: [{ type: "autogenerated", dirName: "." }],

  // exploreDataSidebar: [{ type: "autogenerated", dirName: "explore-data" }],
  // createDataSidebar: [{ type: "autogenerated", dirName: "create-data" }],
  // toolsSidebar: [{ type: "autogenerated", dirName: "tools" }],
  // aboutLincsSidebar: [{ type: "autogenerated", dirName: "about-lincs" }],
  // getStartedSidebar: [{ type: "autogenerated", dirName: "get-started" }],
  termsSidebar: [{ type: "autogenerated", dirName: "terms" }],

  // Sidebar test below
  exploreDataSidebar: [
    {
      type: "category",
      label: "Explore Data",
      collapsible: true,
      collapsed: false,
      link: { type: "doc", id: "explore-data/explore-data" },
      items: [
        {
          type: "autogenerated",
          dirName: "explore-data",
        },
      ],
    },
  ],
  createDataSidebar: [
    {
      type: "category",
      label: "Create Data",
      collapsible: true,
      collapsed: false,
      link: { type: "doc", id: "create-data/create-data" },
      items: [
        {
          type: "autogenerated",
          dirName: "create-data",
        },
      ],
    },
  ],
  toolsSidebar: [
    {
      type: "category",
      label: "Tools",
      collapsible: true,
      collapsed: false,
      link: { type: "doc", id: "tools/tools" },
      items: [
        {
          type: "autogenerated",
          dirName: "tools",
        },
      ],
    },
  ],

  aboutLincsSidebar: [
    {
      type: "category",
      label: "About LINCS",
      collapsible: true,
      collapsed: false,
      link: { type: "doc", id: "about-lincs/about-lincs" },
      items: [
        {
          type: "autogenerated",
          dirName: "about-lincs",
        },
      ],
    },
  ],

  getStartedSidebar: [
    {
      type: "category",
      label: "Get Started",
      collapsible: true,
      collapsed: false,
      link: { type: "doc", id: "get-started/get-started" },
      items: [
        {
          type: "autogenerated",
          dirName: "get-started",
        },
      ],
    },
  ],
  templateSidebar: [
    {
      type: "category",
      label: "Templates",
      collapsible: true,
      collapsed: false,
      link: { type: "doc", id: "templates/templates" },
      items: [
        {
          type: "autogenerated",
          dirName: "templates",
        },
      ],
    },
  ],



};

module.exports = sidebars;
