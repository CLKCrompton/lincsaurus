var _paq = window._paq = window._paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function () {
  var u = "//matomo.lincsproject.ca/";
  _paq.push(['setTrackerUrl', u + 'matomo.php']);
  _paq.push(['setSiteId', '3']);
  var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
  g.async = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
})();

let currentTitle = document.title;
const observer = new MutationObserver(function (mutations) {
  if (document.title !== currentTitle) {
    currentTitle = document.title;
    currentUrl = document.location.href;
    _paq.push(['setReferrerUrl', currentUrl]);
    _paq.push(['setCustomUrl', currentUrl]);
    _paq.push(['setDocumentTitle', currentTitle]);

    // remove all previously assigned custom variables
    _paq.push(['deleteCustomVariables', 'page']);
    _paq.push(['trackPageView']);

    // make Matomo aware of newly added content
    var content = document.getElementById('content');
    _paq.push(['MediaAnalytics::scanForMedia', content]);
    _paq.push(['FormAnalytics::scanForForms', content]);
    _paq.push(['trackContentImpressionsWithinNode', content]);
    _paq.push(['enableLinkTracking']);
  }
});

const config = { subtree: true, childList: true };
observer.observe(document, config);

window.addEventListener('beforeunload', function (event) {
  observer.disconnect();
});
