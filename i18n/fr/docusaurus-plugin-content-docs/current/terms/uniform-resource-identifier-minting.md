---
id: uniform-resource-identifier-minting
title: Frappe d'identificateur de ressource uniforme (URI)
hoverText: Processus de création d'un nouvel identificateur de ressource uniforme (URI) pour représenter une entité.
---

<!-- @format -->

En mappant des données semi-structurées et structurées en %%Linked Open Data (LOD)|linked-open-data%%, un certain nombre de nouvelles %%entités|entity%% représentant des événements, des activités et d'autres conteneurs de données sont générés pendant data %%conversion|conversion%% et %%ingestion|ingestion%%. Pour éviter d'utiliser %%nœuds vides|blank-node%%, nouveau %%Les identificateurs de ressources uniformes (URI)|uniform-resource-identifier%% doivent être créés pour représenter ces nouvelles entités et permettre la construction de nouvelles relations complexes entre les entités représentées par des URI d'origine commune. used %%espaces de noms|namespace%%.

## Exemples

- Projet AdArchives : L'URI suivant, créé par LINCS, est une entité `crm:E33_Linguistic_Object` référençant le texte d'une publicité pour _Women's Studies International Quarterly_.

`lincs:gs2EnZaEjmk`

- University of Saskatchewan Art Gallery : L'URI suivant, créé par LINCS, porte l'étiquette « buff writing paper », utilisée pour former une déclaration de matériaux pour l'objet 1985.007.079 de l'Université de la Saskatchewan.

`lincs:tfi3YsPWqES`

## Autres ressources

- WC3 (2012) [“223 Best Practices URI Construction”](https://www.w3.org/2011/gld/wiki/223_Best_Practices_URI_Construction)
