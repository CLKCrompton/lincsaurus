---
id: international-image-interoperability-framework
title: Cadre international d'interopérabilité des images (IIIF)
hoverText: Ensemble d'outils et de normes qui rendent les images numériques interopérables, fournissant une méthode normalisée de description et de diffusion d'images en ligne.
---

<!-- @format -->

L'International Image Interoperability Framework (IIIF) est un ensemble d'outils et de normes qui rendent les images numériques interopérables, fournissant une méthode normalisée de description et de diffusion d'images en ligne. IIIF permet la livraison de métadonnées d'image et de structure et de présentation, structurées comme %%Données ouvertes liées (LOD)|linked-open-data%% utilisant généralement JSON-LD.

IIIF comprend deux principaux %%Interfaces de programmation d'applications (API)|application-programming-interface%% : une API d'image et une API de présentation. Les formats d'image sont livrés dans un %%Chaîne URL (Uniform Resource Locator)|uniform-resource-locator%% qui spécifie le format de l'image finale, garantissant la cohérence entre les plates-formes. L'API de présentation donne des informations contextuelles, au format JSON-LD, et comprend des métadonnées telles qu'une étiquette de titre, la séquence de l'image dans son contexte d'origine et d'autres informations de ce type. Ces API sont réalisées dans un logiciel compatible IIIF, tel que [Universal Viewer](http://uvviewsoft.com/uviewer/).

## Exemples

\* IIIF (2017) ["Qu'est-ce que l'IIIF ?"](https://www.youtube.com/watch?v=8LiNbf4ELZM) : l'image suivante illustre la compatibilité IIIF, en affichant les informations de présentation à gauche et les informations d'image sur la droite.

![alt=""](/img/documentation/glossary-iiif-example-(fair-dealing).png)

## Autres ressources

- Cramer (2017) [“03 Introduction to IIIF”](https://www.youtube.com/watch?v=EE1YskDrzPs) [Vidéo]
  \* IIIF (2017) ["Qu'est-ce que l'IIIF ?"](https://www.youtube.com/watch?v=8LiNbf4ELZM) [Vidéo]
- [Cadre international d'interopérabilité des images (Wikipedia)](https://en.wikipedia.org/wiki/International_Image_Interoperability_Framework)
- GitHub Inc (2022) ["Cadre international d'interopérabilité des images"](https://github.com/iiif)
