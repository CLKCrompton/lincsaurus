---
id: reification
title: Réification
hoverText: Le processus de concrétisation d'un concept abstrait, comme prendre la notion d'une relation et la considérer comme une entité ou exprimer quelque chose à l'aide d'un langage de programmation, afin qu'il puisse être manipulé par programmation.
---

<!-- @format -->

La réification est le processus de concrétisation d'un concept abstrait. Dans la modélisation conceptuelle et de données, ce processus reviendrait à prendre quelque chose d'abstrait comme la notion de relation et à la considérer comme une %%entité|entity%%% : conversion d'une idée en une chose logique ou conceptuelle. Ce faisant, vous pouvez manipuler par programmation le concept abstrait et lui attacher des informations supplémentaires. La réification consiste à exprimer quelque chose à l'aide d'un langage de programmation afin qu'il puisse être manipulé et traité par ce langage : ainsi, exprimer quelque chose à l'aide du %%Resource Description Framework (RDF)|resource-description-framework%% afin qu'il puisse devenir traitable.

## Exemples

- Exprimer des relations parent-enfant (_isA_) : vous pouvez décrire cette relation dans le concept abstrait d'un arbre, puis construire un objet Tree pour l'exprimer, réifiant ainsi le concept abstrait de cette relation arborescente en un objet Tree manipulable.
- Exprimer des relations d'appartenance à un groupe : vous pouvez avoir des classes de groupe (_Committee_) et individuelles (_Person_) avec des concepts abstraits qui décrivent la relation (_Membership_) entre les deux. Pour réifier cela, vous déclareriez une méthode (_isMemberOf_) qui indique si une instance _Person_ est membre d'un _Committee_, rendant ainsi explicite et manipulable la notion abstraite d'une relation d'appartenance à un groupe.

## Autres ressources

- Berners-Lee (2017) [« Réifier RDF (correctement) et N3 »](https://www.w3.org/DesignIssues/Reify.html)
- [Réification (Wikipédia)](<https://en.wikipedia.org/wiki/Reification_(computer_science)>)
- Techopedia (2022) [« Réification »](https://www.techopedia.com/definition/21674/reification)
