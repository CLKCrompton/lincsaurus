---
id: internationalized-resource-identifier
title: Identifiant de ressource internationalisé (IRI)
hoverText: Identifiant qui s'appuie sur le protocole URI (Universal Resource Identifier) ​​en élargissant le jeu de caractères autorisés pour inclure la majeure partie du jeu de caractères universel.
---

<!-- @format -->

Les identificateurs de ressources internationalisés, ou IRI, étendent le protocole Universal Resource Identifier en élargissant le jeu de caractères autorisés pour inclure la plupart du jeu de caractères universel. En internationalisant le jeu de caractères disponible pour créer %%Uniform Resource Identifiers (URI)|uniform-resource-identifier%%, IRI a encore internationalisé Internet en permettant aux utilisateurs d'identifier les ressources Web dans leur propre langue.

## Autres ressources

- Dürst & Suignard (2005) [“Internationalized Resource Identifiers (IRIs)”](https://www.researchgate.net/publication/200034559_Internationalized_Resource_Identifiers_IRIs)
- [Identifiant de ressource internationalisé (Wikipédia)](https://en.wikipedia.org/wiki/Internationalized_Resource_Identifier)
- W3C (2005) [“World Wide Web Consortium Supports the IETF URI Standard and IRI Proposed Standard”](https://www.w3.org/2004/11/uri-iri-pressrelease)
