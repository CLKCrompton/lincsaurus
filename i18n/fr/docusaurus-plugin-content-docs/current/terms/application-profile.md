---
id: application-profile
title: Profil d'application
hoverText: Un schéma composé d'éléments de métadonnées tirés d'un ou plusieurs espaces de noms, ainsi que des politiques et des directives liées à leur utilisation, préparé pour une application particulière.
---

<!-- @format -->

Un profil d'application est un schéma composé d'éléments de métadonnées tirés d'un ou plusieurs %%espaces de noms|namespace%%, ainsi que les politiques et directives relatives à leur utilisation, préparées pour une application particulière. Il reflète un ensemble de décisions enregistrées concernant une cible de données partagées pour une communauté donnée.

Pour %%Linked Open Data (LOD)|linked-open-data%% projets, un profil d'application doit fournir des détails sur le %%ontologie|ontology%% (classes et %%propriétés|property%%), %%vocabulaires|vocabulary%% et formats de données. Ils doivent documenter la façon dont les données sont modélisées, déclarer quoi %%entités|entity%% et %%properties|property%% seront utilisés et déclareront les types de données pour les valeurs de chaîne, ainsi que des notes d'application pour une utilisation cohérente des champs et des propriétés. Un profil d'application doit clarifier les attentes concernant les données ingérées, traitées et gérées, documenter les modèles et les normes, et noter où la mise en œuvre s'écarte des normes communautaires ([UCLA Library](https://guides.library.ucla.edu/semantic-web/bestpractices)).

## Exemples

- [Spécification des chemins sémantiques CHIN/DOPHEDA](https://chin-rcip.github.io/collections-model/en/semantic-paths-specification/current/introduction)
- [Documentation artistique liée](https://linked.art/model/)
- [Documentation SARI](https://docs.swissartresearch.net)

## Autres ressources

- [Profil d'application (Wikipédia)](https://en.wikipedia.org/wiki/Application_profile)
- Dublin Core Metadata Innovation (2009) [« Lignes directrices pour les profils d'application Dublin Core »](https://www.dublincore.org/specifications/dublin-core/profile-guidelines/)
- Sanderson (2021) ["L'illusion de grandeur : confiance et croyance dans les données ouvertes liées au patrimoine culturel"](https://www.youtube.com/watch?v=o4TzXMz7GBA) [Vidéo]
- Thompson & Sanderson (2021) ["LUX : Illuminating the Collections of Yale's Museums... From Prototypes to Production"](https://www.youtube.com/watch?v=C4lAJHOs1gY) [Vidéo]
- Bibliothèque UCLA (2017) [« Web sémantique et données liées : meilleures pratiques et normes »](https://guides.library.ucla.edu/semantic-web/bestpractices)
- W3C (2022) _[Profile Guidance : W3C Editor's Draft 12 May 2022](https://w3c.github.io/dxwg/profiles/)_
