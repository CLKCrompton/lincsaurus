---
id: linked-open-data
title: Données ouvertes liées (LOD)
hoverText: Données liées et utilisant des sources ouvertes.
---

<!-- @format -->

Les données ouvertes liées (LOD) sont des données qui répondent aux exigences des deux %%Données liées (LD)|linked-data%% et Open Data : elles sont à la fois liées et utilisent des sources ouvertes.

Le niveau de détail 5 étoiles décrit les données qui permettent d'obtenir une liaison et une ouverture idéales :

1. ☆ : Rendez vos contenus disponibles sur le Web sous une licence ouverte
2. ☆☆ : Rendez-le disponible sous forme de données structurées (par exemple, Excel au lieu d'un scan d'un tableau)
3. ☆☆☆ : Utilisez des formats non propriétaires (par exemple, CSV au lieu d'Excel)
4. ☆☆☆☆ : Utilisez %%Uniform Resource Locator (URL)|uniform-resource-locator%% pour identifier les choses, afin que les gens puissent pointer vers vos affaires
5. ☆☆☆☆☆ : reliez vos données aux données d'autres personnes pour fournir un contexte

## Exemples

- [Cloud de données ouvertes lié](https://lod-cloud.net/)

## Autres ressources

- Blaney (2017) [« Introduction aux principes des données ouvertes liées »](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- Données ouvertes 5 étoiles (2015) ["Données ouvertes 5 étoiles"](https://5stardata.info/en/)
- Uyi Idehen (2019) ["Qu'est-ce que le cloud de données ouvertes liées et pourquoi est-il important?"](https://medium.com/virtuoso-blog/what-is-the-linked-open-data-cloud-et-pourquoi-est-ce-important-1901a7cb7b1f)
