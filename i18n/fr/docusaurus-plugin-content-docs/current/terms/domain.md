---
id: domain
title: Domaine
hoverText: L'une des deux entités d'un triplet, représentant le sujet dans une relation sujet-prédicat-objet.
---

<!-- @format -->

Le domaine est l'un des deux %%entités|entity%% dans un %%triple|triple%%, représentant le sujet dans une relation sujet-prédicat-objet. In %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%, la portée (rdfs:domain) et l'ensemble des valeurs autorisées (rdfs:range) sont définis pour un %%property|property%%. Dans sa forme la plus simple, le domaine est la classe pour laquelle la propriété est définie, avec le %%range|range%% (ou objet) comprenant une valeur pour le domaine. Un exemple simple pourrait être qu'un chien est un type de corgi, le chien représentant le domaine, la propriété étant une relation de type, et le corgi représentant la plage de cette propriété « type ».

Dans %%CIDOC CRM|cidoc-crm%%, les noms de propriété sont censés être lus en se déplaçant d'un domaine à l'autre, mais il est finalement possible de lire et de construire des triplets d'une plage à l'autre en utilisant un inverse propriété (donnée entre parenthèses ou construite à l'aide d'un i pour indiquer l'inverse). Passer de domain-predicate-range à range-predicate-domain est analogue au passage d'une voix active à une voix passive dans une construction de phrase.

## Exemples

- Dans l'extrait suivant de Turtle, _LIII Frederico Elodi_ est le domaine dans la relation de prédicat (P102_has_title), ce qui signifie qu'il est le sujet de ce triplet.

```texte
<https://saskcollections.org/kenderdine/Detail/objects/2840>
un crm:E22_Man-Made_Object ;
rdfs:label "LIII Frederico Elodi"@fr ;
crm:P102_has_title <http://id.lincsproject.ca/FOuyTD5XkxE> ;
crm:P108i_was_produced_by
<http://id.lincsproject.ca/e8g2kwVOiQE> .
```

## Autres ressources

- CIDOC CRM (2021) _[Volume A : Définition du modèle conceptuel de référence du CIDOC, 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
- [Domaine d'une fonction (Wikipédia)](https://en.wikipedia.org/wiki/Domain_of_a_function)
- Mitchell (2013) [« Chapitre deux : éléments constitutifs des données ouvertes liées dans les bibliothèques »](https://journals.ala.org/index.php/ltr/article/view/4692/5585)
