---
id: property-graph
title: Graphique de propriété
hoverText: Un graphe où les relations (propriétés) entre les entités sont nommées et portent certaines propriétés définies qui leur sont propres, étendant la base de données de graphes de base des triplets liés pour montrer des connexions complexes qui décrivent comment différents types de métadonnées sont liés.
---

<!-- @format -->

Un graphe de propriétés (également connu sous le nom de graphe de propriétés étiquetées) est un type de modèle de graphe où les relations (%%propriétés|property%%) entre %%les entités|entity%% sont nommées et portent leurs propres propriétés définies. Il étend la base %%base de données de graphes|graph-database%% de %%triples|triple%% pour montrer des connexions complexes qui décrivent comment différents types de métadonnées sont liées, y compris une modélisation plus poussée des dépendances de données. Cependant, les graphes de propriétés n'ont pas le même degré de sophistication que %%les graphes de connaissances|knowledge-graph%%, qui ont tendance à utiliser un schéma plus formalisé, non local %%Uniform Resource Identifiers (URI)|uniform-resource-identifier%%, et sont conçus (en théorie) pour permettre de fédérer des données sur plusieurs ensembles de données.

## Autres ressources

- Foote (2022) [« Graphes de propriétés vs graphes de connaissances »](https://www.dataversity.net/property-graphs-vs-knowledge-graphs/)
- Knight (2021) [« Qu'est-ce qu'un graphique de propriété ? »](https://www.dataversity.net/what-is-a-property-graph/)
