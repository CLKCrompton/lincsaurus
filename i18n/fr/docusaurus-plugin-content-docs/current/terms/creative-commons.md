---
id: creative-commons
title: Creative Commons (CC)
hoverText: Une organisation à but non lucratif qui fournit des licences gratuites afin que les gens puissent accorder une autorisation de droit d'auteur à leur travail de manière standardisée.
---

<!-- @format -->

Creative Commons (CC) est une organisation à but non lucratif qui fournit des licences gratuites afin que les gens puissent accorder une autorisation de droit d'auteur à leur travail de manière standardisée.

Toutes les licences CC sont une combinaison des autorisations suivantes :

- BY : le crédit doit être attribué au créateur
- SA : Les adaptations doivent être partagées dans les mêmes conditions
- NC : Seules les utilisations non commerciales de l'œuvre sont autorisées
- ND : aucune modification ou adaptation de l'œuvre n'est autorisée

Il existe six licences CC différentes :

- CC PAR
- CC BY-SA
- CC BY-NC
- CC BY-NC-SA
- CC BY-ND
- CC BY-NC-ND

Creative Commons offre également l'option CC0, une alternative "sans droits réservés" à l'utilisation d'une licence.

## Autres ressources

- [Creative Commons (Wikipédia)](https://en.wikipedia.org/wiki/Creative_Commons)
- Creative Commons (2022) [« À propos des licences CC »](https://creativecommons.org/about/cclicenses/)
