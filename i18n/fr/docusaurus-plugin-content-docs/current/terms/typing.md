---
id: typing
title: Dactylographie
hoverText: Utiliser un chemin standardisé pour relier toute entité à un vocabulaire, thésaurus ou ontologie externe au sein de CIDOC CRM.
---

<!-- @format -->

Dans %%CIDOC CRM|cidoc-crm%%, en saisissant un %%entity|entity%% décrit l'utilisation d'un chemin standardisé pour lier n'importe quelle entité à un %%vocabulary|vocabulary%% source, %%thesaurus|thesaurus%%, ou %%ontologie|ontology%% en liant un E1_CRM_Entity à un E55_Type en utilisant le %%propriété|property%% P2_has_type. La plupart des ontologies sont destinées à se concentrer sur les entités (classes) et les relations (propriétés) de haut niveau nécessaires pour décrire l'infrastructure de données, et ne se spécialisent pas au-delà de cet objectif immédiat. Par conséquent, une classe attribuée par le type d'entité peut ne pas être suffisamment spécifique pour décrire l'entité pour les utilisateurs finaux. En attribuant un type via une liaison à des sources de vocabulaire spécifiques à un domaine ou à un vocabulaire local conçu pour un ensemble de données spécifique, une granularité suffisante est introduite dans l'ensemble de données pour garantir la convivialité pour les utilisateurs.

## Exemples

- Galerie d'art de l'Université de la Saskatchewan : l'extrait TTL suivant fait référence à une œuvre d'art. Un type lui est attribué à l'aide de crm:P2_has_type, qui spécifie que cette entité est une photographie, une œuvre d'art et une photographie en couleur, en utilisant le [Getty Art and Architecture Thesaurus](https://www.getty.edu/ recherche/outils/vocabulaires/aat/).

```texte
<https://saskcollections.org/kenderdine/Detail/objects/4201> a crm:E22_Human-Made_Object ;
rdfs:label "Gyhldeptis, Esprit du Cèdre"@fr ;
crm:P2_has_type <http://vocab.getty.edu/aat/300046300> ,
<http://vocab.getty.edu/aat/300133025> ,
<http://vocab.getty.edu/aat/300128359> .
```

## Autres ressources

- CIDOC CRM (2021) _[Volume A : Définition du modèle conceptuel de référence du CIDOC, 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
