---
id: relational-database
title: Base de données relationnelle
hoverText: Une base de données qui stocke les données sous forme de tableau (colonnes et lignes), où les colonnes contiennent des attributs de données (tels que les types de données), les lignes contiennent des « enregistrements » et les relations sont définies entre les tables à l'aide de règles.
---

<!-- @format -->

Une base de données relationnelle est un type de base de données qui stocke les données sous forme de tableau (colonnes et lignes). Les bases de données relationnelles sont basées sur le modèle de données relationnelles, permettant ainsi une méthode et un langage d'interrogation cohérents : %%Langage de requête structuré (SQL)|structured-query-language%%. Dans les bases de données relationnelles, les colonnes contiennent des attributs de données (tels que le type de données) et les lignes contiennent des instances appelées «enregistrements». Les relations peuvent être définies entre les tables à l'aide de règles telles que les contraintes de clé étrangère qui déclarent qu'une valeur dans une table doit provenir d'une valeur existante dans une table distincte. Les tables peuvent être liées entre elles (associées) sur la base de données communes pour prendre en charge les requêtes sur plusieurs tables.

| Base de données relationnelle                            | Base de données NoSQL                                  |
| -------------------------------------------------------- | ------------------------------------------------------ |
| Faible flexibilité, structure élevée                     | Grande flexibilité, faible structure                   |
| Efficace pour le stockage de données sur un seul serveur | Conçu pour une utilisation sur des systèmes distribués |
| Langage de requête unique : SQL                          | Langage de requête spécifique au produit               |

## Exemples

- [Microsoft Access](https://www.microsoft.com/en-ca/microsoft-365/access)
- [MySQL](https://www.mysql.com/)
- [DB Oracle](https://www.oracle.com/ca-en/database/)
- [PostgreSQL](https://www.postgresql.org/)
- [SQLite](https://www.sqlite.org/index.html)

## Autres ressources

- Ahuja & Vasudevan (2022) [« Bases de données et SQL pour la science des données avec Python »](https://www.coursera.org/learn/sql-data-science)
- Codecademy (2022) ["Qu'est-ce qu'un système de gestion de base de données relationnelle ?"](https://www.codecademy.com/articles/what-is-rdbms-sql)
- [Base de données relationnelle (Wikipédia)](https://en.wikipedia.org/wiki/Relational_database)
