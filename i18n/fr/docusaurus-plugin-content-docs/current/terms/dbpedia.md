---
id: dbpedia
title: DBPedia
hoverText: Un projet qui crée des données structurées accessibles au public pour le cloud Linked Open Data (LOD).
---

<!-- @format -->

DBPedia est un projet qui crée des données structurées accessibles au public pour le %%Données ouvertes liées (LOD)|linked-open-data%% Cloud. Il met principalement à disposition Wikipédia transformé et %%Wikimedia Foundation|wikimedia-foundation%% projette dans les données de %%triples|triple%%, rendant ces informations disponibles dans un %%graphique de connaissances|knowledge-graph%% librement accessible à tous sur le Web. DBPedia est géré depuis 2014 par l'[Association DBPedia](https://www.dbpedia.org/), qui est actuellement affiliée à [Institute for Applied Informatics (InfAI)](https://infai.org/en/) à l'Université de Leipzig. À ses débuts, il était affilié à l'Université libre de Berlin, où il a été lancé en 2010 sous les auspices du Web Based Systems Group.

## Exemples

- DBPedia (2022) ["Dernières versions principales"](https://www.dbpedia.org/resources/latest-core/)
- DBPedia (2022) ["Ensembles de données individuels populaires"](https://www.dbpedia.org/resources/individual/)

## Autres ressources

- [DBPedia (Wikipédia)](https://en.wikipedia.org/wiki/DBpedia)
- DBPedia (2022) ["À propos de DBPedia"](https://www.dbpedia.org/about/)
- Uyi Idehen (2016) ["Qu'est-ce que DBPedia et pourquoi est-ce important?"](https://medium.com/openlink-software-blog/what-is-dbpedia-and-why-is-it-important-d306b5324f90)
