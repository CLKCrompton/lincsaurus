---
id: uniform-resource-identifier
title: Identifiant de ressource uniforme (URI)
hoverText: Un moyen fiable et utilisable d'identifier une entité unique afin que plusieurs ensembles de données provenant de diverses sources puissent indiquer qu'ils font tous référence à la même chose.
---

<!-- @format -->

Un identificateur de ressource uniforme (URI) est un moyen de représenter un %%entité|entity%% d'une manière fiable et utilisable par d'autres. En nommant une entité non pas par un nom local mais par référence à son URI d'autorité, plusieurs ensembles de données provenant de diverses sources peuvent indiquer qu'ils font tous référence à la même chose. La façon la plus simple d'expliquer un URI est à travers un exemple. Prenons l'exemple d'URI pour Rembrandt : <http://vocab.getty.edu/page/ulan/500011051>. La partie la plus granulaire de cet URI est le numéro (500011051), également appelé le chemin. Sans contexte, ce numéro n'identifie pas de manière unique le Rembrandt que vous essayez de décrire. L'URI complet fournit ce contexte nécessaire : il est clair que vous utilisez le vocabulaire Getty Union List of Artist Names comme fichier d'autorité (vocab.getty.edu/page/ulan/), et c'est dans ce contexte que le numéro, et l'URI complet, devient un moyen unique d'identifier quelque chose. En plus du chemin, les URI ont souvent un fragment, qui est un identifiant qui suit un symbole dièse (#) placé à la fin du chemin dans l'URI, pour pointer vers une partie spécifique d'un document.

Bien que cet exemple ressemble à un %%Uniform Resource Locator (URL)|uniform-resource-locator%%, un URI n'a pas besoin de ressembler à ceci : il doit simplement être unique de manière fiable. De plus, bien que les formats d'URI puissent être identiques à une URL, ils n'ont pas besoin d'être liés à une page Web ou d'avoir un emplacement réseau⁠ - toutes les URL sont des URI mais toutes les URI ne sont pas des URL car une URI peut décrire n'importe quoi, mais une URL décrit uniquement l'emplacement de quelque chose sur le Web. En pratique, cependant, toutes les URL ne servent pas bien d'URI aux fins de %%Données ouvertes liées (LOD)|linked-open-data%%, car certaines sont plus persistantes et font autorité que d'autres. Par exemple, l'utilisation de l'URL du profil [LinkedIn](https://www.linkedin.com/) d'une personne n'est pas aussi souhaitable que l'utilisation sur [identifiant ORCID](https://orcid.org/) ou l'utilisation d'un un lien vers un fichier PDF téléchargeable d'un article à partir de la page Web du département d'un membre du corps professoral n'est pas une bonne pratique par rapport à l'utilisation d'un %%Identifiant d'objet numérique (DOI)|digital-object-identifier%% pour le même article. Si un URI se résout sur une page Web, cela signifie qu'il s'agit de %%dereferencable|dereferenceable%%.

Un URI ne devrait pas changer, et les URI devraient être conçus avec trois choses à l'esprit : simplicité, stabilité et facilité de gestion.

## Exemples

- [The Getty Art & Architecture Thesaurus Online](https://www.getty.edu/research/tools/vocabularies/aat/) : ["URI des noms de naissance"](http://vocab.getty.edu/aat /300404681)
- [Library of Congress Genre/Form Terms](http://id.loc.gov/authorities/genreForms.html): [“Genre Form URI”](http://id.loc.gov/authorities/genreForms/ gf2011026387)
- [Homosaurus](https://homosaurus.org/) : ["URI de l'identité sexuelle"](http://homosaurus.org/terms/sexualIdentity)

## Autres ressources

- Blaney (2017) [« Introduction aux principes des données ouvertes liées »](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- Guide numérique IONOS (2020) ["URI : l'identificateur de ressource uniforme expliqué"](https://www.ionos.ca/digitalguide/websites/web-development/uniform-resource-identifier-uri/)
- [Identifiant de ressource uniforme (Wikipédia)](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier)
