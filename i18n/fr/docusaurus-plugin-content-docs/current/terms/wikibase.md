---
id: wikibase
title: Wikibase
hoverText: Une suite de logiciels de base de connaissances gratuits et open source pour stocker, gérer et accéder aux données ouvertes liées (LOD), écrites et utilisées par le projet Wikidata.
---

<!-- @format -->

Wikibase est une suite de logiciels de base de connaissances gratuits et open source pour stocker, gérer et accéder aux %%Linked Open Data (LOD)|linked-open-data%%, écrit à l'origine pour et toujours utilisé par %%Wikidonnées|wikidata%%. Après le succès de Wikidata, Wikibase a été mis gratuitement à la disposition d'autres %%Projets de données liées (LD)|linked-data%%.

## Exemples

- [Enslaved](https://enslaved.org) (fonctionne sur la plateforme Wikibase)
- [Wikibase](https://wikiba.se)
- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) (projet original développé sur la plateforme Wikibase)

## Autres ressources

- MediaWiki (2021) [“Wikibase/FAQ”](https://www.mediawiki.org/wiki/Wikibase/FAQ#:~:text=General-,What%20is%20the%20difference%20between%20Wikibase%20and%20Wikidata%3F,base%20that%20anyone%20can%20edit.)
- MediaWiki (2022) [“Wikibase/Fédération”](https://www.mediawiki.org/wiki/Wikibase/Federation)
- Bibliothèque UCLA (2017) [« Web sémantique et données liées : Wikibase »](https://guides.library.ucla.edu/semantic-web/wikidata)
