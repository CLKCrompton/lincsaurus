---
id: iconography-authority
title: Iconography Authority (IA)
hoverText: One of five Getty Vocabularies that contains Uniform Resource Identifiers (URIs) for proper names, relationships, themes, and dates related to iconographical narratives, legendary or fictional characters, historical events, literary works, and performing art.
---

<!-- @format -->

The Iconography Authority (IA) is one of five Getty Vocabularies developed by the Getty Research Institute to provide terms and %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% for concepts and objects related to the arts. LINCS uses IA to provide URIs for proper names, relationships, themes, and dates related to iconographical narratives, legendary or fictional characters, historical events, literary works, and performing art.

The IA includes terms relating to:

- Events, themes, and narratives from history, religion, and mythology
- Legendary and fictional characters
- Legendary and fictional places
- Themes from literature and performing arts
- Names not covered by the other Getty Vocabularies

The IA excludes names covered by the other Getty Vocabularies.

## Further Resources

- [Iconography Authority (IA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- J. Paul Getty Trust (2022) [“About CONA and IA”](https://www.getty.edu/research/tools/vocabularies/cona/about.html)
