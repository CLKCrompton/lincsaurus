---
id: property
title: Propriété
hoverText: Une relation spécifiée entre deux classes ou entités, comme le prédicat dans un triplet (sujet-prédicat-objet).
---

<!-- @format -->

Une propriété définit une relation spécifiée entre deux classes ou %%entités|entity%%. Dans les déclarations `<subject><predicate><object>` (%%triples|triple%%) qui comprennent le %%Web sémantique|semantic-web%%, la propriété est le prédicat ou « verbe » dans la phrase et est définie en référence à la fois au sujet (%%domaine|domain%%) et objet (%%range|range%%) du %%triple|triple%%.

## Exemples

- L'exemple suivant montre la propriété, ou le prédicat, reliant deux entités ou classes dans un triple énoncé.

![alt=""](/img/documentation/glossary-property-example-(c-LINCS).jpg)
