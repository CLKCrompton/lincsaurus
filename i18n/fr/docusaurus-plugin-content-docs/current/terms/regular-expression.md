---
id: regular-expression
title: Expression régulière (Regex)
hoverText: Syntaxe pouvant être utilisée dans les langages de programmation pour rechercher, manipuler ou remplacer des modèles dans des textes.
---

<!-- @format -->

Les expressions régulières (Regex) ne sont pas un langage de programmation. Au lieu de cela, il suit une syntaxe utilisée dans de nombreux langages différents pour rechercher, manipuler ou remplacer des modèles dans des textes. Le mot "modèle" est important pour comprendre Regex : Regex est un moyen pour vous de spécifier les modèles de données que vous recherchez, ainsi chaque fois qu'une certaine chose se produit, comme des lettres existant dans le même ordre (formant ainsi un mot), il peut être trouvé. Regex peut être utilisé pour nettoyer des données, valider des données, spécifier les modèles de données que vous souhaitez extraire d'une page Web, transformer des données, extraire des données, etc.

## Exemples

- [RegexOne](https://regexone.com/)
- [Expressions régulières](https://www.regular-expressions.info/)
- [RexEgg](https://www.rexegg.com/regex-quickstart.html)

## Autres ressources

- Fox (2017) [« Tutoriel Regex - Une feuille de triche rapide par exemples »](https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285)
- [Expression régulière (Wikipédia)](https://en.wikipedia.org/wiki/Regular_expression)
- Turner O'Hara (2021) [« Nettoyage du texte OCR avec des expressions régulières »](https://programminghistorian.org/en/lessons/cleaning-ocrd-text-with-regular-expressions)
