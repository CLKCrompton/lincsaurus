---
id: optical-character-recognition
title: Reconnaissance optique de caractères (OCR)
hoverText: La conversion automatique d'images de mots en un fichier texte que les utilisateurs peuvent ensuite rechercher et modifier.
---

<!-- @format -->

La reconnaissance optique de caractères est la conversion d'images de mots en un fichier texte que les utilisateurs peuvent ensuite rechercher et modifier. Il peut être utilisé pour créer des fichiers texte à partir d'images de texte manuscrit, de documents numérisés, d'images contenant des mots ou de toute autre forme de document non textuel contenant visuellement des mots.

## Exemples

- Numérisation pour créer du texte ou des versions consultables de livres ([Project Gutenberg](https://www.gutenberg.org/), [Google Books](https://books.google.com/))
- Prendre une photo d'un chèque pour le déposer
- Reconnaissance automatique des plaques d'immatriculation des véhicules pour le péage électronique ([Autoroute 407](https://www.on407.ca/en/tolls/tolls/tolls-explained.html))

## Autres ressources

- Computerphile (2017) [« Reconnaissance optique de caractères (OCR) »](https://www.youtube.com/watch?v=ZNrteLp_SvY) [Vidéo]
- Khandelwal (2020) ["Une introduction à la reconnaissance optique de caractères pour les débutants"](https://towardsdatascience.com/an-introduction-to-optical-character-recognition-for-beginners-14268c99d60)
- [Reconnaissance optique de caractères (Wikipédia)](https://en.wikipedia.org/wiki/Optical_character_recognition)
