---
id: nosql-database
title: Base de données NoSQL
hoverText: Base de données modélisée d'une manière différente des relations tabulaires utilisées dans une base de données relationnelle, telle qu'une base de données de documents, un magasin clé-valeur, un magasin à colonnes étendues ou une base de données de graphes.
---

<!-- @format -->

Les bases de données NoSQL font référence à des bases de données qui fournissent un stockage de données modélisées d'une manière différente des relations tabulaires utilisées dans %%bases de données relationnelles|relational-database%%. La partie "Non" du terme est mieux comprise comme signifiant "pas seulement" par opposition à pas du tout. Les bases de données NoSQL peuvent prendre en charge des données structurées et relationnelles et %%Langages de requête de type SQL|structured-query-language%%, mais ils ne sont pas liés uniquement à cette méthode. Les bases de données NoSQL sont utilisées pour le Big Data et sont donc conçues pour être rapides et évolutives.

Il existe quatre principaux types de bases de données NoSQL :

1. Bases de données documentaires : les données sont stockées dans des "documents" similaires à JSON
2. Magasins clé-valeur : les valeurs de données sont accessibles par des clés
3. Magasins à colonnes larges : les données sont stockées dans des colonnes comme un magasin clé-valeur à deux dimensions
4. %%Bases de données de graphes|graph-database%% : les données sont stockées dans %%nœuds|node%%, %%edges|edge%%, et %%propriétés|property%%

| Base de données relationnelle                            | Base de données NoSQL                                  |
| -------------------------------------------------------- | ------------------------------------------------------ |
| Faible flexibilité, structure élevée                     | Grande flexibilité, faible structure                   |
| Efficace pour le stockage de données sur un seul serveur | Conçu pour une utilisation sur des systèmes distribués |
| Langage de requête unique : SQL                          | Langage de requête spécifique au produit               |

## Exemples

- [Apache Cassandra](https://cassandra.apache.org/) (magasin à colonnes larges)
- [MongoDB](https://www.mongodb.com/) (base de données de documents)
- [Redis](https://redis.io/) (magasin clé-valeur)

## Autres ressources

- MongoDB (2022) [“Qu'est-ce que NoSQL?”](https://www.mongodb.com/nosql-explained)
- [NoSQL (Wikipédia)](https://en.wikipedia.org/wiki/NoSQL)
- Yegulalp (2017) [« Qu'est-ce que NoSQL ? Bases de données pour un avenir à l'échelle du cloud »](https://www.infoworld.com/article/3240644/what-is-nosql-databases-for-a-cloud-scale-future.html)
