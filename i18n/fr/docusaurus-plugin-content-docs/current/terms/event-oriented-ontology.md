---
id: event-oriented-ontology
title: Ontologie orientée événement
hoverText: Une ontologie qui utilise des événements pour relier les choses, les concepts, les personnes, le temps et le lieu.
---

<!-- @format -->

%%l'ontologie|ontology%% utilise des événements pour relier les choses, les concepts, les personnes, le temps et le lieu ([Fichtner & Ribaud](https://www.researchgate.net/publication/267462327_Paths_and_Shortcuts_in_an_Event -Orienté_Ontologie)). En revanche, les ontologies orientées objet utilisent des objets comme connecteurs, imitant la pensée humaine dans ses connexions, par exemple, entre un objet et son créateur. Pour une ontologie orientée événement, cette relation serait modélisée à l'aide d'un événement pour connecter les deux - dans ce cas, un événement de production - tandis que dans une ontologie orientée objet, le fabricant serait directement connecté à l'objet sans cet événement de production intermédiaire. disponible pour représenter les détails autour de cet événement en relation avec le créateur et l'objet.

L'utilisation d'une ontologie orientée événement permet la création de métadonnées qui fournissent un chemin plus complet des activités humaines, permettant ainsi un meilleur apprentissage automatique. De plus, la modélisation explicite des événements conduit à une intégration plus complète des informations culturelles tout en permettant l'intégration, la médiation et l'échange d'ensembles de données hétérogènes sur le patrimoine culturel.

## Exemples

- Doerr (2003) [“The CIDOC Conceptual Reference Module An Ontological Approach to Semantic Interoperability of Metadata”](https://ojs.aaai.org//index.php/aimagazine/article/view/1720): Le diagramme suivant capture la modélisation des événements qui représentent la rencontre du pape Léon Ier et d'Attila le Hun.

![Avec l'événement de réunion au centre, ce graphe RDF montre sa connexion à d'autres événements et personnes.](/img/documentation/glossary-event-oriented-example-(cc-by).png)

## Autres ressources

- CIDOC CRM (2021) _[Volume A : Définition du modèle conceptuel de référence du CIDOC, 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_v.7.1.1_0.pdf)_
- Fichtner & Ribaud (2012) [« Paths and Shortcuts in an Event-Oriented Ontology »](https://www.researchgate.net/publication/267462327_Paths_and_Shortcuts_in_an_Event-Oriented_Ontology)
