---
id: authority-record
title: Notice d'autorité
hoverText: Un identificateur de ressource uniforme (URI) stable et persistant pour un concept dans l'écosystème des données liées (LD).
---

<!-- @format -->

Une notice d'autorité est une %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% pour un concept dans le %%Écosystème de données liées (LD)|linked-data%%. En tant qu'ensemble de termes, les notices d'autorité représentent un type de %%vocabulaire contrôlé|controlled-vocabulary%%.
