---
id: virtual-international-authority-file
title: Fichier d'autorité international virtuel (VIAF)
hoverText: Un service qui regroupe les catalogues de nombreuses bibliothèques nationales et divers fichiers d'autorité.
---

<!-- @format -->

Le Virtual International Authority File (VIAF) est un service qui regroupe les catalogues de nombreuses bibliothèques nationales et divers %%enregistrements d'autorité|authority-record%% pour fournir un accès pratique aux principaux fichiers d'autorité de noms dans le monde. Au LINCS, VIAF est utilisé comme source de %%Uniform Resource Identifiers (URI)|uniform-resource-identifier%% pour les personnes et pour les entités bibliographiques pendant %%rapprochement|reconciliation%%.

## Autres ressources

- OCLC (2022) [« Présentation du VIAF »](https://www.oclc.org/en/viaf.html)
- [VIAF : Le fichier d'autorité international virtuel](https://viaf.org/)
