---
id: inferencing
title: Inférence
hoverText: La découverte automatisée de nouveaux faits générés à partir de triplets existants.
---

<!-- @format -->

L'inférence implique la découverte automatisée de nouveaux faits basés sur une combinaison de données et de règles. Inférence avec %%triples|triple%% signifie que des procédures automatiques peuvent générer de nouveaux faits à partir de triplets existants. L'inférence nous permet de dire, par exemple, que puisqu'une instance est membre d'un ensemble de sous-classes, elle est également membre de la classe mère. Si vous dites que Susan est un corgi et qu'un corgi est une sous-classe (dans ce cas, une race) de chien, il s'ensuit que Susan le corgi est un chien. L'inférence permet d'améliorer la qualité des données en découvrant de nouvelles relations et en analysant automatiquement le contenu des données. L'inférence est parfois appelée _raisonnement_.

## Exemples

- Corgi est une sous-classe de chien, et les instances de la sous-classe corgi doivent également être considérées comme des membres de la classe parent chien. L'inférence suivante nous permet de voir quelles entités sont membres des classes parentes lorsqu'elles ont été modélisées uniquement en tant que membres d'une sous-classe. Vous pouvez poser des questions telles que "combien de chiens sont dans cet ensemble de données" et faire compter Susan, sans avoir besoin de la modéliser explicitement comme un chien et pas seulement comme un corgi.

```texte
Susan est un corgi +
le corgi est un chien >
Susan est un chien
```

- L'inférence suivante nous permet d'appliquer des règles pour aider à nettoyer notre ensemble de données. Si une série de conditions est remplie (dans ce cas, si deux personnes ont le même nom, adresse e-mail et adresse personnelle), alors une certaine signification de la relation entre les deux entités est dérivée (elles font en fait référence à la même personne et devraient seront probablement réconciliés).

```texte
SI personneA.nom == personneB.nom
personA.email Address == personAL.email Address +
adresse personAL.home == adresse personne.home
LA personne = personne
```

## Autres ressources

- Ontotext (2022) ["Qu'est-ce que l'inférence?"](https://www.ontotext.com/knowledgehub/fundamentals/what-is-inference/)
- [Raisonneur sémantique (Wikipédia)](https://en.wikipedia.org/wiki/Semantic_reasoner)
- W3C (2015) [“Inférence”](https://www.w3.org/standards/semanticweb/inference)
