---
id: sparql-endpoint
title: Point de terminaison SPARQL
hoverText: Emplacement sur Internet identifié par une URL (Uniform Resource Locator) et capable de recevoir et de traiter des requêtes SPARQL, permettant aux utilisateurs d'accéder à une collection de triplets.
---

<!-- @format -->

Un point de terminaison SPARQL est un emplacement sur Internet, plus formellement décrit comme un point de présence sur un réseau HTTP, qui est identifié par une %%Uniform Resource Locator (URL)|uniform-resource-locator%% et est capable de recevoir et de traiter %%Requêtes SPARQL|sparql-protocol-and-rdf-query-language%%. Un utilisateur peut accéder à la collection de %%triples|triple%% que vous avez dans une %%triplestore|triplestore%% via un point de terminaison SPARQL : c'est la porte qui connecte les gens à vos données. Une clause SERVICE dans le corps d'une requête SPARQL peut également être utilisée pour appeler un point de terminaison SPARQL distant.

## Exemples

- [Point de terminaison CWRC SPARQL](https://yasgui.lincsproject.ca/#)
- [Liste Wikidata des points de terminaison SPARQL](https://www.wikidata.org/wiki/Wikidata:Lists/SPARQL_endpoints)
- [Liste W3C des points de terminaison SPARQL](https://www.w3.org/wiki/SparqlEndpoints)

## Autres ressources

- [SPARQL (Wikipédia)](https://en.wikipedia.org/wiki/SPARQL)
- Uyi Idehen (2018) ["Qu'est-ce qu'un point de terminaison SPARQL et pourquoi est-ce important?"](https://medium.com/virtuoso-blog/what-is-a-sparql-endpoint-and-why-is-it-important-b3c9e6a20a8b)
