---
id: application-programming-interface
title: Interface de programmation d'applications (API)
hoverText: Une bibliothèque de codes qui permet à des applications tierces de communiquer avec une plateforme de services Web.
---

<!-- @format -->

Une interface de programmation d'applications (API) est une bibliothèque de codes assemblée par une société de services Web pour permettre à des applications tierces de communiquer avec une plate-forme de services Web. Bien qu'une API soit une interface, il s'agit d'une interface pour la communication d'ordinateur à ordinateur et n'est donc pas présentée à l'utilisateur humain de la manière à laquelle vous pensez normalement lorsque vous entendez le mot "interface". Au lieu de cela, une entreprise peut créer un affichage frontal au-dessus de son API pour prendre en charge l'utilisation humaine. Chaque fois que vous utilisez un service Web comme Facebook ou Twitter, vous interagissez avec une API via les outils frontaux développés pour les utilisateurs humains. Cependant, comme les API sont destinées à la communication d'ordinateur à ordinateur, il existe des moyens plus directs d'interagir avec les API ainsi qu'en utilisant des langages de programmation.

## Exemples

- [API du musée du design Cooper Hewitt Smithsonian](https://collection.cooperhewitt.org/api/)
- [API Europeana](https://pro.europeana.eu/page/apis)
- [API Bibliothèque du Congrès](https://libraryofcongress.github.io/data-exploration/)
- [API du Rijksmuseum](https://www.rijksmuseum.nl/en/api/-rijksmuseum-oai-api-instructions-for-use)
- [API WCLC](https://www.oclc.org/developer/develop/web-services/worldcat-search-api.en.html)

## Autres ressources

- [API (Wikipédia)](https://en.wikipedia.org/wiki/Application_programming_interface)
- Wrubel (2018) [« Unboxing the Library : Introduction to APIs »](https://labs.loc.gov/static/labs/meta/Wrubel-UnboxingtheLibrary-IntroductiontoAPIs.pdf) [PowerPoint]
- Wuyts (2018) _[Cultivating APIs in the Cultural Heritage Sector](http://jolanwuyts.eu/files/Cultivating_APIs_in_the_cultural_heritage_sector_Jolan_Wuyts_2018.pdf)_
