---
id: cultural-objects-name-authority
title: Cultural Objects Name Authority (CONA)
hoverText: One of five Getty Vocabularies that contains Uniform Resource Identifiers (URIs) for titles, creator attributions, physical characteristics, and depicted subjects concerning works of art, architecture, and visual cultural heritage.
---

<!-- @format -->

The Cultural Objects Name Authority (CONA) is one of five Getty Vocabularies developed by the Getty Research Institute to provide terms and %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% for concepts and objects related to the arts. LINCS uses CONA to provide URIs for titles, creator attributions, physical characteristics, and depicted subjects concerning works of art, architecture, and visual cultural heritage.

CONA includes terms related to:

- Architecture
- Artifacts
- Basketry
- Ceramics
- Drawings
- Frescoes
- Functional or ceremonial objects
- Manuscripts
- Paintings
- Performance art
- Photographs
- Prints
- Sculpture
- Textiles

CONA excludes terms for films, literary works, musical works, and objects in natural history and scientific collections.

## Further Resources

- [Cultural Objects Name Authority (CONA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- J. Paul Getty Trust (2022) [“About CONA and IA”](https://www.getty.edu/research/tools/vocabularies/cona/about.html)
