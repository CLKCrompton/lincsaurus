---
id: sparql-protocol-and-rdf-query-language
title: Protocole SPARQL et langage de requête RDF (SPARQL)
hoverText: Un langage de requête pour les triplestores qui traduit les données graphiques en données tabulaires normalisées avec des lignes et des colonnes.
---

<!-- @format -->

SPARQL Protocol and RDF Query Language (SPARQL) est un langage de requête qui vous permet d'interroger %%triplestores|triplestore%%. Il traduit les données graphiques en données tabulaires normalisées avec des lignes et des colonnes. Il est utile de considérer une requête SPARQL comme une Mad Lib, un ensemble de phrases contenant des blancs. La base de données prendra cette requête et trouvera chaque ensemble d'instructions correspondantes qui remplissent correctement ces blancs. Ce qui rend SPARQL puissant, c'est la possibilité de créer des requêtes complexes qui référencent de nombreux %%variables|query-variable%% à la fois.

## Exemples

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL): La requête suivante indique à la base de données de rechercher tous valeurs de `?painting` qui complètent correctement le %%Resource Description Framework (RDF)|resource-description-framework%% déclaration `<a un support><oil on canvas>` . `?painting` représente le(s) nœud(s) que la base de données renverra.

```texte
SÉLECTIONNER ?peinture
OÙ {
?peinture <a médium> <huile sur toile> .

```

- La requête suivante a une seconde variable : `?artist`. La base de données renverra toutes les combinaisons correspondantes de `?artist` et `?painting` qui remplissent ces deux déclarations.

```texte
SELECT ?artiste ?peinture
OÙ {
?artiste <a la nationalité> <Néerlandais> .
?peinture <a été créé par> ?artiste .

```

- CWRC Linked Data (2022) [“CWRC SPARQL Endpoint”](https://yasgui.lincsproject.ca/#): La requête SPARQL sur le lien ci-dessus renvoie toutes les personnes dans le LODset actuel de CWRC (actuellement la première extraction de l'ensemble de données d'Orlando sur l'écriture des femmes britanniques) nées au XIXe siècle, classées par date de naissance.

## Autres ressources

- Lincoln (2015) ["Utilisation de SPARQL pour accéder aux données ouvertes liées"](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL)
- [Requêtes sémantiques (Wikipédia)](https://en.wikipedia.org/wiki/Semantic_query)
- [SPARQL (Wikipédia)](https://en.wikipedia.org/wiki/SPARQL)
- Wikibooks (2018) [« Tutoriel XQuery/SPARQL »](https://en.wikibooks.org/wiki/XQuery/SPARQL_Tutorial)
