---
id: controlled-vocabulary
title: Vocabulaire contrôlé
hoverText: Un arrangement standardisé et organisé de mots et de phrases, qui fournit une manière cohérente de décrire les données.
---

<!-- @format -->

Les vocabulaires contrôlés sont des arrangements normalisés et organisés de mots et de phrases qui fournissent une manière cohérente de décrire les données. Ils fournissent un langage concis et cohérent, permettant une recherche et une récupération plus faciles.

## Exemples

- [Getty Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Thesaurus Getty des noms géographiques (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn/)
- [Liste Getty Union des noms d'artistes (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/)
- [Homosaurus](https://homosaurus.org/)
- [Fichier d'autorité des noms de la Bibliothèque du Congrès (NAF)](http://id.loc.gov/authorities/names.html)
- [Library of Congress Subject Headings (LCSH)](http://id.loc.gov/authorities/subjects.html)
- [Nomenclature](https://www.nomenclature.info/)
- [Fichier d'autorité international virtuel (VIAF)](http://viaf.org/)

## Autres ressources

- [Vocabulaire contrôlé (Wikipédia)](https://en.wikipedia.org/wiki/Controlled_vocabulary)
- Harping (2010) [« Que sont les vocabulaires contrôlés ? »](https://www.getty.edu/research/publications/electronic_publications/intro_controlled_vocab/what.pdf)
- Bibliothèque et Archives Canada (2018) [« Vocabulaires contrôlés »](https://www.bac-lac.gc.ca/fra/services/ressources-dinformation-gouvernementales/vocabulaires-controles/Pages/vocabulaires-controles.aspx)
