---
id: semantic-web
title: Web sémantique
hoverText: L'idée d'étendre le World Wide Web en incluant des descripteurs de données supplémentaires au contenu publié sur le Web afin que les ordinateurs puissent faire des interprétations significatives des données publiées.
---

<!-- @format -->

Le Web sémantique est l'idée d'étendre le World Wide Web en incluant des descripteurs de données supplémentaires au contenu publié sur le Web afin que les ordinateurs puissent faire des interprétations significatives des données publiées. %%Les données ouvertes liées (LOD)|linked-open-data%% sont essentielles à la réalisation du Web sémantique, car pour qu'il fonctionne, les ordinateurs doivent pouvoir accéder aux des ensembles de données structurées, ainsi que des ensembles de règles pouvant être utilisées pour l'inférence. En soutenant la création d'un Web de données lisibles par machine, LOD travaille à faire avancer le rêve d'un Web sémantique. La pile technologique du Web sémantique implique le %%Resource Description Framework (RDF)|resource-description-framework%%, %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%, %%SPARQL|sparql-protocol-and-rdf-query-language%%, et %%Langage d'ontologie Web (OWL)|web-ontology-language%%.

## Exemples

- [Web sémantique (Wikipédia)](https://en.wikipedia.org/wiki/Semantic_Web) : « La pile Web sémantique est une illustration de la hiérarchie des langages, où chaque couche exploite et utilise les capacités des couches inférieures. Il montre comment les technologies standardisées pour le Web sémantique sont organisées pour rendre le Web sémantique possible.

![Diagramme de la pile du Web sémantique, montrant une hiérarchie de langages sémantiques codés par couleur.](/img/documentation/glossary-semantic-web-example-(cc0).png)

## Autres ressources

- Berners-Lee, Hendler, & Lassila (2001) [“The Semantic Web”](https://www-sop.inria.fr/acacia/cours/essi2006/Scientific%20American_%20Feature%20Article_%20The%20Semantic%20Web_%20May%202001.pdf)
- Cambridge Semantics (2016) [« Une introduction au Web sémantique »](https://www.youtube.com/watch?v=V6BR9DrmUQA) [Vidéo]
- [Web sémantique (Wikipédia)](https://en.wikipedia.org/wiki/Semantic_Web)
