---
id: quad
title: Quadruple
hoverText: Une extension d'un triplet pour inclure une quatrième section qui fournit un contexte pour le triplet, comme l'URI (Uniform Resource Identifier) ​​du graphe dans son ensemble (sujet-prédicat-objet-contexte).
---

<!-- @format -->

Un quad est une extension d'un %%triple|triple%% pour inclure une quatrième section—donc le changement de triple, en trois parties, à quad, en quatre parties—qui fournit un contexte pour le triplet, comme le %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% du graphique dans son ensemble. Alors qu'un triplet prend la forme de `<subject><predicate><object>`, un quad étend cela pour être `<subject><predicate><object><context>`. Dans le cas d'un %%graphe nommé|named-graph%%, ce `<context>` morceau est le `<graphname>`. Bien qu'un discours désinvolte sur %%Les données ouvertes liées (LOD)|linked-open-data%% ont tendance à se référer le plus souvent aux triplets et %%triplestores|triplestore%%, de nombreux %%Les référentiels de données liées (LD)|linked-data%% sont désormais des quadstores et les quads sont couramment utilisés, y compris par LINCS.

## Exemples

- W3C (2014) *[RDF 1.1 N-Quads](https://www.w3.org/TR/n-quads/)* : l'exemple suivant montre un sujet (_spiderman_), un prédicat (_relationship/enemyOf_), et objet (_green-goblin_), et fournit un contexte au triplet en se référant à un graphique sur le domaine de Spiderman (_graphs/spiderman_).

```texte
<http://example.org/#spiderman>
<http://www.perceive.net/schemas/relationship/enemyOf>
<http://example.org/#green-goblin>
<http://example.org/graphs/spiderman> .
```

## Autres ressources

- W3C (2014) _[RDF 1.1 N-Quads](https://www.w3.org/TR/n-quads/)_
