---
id: knowledge-pattern
title: Modèle de connaissances (ResearchSpace)
hoverText: Chemins de graphe prédéfinis qui résument les activités du monde réel à l'aide de classes et de propriétés dans l'environnement ResearchSpace.
---

<!-- @format -->

Dans l'environnement [ResearchSpace](/docs/tools/researchspace), les modèles de connaissances sont des chemins de graphe prédéfinis qui résument les activités du monde réel à l'aide de classes et %%propriétés|property%%. Les modèles de connaissances sont créés par des experts en la matière et sont destinés à refléter les conventions narratives du domaine dans lequel l'expert en la matière travaille. Bien que sémantiquement complets en eux-mêmes, les modèles de connaissances peuvent être combinés avec d'autres pour enrichir davantage un %%graphique des connaissances|knowledge-graph%%.

Un exemple de modèle de connaissances ou d'abstraction pourrait être la cartographie de la façon dont un artiste est attribué en tant que créateur d'une œuvre d'art. Un modèle de connaissance peut indiquer qu'une œuvre d'art est créée lors d'un événement de production, l'artiste étant lié à l'événement plutôt que directement à l'œuvre elle-même. Le modèle de connaissances est ensuite utilisé dans un ensemble de données dans chaque cas où un artiste est cité comme l'auteur d'une œuvre pour assurer la cohérence du graphe de connaissances.

## Autres ressources

- [Abstraction (Wikipédia)](<https://en.wikipedia.org/wiki/Abstraction_(computer_science)>)
- Oldman & Tanase (2018) [« Remodeler le graphe des connaissances en connectant les chercheurs, les données et les pratiques dans ResearchSpace »](https://link.springer.com/chapter/10.1007/978-3-030-00668-6_20)
- Oldman, Tanase et Satschi (2019) ["Le problème de la distance dans l'histoire de l'art numérique : une étude de cas ResearchSpace sur le séquençage des impressions d'impression Hokusai pour former un réseau de connaissances organisé par l'homme"](https://www.zora.uzh.ch/id/eprint/197623/1/Oldman%2C_Dominic%2C_Diana_Tanase%2C_and_Stephanie_Santschi._2019._The_Problem_of_Distance_in_Digital_Art_History.pdf)
