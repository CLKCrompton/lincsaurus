---
id: library-of-congress-classification-system
title: Library of Congress Classification System (LCC)
hoverText: A library classification system that is commonly used by large research and academic libraries to organize print collections.
---

<!-- @format -->

The Library of Congress Classification System (LCC) is a library classification system that was developed by the Library of Congress in 1897. While it was originally created for the specific needs of the Library of Congress, it is currently used by large research and academic libraries to organize their print collections.

## Further Resources

* Librarianship Studies & Information Technology (2020) [“Library of Congress Classification”](https://www.librarianshipstudies.com/2017/11/library-of-congress-classification.html)
* [Library of Congress Classification Outline](https://www.loc.gov/catdir/cpso/lcco/)
* [Library of Congress Classification (Wikipedia)](https://en.wikipedia.org/wiki/Library_of_Congress_Classification)
