---
id: digital-humanities-summer-institute
title: Institut d'été des humanités numériques (DHSI)
hoverText: Un institut annuel de formation en bourses numériques organisé à l'Université de Victoria.
---

<!-- @format -->

Le Digital Humanities Summer Institute (DHSI) est un institut annuel de formation en bourses numériques organisé à l'Université de Victoria. DHSI rassemble des professeurs, du personnel et des étudiants des communautés des arts, des sciences humaines, des bibliothèques et des archives pour discuter et apprendre de nouvelles technologies et méthodes par le biais de cours, de séminaires et de conférences.

## Autres ressources

- Institut d'été des humanités numériques (2022) [« À propos de DHSI »](https://dhsi.org/about-dhsi/)
