---
id: blank-node
title: Nœud vide
hoverText: Un sujet ou un objet dans un graphe RDF (Resource Description Framework) pour lequel un URI (Uniform Resource Identifier) ​​ou un littéral n'est pas donné.
---

<!-- @format -->

Un nœud vide ou _bnode_ est un sujet ou un objet dans un %%Resource Description Framework (RDF)|resource-description-framework%% graphique pour lequel un %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% ou %%littéral|literal%% n'est pas donné. L'utilisation de nœuds vides est un moyen de créer un conteneur qui rassemble des informations disparates sur une entité sans créer de nouvel URI, mais cela peut introduire de la complexité lorsque %%SPARQL interrogeant|sparql-protocol-and-rdf-query-language%% des données ou fusionnant des données provenant de différentes sources. Dans le premier cas, les requêtes SPARQL sur des ensembles de données exprimant les mêmes informations, avec et sans nœuds vides, peuvent renvoyer des informations différentes en raison de nœuds vides exprimant des valeurs indéfinies ou redondantes. Dans le second cas, les identifiants de nœuds vides ont souvent une portée locale, de sorte que la fusion d'ensembles de données peut entraîner une duplication ou une fusion de nœuds vides, ou des nœuds vides redondants où certains pourraient être fusionnés. Pour ces raisons, les nœuds vides peuvent être utilisés comme identifiant local dans un ensemble de données spécifique, mais doivent être correctement déclarés (fournis avec un URI) lorsqu'ils sont combinés avec d'autres ensembles de données. LINCS n'inclut pas de nœuds vides dans ses ensembles de données ingérés. Toutes les entités sont identifiées par des URI, soit pendant le %%processus de réconciliation|reconciliation%% ou par %%frappe de nouveaux URI|uniform-resource-identifier-minting%%.

## Exemples

\*Hogan et al. (2016) [« Tout ce que vous avez toujours voulu savoir sur les nœuds vierges »](https://aidanhogan.com/docs/blank_nodes_jws.pdf) : Le graphique suivant indique que le joueur de tennis :Federer a remporté le :FrenchOpen en 2009. Il déclare également qu'il a gagné: Wimbledon où une telle victoire était en 2003. Les nœuds vides représentent un événement gagnant qui relie Federer et le tournoi spécifique (Wimbledon et Roland-Garros) et dans deux cas l'année de la victoire.

![Graphique RDF montrant les victoires de tennis de Roger Federer sous forme de nœuds vides.](/img/documentation/glossary-blank-node-example-(fair-dealing).png)

## Autres ressources

- [Nœud vide (Wikipédia)](https://en.wikipedia.org/wiki/Blank_node)
- Chen (2012) [« Nœuds vides en RDF »](http://www.jsoftware.us/vol7/jsw0709-09.pdf)
- W3C (2014) [« 3.5 Remplacer les nœuds vides par des IRI »](https://www.w3.org/TR/rdf11-concepts/#dfn-blank-node-identifier)
