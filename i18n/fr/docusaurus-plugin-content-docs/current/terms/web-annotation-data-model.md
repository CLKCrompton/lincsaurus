---
id: web-annotation-data-model
title: Web Annotation Data Model (WADM)
hoverText: A standard for the formatting and structuring of web annotations.
---

<!-- @format -->

Web Annotation Data Model (WADM) is a standard for the formatting and structuring of web annotations. It was designed to make descriptions of web annotations consistent and therefore more shareable and reusable across different sources. LINCS uses WADM to describe the sources used in the datasets that are being converted.

WADM is not an extension of %%CIDOC CRM|cidoc-crm%%, nor is it an %%event-centric|event-oriented-ontology%% by nature. While there is no pre-existing alignment between WADM and CIDOC CRM, however, there is overlap in people who work on the ontologies, which means that efforts are being made to find ways for WADM and CIDOC CRM to work together.
