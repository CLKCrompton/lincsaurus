---
id: union-list-of-artist-names
title: Union List of Artist Names (ULAN)
hoverText: One of five Getty Vocabularies that contains Uniform Resource Identifiers (URIs) for names, relationships, and biographical information concerning people and corporate bodies related to art, architecture, and other material culture.
---

The Union List of Artist Names (ULAN) is one of five Getty Vocabularies developed by the Getty Research Institute to provide terms and %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% for concepts and objects related to the arts. LINCS uses the ULAN to provide URIs for names, relationships, and biographical information concerning people and corporate bodies related to art, architecture, and other material culture.

The ULAN includes terms relating to:

- Architects
- Artists
- Firms
- Other makers (named or anonymous)
- Patrons
- Repositories of art

The ULAN excludes terms for fictional and literary characters who are the subject of a visual work, people and corporate bodies that are named in documentation, but whose identity are unknown, and attribution statements, such as those naming studios or workshops.

## Further Resources

- J. Paul Getty Trust (2022) [“About the ULAN”](https://www.getty.edu/research/tools/vocabularies/ulan/about.html)
- [Union List of Artist Names (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/index.html)
