---
id: provenance
title: Provenance
hoverText: L'historique de la propriété, de la garde ou de l'emplacement d'un objet décrit ou les données décrivant cet objet.
---

<!-- @format -->

Au sens large, la provenance est l'historique de la propriété, de la garde ou de l'emplacement de quelque chose. Dans le contexte de LINCS, il peut faire référence à deux choses : la provenance d'un objet décrit ou la provenance des données décrivant cet objet. Dans le premier cas, les données de provenance incluraient les noms des propriétaires et des gardiens, ainsi que les emplacements géographiques de ces acteurs et la durée pendant laquelle ils ont détenu le contrôle de l'objet, dans une chronologie qui suit l'objet de sa création à son emplacement actuel. . Dans ce dernier cas, les données de provenance incluraient des informations sur qui a créé ou édité les données et les dates de création et d'édition. La provenance des données est aussi parfois appelée _data lineage_.

## Exemples

- [Pistes artistiques](http://www.museumprovenance.org/)
- [Cleveland Museum of Art](https://www.clevelandart.org/art/collection/search) (comprend des informations sur la provenance des objets)

## Autres ressources

- Brody (2018) [« Savez-vous d'où viennent vos données ? »](https://www.dataversity.net/know-data-came/)
- [Lignée de données (Wikipédia)](https://en.wikipedia.org/wiki/Data_lineage)
- [Provenance (Wikipédia)](https://en.wikipedia.org/wiki/Provenance#Data_provenance)
