---
id: federated-sparql-search
title: Recherche SPARQL fédérée
hoverText: Un point d'entrée unique pour accéder aux terminaux SPARQL distants afin qu'un service de requête puisse récupérer des informations à partir de plusieurs sources de données.
---

<!-- @format -->

La recherche SPARQL fédérée donne accès à un %%SPARQL Endpoints|sparql-endpoint%% afin qu'un service de requête puisse récupérer des informations à partir de plusieurs sources de données. La recherche fédérée fournit un point d'entrée unique vers plusieurs sources de données. Une clause SERVICE dans le corps d'un %%La requête SPARQL|sparql-protocol-and-rdf-query-language%% peut être utilisée pour appeler un point de terminaison SPARQL distant.

## Exemples

- L'exemple suivant montre une requête SPARQL vide.

```texte
SÉLECTIONNER [...]
OÙ {
SERVICE <http://exemple1.exemple.org/sparql> {
[...]
OPTIONNEL {
[...]
SERVICE <http://exemple2.exemple.org/sparql> {
[...] } }


```

## Autres ressources

- [Recherche fédérée (Wikipédia)](https://en.wikipedia.org/wiki/Federated_search)
- W3C (2013) [« Requête fédérée SPARQL 1.1 »](https://www.w3.org/TR/sparql11-federated-query/)
