---
id: crosswalking
title: Passage pour piétons
hoverText: Le processus conceptuel de cartographie des données entre les modèles de données.
---

<!-- @format -->

Le croisement fait référence au processus conceptuel de cartographie des données entre les modèles de données. Le crosswalking est également appelé _mapping_, faisant référence à la manière dont les données sont mappées d'un modèle à un autre.

## Exemples

- Le tableau suivant montre comment mapper le « titre » dans les modèles de données.

| Nom du modèle de départ | Début du mandat du modèle | Nom du modèle de destination | Terme du modèle de destination                             |
| ----------------------- | ------------------------- | ---------------------------- | ---------------------------------------------------------- |
| CDWA                    | Texte du titre            | Noyau VRA                    | `<vra : titre> dans <vra : travail> ou <vra : collection>` |
| Noyau de Dublin         | Carrelage                 | MODS                         | `<titleInfo><title>`                                       |
| DAC                     | 2.3 Titre                 | CIDOC CRM                    | `E35_Titre`                                                |

## Autres ressources

- Harping (2022) [« Concordance des normes de métadonnées »](https://www.getty.edu/research/publications/electronic_publications/intrometadata/crosswalks.html)
- [Schema Crosswalk (Wikipédia)](https://en.wikipedia.org/wiki/Schema_crosswalk)
- TEI (2018) [« Passages pour piétons »](https://wiki.tei-c.org/index.php/Crosswalks)
