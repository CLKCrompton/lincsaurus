---
id: entity
title: Entité
hoverText: Une chose discrète, souvent décrite comme le sujet et l'objet (ou le domaine et la gamme) d'un triplet (sujet-prédicat-objet).
---

<!-- @format -->

Les entités sont des choses discrètes qui existent, qui peuvent être liées entre elles à l'aide d'attributs qui établissent des relations entre elles. Dans %%Données ouvertes liées (LOD)|linked-open-data%%, les entités sont souvent décrites comme le sujet et l'objet (ou le %%domaine|domain%% et %%plage|range%%) d'un %%triple|triple%% (sujet-prédicat-objet).

## Exemples

- CIDOC CRM (2022) [“E1 CRM Entity in version 7.1.1”](https://cidoc-crm.org/Entity/E1-CRM-Entity/version-7.1.1)

## Autres ressources

- [Entité (Wikipédia)](https://en.wikipedia.org/wiki/Entité)
