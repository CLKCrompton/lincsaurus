---
id: namespace
title: Espace de noms
hoverText: Un répertoire de concepts qui sont utilisés pour identifier et faire référence à des entités au sein d'un ensemble de données.
---

<!-- @format -->

Un espace de noms est un répertoire de concepts qui sont utilisés pour identifier et faire référence à %%entités|entity%% au sein d'un ensemble de données. Les espaces de noms peuvent être internes, utilisés comme base pour %%frapper des URI (Uniform Resource Identifiers)|uniform-resource-identifier-minting%% pour un ensemble de données discret, ou ils peuvent être externes %%ontologies|ontology%%, %%vocabulaires|vocabulary%%, et %%thesaurus|thesaurus%% qui peuvent être soit encyclopédiques, soit spécifiques à l'industrie. Il est préférable d'utiliser ou d'étendre un vocabulaire préexistant et de créer un lien vers ce vocabulaire en utilisant des URI stables fournis par leur espace de noms, plutôt que de créer un vocabulaire sur mesure. En utilisant des vocabulaires standard maintenus dans des espaces de noms stables, les réalisations de %%Les données ouvertes liées (LOD)|linked-open-data%% sont réalisées.

## Exemples

- L'exemple suivant montre la déclaration des espaces de noms pour Y90s Personography dans l'en-tête du fichier TTL. Les espaces de noms fournissent des URI pour chaque entité répertoriée. Les espaces de noms ci-dessus consistent en un nom de domaine et un chemin, ainsi qu'un préfixe qui permet de raccourcir les ressources dans le fichier TTL (par exemple, bf:title ferait référence à une version tronquée de <http://id.loc.gov/ontologies/bibframe/title>). La déclaration et l'utilisation d'espaces de noms permettent aux lecteurs machine et humains de différencier les éléments nommés de manière identique à partir de plusieurs ensembles de données.

```texte
@prefix bf : <http://id.loc.gov/ontologies/bibframe/> .
@préfixe bibo : <http://purl.org/ontology/bibo/> .
@prefix crm : <http://www.cidoc-crm.org/cidoc-crm/> .
```

## Autres ressources

- Données de conservation liées (2022) ["Qu'est-ce qu'un espace de noms?"](https://www.ligatus.org.uk/lcd/faq/211)
- [Espace de noms (Wikipédia)](https://en.wikipedia.org/wiki/Namespace)
- W3C (2014) [“6. Vocabulaires standards »](https://www.w3.org/TR/ld-bp/#VOCABULARIES)
