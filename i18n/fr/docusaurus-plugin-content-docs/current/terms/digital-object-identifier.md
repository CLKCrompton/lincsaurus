---
id: digital-object-identifier
title: Identificateur d'objet numérique (DOI)
hoverText: Type d'URI (Uniform Resource Identifier) ​​utilisé pour identifier de manière unique divers objets d'information universitaires, professionnels et gouvernementaux, tels que des articles de revues, des rapports de recherche, des ensembles de données, des publications officielles et des vidéos.
---

<!-- @format -->

Un identificateur d'objet numérique (DOI) est un type de %%Uniform Resource Identifier (URI)|uniform-resource-identifier%% utilisé pour identifier de manière unique divers objets d'information universitaires, professionnels et gouvernementaux, tels que des articles de revues, des rapports de recherche, des ensembles de données , publications officielles et vidéos. Chaque DOI commence par le chiffre 10 et a un préfixe et un suffixe, séparés par une barre oblique. Le préfixe est composé de quatre chiffres ou plus, qui sont attribués à une organisation. L'éditeur des objets d'information crée le suffixe qui adhère à leurs normes d'identification particulières.

Les DOI sont souvent inclus dans les métadonnées de l'information de l'objet auquel ils se réfèrent. Le DOI d'un objet d'information reste fixe tout au long de sa durée de vie, même lorsque son emplacement ou d'autres métadonnées changent. Par conséquent, un DOI a tendance à être un moyen plus stable de faire référence à un objet d'information qu'un %%Uniform Resource Locator (URL)|uniform-resource-locator%%.

## Autres ressources

- [Identifiant d'objet numérique (Wikipédia)](https://en.wikipedia.org/wiki/Digital_object_identifier)
- DOI (2021) [“Le système DOI”](https://www.doi.org/index.html)
