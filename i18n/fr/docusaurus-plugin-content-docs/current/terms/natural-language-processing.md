---
id: natural-language-processing
title: Traitement automatique des langues naturelles (TALN)
hoverText: Une branche de l'intelligence artificielle qui implique le traitement automatique et/ou la manipulation de la parole, du texte et d'autres formes de données non structurées qui représentent la façon dont les humains communiquent entre eux.
---

<!-- @format -->

Le traitement automatique des langues naturelles est une branche de l'intelligence artificielle qui implique le traitement automatique et/ou la manipulation de la parole, du texte et d'autres formes de données non structurées qui représentent la façon dont les humains communiquent entre eux. Il décrit le travail nécessaire pour essayer d'amener les ordinateurs à comprendre notre langage naturel et nos modes de communication. La plupart des techniques de traitement automatique des langues naturelles reposent sur l'apprentissage automatique pour tirer un sens des communications humaines.

## Exemples

\* [Google Traduction](https://translate.google.ca/)

- Alexa, Siri et autres assistants personnels qui utilisent la reconnaissance vocale
- Chatbots

## Autres ressources

- Lopez Yse (2019) [“Votre guide du traitement automatique des langues naturelles (TALN)”](https://towardsdatascience.com/your-guide-to-natural-language-processing-nlp-48ea2511f6e1)
- [Traitement automatique des langues (Wikipédia)](https://fr.wikipedia.org/wiki/Traitement_automatique_des_langues)
- SparkCognition (2018) ["Les bases du traitement du langage naturel"](https://www.youtube.com/watch?v=d4gGtcobq8M) [Vidéo]
