---
id: dereferenceable
title: Déréférencable
hoverText: Adjectif utilisé en relation avec les URI (Uniform Resource Identifiers) qui peuvent passer d'une référence abstraite à quelque chose de plus concret, à savoir une ressource Web.
---

<!-- @format -->

Déréférencer consiste à accéder à une valeur ou à un objet situé dans un emplacement de mémoire stocké dans un pointeur - le pointeur vous dirige vers la valeur stockée. Dans le contexte de %%Données liées (LD)|linked-data%%, le déréférencement est utilisé en relation avec %%Uniform Resource Identifiers (URI)|uniform-resource-identifier%% et s'ils sont déréférencables ou non. Un URI déréférencable est un URI qui se résout en une page Web : il peut passer d'une référence abstraite à quelque chose de plus concret, à savoir une ressource Web. Si vous pouvez mettre un URI dans la barre d'adresse d'un navigateur et accéder à une page Web via cette adresse, alors il est déréférencable.

## Autres ressources

- [Opérateur de déférence (Wikipédia)](https://en.wikipedia.org/wiki/Dereference_operator)
- [Référence (Wikipédia)](<https://en.wikipedia.org/wiki/Reference_(computer_science)>)
