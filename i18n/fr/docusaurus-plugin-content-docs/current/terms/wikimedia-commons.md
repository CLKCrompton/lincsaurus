---
id: wikimedia-commons
title: Wikimédia Commons
hoverText: Un référentiel de fichiers multimédias (images, sons et clips vidéo) qui met à disposition du domaine public et du contenu multimédia sous licence libre, et qui agit en tant que gestionnaire de ressources numériques pour tous les projets de la Wikimedia Foundation.
---

<!-- @format -->

Wikimedia Commons est un référentiel de fichiers multimédias (images, sons et clips vidéo) qui rend le contenu multimédia du domaine public et sous licence libre accessible à tous, dans plusieurs langues. Il agit en tant que gestionnaire d'actifs numériques pour tous les projets du %%Wikimedia Foundation|wikimedia-foundation%%, mais les fichiers sont également disponibles pour les utilisateurs externes. Comme Wikipédia, Wikimedia Commons utilise une technologie wiki qui permet une édition publique à grande échelle.

## Exemples

- [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page)

## Autres ressources

- Wikimedia Commons (2021) [“Qu'est-ce que Wikimedia Commons?”](https://commons.wikimedia.org/wiki/Commons:Welcome)
