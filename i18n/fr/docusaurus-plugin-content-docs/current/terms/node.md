---
id: node
title: Nœud
hoverText: Représentation d'une entité ou d'une instance à suivre dans une base de données de graphes ou un triplestore, telle qu'une personne, un objet ou une organisation.
---

<!-- @format -->

Un nœud représente une entité ou une instance à suivre dans une %%base de données de graphes|graph-database%% ou %%triplestore|triplestore%%, comme une personne, un objet ou une organisation. Les nœuds sont connectés par %%edges|edge%% et les informations sur un nœud sont appelées une propriété.

## Exemples

- L'image suivante montre un %%Resource Description Framework (RDF)|resource-description-framework%% graphique mettant en évidence les arêtes qui relient trois nœuds.

![alt=""](/img/documentation/glossary-node-example-(c-LINCS).jpg)
