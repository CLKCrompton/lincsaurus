---
id: thesaurus
title: Thésaurus
hoverText: Un vocabulaire structuré qui montre les relations hiérarchiques, associatives et d'équivalence entre les concepts, afin que les utilisateurs trouvent non seulement des termes plus larges et plus étroits que d'autres, mais aussi des termes synonymes, antonymes ou autrement liés (associés) d'une manière définie.
---

<!-- @format -->

Un thésaurus est un <%%vocabulaire|vocabulary%% qui montre les relations de base entre les concepts : hiérarchique, associatif et d'équivalence. Les utilisateurs d'un thésaurus trouveront non seulement des termes plus larges et plus étroits que d'autres, mais également des termes synonymes, antonymes ou autrement liés (associés) d'une manière définie.

|                | Ontologie                                                                                            | Taxonomie                                                                                        | Thésaurus                                                   | Vocabulaire                                                              |
| -------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ | ----------------------------------------------------------- | ------------------------------------------------------------------------ |
| **Décrit**     | Concepts (contenu) et les relations entre eux (structure), y compris les axiomes et les restrictions | Relations hiérarchiques entre les concepts, et spécifie le terme à utiliser pour désigner chacun | Relations hiérarchiques et non hiérarchiques entre concepts | Terme général désignant un ensemble de concepts (mots) liés à un domaine |
| **Relations**  | Hiérarchique typé et associatif                                                                      | Fondamentalement hiérarchique, mais tous modélisés en utilisant la même notation                 | Non typé hiérarchique, associatif et équivalence            | Non spécifié (concept abstrait)                                          |
| **Propriétés** | RDFS définit les propriétés et les restrictions des relations                                        | Aucun                                                                                            | Peut être décrit dans les notes d'application si nécessaire | Non spécifié (concept abstrait)                                          |
| **Structuré**  | Réseau                                                                                               | Arbre                                                                                            | Arbre à branches croisées                                   | Non spécifié (concept abstrait)                                          |

## Exemples

- [Getty Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Library of Congress Subject Headings (LCSH)](http://id.loc.gov/authorities/subjects.html)

## Autres ressources

- Schwarz (2005) [« Recherche améliorée du modèle de domaine - Une comparaison de la taxonomie, du thésaurus et de l'ontologie »](https://pdfs.semanticscholar.org/5b63/f9696de8627500ee62e13ae32b5dfe0c857b.pdf)
- [Thésaurus (Wikipédia)](https://en.wikipedia.org/wiki/Thesaurus)
- [Thésaurus (Wikipédia)](<https://en.wikipedia.org/wiki/Thesaurus_(information_retrieval)>)
