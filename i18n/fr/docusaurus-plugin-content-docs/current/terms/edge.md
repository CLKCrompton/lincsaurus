---
id: edge
title: Bord
hoverText: Une ligne qui relie un nœud à un autre dans une base de données de graphes, représentant une relation entre les nœuds.
---

<!-- @format -->

Une arête est une ligne qui relie un %%nœud|node%% à un autre dans un %%base de données de graphes|graph-database%%, représentant une relation entre eux. Les bords peuvent être dirigés ou non dirigés. Lorsqu'ils sont dirigés, ils ont des significations différentes selon leur direction.

## Exemples

- L'image suivante montre un %%Resource Description Framework (RDF)|resource-description-framework%% graphique mettant en évidence les arêtes qui relient trois nœuds.

![alt=""](/img/documentation/glossary-edge-example-(c-LINCS).jpg)
