---
id: structured-query-language
title: Langage de requête structuré (SQL)
hoverText: Un langage de requête pour les bases de données relationnelles qui permet aux utilisateurs de spécifier quelles données renvoyer, quelles tables rechercher, quelles relations suivre et comment ordonner les données qui répondent à ces conditions définies.
---

<!-- @format -->

Structured Query Language (SQL) est le langage de requête pour %%bases de données relationnelles|relational-database%%. Pour créer une requête SQL, l'utilisateur doit spécifier les données à renvoyer, les tables à rechercher, les relations à suivre (comment connecter les tables entre elles, appelées JOIN) et, éventuellement, comment ordonner les données qui répondent à tous ces ensembles. les conditions.

## Exemples

- La requête suivante indique à la base de données que vous voulez deux colonnes (_displayName_ et _title_), l'une de ces colonnes étant constituée de données de la table _artist_ (contenant des informations sur les artistes) et l'autre constituée de données de la table _object_ (contenant des informations sur objets d'art). Pour obtenir des données des deux tables, vous devez les joindre sur un champ commun (_artistID_) qui apparaît sur les deux tables. Vous pouvez imaginer qu'un artiste aura un _artistID_ unique et que _artistID_ apparaîtra également sur la même ligne de la table _object_ pour chaque objet d'art dont on sait qu'il a été réalisé par cet artiste. Bien qu'il s'agisse d'un champ unique pour la table _artist_, il ne l'est pas pour la table _object_, puisque vous pouvez avoir plusieurs objets dans la base de données par le même artiste. Cette requête indique à la base de données de renvoyer toutes les lignes où (1) il y a une correspondance entre ces colonnes, (2) où le champ _artist.displayName_ contient le mot "Rembrandt" quelque part, et (3) où le champ _object.dateMade_ contient une date postérieure au 1er janvier 1660. Enfin, cette requête indique à la base de données de nous renvoyer toutes ces lignes dans l'ordre des _objectID_.

```texte
SELECT artist.displayName, object.title
DE l'artiste
JOIN object ON artist.artistID = object.artistID
WHERE artist.displayName LIKE ‘%Rembrandt%’ AND object.dateMade > ‘1/01/1660’
ORDRE PAR objet.objectID
```

## Autres ressources

- Heller (2019) [« Qu'est-ce que SQL ? La lingua franca de l'analyse de données »](https://www.infoworld.com/article/3219795/what-is-sql-the-first-language-of-data-analysis.html)
- [SQL (Wikipédia)](https://en.wikipedia.org/wiki/SQL)
- W3Schools (2022) [« Tutoriel SQL »](https://www.w3schools.com/sql/)
