---
id: taxonomy
title: Taxonomie
hoverText: Un système qui identifie les relations hiérarchiques entre les concepts au sein d'un domaine.
---

<!-- @format -->

Une taxonomie identifie les relations hiérarchiques entre les concepts au sein d'un domaine. Une taxonomie catégorise les éléments au sein d'une seule dimension : les catégories de haut niveau sont décomposées en sous-catégories, puis en d'autres sous-catégories de manière ramifiée mais linéaire, jusqu'à une entité individuelle. Une taxonomie ne représente pas les relations entre les catégories, uniquement de manière hiérarchique.

|                | Ontologie                                                                                            | Taxonomie                                                                                        | Thésaurus                                                   | Vocabulaire                                                              |
| -------------- | ---------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ | ----------------------------------------------------------- | ------------------------------------------------------------------------ |
| **Décrit**     | Concepts (contenu) et les relations entre eux (structure), y compris les axiomes et les restrictions | Relations hiérarchiques entre les concepts, et spécifie le terme à utiliser pour désigner chacun | Relations hiérarchiques et non hiérarchiques entre concepts | Terme général désignant un ensemble de concepts (mots) liés à un domaine |
| **Relations**  | Hiérarchique typé et associatif                                                                      | Fondamentalement hiérarchique, mais tous modélisés en utilisant la même notation                 | Non typé hiérarchique, associatif et équivalence            | Non spécifié (concept abstrait)                                          |
| **Propriétés** | RDFS définit les propriétés et les restrictions des relations                                        | Aucun                                                                                            | Peut être décrit dans les notes d'application si nécessaire | Non spécifié (concept abstrait)                                          |
| **Structuré**  | Réseau                                                                                               | Arbre                                                                                            | Arbre à branches croisées                                   | Non spécifié (concept abstrait)                                          |

## Exemples

- [Système de classification décimale Dewey](https://www.oclc.org/en/dewey.html)
- [Système de classification de la Bibliothèque du Congrès](https://www.loc.gov/catdir/cpso/lcco/)

## Autres ressources

- Schwarz (2005) [« Recherche améliorée du modèle de domaine - Une comparaison de la taxonomie, du thésaurus et de l'ontologie »](https://pdfs.semanticscholar.org/5b63/f9696de8627500ee62e13ae32b5dfe0c857b.pdf)
- [Taxonomy (Wikipedia)](<https://en.wikipedia.org/wiki/Taxonomy_(general)>)
