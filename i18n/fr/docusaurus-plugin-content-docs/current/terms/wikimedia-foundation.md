---
id: wikimedia-foundation
title: Fondation Wikimédia
hoverText: L'organisation faîtière qui gère Wikipedia, Wikibase, Media Wiki, Wiktionary et d'autres projets et chapitres Wiki.
---

<!-- @format -->

La Wikimedia Foundation, souvent appelée Wikimedia, est l'organisation faîtière qui gère Wikipédia, %%Wikibase|wikibase%%, Media Wiki, Wiktionary et autres projets et chapitres Wiki.

## Exemples

- [Wiktionnaire](https://www.wiktionary.org)
- [Wikisource](https://wikisource.org/wiki/Main_Page)
- [Wikispecies](https://species.wikimedia.org/wiki/Main_Page)

## Autres ressources

- [Wikimédia](https://www.wikimedia.org)
