---
id: digital-humanities
title: Humanités numériques (DH)
hoverText: Domaine scientifique dans lequel les outils et technologies numériques sont utilisés pour explorer les questions de recherche en sciences humaines.
---

<!-- @format -->

Les humanités numériques (DH) sont un domaine scientifique dans lequel les outils et technologies numériques sont utilisés pour explorer les questions de recherche en sciences humaines. Les projets en DH peuvent être grands ou petits ; les grands projets sont souvent transdisciplinaires et impliquent une équipe de collaborateurs, qui peut comprendre des professeurs, des étudiants, des spécialistes des technologies de l'information et des partenaires institutionnels (universités, archives, bibliothèques et musées). Les activités courantes de DH comprennent la numérisation, la compilation de données, la conception de bases de données, la cartographie du système d'information géographique (SIG), l'extraction et l'analyse de texte, la visualisation de données et le développement Web.

## Exemples

- [Carte du début de Londres moderne](https://mapoflondon.uvic.ca/)
- [Orlando](http://www.artsrn.ualberta.ca/orlando/)
- [Jaune des années 90](https://1890s.ca/)

## Autres ressources

- [Humanités numériques (Wikipédia)](https://en.wikipedia.org/wiki/Digital_humanities)
- Heppler (2015) [« Qu'est-ce que les humanités numériques ? »](https://whatisdigitalhumanities.com/)
