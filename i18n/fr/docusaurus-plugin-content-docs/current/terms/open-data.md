---
id: open-data
title: Données ouvertes
hoverText: Des données qui peuvent être consultées, utilisées et réutilisées par n'importe qui, sur la base de l'idée que les données doivent être librement accessibles à tous pour être vues et utilisées, sans restrictions de droit d'auteur.
---

<!-- @format -->

Les données ouvertes sont des données qui peuvent être consultées, utilisées et réutilisées par n'importe qui. C'est l'idée que les données doivent être librement accessibles à tous pour être vues et utilisées, sans restrictions de droits d'auteur. Pour être des données ouvertes, un ensemble de données doit être publié sous une licence qui autorise explicitement la liberté d'utilisation, telle que %%Creative Commons (CC)|creative-commons%% ou [Open Data Commons Public Domain Dedication and License (PDDL)](https://opendatacommons.org/licenses/pddl/1-0/).

Les données sont considérées comme "ouvertes" lorsqu'elles répondent à cinq exigences :

1. Disponible : il peut être trouvé en ligne et téléchargé
2. Accessible : c'est dans un format modifiable
3. Réutilisable : il est publié sous une licence qui permet la réutilisation
4. Redistribuable : il peut être combiné avec d'autres données provenant d'autres sources
5. Illimité : tout le monde peut utiliser, transformer, combiner et partager les données, quelle que soit la manière dont il les utilise, que ce soit à des fins éducatives, non commerciales ou commerciales

## Exemples

- [Centre ArcGIS](https://hub.arcgis.com/search)
- [Portails de données](https://dataportals.org/)
- [Portail des données ouvertes du gouvernement du Canada](https://open.canada.ca/fr/open-data)
- [Index mondial des données ouvertes](https://index.okfn.org/)
  \* [IEEE DataPort](https://ieee-dataport.org/datasets?t%5B%5D=open_access)

## Autres ressources

- Charte des données ouvertes (2022) [“Qui nous sommes”](https://opendatacharter.net/who-we-are/)
- [Données ouvertes (Wikipédia)](https://en.wikipedia.org/wiki/Open_data)
- Open Knowledge Foundation (2022) [“Qu'est-ce que l'Open?”](https://okfn.org/opendata/)
