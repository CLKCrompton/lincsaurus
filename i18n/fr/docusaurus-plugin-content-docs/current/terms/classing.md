---
id: classing
title: Classement
hoverText: Pour déclarer une entité comme étant une instance d'une classe en utilisant rdf:type dans l'ontologie choisie pour l'ensemble de données.
---

<!-- @format -->

Classer, c'est déclarer un %%entité|entity%%% pour être une instance d'une classe utilisant rdf:type dans le %%ontologie|ontology%% pour l'ensemble de données. La déclaration de la classe d'une entité attribue à cette entité des propriétés potentielles qui prennent cette classe comme leur %%domain|domain%% ou %%range|range%%, ainsi que de déclarer en termes généraux ce qu'est ce type de "chose" qu'est cette entité. La classification structure les entités et ne doit pas être confondue avec %%saisie|typing%%.

## Exemples

- Projet AdArchives : dans l'extrait TTL suivant, wikidata:Q8030842, le Women's Caucus for Art, est classé comme un E74_Group, selon la définition suivante fournie par le CIDOC CRM : "Cette classe comprend tous les rassemblements ou organisations d'individus ou de groupes humains qui agissent collectivement ou de manière similaire en raison de toute forme de relation fédératrice ». Comme cette entité est classée comme E74_Group, on peut alors lui attribuer une résidence actuelle ou ancienne en utilisant la propriété P74, l'entité agissant comme domaine et la localisation comme plage. De même, c'est la gamme d'une relation construite avec la propriété P95i. Cette propriété relie les événements de formation aux groupes, le premier agissant comme domaine et le second comme plage.

```texte
wikidata:Q8030842 a crm:E74_Group ;
rdfs:label "Women's Caucus for Art"@fr ;
crm:P2_has_type wikidata:Q15911314 ;
crm:P74_has_current_or_former_residence lincs:y0cAxtVpahj ;
crm:P95i_was_formed_by lincs:HKa2CAjHuYd .
```

## Autres ressources

- [Classe (Wikipédia)](<https://en.wikipedia.org/wiki/Class_(knowledge_representation)>)
- Université sémantique (2022) [“RDFS Introduction”](https://cambridgesemantics.com/blog/semantic-university/learn-owl-rdfs/)
- W3C (2014) [“2. Classes”](https://www.w3.org/TR/rdf-schema/#ch_classes)
