---
id: linked-data
title: Données liées (LD)
hoverText: Données structurées qui sont liées à d'autres données via le Web et s'appuient sur des technologies Web standard pour partager des données lisibles par machine entre ordinateurs.
---

<!-- @format -->

Les données liées (LD) sont des données structurées qui sont liées à d'autres données via le Web. Il s'appuie sur des technologies Web standard, telles que le %%Resource Description Framework (RDF)|resource-description-framework%% et %%Uniform Resource Identifiers (URI)|uniform-resource-identifier%%, et les utilise pour partager des données lisibles par machine entre ordinateurs.

LD a quatre exigences :

1. Utilise les URI comme noms pour les choses
2. Utilise des URI HTTP pour que les gens puissent rechercher ces noms
3. Fournit des informations utiles, en utilisant des normes ouvertes telles que RDF et %%SPARQL|sparql-protocol-and-rdf-query-language%%, lorsque les gens recherchent un URI
4. Inclut des liens vers d'autres URI afin que les gens puissent découvrir plus de choses

## Exemples

- [Cloud de données ouvertes lié](https://lod-cloud.net/)

## Autres ressources

- [Données liées (Wikipédia)](https://en.wikipedia.org/wiki/Linked_data)
- W3C (2015) [« Données liées »](https://www.w3.org/standards/semanticweb/data)
- W3C (2016) [« Données liées »](https://www.w3.org/wiki/LinkedData)
