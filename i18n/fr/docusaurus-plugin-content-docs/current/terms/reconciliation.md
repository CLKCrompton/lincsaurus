---
id: reconciliation
title: Réconciliation
hoverText: Processus consistant à s'assurer qu'une entité dans un ensemble de données fait référence à un identificateur de ressource uniforme (URI) stable, idéalement à partir d'un espace de noms stable, pour rendre les données plus accessibles, interopérables et efficaces lors de la recherche, du stockage et de la récupération.
---

<!-- @format -->

La réconciliation est un processus qui garantit que les personnes, les lieux, les attributs, les événements et autres %%les entités|entity%% dans un ensemble de données se réfèrent à stable %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%%, idéalement à partir de stables, standard %%espaces de noms|namespace%%. L'avantage du rapprochement d'entités est qu'il améliore l'accès et l'interopérabilité entre les institutions, est plus efficace lors de la recherche, du stockage et de la récupération des données, améliore la qualité de la description et nécessite moins de ressources pour la maintenance dans le temps. Cela permet également de connecter plusieurs ensembles de données, car ils se réfèrent aux mêmes choses de la même manière. Réconcilier les entités d'un ensemble de données discrètes avec un %%les enregistrements d'autorité|authority-record%% permettent aux machines de reconnaître les entités sur les plates-formes et les systèmes.

## Exemples

- Virtual International Authority File (2021) [“Cuthland, Ruth”](https://viaf.org/viaf/266374245/#skos:Concept)

## Autres ressources

- OpenRefine (2022) ["Réconciliation"](https://docs.openrefine.org/manual/reconciling)
- Rob Sanderson (2016) [« Boule de neige de données liées ou pourquoi nous avons besoin de réconciliation »](https://www.slideshare.net/azaroth42/linked-data-snowball-or-why-we-need-reconciliation) [PowerPoint]
- Smith-Yoshimura (2016) [« Réconciliation des métadonnées »](https://hangingtogether.org/metadata-reconciliation/)
