---
id: world-wide-web-consortium
title: Consortium World Wide Web (W3C)
hoverText: Une communauté internationale qui travaille à développer des standards Web.
---

<!-- @format -->

Le World Wide Web Consortium (W3C) est une communauté internationale dirigée par Tim Berners-Lee et Jeffrey Jaffe qui travaille au développement de normes Web. La mission du W3C est de « conduire le World Wide Web à son plein potentiel en développant des protocoles et des lignes directrices qui assurent la croissance à long terme du Web ».

## Autres ressources

- W3C (2022) [« Faits sur le W3C »](https://www.w3.org/Consortium/facts)
- W3C (2022) [« Mission du W3C »](https://www.w3.org/Consortium/mission)
