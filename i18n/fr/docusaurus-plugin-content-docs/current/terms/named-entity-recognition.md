---
id: named-entity-recognition
title: Reconnaissance d'entité nommée (NER)
hoverText: Processus d'identification et de catégorisation d'entités (un mot ou un ensemble de mots faisant référence à la même chose) dans un texte.
---

<!-- @format -->

La reconnaissance d'entité nommée (NER) consiste à identifier et à catégoriser %%entités|entity%%—un mot ou un ensemble de mots qui fait référence à la même chose—dans le texte. Le NER implique donc deux étapes : (1) identifier l'entité et (2) la catégoriser. Des exemples de catégories d'entités peuvent être des personnes, des lieux, des heures, des valeurs monétaires, etc. Le processus d'attribution d'un identifiant unique aux entités est appelé %%Désambiguisation d'entité nommée (NED)|named-entity-disambiguation%%. NER est une application de %%Traitement du langage naturel (TLN)|natural-language-processing%%.

## Exemples

- [NERVE](https://github.com/cwrc/NERVE)
- [Stanford Named Entity Recognizer](https://nlp.stanford.edu/software/CRF-NER.html)

## Autres ressources

- Holand et al. (2015) ["Explorer la reconnaissance et la désambiguïsation des entités pour les collections du patrimoine culturel"](https://freeyourmetadata.org/publications/named-entity-recognition.pdf)
- [Reconnaissance d'entité nommée (Wikipedia)](https://en.wikipedia.org/wiki/Named-entity_recognition)
- Selig (2021) [« Extraction d'entité : comment ça marche ? »](https://expertsystem.com/entity-extraction-work/)
