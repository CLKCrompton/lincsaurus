---
id: knowledge-map
title: Carte des connaissances (ResearchSpace)
hoverText: Un outil de visualisation dans l'environnement ResearchSpace qui affiche les différentes entités de données dans le triplestore et comment elles sont connectées à d'autres entités de données.
---

<!-- @format -->

[ResearchSpace](/docs/tools/researchspace) utilise le terme carte de connaissances pour décrire comment leur %%le graphe de connaissances|knowledge-graph%% contient et visualise les %%Données liées (LD)|linked-data%%. La carte des connaissances de ResearchSpace est un outil de visualisation pour afficher les différentes données %%entités|entity%% dans %%triplestore|triplestore%% et comment ceux-ci sont connectés à d'autres entités de données à l'aide de relations spécifiques.

## Autres ressources

- Oldman & Tanase (2018) [« Remodeler le graphe des connaissances en connectant les chercheurs, les données et les pratiques dans ResearchSpace »](https://pdfs.semanticscholar.org/9bc8/63036314d24f2d9851b3dc6ae727a55b8b9e.pdf)
