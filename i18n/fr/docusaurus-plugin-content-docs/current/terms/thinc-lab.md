---
id: thinc-lab
title: Laboratoire THINC
hoverText: Un espace à l'Université de Guelph qui soutient la recherche collaborative, interdisciplinaire et numérique en sciences humaines.
---

<!-- @format -->

Le laboratoire de collaboration interdisciplinaire en sciences humaines (THINC) est un espace de la bibliothèque McLaughlin de l'Université de Guelph qui soutient la collaboration, l'interdisciplinarité et %%Recherche en humanités numériques (DH)|digital-humanities%%. Le laboratoire THINC fournit un espace, une programmation, des ressources et un soutien à ceux qui souhaitent adopter des méthodes numériques pour mener des recherches en sciences humaines.

## Autres ressources

- Université de Guelph (2022) [“THINC Lab”](https://www.uoguelph.ca/arts/dhguelph/thinc)
