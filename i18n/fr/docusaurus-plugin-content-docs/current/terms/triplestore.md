---
id: triplestore
title: Triplestore
hoverText: Une base de données NoSQL qui stocke les triplets.
---

<!-- @format -->

Un triplestore est une sorte de %%Base de données NoSQL|nosql-database%% qui stocke %%triples|triple%%. Il présente des similitudes avec %%bases de données de graphes|graph-database%%, mais les deux termes ne sont pas synonymes - voir le tableau de comparaison ci-dessous. Les triplestores peuvent être interrogés à l'aide de %%SPARQL|sparql-protocol-and-rdf-query-language%%.

| Base de données graphique                                          | Triplestore                             |
| ------------------------------------------------------------------ | --------------------------------------- |
| Prend en charge une variété de langages de requête tels que Cypher | Utilise SPARQL comme langage de requête |
| Stocke divers types de graphiques                                  | Stocke des rangées de triples           |
| Centré sur les nœuds/propriétés                                    | Centré sur les bords                    |
| Ne fournit pas d'inférences sur les données                        | Fournit des inférences sur les données  |
| Moins académique                                                   | Plus synonyme de « web sémantique »     |

## Exemples

- [Apache Jena](https://jena.apache.org/)
- [BlazeGraph](https://blazegraph.com/)
- [GraphDB](https://www.ontotext.com/products/graphdb/)
- [Virtuose](https://virtuoso.openlinksw.com/)

## Autres ressources

- Gilbert (2016) [« Triplestores 101 : Stocker des données pour une inférence efficace »](https://www.dataversity.net/triplestores-101-storing-data-efficient-inferencing/#)
- Ontotext (2022) ["Qu'est-ce qu'un Triplestore RDF?"](https://www.ontotext.com/knowledgehub/fundamentals/what-is-rdf-triplestore/)
- [Triplestore (Wikipédia)](https://en.wikipedia.org/wiki/Triplestore)
