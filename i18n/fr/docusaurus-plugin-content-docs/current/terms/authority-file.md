---
id: authority-file
title: Authority File
hoverText: An authority file is a list that contains the authoritative way to reference people, places, things, or concepts, usually as a heading or numeric identifier.
---

<!-- @format -->

An authority file is a list that contains the authoritative way to reference people, places, things, or concepts, usually as a heading or numeric identifier. Authority files are used to promote authority control, the use of a single spelling and/or format to reference something.

Authority files are often created by authoritative bodies. For example, the Library of Congress, the largest library in the world, has created authority files for the names of authors and the titles of books. These authority files are used by libraries across North America to guide cataloguing decisions. If many libraries use the same heading or numeric identifier to refer to the same author in their catalogues, the catalogue records can be more easily linked together.

Many authority files are considered to be %%controlled vocabularies|controlled-vocabulary%%.

## Examples

- [Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- [Cultural Objects Name Authority (CONA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- [Iconography Authority (IA)](https://www.getty.edu/research/tools/vocabularies/cona/index.html)
- [Library of Congress Authorities](https://authorities.loc.gov/)
- [Thesaurus of Geographic Names (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn)
- [Union List of Artist Names (ULAN)](https://www.getty.edu/research/tools/vocabularies/ulan/index.html)
- [VIAF: The Virtual International Authority File](https://viaf.org/)

## Further Resources

- [Authority Control (Wikipedia)](https://en.wikipedia.org/wiki/Authority_control)
- Mason (2023)[“Purpose of Authority Work and Files”](https://www.moyak.com/papers/libraries-bibliographic-control.html)
- OCLC (2023) [“Available Authority Files”](https://help.oclc.org/Metadata_Services/Authority_records/Authorities_Format_and_indexes/Get_started/40Available_authority_files)
