---
id: shapes-constraint-language
title: Langage de contraintes de formes (SHACL)
hoverText: Une norme pour décrire les graphes RDF (Resource Description Framework) et les valider par rapport à un ensemble de conditions.
---

<!-- @format -->

Shapes Constraint Language (SHACL) est une norme pour décrire %%Resource Description Framework (RDF)|resource-description-framework%% graphes et en les validant par rapport à un ensemble de conditions, fournies sous la forme d'un graphe RDF et appelées _shapes graphs_. Les graphes RDF qui sont validés par rapport à un graphe de formes sont appelés _graphes de données_. SHACL utilise %%SPARQL|sparql-protocol-and-rdf-query-language%% pour valider les graphiques de données par rapport aux graphiques de formes.

## Exemples

- Cagle (2016) ["Meet SHACL, the Next OWL"](https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/) : l'exemple suivant décrit une forme utilisateur nécessitant deux propriétés : la présence d'un seul foaf:name sous forme de chaîne et d'au moins un foaf:mbox qui est une adresse e-mail valide.

```texte
Forme "Utilisateur" SHACL :
ex : FormeUtilisateur
a sh:Forme ;
sh:propriété [
sh:prédicat foaf:nom ;
sh:type de données xsd:chaîne ;
sh:minCount 1 ;
sh:maxCount 1 ;
] ;
sh:propriété [
sh:prédicat foaf:mbox ;
sh:nodeKind sh:IRI ;
sh:minCount 1 ;
] .
```

- Cagle (2016) ["Meet SHACL, the Next OWL"](https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/) : L'instance suivante satisferait l'instruction SHACL . Il réussit car il remplit les conditions définies par la forme : un (et un seul) foaf:name qui est une chaîne et au moins un foaf:mbox qui est une ressource Web.

```texte
inst : Utilisateur1
un pouf:Personne ;
foaf:nom "Jane Doe" ;
foaf:mbox mailto:jd@example.org> ;
foaf:mbox mailto:janedoe@gmail.com> .
```

- Cagle (2016) ["Meet SHACL, the Next OWL"](https://www.linkedin.com/pulse/meet-shacl-next-owl-kurt-cagle/) : L'instance suivante ne satisferait pas à la Déclaration SHACL. Cette instance échoue car il y a deux entrées pour foaf:name au lieu d'une, enfreignant ainsi la règle sh:maxCount 1.

```texte
inst : Utilisateur2
un pouf:Personne ;
foaf:nom "Sarah Doe", "Sara Doe" ;
foaf:mbox mailto:sd@example.org> ;
foaf:mbox mailto:sarahdoe@gmail.com> .
```

## Autres ressources

- [SHACL (Wikipédia)](https://en.wikipedia.org/wiki/SHACL)
- TopQuadrant (2017) ["An Overview of SHACL Shapes Constraint Language"](https://www.youtube.com/watch?v=_i3zTeMyRzU) [Vidéo]
- W3C (2017) _[Langage de contraintes de formes (SHACL)](https://www.w3.org/TR/shacl/)_
