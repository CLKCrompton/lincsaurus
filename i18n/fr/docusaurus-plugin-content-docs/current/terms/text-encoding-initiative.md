---
id: text-encoding-initiative
title: Initiative de codage de texte (TEI)
hoverText: Un langage d'encodage qui prend en charge l'encodage détaillé de documents complexes et est largement utilisé par un certain nombre de différents projets d'humanités numériques (DH).
---

<!-- @format -->

La Text Encoding Initiative (TEI) est un langage d'encodage destiné à prendre en charge l'encodage détaillé de documents complexes et est largement utilisé par un certain nombre de %%Projets Humanités Numériques (DH)|digital-humanities%%. En tant que langage d'encodage, il sert à la fois à décrire les caractéristiques physiques d'un texte (sauts de ligne, pagination, caractères spéciaux, etc.), à classer les sujets, à taguer les personnes, les lieux et les événements, et à ajouter des commentaires.

## Exemples

- [Carte du début de Londres moderne](https://mapoflondon.uvic.ca/)
- [Orlando](http://www.artsrn.ualberta.ca/orlando/)

## Autres ressources

- Crompton et al. (2021), [« Cartographie et vérification : conversion de la TEI en données liées »](https://www.dropbox.com/s/lj94h40odvrlnuk/Crompton-Zafar-Canning-Lipski-Defours-LINCS-TEI-2021.pdf?dl=0) [PowerPoint]
- Crompton (2022) [« Ce que l'ordinateur ne sait pas : représenter les documents sources primaires dans la TEI »](http://constancecrompton.com/2015/cin/CromptonTEI.pdf) [PowerPoint]
- Text Encoding Initiative (2022) [“Projets utilisant la TEI”](https://tei-c.org/activities/projects/)
