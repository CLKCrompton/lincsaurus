---
id: dewey-decimal-classification-system
title: Dewey Decimal Classification System (DDC)
hoverText: A library classification system that is commonly used by public libraries and small academic libraries to organize print collections.
---

<!-- @format -->

The Dewey Decimal Classification System (DDC) is a library classification system that was developed by Melvil Dewey in 1876. It is currently used by public libraries and small academic libraries to organize their print collections.

## Further Resources

* [Dewey Decimal Classification (Wikipedia)](https://en.wikipedia.org/wiki/Dewey_Decimal_Classification)
* OCLC (2003) [“Summaries: DDC Dewey Decimal Classification”](https://www.oclc.org/content/dam/oclc/dewey/resources/summaries/deweysummaries.pdf)
