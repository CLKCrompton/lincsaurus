---
id: web-ontology-language
title: Langage d'ontologie Web (OWL)
hoverText: Un langage de représentation des connaissances pour les ontologies qui représente explicitement la signification des termes dans les vocabulaires et les relations entre ces termes, ainsi qu'entre les groupes de termes.
---

<!-- @format -->

Le langage d'ontologie Web (OWL) est un langage de représentation des connaissances pour %%ontologies|ontology%%. Conçu pour la création d'ontologies, OWL peut être utilisé pour représenter explicitement la signification des termes dans %%vocabulaires|vocabulary%% et les relations entre ces termes ainsi que des groupes de termes. OWL répond aux limitations de %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%⁠—à savoir qu'il est incapable de localiser %%range|range%% et %%domain|domain%% contraintes, définir des contraintes de cardinalité, ou représenter inverse ou symétrique %%properties|property%%⁠—et en tant que tel est plus capable d'exprimer le sens et la sémantique. OWL est capable de résoudre ces problèmes et de prendre en charge %%inférence|inferencing%% à travers une ontologie, et va donc au-delà de RDFS dans sa capacité à représenter du contenu interprétable par machine sur le Web.

Il existe trois variantes d'OWL, avec différents niveaux d'expressivité : OWL Lite, OWL DL et OWL Full. Parce que leur relation repose sur des degrés de complexité, chaque ontologie OWL Lite (juridique) est également une ontologie OWL DL, qui est également une ontologie OWL Full. OWL Lite fournit une hiérarchie de classification simplifiée et des contraintes de base pour les utilisateurs souhaitant migrer rapidement des ensembles de données simples tels que %%thesaurus|thesaurus%% et autres %%taxonomies|taxonomy%%. C'est une ontologie formellement moins complexe que OWL DL. OWL DL ou OWL Description Logic est une version contrainte d'OWL Full qui permet l'utilisation de systèmes de raisonnement puissants qui s'appuient sur Description Logic pour fonctionner. OWL Full représente le langage OWL complet. Il permet l'adoption et l'adaptation totales du %%Resource Description Framework (RDF)|resource-description-framework%%, et il est donc préféré pour ceux qui veulent utiliser l'expressivité de OWL avec la flexibilité et les forces de métamodélisation de RDF . Cependant, son utilisation le rend moins utilisable pour les systèmes de raisonnement que OWL Lite ou OWL DL.

## Autres ressources

- [Langage d'ontologie Web (Wikipédia)](https://en.wikipedia.org/wiki/Web_Ontology_Language)
- W3C (2004) [« Présentation du langage d'ontologie Web OWL »](https://www.w3.org/TR/owl-features/)
- W3C (2004) ["OWL Web Ontology Language Reference"](https://www.w3.org/TR/owl-ref/)
- W3C (2013) ["OWL"](https://www.w3.org/OWL/)
