---
id: metadata
title: Metadata
hoverText: Structured information that describes or explains an information object so it can be searched for, retrieved, contextualized, validated, preserved, or managed.
---

<!-- @format -->

Metadata is structured data about data. It is information that describes or explains an information object so it can be searched for, retrieved, contextualized, validated, preserved, or managed. There are many different types of metadata, including:

- **Administrative:** Metadata that helps manage or administer an information object (e.g., resource type, version number, intellectual property rights, provenance, preservation information, circulation records)
- **Descriptive:** Metadata that helps identify, authenticate, describe, discover, or retrieve an information object (e.g., title, author, publisher, publication date, abstract, keywords)
- ***Structural:** Metadata that helps encode relationships within and among objects through elements such as links to other components (e.g., how pages are put together to form a chapter, how a system functions)

To ensure that metadata is consistent and useful, it is important to follow a metadata standard: a documented way to structure and implement metadata, usually used in a particular domain or discipline.

## Examples

- Frontmatter of a print book (title, author, publisher, date, etc.)
- Contextual information stored in a digital photograph (make of the camera, time the photograph was taken, image resolution, GPS coordinates, etc.)
- Contextual information about Geographic Information System (GIS) data (data quality, source agency, coordinate system, access and use constraints, etc.)
