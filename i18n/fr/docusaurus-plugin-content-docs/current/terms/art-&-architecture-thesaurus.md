---
id: art-&-architecture-thesaurus
title: Art & Architecture Thesaurus (AAT)
hoverText: One of five Getty Vocabularies that contains Uniform Resource Identifiers (URIs) for generic terms related to art, architecture, and visual cultural heritage.
---

The Art & Architecture Thesaurus (AAT) is one of five Getty Vocabularies developed by the Getty Research Institute to provide terms and %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% for concepts and objects related to the arts. LINCS uses the AAT to provide URIs for generic terms related to art, architecture, and visual cultural heritage.

The AAT includes terms related to:

- Cultures
- Materials
- Roles
- Styles
- Subjects
- Techniques
- Work types

The AAT excludes iconographical subject terms, proper names, and unbound compound terms (e.g., “Baroque cathedral”).

## Further Resources

- [Art & Architecture Thesaurus (AAT)](https://www.getty.edu/research/tools/vocabularies/aat/)
- J. Paul Getty Trust (2022) [“About the AAT”](https://www.getty.edu/research/tools/vocabularies/aat/about.html)
