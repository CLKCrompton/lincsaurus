---
id: turtle
title: Turtle
hoverText: A human- and machine-readable markup language that allows users to serialize triples.
---
