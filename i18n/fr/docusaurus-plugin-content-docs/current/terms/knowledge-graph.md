---
id: knowledge-graph
title: Graphique des connaissances
hoverText: Représentation d'un ensemble de triplets liés qui illustre les relations entre eux.
---

<!-- @format -->

Un graphe de connaissances représente un ensemble fini de %%triples|triple%% et illustre les relations entre eux. Les informations sont stockées dans une %%base de données de graphes|graph-database%% et visualisée sous forme de structure de graphe. Le %%les entités|entity%% et les relations qui forment les triplets liés doivent être créées à l'aide d'une sémantique formelle, contribuer les unes aux autres et représenter diverses données connectées et décrites par des métadonnées sémantiques. Un graphe de connaissances est similaire dans sa structure à un %%property-graph|property-graph%%, mais il a des schémas formalisés et d'autres structures plus sophistiquées qui étendent son utilité sur différentes plates-formes et avec divers ensembles de données.

## Exemples

- WC3 (2014) *[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)* : le schéma suivant décrit un graphe informel de triplets simples.

![Graphique de connaissances montrant la relation d'un exemple de personne avec La Joconde et les métadonnées associées en triplets.](/img/documentation/glossary-knowledge-graph-example-(fair-dealing).png)

_Autorisé avec [licence W3C](https://www.w3.org/Consortium/Legal/2015/doc-license)._

## Autres ressources

- British Museum (2021) ["ResearchSpace : Qu'est-ce qu'un graphe de connaissances ?"](https://researchspace.org/knowledge-graph-and-patterns/)
- [Graphique des connaissances (Wikipédia)](https://en.wikipedia.org/wiki/Knowledge_graph)
- Oldman & Tanase (2018) [« Remodeler le graphe des connaissances en connectant les chercheurs, les données et les pratiques dans ResearchSpace »](https://link.springer.com/content/pdf/10.1007/978-3-030-00668-6 .pdf)
- Ontotext (2022) [« Qu'est-ce qu'un graphe de connaissances ? »](https://www.ontotext.com/knowledgehub/fundamentals/what-is-a-knowledge-graph/)
- WC3 (2014) _[RDF 1.1 Primer](https://www.w3.org/TR/rdf11-primer/)_
