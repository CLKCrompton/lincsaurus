---
id: triple
title: Tripler
hoverText: Une déclaration sous la forme sujet-prédicat-objet qui suit le cadre de description des ressources (RDF).
---

<!-- @format -->

Un triplet est un énoncé sous la forme sujet-prédicat-objet. Ces assertions sont faites à l'aide de la %%Resource Description Framework (RDF)|resource-description-framework%% et sont ce qui constitue le %%Web sémantique|semantic-web%%.
