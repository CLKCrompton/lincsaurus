---
id: conversion
title: Conversion
hoverText: Le processus de modification des données d'un format à un autre.
---

<!-- @format -->

La conversion de données est le processus de modification des données d'un format à un autre. L'objectif de la conversion de données est d'empêcher la perte ou la corruption de données en maintenant l'intégrité des données et des structures intégrées. La conversion des données doit avoir pour objectif de maintenir ou d'augmenter le sens communiqué par les données, leur structure et leur format.

## Autres ressources

- [Conversion de données (Wikipédia)](https://en.wikipedia.org/wiki/Data_conversion)
- Langmann (2022) ["Comment convertir une feuille de calcul Excel en XML"](https://spreadsheeto.com/xml/)
- W3C (2015) [« Génération de RDF à partir de données tabulaires sur le Web »](https://www.w3.org/TR/csv2rdf/)
