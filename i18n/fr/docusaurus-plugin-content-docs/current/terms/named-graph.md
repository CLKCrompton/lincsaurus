---
id: named-graph
title: Graphique nommé
hoverText: Une extension du modèle de données RDF (Resource Description Framework) dans lequel un graphe RDF est identifié à l'aide d'un URI (Uniform Resource Identifier), permettant ainsi la publication et la présentation de métadonnées sur ce graphe dans son ensemble.
---

<!-- @format -->

Les graphes nommés sont une extension du %%Resource Description Framework (RDF)|resource-description-framework%% modèle de données dans lequel un graphe RDF est identifié à l'aide d'un %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%, permettant ainsi la publication et la présentation de métadonnées sur ce graphique dans son ensemble. Le concept derrière les graphes nommés est que le fait d'avoir plusieurs graphes RDF dans un seul référentiel et de les nommer avec des URI fournit des fonctionnalités supplémentaires utiles. Les graphes nommés peuvent être représentés comme la quatrième partie d'un %%quad|quad%%, `<graphname>` étant l'URI du graphe nommé. Ce processus permet de suivre le %%provenance|provenance%% et la source des données RDF et prend en charge la gestion des versions en fournissant un moyen de décrire des éléments tels que des informations sur la création et la modification de l'instruction %%triples|triple%% dans le graphe nommé. Un graphe nommé est parfois appelé _quad store_.

## Autres ressources

- Dodds & Davis (2012) ["Graphiques nommés"](https://patterns.dataincubator.org/book/named-graphs.html)
- [Graphique nommé (Wikipédia)](https://en.wikipedia.org/wiki/Named_graph)
- W3C (2010) ["RDF Graph Literals and Named Graphs"](https://www.w3.org/2009/07/NamedGraph.html#named-graphs)
