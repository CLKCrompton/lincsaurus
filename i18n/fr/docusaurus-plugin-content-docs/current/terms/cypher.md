---
id: cypher
title: Zéro
hoverText: Un langage de requête pour les bases de données de graphes qui reflète la nature sémantique des triplets mais le fait avec sa propre syntaxe et sa propre mise en forme.
---

<!-- @format -->

Cypher est un langage de requête qui vous permet d'interroger %%base de données de graphes|graph-database%%. Comme %%SPARQL|sparql-protocol-and-rdf-query-language%%, le langage de requête Cypher reflète la nature sémantique de %%triple|triple%% mais le fait avec sa propre syntaxe et sa propre mise en forme. En raison de cette similitude, Cypher peut également être considéré comme un ensemble de phrases contenant des blancs. La base de données graphique prendra cette requête et trouvera chaque ensemble d'instructions correspondantes qui remplit correctement ces blancs.

## Exemples

- La requête suivante indique à la base de données de rechercher tous les nœuds Person connectés aux nœuds Movie via une relation `ACTED_IN`. Il réduit davantage les nœuds Movie à ceux où la propriété title commence par la lettre « t ». Enfin, il indique à la base de données de renvoyer toutes ces informations sur deux colonnes : le movie.title dans une (_title_) et tous les noms d'acteurs (_cast_) ensemble dans une seconde et de classer cette liste par ordre alphabétique des titres.

```texte
MATCH (acteur:Personne)-[:ACTED_IN]->(film:Film)
OÙ movie.title COMMENCE PAR "T"
RETURN movie.title AS title, collect(actor.name) AS cast
ORDRE PAR titre ;
```

## Autres ressources

- [Cypher (Wikipédia)](<https://en.wikipedia.org/wiki/Cypher_(query_language)>)
- Neo4j (2017) [« Introduction à Cypher »](https://www.youtube.com/watch?v=pMjwgKqMzi8) [Vidéo]
- Neo4j (2022) ["Cypher Query Language"](https://neo4j.com/developer/cypher-basics-i/)
