---
id: thesaurus-of-geographic-names
title: Thesaurus of Geographic Names (TGN)
hoverText: One of five Getty Vocabularies that contains Uniform Resource Identifiers (URIs) for names, relationships, place types, dates, and coordinates.
---

The Thesaurus of Geographic Names (TGN) is one of five Getty Vocabularies developed by the Getty Research Institute to provide terms and %%Uniform Resource Identifiers (URIs)|uniform-resource-identifier%% for names, relationships, place types, dates, and coordinates for places related to art, architecture, and other visual cultural heritage.

The TGN includes terms related to:

- Archaeological sites
- Empires
- Inhabited places (cities, towns, villages)
- Lost settlements (historically documented)
- Named general areas
- Nations
- Physical features
- Tribal areas

The TGN excludes imaginary or legendary places and underwater features.

## Further Resources

- The Collections Trust (2023) [“Thesaurus of Geographic Names (TGN) (Getty)”](https://collectionstrust.org.uk/resource/thesaurus-of-geographic-names-tgn-getty/)
- [Thesaurus of Geographic Names (TGN)](https://www.getty.edu/research/tools/vocabularies/tgn)
