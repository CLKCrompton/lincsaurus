---
id: resource-description-framework-serialization
title: Sérialisation du cadre de description des ressources (RDF)
hoverText: Une syntaxe qui peut être utilisée pour écrire des triplets, y compris Turtle (TTL), XML (XML-RDF) et JSON.
---

<!-- @format -->

Le %%Resource Description Framework (RDF)|resource-description-framework%% est une norme pour l'échange de données et le format standard pour %%Données liées (LD)|linked-data%%. RDF représente les informations dans une série d'«instructions» en trois parties appelées %%triple|triple%% qui comprend un sujet, un prédicat et un objet sous la forme : `<subject><predicate><object>`. De cette manière, RDF décrit les données en définissant les relations entre les objets de données. RDF peut être sérialisé de différentes manières, avec des formats courants tels que Turtle (TTL), %%XML|xml%% (XML-RDF) et JSON. Le LINCS %%triplestore|triplestore%% ingère des données au format TTL.

Turtle (TTL), ou Terse RDF Triple Language, est un format de sérialisation RDF hautement lisible par l'homme. Le format TTL définit des préfixes au début du fichier et encourage le blocage des triplets avec le même sujet, permettant au lecteur d'interpréter les données beaucoup plus efficacement. Il est plus coûteux à analyser car il s'agit d'un système relativement verbeux. Cependant, il reste l'un des formats de sérialisation les plus simples à éditer à la main.

XML-RDF est le format de sérialisation RDF le plus ancien et le plus difficile à analyser. Il combine un ancien format arborescent hiérarchique avec le format de graphique à base triple requis pour LD, ce qui signifie qu'il ne reflète pas clairement le modèle triple et peut semer la confusion chez les lecteurs.

JSON-LD est une version LD de JSON, un format de fichier standard ouvert et un format d'échange de données. JSON est le moyen le plus populaire de sérialiser les données dans les applications Web et, par conséquent, son adoption pour LD bénéficie de la familiarité de JSON avec de nombreux programmeurs. Cependant, JSON-LD est difficile et coûteux à analyser, ce qui signifie que sa faisabilité dans des projets LD plus complexes est limitée.

## Exemples

- Meindertma (2019) ["Quel est le meilleur format de sérialisation RDF ?"](https://ontola.io/blog/rdf-serialization-formats/) : les extraits RDF suivants indiquent que Tim Berners Lee est né le 8 juin 1955 à Londres, en Angleterre.

Tortue:

```texte
@prefix tim : <https://www.w3.org/People/Berners-Lee/>.
Schéma @prefix : <http://schema.org/>.
@prefix dbpedia : <http://dbpedia.org/resource/>.

<tim> schema:birthDate "1955-06-08"^^<http://www.w3.org/2001/XMLSchema#date>.
<tim> schema:birthPlace <dbpedia:London>.
```

RDF/XML :

```texte
<?xml version="1.0"?>
<rdf:RDF
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:schema="http://schema.org/">
<rdf:Description rdf:about="https://www.w3.org/People/Berners-Lee/">
<schema:birthDate>1966-06-08</schema:birthDate>
<schema:birthPlace rdf:resource="http://dbpedia.org/resource/Londres"/>
</rdf:Description>
</rdf:RDF>
```

JSON-LD :

```texte

"@le contexte": {
"dbpedia": "http://dbpedia.org/resource/",
"schéma": "http://schema.org/"
},
"@id": "https://www.w3.org/People/Berners-Lee/",
"schema:birthDate": "1955-06-08",
"schema:lieu de naissance": {
"@id": "dbpedia:Londres"


```

## Autres ressources

- Ressources de bourses d'études numériques (2019) ["Bases des données liées : sérialisations RDF et triplestores"](https://heardlibrary.github.io/digital-scholarship/lod/serialization/)
- Meindertma (2019) ["Quel est le meilleur format de sérialisation RDF ?"](https://ontola.io/blog/rdf-serialization-formats/)
- [Formats de sérialisation RDF (Wikipédia)](https://en.wikipedia.org/wiki/Resource_Description_Framework#Serialization_formats)
