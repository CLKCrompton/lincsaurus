---
id: query-variable
title: Variable de requête
hoverText: Un proxy pour l'objet que vous recherchez lors de la construction d'une requête SPARQL.
---

<!-- @format -->

Une variable de requête est un proxy pour le ou les objets que vous recherchez lors de la construction d'un %%Requête SPARQL|sparql-protocol-and-rdf-query-language%%. Les requêtes SPARQL sont composées de %%triples|triple%% : un sujet, un prédicat et un objet. Dans les requêtes SPARQL, chaque composant du triplet doit être soit une variable de requête, soit un %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%.

Les variables de requête sont indiquées par un point d'interrogation ou un signe dollar suivi d'un mot. Le mot que vous choisissez pour une variable est arbitraire, mais doit être lisible par l'homme pour faciliter la compréhension et doit être utilisé de manière cohérente dans une requête.

## Exemples

- Lincoln (2015) [“Using SPARQL to Access Linked Open Data”](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL) : Dans la requête suivante, la variable est `? peinture`. Cette variable indique au triplestore de rechercher toutes les valeurs de `?painting` qui complètent correctement la déclaration `<a un support><oil on canvas>`. `?painting` représente le(s) nœud(s) que la base de données renverra.

```texte
SÉLECTIONNER ?peinture
OÙ {
?peinture <a médium> <huile sur toile> .

```
