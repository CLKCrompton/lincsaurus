---
id: wemi
title: WEMI
hoverText: Acronyme signifiant Œuvre, Expression, Manifestation et Élément—termes dérivant des Exigences fonctionnelles pour les notices bibliographiques (FRBR), qui est le principal moyen de décrire les notices bibliographiques.
---

<!-- @format -->

WEMI est un acronyme signifiant Travail, Expression, Manifestation et Objet. Ces termes dérivent de [Functional Requirements for Bibliographic Records (FRBR)](https://www.oclc.org/research/activities/frbr.html), qui a ensuite été remplacé par le Library Reference Model dans [Resource Description and Access (RDA)](https://www.loc.gov/aba/rda/). Cependant, WEMI reste le principal moyen de description des notices bibliographiques. Par conséquent, WEMI est à la base de [FRBRoo](https://www.cidoc-crm.org/frbroo/home-0), le conforme au CRM. %%Version Linked Open Data (LOD)linked-open-data%% du modèle conceptuel, et [LRMoo](https://www.cidoc-crm.org/frbroo/ Issue/ID-360-lrmoo), qui remplacera FRBRoo une fois approuvé par le %%CIDOC CRM|cidoc-crm%% Groupe d'Intérêt Spécial.

Dans WEMI, l'œuvre représente l'histoire, la chanson, le poème ou la forme intangible d'une œuvre. L'Expression représente une forme spécifique de l'Œuvre, telle qu'un texte anglais (Expression) d'une histoire (Œuvre). Une Manifestation représente une Expression spécifique publiée ou produite, telle qu'une édition Penguin (Manifestation) d'un texte anglais (Expression) d'une histoire (Œuvre). L'Item représente l'unité individuelle de la Manifestation, telle que la copie de la Bibliothèque McLaughlin (Item) de l'édition Penguin (Manifestation) d'un texte anglais (Expression) d'une histoire (Work). Bref, une Œuvre se réalise à travers une Expression ; une Expression s'incarne dans une Manifestation ; et une Manifestation est illustrée par un Item ([Library of Congress, 2004](https://www.loc.gov/cds/downloads/FRBR.PDF)).

## Autres ressources

- [FRBRoo (Wikipédia)](https://en.wikipedia.org/wiki/FRBRoo)
- [Exigences fonctionnelles pour les notices bibliographiques (Wikipedia)](https://en.wikipedia.org/wiki/Functional_Requirements_for_Bibliographic_Records)
- [Modèle de référence de la bibliothèque IFLA (Wikipedia)](https://en.wikipedia.org/wiki/IFLA_Library_Reference_Model)
- RDA Basics (2012) ["Fondements théoriques de RDA (ou, qu'est-ce que WEMI ?)"](https://rdabasics.com/2012/08/24/theoretical-foundations/)
- Riva, Le Bœuf, & Žumer (2017) _[IFLA Library Reference Model: A Conceptual Model for Bibliographic Information](https://www.ifla.org/wp-content/uploads/2019/05/assets/cataloguing/frbr-lrm/ifla-lrm-august-2017_rev201712.pdf)_
- Tillett (2004) _[Qu'est-ce que FRBR ? : Un modèle conceptuel pour l'univers bibliographique](https://www.loc.gov/cds/downloads/FRBR.PDF)_
- Žumer, Aalberg, & O'Neill (2019) ["Application du modèle FRBR/LRM aux ressources continues"](http://library.ifla.org/id/eprint/2464/1/208-zumer-en.pdf)
