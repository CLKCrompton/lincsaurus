---
id: semantic-narrative
title: Récit sémantique (ResearchSpace)
hoverText: Un document interactif dans l'environnement ResearchSpace qui combine une narration textuelle et des données liées (LD) pour communiquer des idées sur les personnes, les lieux et les événements.
---

<!-- @format -->

Un récit sémantique est un document interactif qui combine un récit textuel et %%Données liées (LD)|linked-data%% pour communiquer des idées sur des personnes, des lieux et des événements. Dans l'environnement [ResearchSpace](/docs/tools/researchspace), un récit sémantique prend la forme d'un document texte interactif qui intègre des données intégrées, des visualisations de données et est connecté à l'%%graphique des connaissances|knowledge-graph%% pour s'assurer que le récit est automatiquement mis à jour lorsqu'il est modifié ou développé.

## Autres ressources

- British Museum (2021) [“ResearchSpace : Outils sémantiques"](https://researchspace.org/semantic-tools/)
- Oldman & Tanase (2018) [« Remodeler le graphe des connaissances en connectant les chercheurs, les données et les pratiques dans ResearchSpace »](https://link.springer.com/chapter/10.1007/978-3-030-00668-6_20)
