---
id: wikidata
title: Wikidata
hoverText: La plus grande instance de Wikibase, qui agit comme un référentiel de stockage central pour les données structurées utilisées par Wikipédia, par ses projets frères et par quiconque souhaite utiliser une grande quantité de données ouvertes à usage général.
---

<!-- @format -->

Wikidata est la plus grande instance de %%Wikibase|wikibase%%, une base de connaissances gratuite que tout le monde peut modifier. Il agit comme un référentiel de stockage central pour les données structurées utilisées par Wikipédia, par ses projets frères et par toute autre personne souhaitant utiliser une grande quantité de données ouvertes à usage général. Le contenu de Wikidata est disponible sous une licence gratuite et peut être exporté et lié à d'autres ensembles de données ouverts sur le Web ([Mediawiki, 2021](https://www.mediawiki.org/wiki/Wikibase/FAQ#What_is_the_difference_between_Wikibase_and_Wikidata?) ).

## Exemples

- [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page)

## Autres ressources

- MediaWiki (2021) [“Wikibase/FAQ”](https://www.mediawiki.org/wiki/Wikibase/FAQ#:~:text=General-,What%20is%20the%20difference%20between%20Wikibase%20and%20Wikidata%3F,base%20that%20anyone%20can%20edit.)
- Bibliothèque UCLA (2017) [« Web sémantique et données liées : Wikibase »](https://guides.library.ucla.edu/semantic-web/wikidata)
