---
sidebar_position: 6
title: "Validate and Enhance"
description: "PLACEHOLDER"
---

<!-- @format -->

<!-- NEEDS TRANSLATION -->

## Introduction

Nisi ipsum ex pariatur dolor minim et sunt ad. Consequat adipisicing nostrud pariatur proident. Ut ipsum amet labore sunt. Voluptate ex aute Lorem quis consequat adipisicing deserunt officia. Reprehenderit excepteur sint ipsum occaecat adipisicing adipisicing cillum. Quis sunt labore sint laborum aliqua minim est voluptate culpa sint irure officia ut. Consequat ex quis veniam deserunt commodo ipsum.

<!-- REPLACE TEXT ABOVE-->

## Setting Up Your Data

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
<TabItem value="structured" label="Structured Data" default>
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute incididunt sit.</li>
</ol>
</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute incididunt sit.</li>
</ol>
</TabItem>
<TabItem value="tei" label="TEI Data">
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute incididunt sit.</li>
</ol>
</TabItem>
<TabItem value="natural" label="Natural Language Data">
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute incididunt sit.</li>
</ol>
</TabItem>
</Tabs>

## Officia esse enim

Nisi ipsum ex pariatur dolor minim et sunt ad. Consequat adipisicing nostrud pariatur proident. Ut ipsum amet labore sunt. Voluptate ex aute Lorem quis consequat adipisicing deserunt officia. Reprehenderit excepteur sint ipsum occaecat adipisicing adipisicing cillum. Quis sunt labore sint laborum aliqua minim est voluptate culpa sint irure officia ut. Consequat ex quis veniam deserunt commodo ipsum.

<!-- REPLACE TEXT ABOVE-->

## Officia esse enim

Nisi ipsum ex pariatur dolor minim et sunt ad. Consequat adipisicing nostrud pariatur proident. Ut ipsum amet labore sunt. Voluptate ex aute Lorem quis consequat adipisicing deserunt officia. Reprehenderit excepteur sint ipsum occaecat adipisicing adipisicing cillum. Quis sunt labore sint laborum aliqua minim est voluptate culpa sint irure officia ut. Consequat ex quis veniam deserunt commodo ipsum.

<!-- REPLACE TEXT ABOVE-->

## Expected Outputs

Proident ea amet sit culpa eu adipisicing commodo eiusmod irure commodo id commodo et:

1. Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.
2. Non sint ipsum dolore laborum ex cillum ullamco.
3. Aute labore reprehenderit est aute incididunt sit.
4. Nostrud consequat ex voluptate aliquip enim consequat mollit magna aliquip aliqua ullamco ut.
<!-- REPLACE TEXT ABOVE-->
