---
sidebar_position: 4
title: "Clean Data"
description: "LINCS Conversion Workflows — Clean Data Step"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Introduction

In the data cleaning step, you ensure your data is consistent in how it expresses entities and the relationships between them. Your data needs to be consistent and clean to be mapped and converted.

## When should I clean my data?

By cleaning your data at this early stage, the rest of the conversion steps will be easier and your converted data will be more accurate and more meaningful. 

:::info
Data cleaning is often time consuming; LINCS recommends you start as soon as you can. Go ahead and follow the tips on this page, even before committing to the entire conversion process.
:::

Typically, it is fastest to do bulk cleaning tasks on your original data because:
- Your team is already familiar with the format
- In the case of structured data, for example, it's efficient to make changes to a whole column of a spreadsheet at once and there are many common tools to help
- By cleaning your original data, it is improved for other uses you have for your data besides publishing with LINCS

Still, you can continue with more cleaning at the [Validate and Enhance Step](/docs/create-data/publish-data/publish-validate) and, once the conversion is complete, you can edit your data directly in ResearchSpace.

:::info
You may find it easiest to apply your data cleaning directly to the data store where it comes from and then follow the [Export Data](/docs/create-data/publish-data/publish-export) step after. 

Or you may choose to export your data into an easy to work with format and clean that version of the data. 

This decision will depend on how easy it is to edit the data in the data store, and whether you want the version of the data in the original data store to continue matching the cleaned version of the data.
:::


## How long will it take?

There is a trade-off of initial cleaning time vs data quality. Each research team needs to determine when they want to stop cleaning and move on to the next steps. You may also choose to split your full dataset into parts, where you clean and convert one part initially, and then repeat for each part as you have time.

Depending on the size of the dataset and how clean it is to begin with, cleaning usually ranges from a few hour task to a few weeks of part-time research assistant work.

## Who cleans my data?

Data cleaning is always done by **the Research Team** because you are the experts in your own data. This is a task likely suited for a research assistant in your group.

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Clean Source Data | ✓ |  |  |  |
| Send Cleaned Data | ✓ |  |  |  | -->


:::info

The **LINCS Conversion Team** can offer you guidance specific to your data, but first, see our [Data Cleaning Guide](/docs/create-data/publish-data/publish-clean/cleaning-guide) which should be a good starting point, covering the data cleaning steps and tools we have used with previous research teams. For more information about data cleaning tools, see [Data Cleaning](/docs/create-data/clean).
:::

## Cleaning Tools

<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>

<p><a href="/docs/tools/openrefine/">OpenRefine</a> is LINCS's preferred tool for cleaning structured data because it offers build in functionality and good documentation for many of the cleaning tasks outlined in our <a href="/docs/create-data/publish-data/publish-clean/cleaning-guide">Data Cleaning Guide</a>.</p>

<p>We also use a mix of spreadsheet editors like Google Sheets and Microsoft Excel, as well as custom Python scripts.</p>

<p>If your data is in a relational database or datastore with an editing interface, discuss options with your database administrator as there may already be editing methods in place.</p>

</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">
<p>LINCS often uses custom Python scripts using XML parsing libraries because each project has its own data structure and needs.</p>
<p>LEAF Writer and NERVE may be useful if you have XML data.</p>
</TabItem>
<TabItem value="tei" label="TEI Data">
<p>Coming soon...</p>
</TabItem>
<TabItem value="natural" label="Natural Language Data">
<p>LINCS typically use custom Python scripts or manual changes in a text editor for smaller fixes. </p>
</TabItem>
</Tabs>

## Expected Outputs

The output from this step should be the same as the output from the [Export Data](/docs/create-data/publish-data/publish-export) step, except with cleaning applied to the data. Similar to that step, send LINCS a copy of your cleaned data if you would like approval or if LINCS is helping to implement your next conversion steps.
