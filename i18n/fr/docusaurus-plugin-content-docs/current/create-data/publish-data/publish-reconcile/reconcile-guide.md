---
sidebar_position: 1
title: "Entity Reconciliation Guide"
description: "Complete common entity reconciliation tasks"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Introduction



how to get data out of your source data to reconcile it

what sources to reconcile against

how to get data back into source data

how to confirm a match
	- use open refine examples
	

