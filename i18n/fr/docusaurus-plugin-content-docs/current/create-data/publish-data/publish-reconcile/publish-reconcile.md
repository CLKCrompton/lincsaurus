---
sidebar_position: 5
title: "Reconcile Entities"
description: "LINCS Conversion Workflows — Reconcile Entities"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


## Introduction
- This will always be on the researchers to do and why it’s important
- Explain what is needed to make getting entities in and out of the data easier (like introducing unique identifiers for entities)
- talk about how this can happen slowly and it should be started early on

[Link to general page about reconciliation if separate so we cover all the basics of reconciliation in general and in the context of LINCS. (what authorities do we use and prioritize, what is our approach to using external URIs, what is our approach to minting new URIs)]


## Set up the Reconciliation Process

- info about adding unique identifiers into your data so that we can put reconciled values back in
- info about extracting entities and context to make the reconciliation process easier

The **Conversion Team** helps set up the reconciliation process and guides the **Research Team** as they begin reconciling. The reconciliation process typically requires the **Research Team** to pull entities out of their data, find <Term popup="A reliable and usable way to identify a unique entity so multiple datasets from various sources can communicate that they are all referring to the same thing." reference="/docs/terms/uniform-resource-identifier">Uniform Resource Identifiers (URIs)</Term> for them, and then add those URIs back into their data. Depending on the domain of the data, the **Research Team** may choose to use [OpenRefine](/docs/tools/openrefine), [VERSD](/docs/tools/versd), another tool, or a manual process to find candidate URIs and vet the results to choose the correct matches. For more information, see [Reconciliation](/docs/create-data/reconcile).

<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>
</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">
</TabItem>
<TabItem value="tei" label="TEI Data">
</TabItem>
<TabItem value="natural" label="Natural Language Data">
</TabItem>
</Tabs>


## Reconcile Your Data
[link to page about reconciliation – with general info about the standards we follow]


The **Research Team** reconciles the entities in their dataset. For more information, see [Reconciliation](/docs/create-data/reconcile). The reconciliation phase can be completed in tandem with the other steps in the conversion workflow, as placeholder values can be used for the conversion until the **Research Team** has finished reconciling. The dataset, however, is not be published by LINCS until either the **Research Team** finishes their reconciliation or LINCS and the **Research Team** come to an agreement that no more reconciliation to external <Term popup="A stable, persistent Uniform Resource Identifier (URI) for a concept in the Linked Data (LD) ecosystem." reference="/docs/terms/authority-record">authorities</Term> can take place, and new URIs need to be <Term popup="The process of creating a new Uniform Resource Identifier (URI) to represent an entity." reference="/docs/terms/uniform-resource-identifier-minting">minted</Term> for the remaining entities. Note that once the data is published, the **Research Team** can continue to enhance it, including further reconciliation.

The <Term popup="The process of ensuring that an entity in a dataset refers to a stable Uniform Resource Identifier (URI), ideally from a stable namespace, to make data more accessible, interoperable, and efficient when searching, storing, and retrieving." reference="/docs/terms/reconciliation">reconciliation</Term> phase is often the most time-consuming part of the conversion workflow.

|  | Research Team | Ontology Team | Conversion Team | Storage Team |
| --- | --- | --- | --- | --- |
| Set-Up Reconciliation Process | ✓ |  | ✓ |  |
| Reconcile Matches | ✓ |  |  |  |
| Merge | ✓ |  | ✓ |  |

:::note

The **Research Team** and **Conversion Team** meet to identify what <Term popup="A discrete thing, often described as the subject and object (or the domain and range) of a triple (subject-predicate-object)." reference="/docs/terms/entity">entities</Term> and concepts need to be reconciled and determine a strategy that suits the data.

:::

<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>
<ol>
<li>suggestion of OpenRefine workflow or VERSD workflow and explain (may need additional preprocessing before either workflow to work well)</li>
</ol>
</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">
this can happen in a few ways:
<ol>
<li>Custom scripts to extract entities and some context and then use a tool like OpenRefine. Then put the entity matches back in the data with another script</li>
<li>Do it a document at a time using a tool like LEAF-Writer</li>
<li>Do it manually without a tool</li>
</ol>
</TabItem>
<TabItem value="tei" label="TEI Data">
<ol>
<li>done using NSSI and an instance of LEAF-Writer</li>
</ol>
</TabItem>
<TabItem value="natural" label="Natural Language Data">
<ol>
<li>talk about options with LEAF-Writer, Diffbot, etc.</li>
</ol>
</TabItem>
</Tabs>


## Merge Reconciled Data

- Go into details of how to get reconciled values back into the data and show examples of what the data could look like for each type of data
- they can do it now or link to doing it at the validate and enhance step


The **Research Team** and **Conversion Team** use a custom script or the [Linked Data Enhancement API](/docs/tools/linked-data-enhancement-api) to merge the new URIs with either the cleaned version of the source data or the converted data.

<Tabs groupId="conversion-workflows" queryString="workflow">
<TabItem value="structured" label="Structured Data" default>
</TabItem>
<TabItem value="semistructured" label="Semi-Structured Data">
</TabItem>
<TabItem value="tei" label="TEI Data">
</TabItem>
<TabItem value="natural" label="Natural Language Data">
</TabItem>
</Tabs>


## Handle Vocabularies

- Not specific to any data type. Info about vocab usage or link to other documentation about LINCS vocab usage
- [unless this is included in the general page about reconciliation. But we need to instruct people on how to use vocabs in their data and how they would go about creating vocab terms.]
  - [Vocabulary Policy document](https://docs.google.com/document/d/1Lv1qOglW0SM2pi4hRu3asTrACQ3OW9RflCOqa90k_Do/edit?usp=sharing) (in progress)
  - [Vocabulary Workflow document](https://docs.google.com/document/d/1W_sbnIPAMOE2Bqr12P5v0YBei83dzyE2xY_UQ5xLvRQ/edit?usp=sharing) (in progress)
  - Requirements for creating vocab terms (in progress)
