# MoEML Gazetteer Application Profile

WILL BE MOVED LATER, THIS IS A TEST

## Introduction

### Purpose

To document how Map of Early Modern London (MoEML) Gazetteer data is modelled for compatibility with the wider LINCS data model.

### Acronyms

**Ontology Acronyms:**

* CIDOC-CRM - [CIDOC Conceptual Reference Model](http://cidoc-crm.org/)
* RDF - [Resource Description Framework](https://www.w3.org/RDF/)

**Vocabulary and Authority Acronyms:**

* EML Place Types - [Early Modern London Place Types Vocabulary](https://skosmos.lincsproject.ca/Skosmos/eml/en_US/?clang=en)
* MoEML Placeography - [Map of Early Modern London Placeography](https://jenkins.hcmc.uvic.ca/job/MoEML/lastSuccessfulBuild/artifact/static/site/mdtEncyclopediaLocation.htm)

## Places

### Place Declaration

| ![Place Declaration](/img/documentation/application-profile-moeml-gazetteer-placedeclaration-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares the existence of a place.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td><code>crm:E53_Place</code>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URI pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/Requirements for the Value</strong>
   </td>
   <td>URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>&lt;http://mapoflondon.uvic.ca/STSA1></code>
   </td>
  </tr>
  <tr>
   <td rowspan="2" ><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that &lt;http://mapoflondon.uvic.ca/STSA1> is a place, St. Saviour (Southwark).</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> a crm:E53_Place ; rdfs:label “St. Saviour (Southwark)”</code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>MoEML: <code>&lt;ref></code>
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ;
    rdfs:label "<display_name>" .
```

### Identifiers

#### IDs

| ![IDs](/img/documentation/application-profile-moeml-gazetteer-ids-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares the identifier of a place.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td>
   <p><code>crm:E53_Place → crm:P1_is_identified_by → crm:E42_Identifier
   <br/>    → crm:P190_has_symbolic_content → rdfs:literal
   <br/>    → crm:P2_has_type → crm:E55_Type</code></p>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier; Literal or string value
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URIs minted by LINCS; Literal or string value pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/Requirements for the Value</strong>
   </td>
   <td>Rdfs:literal, xsd:string, URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>"STSA1"</code>, <code>&lt;http://temp.lincsproject.ca/moeml_id></code>, <code>&lt;http://vocab.getty.edu/aat/300404012></code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
      <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) (http://mapoflondon.uvic.ca/STSA1) is identified by “STSA1”.</p>
      <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> → crm:P1_is_identified_by → crm:E42_Identifier
      <br/>    → crm:P190_has_symbolic_content  → “STSA1”
      <br/>    → crm:P2_has_type → &lt;http://temp.lincsproject.ca/moeml_id>, &lt;http://vocab.getty.edu/aat/300404012> .</code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>MoEML: <code>&lt;ref></code>
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ; 
   rdfs:label "<display_name>" ; 
   crm:P1_is_identified_by <place_id> .

<place_id> a crm:E42_Identifier ; 
   rdfs:label "Identifier for MoEML place <place_display_name>" ; 
   crm:P190_has_symbolic_content "<xml:id>" ;
   crm:P2_has_type <http://temp.lincsproject.ca/moeml_id>, 
    <http://vocab.getty.edu/aat/300404012> .

<http://temp.lincsproject.ca/moeml_id> a crm:E55_Type ;
   rdfs:label "MoEML project identifier" .

<http://vocab.getty.edu/aat/300404012> a crm:E55_Type ;
   rdfs:label "unique identifier" .
```

#### Names

| ![Names](/img/documentation/application-profile-moeml-gazetteer-names-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares a name used to identify a place.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td>
   <p><code>crm:E53_Place → crm:P1_is_identified_by → crm:E33_E41_Linguistic_Appellation
   <br/>    → crm:P2_has_type →  crm:E55_Type
   <br/>    → crm:P72_has_language → crm:E56_Language
   <br/>    → crm:P190_has_symbolic_content → rdfs:literal</code></p>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier; Literal or string value
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URIs minted by LINCS; Literal or string value pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/ Requirements for the Value</strong>
   </td>
   <td>Rdfs:literal, xsd:string, URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>&lt;http://temp.lincsproject.ca/name></code>, <code>&lt;http://temp.lincsproject.ca/english></code>, <code>"St. Saviour (Southwark)"</code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) (<a href="http://mapoflondon.uvic.ca/STSA1">http://mapoflondon.uvic.ca/STSA1</a>) is named “St. Saviour (Southwark)” which is in English (http://temp.lincsproject.ca/english).</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> → crm:P1_is_identified_by → crm:E33_E41_Linguistic_Appellation
   <br/>    → crm:P2_has_type → &lt;http://temp.lincsproject.ca/name>
   <br/>    → crm:P72_has_language → &lt;http://temp.lincsproject.ca/english>
   <br/>    → crm:P190_has_symbolic_content → "St. Saviour (Southwark)"</code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>MoEML: <code>&lt;placename></code>
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ; 
   rdfs:label "<display_name>" ; 
   crm:P1_is_identified_by <name>.

<name> a crm:E33_E41_Linguistic_Appellation ; 
   rdfs:label "<name>" ; 
crm:P2_has_type <type> ;
crm:P72_has_language <language> ;
   crm:P190_has_symbolic_content "<display_name>" .

<type> a crm:E55_Type ; 
   rdfs:label "<type>" .

<language> a crm:E55_Type ;
rdfs:label "<language>" .
```

### Geographies (e.g., coordinates)

| ![Geographies](/img/documentation/application-profile-moeml-gazetteer-coordinates-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/ Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares the coordinates of a place.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td><code>crm:E53_Place → crm:P168_place_is_defined_by → rdfs:literal</code>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Literal or string value
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>Literal or string value pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/ Requirements for the Value</strong>
   </td>
   <td>Rdfs:literal or xsd:string
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>ADD CODE HERE</code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) could be located at the coordinates “-0.089722,51.506111”.</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> →
   <br/>    → crm:P168_place_is_defined_by → ADD CODE HERE</code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>
   <p><a href="https://geojson.org/">GeoJSON</a> can use 6 types of coordinates:
   <ol>
   <li>Point</li>
   <li>MultiPoint</li>
   <li>LineString</li>
   <li>Polygon</li>
   <li>MultiPolygon</li>
   <li>MultiLineString</li>
   </ol>
   This dataset uses all of these; most commonly used is “point.”</p>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>Internet Engineering Task Force (IETF). GeoJSON. <a href="https://geojson.org/">https://geojson.org/</a>
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>MoEML: <code>&lt;geo></code>
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ; 
   rdfs:label "display_name" ;
crm:P168_place_is_defined_by "<GeoJSON>" .
```

### Uses/Functions (Types)

| ![Uses/Functions](/img/documentation/application-profile-moeml-gazetteer-uses-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/ Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares the type of a place.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td><code>crm:E53_Place → crm:P2_has_type → crm:E55_Type</code>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URI minted by LINCS
   </td>
  </tr>
  <tr>
   <td><strong>Format/ Requirements for the Value</strong>
   </td>
   <td>URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>&lt;https://id.lincsproject.ca/vocabularies/EML#Church></code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) is a type of eml:Church.</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> → crm:P2_has_type → &lt;https://id.lincsproject.ca/vocabularies/EML#Church>.</code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>This dataset uses the <a href="https://gitlab.com/calincs/infrastructure/vocabularies/-/blob/main/EMLPlaceTypes.ttl">Early Modern London Place Types vocabulary</a> (see also on <a href="https://skosmos.lincsproject.ca/Skosmos/eml/en_US/?clang=en">SKOSMOS</a>) to represent uses and functions.
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>The MoEML Team and Martin D. Holmes. Locations in Early Modern London. <em>The Map of Early Modern London</em>, Edition 7.0. Ed. Janelle Jenstad. Victoria: University of Victoria. mapoflondon.uvic.ca/edition/7.0/mdtEncyclopediaLocation_subcategories.htm.
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td><a href="https://docs.google.com/spreadsheets/d/1JenbKC2fDMvUlAkcfxFxWn8gTZcOHwQj8YyeYbb_w48/edit#gid=1379339222">Early Modern London Place Type Vocabulary Alignment</a>
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>MoEML: <code>&lt;catRef></code>
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ;
rdfs:label "<display_name>" ;
   crm:P2_has_type <place_type>.

<place_type> a crm:E55_Type ;
   rdfs:label "<place_type>".
```

### Subject of Other Documents

#### Descriptions

| ![Descriptions](/img/documentation/application-profile-moeml-gazetteer-descriptions-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/ Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares a place is the main subject of a description.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td>
   <p><code>crm:E53_Place → crm:P129i_is_subject_of → crm:E33_Linguistic_Object
   <br/>    → crm:P72_has_language → crm:E56_Language
   <br/>    → crm:P190_has_symbolic_content → rdfs:literal
   <br/>    → crm:P2_has_type → crm:E55_Type</code></p>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier; Literal or string value
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URIs minted by LINCS; Literal or string value pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/ Requirements for the Value</strong>
   </td>
   <td>Rdfs:literal, xsd:string, URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>&lt;http://temp.lincsproject.ca/english></code>, <code>"St. Saviour (Southwark) dates back at least to 1106. It was originally known by the name St. Mary Overies, with Overies referring to its being over the Thames, that is, on its southern bank. After the period of the Dissolution, the church was rededicated and renamed St. Saviour (Sugden 335). St. Saviour (Southwark) is visible on the Agas map along New Rents street in Southwark. It is marked with the label S. Mary Owber."</code>, <code>&lt;http://temp.lincsproject.ca/description></code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) is the subject of the description &lt;http://temp.lincsproject.ca/STSA1_description>.</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> → crm:P129i_is_subject_of → crm:E33_Linguistic_Object
   <br/>    → crm:P72_has_language → &lt;http://temp.lincsproject.ca/english>
   <br/>    → crm:P190_has_symbolic_content → "St. Saviour (Southwark) dates back at least to 1106. It was originally known by the name St. Mary Overies, with Overies referring to its being over the Thames, that is, on its southern bank. After the period of the Dissolution, the church was rededicated and renamed St. Saviour (Sugden 335). St. Saviour (Southwark) is visible on the Agas map along New Rents street in Southwark. It is marked with the label S. Mary Owber."
   <br/>    → crm:P2_has_type → &lt;http://temp.lincsproject.ca/description></code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ; 
   rdfs:label "<display_name>" ;
crm:P129i_is_subject_of "<description>" .

<description> a crm:E33_Linguistic_Object ; 
   rdfs:label "Description of <MoEML:Place>" ;
crm:P72_has_language <language> ;
   crm:P190_has_symbolic_content "<description>" ;
crm:P2_has_type <desc_type> .

<language> a crm:E56_Language ;
rdfs:label "<language_type>" .

<desc_type> a crm:E55_Type ; 
   rdfs:label "<desc_type>" .
```

#### Web Pages

| ![Web Pages](/img/documentation/application-profile-moeml-gazetteer-webpage-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/ Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares a place is the main subject of a web page.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td><code>crm:E53_Place → crm:P129i_is_subject_of → crm:E73_Information_Object</code>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URI pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/ Requirements for the Value</strong>
   </td>
   <td>URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>&lt;http://mapoflondon.uvic.ca/STSA1.htm></code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) is the main subject of  &lt;https://mapoflondon.uvic.ca/PERS1.htm>. This pattern pertains to subject web pages.</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> → crm:P129i_is_subject_of → &lt;http://mapoflondon.uvic.ca/STSA1.htm></code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>MoEML: <code>&lt;ref></code>
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ; 
   rdfs:label "<display_name>" ;
crm:P129i_is_subject_of "<web_page>" .

<web_page> a crm:E73_Information_Object ; 
   rdfs:label "Web page for <MoEML:Place>" .
```

#### Mentioned/Referenced/Described in Documents

| ![Mentioned/Referenced/Described in Documents](/img/documentation/application-profile-moeml-gazetteer-mentioned-(c-LINCS).png) |
|:--:|

<table>
  <tr>
   <td rowspan="2"><strong>Pattern/ Structure Values</strong>
   </td>
   <td><strong>Definition</strong>
   </td>
   <td>This pattern declares that a place is referred to by a document.
   </td>
  </tr>
  <tr>
   <td><strong>Abstraction</strong>
   </td>
   <td><code>crm:E53_Place → crm:P67i_is_referred_to_by → crm:E73_Information_Object</code>
   </td>
  </tr>
  <tr>
   <td rowspan="4"><strong>Content Values</strong>
   </td>
   <td><strong>Type of Value</strong>
   </td>
   <td>Uniform Resource Identifier
   </td>
  </tr>
  <tr>
   <td><strong>Expected Value</strong>
   </td>
   <td>URI pulled directly from the MoEML dataset
   </td>
  </tr>
  <tr>
   <td><strong>Format/ Requirements for the Value</strong>
   </td>
   <td>URI
   </td>
  </tr>
  <tr>
   <td><strong>Example Values</strong>
   </td>
   <td><code>&lt;https://mapoflondon.uvic.ca/PERS1.htm></code>
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Case Examples</strong>
   </td>
   <td><strong>Typical Example & Abstraction</strong>
   </td>
   <td>
   <p>The Map of Early Modern London Gazetteer states that St. Saviour (Southwark) is referred to by &lt;https://mapoflondon.uvic.ca/PERS1.htm>.</p>
   <p><code>&lt;http://mapoflondon.uvic.ca/STSA1> → crm:P67i_is_referred_to_by → &lt;https://mapoflondon.uvic.ca/PERS1.htm></code></p>
   </td>
  </tr>
  <tr>
   <td><strong>Edge Case Example & Abstraction</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td rowspan="2"><strong>Resource Links</strong>
   </td>
   <td><strong>Entity and Property Definitions from Source Creators</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td><strong>Additional Related References</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>Discussion Elements Pertaining to the Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
  <tr>
   <td colspan="2"><strong>MoEML Elements Following this Pattern</strong>
   </td>
   <td>N/A
   </td>
  </tr>
</table>

**Pattern in TTL:**

```text
<MoEML:Place> a crm:E53_Place ; 
   rdfs:label "<display_name>" ;
crm:P67i_is_referred_to_by <document> .

<document> a crm:E73_Information_Object ;
rdfs:label "<document>" .
```
