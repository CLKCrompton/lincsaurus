---
sidebar_class_name: "landing-page"
title: "Commencer"
translated: 1
---

<!-- @format -->

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

LINCS développe une infrastructure et des outils permettant aux chercheurs et aux étudiants en sciences humaines de créer et d'accéder aux %%%Données ouvertes et liés (LOD)|linked-open-data%%. La promesse et le potentiel des LOD sont au cœur du LINCS.

Bien que le terme **données** ne soit pas couramment utilisé pour décrire la production scientifique des chercheurs en sciences humaines, de nombreux chercheurs en sciences humaines produisent des données. Les données des sciences humaines peuvent prendre de nombreuses formes, telles que:

Fichiers d'images (p.ex. images, peintures, photographies)
- Fichiers texte (p.ex. poèmes, manuscrits, lettres, journaux, revues, nouvelles, messages sur les médias sociaux)
- Cartes et coordonnées géospatiales
- Enregistrements audio (p.ex. musique, histoires orales)
- Fichiers de %%métadonnées|metadata%%

Tous ces éléments peuvent être transformés en formats standard lisibles à la fois par les humains et les ordinateurs. Lorsque les données sont mises à disposition sur le web, les chercheurs - et tout le monde ! - peuvent découvrir les données, établir des liens avec les données et faire progresser la recherche dans le domaine des sciences humaines.

Lorsque les documents de recherche sont convertis en LOD, tout le monde peut :

- Découvrir des liens et des influences inattendus entre des œuvres créatives, des personnes, des lieux, des événements et des organisations  

> _Quels sont les liens sociaux entre les personnes et les réseaux qui ont créé les œuvres d'art et la littérature dans ces magazines fin-de-siècle ?_  

- Posez de nouvelles questions en effectuant des recherches dans plusieurs ensembles de données différents  

> Qui (données démographiques) vivait dans ces rues (données de localisation) au début de l'époque moderne à Londres ?

- Interroger les données à l'aide de filtres multiples  

> Afficher toutes les auteures publiées par des maisons d'édition indépendantes et dont les pères étaient membres du clergé._  

- Révéler les relations en visualisant les données sous forme de graphique, de nuage de mots, de réseau ou de tableau

## Notions de base sur les données ouvertes liées

Apprenez les concepts fondamentaux des données ouvertes liées. Pour une liste complète des termes, consultez le [Glossaire](/docs/get-started/glossary).

:::conseil

<!-- - Consultez nos lectures recommandées (lien vers Zotero à venir) -->
- [Regardez des vidéos](https://www.youtube.com/@lincsproject/featured) sur les sujets LOD et les outils LINCS.
- Recherchez les prochains [ateliers et événements LINCS](/docs/about-lincs/get-involved/events)

:::

<DocCardList items={useCurrentSidebarCategory().items.slice(0,2)}/>
