---
id: glossary
title: Glossaire
sidebar_position: 3
description: "Definitions, Examples, Further Resources"
---

<!-- @format -->

### [Profil d'application](/fr/docs/terms/application-profile)

Un schéma composé d'éléments de métadonnées tirés d'un ou plusieurs espaces de noms, ainsi que des politiques et des directives liées à leur utilisation, préparé pour une application particulière.

### [Application Programming Interface (API)](/fr/docs/terms/application-programming-interface)

Une bibliothèque de codes qui permet à des applications tierces de communiquer avec une plateforme de services Web.

### [Argument](/fr/docs/terms/argument)

Une série de raisons, d'énoncés ou de faits dans un schéma de métadonnées destinés à soutenir ou à établir un point de vue, plutôt qu'un énoncé neutre, qui décrit une personne, un événement ou un objet.

### [Enregistrement d'autorité](/fr/docs/terms/authority-record)

Un identificateur de ressource uniforme (URI) stable et persistant pour un concept dans l'écosystème des données liées (LD).

### [Noeud vide](/fr/docs/terms/blank-node)

Un sujet ou un objet dans un graphe RDF (Resource Description Framework) pour lequel un URI (Uniform Resource Identifier) ​​ou un littéral n'est pas donné.

### [CIDOC CRM](/fr/docs/terms/cidoc-crm)

Une suite d'ontologies centrées sur les événements pour décrire les données dans le domaine du patrimoine culturel, développées pour relier des ensembles hétérogènes de données gérés par des musées, des galeries et d'autres institutions patrimoniales.

### [Classement](/fr/docs/terms/classing)

Pour déclarer une entité comme étant une instance d'une classe en utilisant rdf:type dans l'ontologie choisie pour l'ensemble de données.

### [Vocabulaire contrôlé](/fr/docs/terms/controlled-vocabulary)

Un arrangement standardisé et organisé de mots et de phrases, qui fournit une manière cohérente de décrire les données.

### [Conversion](/fr/docs/terms/conversion)

Le processus de modification des données d'un format à un autre.

### [Concordance](/fr/docs/terms/crosswalking)

Le processus conceptuel de cartographie des données entre les modèles de données.

### [Cypher](/fr/docs/terms/cypher)

Un langage de requête pour les bases de données de graphes qui reflète la nature sémantique des triplets mais le fait avec sa propre syntaxe et sa propre mise en forme.

### [DBPedia](/fr/docs/terms/dbpedia)

Un projet qui crée des données structurées accessibles au public pour le cloud Linked Open Data (LOD).

### [Déréférencable](/fr/docs/terms/dereferenceable)

Adjectif utilisé en relation avec les URI (Uniform Resource Identifiers) qui peuvent passer d'une référence abstraite à quelque chose de plus concret, à savoir une ressource Web.

### [Domaine](/fr/docs/terms/domain)

L'une des deux entités d'un triplet, représentant le sujet dans une relation sujet-prédicat-objet.

### [Edge](/fr/docs/terms/edge)

Une ligne qui relie un nœud à un autre dans une base de données de graphes, représentant une relation entre les nœuds.

### [Entité](/fr/docs/terms/entity)

Une chose discrète, souvent décrite comme le sujet et l'objet (ou le domaine et la gamme) d'un triplet (sujet-prédicat-objet).

### [Event-Oriented Ontology](/fr/docs/terms/event-oriented-ontology)

Une ontologie qui utilise des événements pour relier les choses, les concepts, les personnes, le temps et le lieu.

### [Recherche SPARQL fédérée](/fr/docs/terms/federated-sparql-search)

Un point d'entrée unique pour accéder aux terminaux SPARQL distants afin qu'un service de requête puisse récupérer des informations à partir de plusieurs sources de données.

### [Base de données de graphes](/fr/docs/terms/graph-database)

Une base de données qui structure les informations sous forme de graphique ou de réseau, où un ensemble de ressources, ou nœuds, sont reliés entre eux par des arêtes qui décrivent les relations entre chaque ressource.

### [Inférence](/fr/docs/terms/inferencing)

La découverte automatisée de nouveaux faits générés à partir de triplets existants.

### [Ingestion](/fr/docs/terms/ingestion)

Processus par lequel les données sont déplacées d'une ou plusieurs sources vers une nouvelle destination où elles peuvent être stockées et analysées plus en détail.

### [Cadre international d'interopérabilité des images (IIIF)](/fr/docs/terms/international-image-interoperability-framework)

Ensemble d'outils et de normes qui rendent les images numériques interopérables, fournissant une méthode normalisée de description et de diffusion d'images en ligne.

### [Identifiant de ressource internationalisé (IRI)](/fr/docs/terms/internationalized-resource-identifier)

Identifiant qui s'appuie sur le protocole URI (Universal Resource Identifier) ​​en élargissant le jeu de caractères autorisés pour inclure la majeure partie du jeu de caractères universel.

### [Graphique des connaissances](/fr/docs/terms/knowledge-graph)

Représentation d'un ensemble de triplets liés qui illustre les relations entre eux.

### [Carte des connaissances (ResearchSpace)](/fr/docs/terms/knowledge-map)

Un outil de visualisation dans l'environnement ResearchSpace qui affiche les différentes entités de données dans le triplestore et comment elles sont connectées à d'autres entités de données.

### [Modèle de connaissances (ResearchSpace)](/fr/docs/terms/knowledge-pattern)

Chemins de graphe prédéfinis qui résument les activités du monde réel à l'aide de classes et de propriétés dans l'environnement ResearchSpace.

### [Données liées](/fr/docs/terms/linked-data)

Données structurées qui sont liées à d'autres données via le Web et s'appuient sur des technologies Web standard pour partager des données lisibles par machine entre ordinateurs.

### [Données ouvertes liées](/fr/docs/terms/linked-open-data)

Données liées et utilisant des sources ouvertes.

### [Literal](/fr/docs/terms/literal)

Un objet dans un triplet qui ne fait pas référence à une ressource avec un identificateur de ressource uniforme (URI), mais transmet à la place une valeur, telle que du texte, un nombre ou une date.

### [Named Entity Disambiguation (NED)](/fr/docs/terms/named-entity-disambiguation)

Attribuer une identité unique à une entité dans un texte pour la différencier d'une autre entité qui partage le même nom.

### [Reconnaissance d'entité nommée (NER)](/fr/docs/terms/named-entity-recognition)

Processus d'identification et de catégorisation d'entités (un mot ou un ensemble de mots faisant référence à la même chose) dans un texte.

### [Graphique nommé](/fr/docs/terms/named-graph)

Une extension du modèle de données RDF (Resource Description Framework) dans lequel un graphe RDF est identifié à l'aide d'un URI (Uniform Resource Identifier), permettant ainsi la publication et la présentation de métadonnées sur ce graphe dans son ensemble.

### [Espace de noms](/fr/docs/terms/namespace)

Un répertoire de concepts qui sont utilisés pour identifier et faire référence à des entités au sein d'un ensemble de données.

### [Traitement automatique des langues naturelles (TALN)](/fr/docs/terms/natural-language-processing)

Une branche de l'intelligence artificielle qui implique le traitement automatique et/ou la manipulation de la parole, du texte et d'autres formes de données non structurées qui représentent la façon dont les humains communiquent entre eux.

### [Nœud](/fr/docs/terms/node)

Représentation d'une entité ou d'une instance à suivre dans une base de données de graphes ou un triplestore, telle qu'une personne, un objet ou une organisation.

### [Base de données NoSQL](/fr/docs/terms/nosql-database)

Base de données modélisée d'une manière différente des relations tabulaires utilisées dans une base de données relationnelle, telle qu'une base de données de documents, un magasin clé-valeur, un magasin à colonnes étendues ou une base de données de graphes.

### [Ontologie](/fr/docs/terms/ontology)

Modèle abstrait et lisible par machine d'un phénomène qui capture et structure la connaissance des entités, des propriétés et des relations dans un domaine afin qu'une conceptualisation puisse être partagée et réutilisée par d'autres.

### [Données ouvertes](/fr/docs/terms/open-data)

Des données qui peuvent être consultées, utilisées et réutilisées par n'importe qui, sur la base de l'idée que les données doivent être librement accessibles à tous pour être vues et utilisées, sans restrictions de droit d'auteur.

### [Reconnaissance optique de caractères (OCR)](/fr/docs/terms/optical-character-recognition)

La conversion automatique d'images de mots en un fichier texte que les utilisateurs peuvent ensuite rechercher et modifier.

### [Propriété](/fr/docs/terms/property)

Une relation spécifiée entre deux classes ou entités, comme le prédicat dans un triplet (sujet-prédicat-objet).

### [Graphique de propriétés](/fr/docs/terms/property-graph)

Un graphe où les relations (propriétés) entre les entités sont nommées et portent certaines propriétés définies qui leur sont propres, étendant la base de données de graphes de base des triplets liés pour montrer des connexions complexes qui décrivent comment différents types de métadonnées sont liés.

### [Provenance](/fr/docs/terms/provenance)

L'historique de la propriété, de la garde ou de l'emplacement d'un objet décrit ou les données décrivant cet objet.

### [Quad](/fr/docs/terms/quad)

Une extension d'un triplet pour inclure une quatrième section qui fournit un contexte pour le triplet, comme l'URI (Uniform Resource Identifier) ​​du graphe dans son ensemble (sujet-prédicat-objet-contexte).

### [Plage](/fr/docs/terms/range)

L'une des deux entités d'un triplet, représentant l'objet dans une relation sujet-prédicat-objet.

### [Réconciliation](/fr/docs/terms/reconciliation)

Processus consistant à s'assurer qu'une entité dans un ensemble de données fait référence à un identificateur de ressource uniforme (URI) stable, idéalement à partir d'un espace de noms stable, pour rendre les données plus accessibles, interopérables et efficaces lors de la recherche, du stockage et de la récupération.

### [Expression régulière (Regex)](/fr/docs/terms/regular-expression)

Syntaxe pouvant être utilisée dans les langages de programmation pour rechercher, manipuler ou remplacer des modèles dans des textes.

### [Réification](/fr/docs/terms/reification)

Le processus de concrétisation d'un concept abstrait, comme prendre la notion d'une relation et la considérer comme une entité ou exprimer quelque chose à l'aide d'un langage de programmation, afin qu'il puisse être manipulé par programmation.

### [Base de données relationnelle](/fr/docs/terms/relational-database)

Une base de données qui stocke les données sous forme de tableau (colonnes et lignes), où les colonnes contiennent des attributs de données (tels que les types de données), les lignes contiennent des « enregistrements » et les relations sont définies entre les tables à l'aide de règles.

### [Cadre de description des ressources (RDF)](/fr/docs/terms/resource-description-framework)

Une norme pour les données liées (LD) qui représente des informations dans une série d'«instructions» en trois parties appelées un triplet, qui comprend un sujet, un prédicat et un objet sous la forme sujet-prédicat-objet.

### [Sérialisation du cadre de description des ressources (RDF)](/fr/docs/terms/resource-description-framework-serialization)

Une syntaxe qui peut être utilisée pour écrire des triplets, y compris Turtle (TTL), XML (XML-RDF) et JSON.

### [Resource Description Framework Schema (RDFS)](/fr/docs/terms/resource-description-framework-schema)

Une extension du vocabulaire RDF (Resource Description Framework) de base qui peut être utilisé pour définir le vocabulaire (termes) à utiliser dans un graphe RDF.

### [Récit sémantique (ResearchSpace)](/fr/docs/terms/semantic-narrative)

Un document interactif dans l'environnement ResearchSpace qui combine une narration textuelle et des données liées (LD) pour communiquer des idées sur les personnes, les lieux et les événements.

### [Web sémantique](/fr/docs/terms/semantic-web)

L'idée d'étendre le World Wide Web en incluant des descripteurs de données supplémentaires au contenu publié sur le Web afin que les ordinateurs puissent faire des interprétations significatives des données publiées.

### [Forme d'expressions (ShEx)](/fr/docs/terms/shape-expressions)

Un langage pour valider et décrire les structures de graphes RDF (Resource Description Framework).

### [Shapes Constraint Language (SHACL)](/fr/docs/terms/shapes-constraint-language)

Une norme pour décrire les graphes RDF (Resource Description Framework) et les valider par rapport à un ensemble de conditions.

### [Système d'organisation des connaissances simples (SKOS)](/fr/docs/terms/simple-knowledge-organization-system)

Une norme qui fournit un moyen de représenter des thésaurus, des taxonomies et des vocabulaires contrôlés conformément au cadre de description des ressources (RDF).

### [SPARQL Endpoint](/fr/docs/terms/sparql-endpoint)

Emplacement sur Internet identifié par une URL (Uniform Resource Locator) et capable de recevoir et de traiter des requêtes SPARQL, permettant aux utilisateurs d'accéder à une collection de triplets.

### [Protocole SPARQL et langage de requête RDF (SPARQL)](/fr/docs/terms/sparql-protocol-and-rdf-query-language)

Un langage de requête pour les triplestores qui traduit les données graphiques en données tabulaires normalisées avec des lignes et des colonnes.

### [Langage de requête structuré (SQL)](/fr/docs/terms/structured-query-language)

Un langage de requête pour les bases de données relationnelles qui permet aux utilisateurs de spécifier quelles données renvoyer, quelles tables rechercher, quelles relations suivre et comment ordonner les données qui répondent à ces conditions définies.

### [Taxonomie](/fr/docs/terms/taxonomy)

Un système qui identifie les relations hiérarchiques entre les concepts au sein d'un domaine.

### [Initiative de codage de texte (TEI)](/fr/docs/terms/text-encoding-initiative)

Un langage d'encodage qui prend en charge l'encodage détaillé de documents complexes et est largement utilisé par un certain nombre de différents projets d'humanités numériques (DH).

### [Thésaurus](/fr/docs/terms/thesaurus)

Un vocabulaire structuré qui montre les relations hiérarchiques, associatives et d'équivalence entre les concepts, afin que les utilisateurs trouvent non seulement des termes plus larges et plus étroits que d'autres, mais aussi des termes synonymes, antonymes ou autrement liés (associés) d'une manière définie.

### [Triple](/fr/docs/terms/triple)

Une déclaration sous la forme sujet-prédicat-objet qui suit le cadre de description de ressource (RDF).

### [Triplestore](/fr/docs/terms/triplestore)

Une base de données NoSQL qui stocke des triplets.

### [Saisie](/fr/docs/terms/typing)

Utiliser un chemin standardisé pour relier toute entité à un vocabulaire, thésaurus ou ontologie externe au sein de CIDOC CRM.

### [Uniform Resource Identifier (URI)](/fr/docs/terms/uniform-resource-identifier)

Un moyen fiable et utilisable d'identifier une entité unique afin que plusieurs ensembles de données provenant de diverses sources puissent indiquer qu'ils font tous référence à la même chose.

### [Uniform Resource Identifier (URI) Minting](/fr/docs/terms/uniform-resource-identifier-minting)

Processus de création d'un nouvel identificateur de ressource uniforme (URI) pour représenter une entité.

### [Uniform Resource Locator (URL)](/fr/docs/terms/uniform-resource-locator)

Une déclaration qui décrit l'emplacement de quelque chose sur le Web spécifiquement pour localiser des actifs en ligne.

### [Vocabulaire](/fr/docs/terms/vocabulary)

Ensemble de termes pouvant être concrètement décrits dans une ontologie, une taxonomie ou un thésaurus.

### [Langage d'ontologie Web (OWL)](/fr/docs/terms/web-ontology-language)

Un langage de représentation des connaissances pour les ontologies qui représente explicitement la signification des termes dans les vocabulaires et les relations entre ces termes, ainsi qu'entre les groupes de termes.

### [WEMI](/fr/docs/terms/wemi)

Acronyme signifiant Œuvre, Expression, Manifestation et Élément—termes dérivant des Exigences fonctionnelles pour les notices bibliographiques (FRBR), qui est le principal moyen de décrire les notices bibliographiques.

### [Wikibase](/fr/docs/terms/wikibase)

Une suite de logiciels de base de connaissances gratuits et open source pour stocker, gérer et accéder aux données ouvertes liées (LOD), écrites et utilisées par le projet Wikidata.

### [Wikidata](/fr/docs/terms/wikidata)

La plus grande instance de Wikibase, qui agit comme un référentiel de stockage central pour les données structurées utilisées par Wikipédia, par ses projets frères et par quiconque souhaite utiliser une grande quantité de données ouvertes à usage général.

### [Wikimedia Commons](/fr/docs/terms/wikimedia-commons)

Un référentiel de fichiers multimédias (images, sons et clips vidéo) qui met à disposition du domaine public et du contenu multimédia sous licence libre, et qui agit en tant que gestionnaire de ressources numériques pour tous les projets de la Wikimedia Foundation.

### [Fondation Wikimedia](/fr/docs/terms/wikimedia-foundation)

L'organisation faîtière qui gère Wikipedia, Wikibase, Media Wiki, Wiktionary et d'autres projets et chapitres Wiki.

### [XML](/fr/docs/terms/xml)

Un langage de balisage lisible par l'homme et la machine qui permet aux utilisateurs de créer leurs propres balises pour décrire les documents.
