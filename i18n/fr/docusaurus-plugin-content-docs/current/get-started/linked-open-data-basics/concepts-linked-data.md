---
sidebar_position: 2
title: "Données liées"
description: "Resource Description Framework (RDF), Linked Open Data, Resources"
---

<!-- @format -->

## Bases

%%Les données liées (LD)|linked-data%% sont des informations structurées contenues dans une base de données ou un ensemble de données qui suit le %%Resource Description Framework (RDF)|resource-description-framework%%, un format standard pour représenter des données interconnectées sur le Web.

## Structure

LD suivant le RDF est organisé en %%triples|triple%%, qui comportent trois parties :

- Matière
- Prédicat
- Objet

Le sujet est tout point de données décrit. L'objet est un autre point de données auquel le sujet est lié. Le prédicat décrit la relation entre les deux.

> Margaret Laurence (Sujet) → auteur (Prédicat) → _The Stone Angel_ (Objet)

> The Stone Angel (Sujet) → a pour auteur (Prédicat) → Margaret Laurence (Objet)

Au LINCS, les sujets et les objets sont tous deux appelés %%entités|entity%%, tandis que les prédicats sont appelés %%propriétés|property%%. Les ensembles de données constitués de triplets sont souvent appelés %%triplestores|triplestore%%.

## Identifiants de ressources uniformes (URI)

Chaque entité dans un triplet RDF doit être identifiée de manière unique pour que le triplet soit interconnecté avec d'autres triplets. Par exemple, Margaret Laurence a écrit à la fois _The Stone Angel_ et _The Diviners_.

> Margaret Laurence (Sujet) → auteur (Prédicat) → _The Stone Angel_ (Objet)

> Margaret Laurence (Sujet) → auteur (Prédicat) → _The Diviners_ (Objet)

Pour qu'un ordinateur reconnaisse que la même personne a écrit _The Stone Angel_ et _The Diviners_, la même entité "Margaret Laurence" doit être utilisée dans chaque triplet. Les ordinateurs, cependant, sont les littéralistes ultimes. Les mots sont des chaînes (séquences de caractères), mais la même chaîne peut signifier différentes choses. Par exemple, il y a eu de nombreuses personnes nommées Margaret Laurence à travers l'histoire. L'utilisation de la chaîne "Margaret Laurence" n'est pas assez précise pour dire à l'ordinateur quelle Margarat Laurence a écrit ces romans.

Afin d'identifier à quelle Margaret Laurence nous faisons référence, nous devons utiliser un %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%. Un URI est une chaîne de chiffres unique ou un lien qui représente de manière cohérente une personne, un lieu, une chose ou un élément d'information distinct, et qui peut être utilisé dans de nombreux ensembles de données différents. En utilisant un URI pour faire référence à la même entité dans plusieurs triplets et plusieurs ensembles de données, des groupes de données distincts peuvent ensuite être connectés les uns aux autres, les reliant tous ensemble.

Le processus de recherche et de mise en correspondance d'un URI d'une base de données externe avec une entité qui existe dans un ensemble de données est appelé %%rapprochement|reconciliation%%. Il existe différentes autorités que vous pouvez consulter pour les URI, selon le type d'entité que vous devez réconcilier. Par exemple, voici quelques URI du %%Fichier d'autorité international virtuel (VIAF)|virtual-international-authority-file%% :

- Margaret Laurence : [https://viaf.org/viaf/44317974/](https://viaf.org/viaf/44317974/)
- _L'ange de pierre_ : [http://viaf.org/viaf/187423217/](http://viaf.org/viaf/187423217/)
- _Les devins_ : [http://viaf.org/viaf/187586333](http://viaf.org/viaf/187586333)

Pour plus d'informations sur la recherche d'URI pour les entités, consultez [Réconciliation](/docs/create-data/reconcile).

## Ontologies

Afin de rendre LD aussi cohérent et précis que possible, il est important de le conformer à un %%ontologie|ontology%%. Une ontologie crée efficacement les règles pour vos données, définissant exactement quels types d'entités et de relations peuvent exister dans vos données. Les ontologies rendent les liens entre vos points de données sémantiques, afin qu'un ordinateur puisse lire et comprendre la signification spécifique des relations. Pour plus d'informations, voir [Ontologies](/docs/get-started/linked-open-data-basics/concepts-ontologies).

## Données ouvertes liées (LOD)

Lorsque LD est publié sous une licence ouverte, il est considéré comme %%Données ouvertes liées (LOD)|linked-open-data%%. Cependant, il existe des critères plus spécifiques. Les données sont considérées comme vraies LOD lorsqu'elles :

1. Suit un format standard LOD reconnu
2. Utilise les URI pour identifier les entités
3. Est publié ouvertement

LOD présente de nombreux avantages. Puisqu'il est basé sur un standard développé par le %%Wide Web Consortium (W3C)|world-wide-web-consortium%% et est structuré de manière compréhensible pour plusieurs systèmes, LOD favorise l'interopérabilité. Cette interopérabilité permet aux données d'être davantage partagées et découvertes, car les données hébergées sur différents sites peuvent être connectées ensemble puis recherchées en même temps.

En connectant les données de plusieurs sites, les entités et les relations entre elles peuvent être davantage contextualisées. Surtout, ces informations contextuelles permettent %%inférence|inferencing%%. Par exemple, dans le graphique ci-dessous, les triplets nous permettent de déduire que Zadie Smith a lu E. M. Forster, puisque le livre de Zadie Smith _On Beauty_ fait allusion à E. M. Forster.

LOD favorise également la sérendipité, une qualité généralement associée aux bibliothèques physiques. En se déplaçant physiquement dans un environnement de connaissances structuré comme une bibliothèque et en fouillant les piles de livres, les usagers ont tendance à découvrir des ressources qu'ils ne savaient pas qu'ils recherchaient. Répliquer l'exploration et la découverte fortuites en ligne est difficile. La plupart des outils Web restreignent les utilisateurs en leur faisant taper une recherche spécifique dans une barre de recherche pour localiser les ressources. LOD, cependant, fournit un moyen de reconstruire virtuellement un environnement de données structuré afin que les relations entre les entités puissent être interagies et visualisées.

## Ressources

Pour en savoir plus sur LD et LOD, consultez les ressources suivantes.

- Crompton (2020) ["Données ouvertes liées : comprenez-les, utilisez-les, créez-les !"](https://www.youtube.com/watch?v=mZzNWS7-Vpk) [Vidéo]
- Blaney (2017) [« Introduction aux principes des données ouvertes liées »](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- [Cloud de données ouvertes lié](https://lod-cloud.net/)
- Posner (2021) ["Qu'est-ce que les données ouvertes liées"](https://www.youtube.com/watch?v=VZBpFiLbi-Y) [Vidéo]
- Sanderson (2021) ["L'illusion de grandeur : confiance et croyance dans les données ouvertes liées au patrimoine culturel"](https://youtu.be/o4TzXMz7GBA)
