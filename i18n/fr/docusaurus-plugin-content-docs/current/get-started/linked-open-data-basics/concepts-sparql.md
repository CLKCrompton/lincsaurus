---
sidebar_position: 5
title: "SPARQL"
description: "Query Constructions, Types of Questions, Resources"
---

<!-- @format -->

## Bases

SPARQL, abréviation de "SPARQL Protocol and RDF Query Language" et prononcé "sparkle", est un langage de requête qui permet aux utilisateurs d'interroger %%triplestores|triplestore%%.

Les requêtes SPARQL prennent la forme d'une chaîne. Ils sont dirigés vers un %%Point de terminaison SPARQL|sparql-endpoint%%, un emplacement sur Internet capable de recevoir et de traiter les requêtes SPARQL.

Il est utile de considérer une requête SPARQL comme un ensemble de phrases avec des blancs. La base de données prendra cette requête et trouvera chaque ensemble d'instructions correspondantes qui remplit correctement ces blancs. En d'autres termes, la requête recherche des données qui suivent un modèle que vous avez décrit. Ce qui rend SPARQL puissant, c'est la possibilité de créer des requêtes complexes qui référencent plusieurs variables à la fois.

Les requêtes SPARQL peuvent être utilisées pour interroger %%graphiques nommés|named-graph%%, tels que ceux créés et maintenus par LINCS.

Pour faire des requêtes SPARQL, vous aurez besoin de savoir :

- Comment construire des requêtes
- Quelles sortes de questions peuvent être posées avec une requête

## Construire une requête

Une requête SPARQL est comme une recette. Il y a quatre ingrédients principaux :

1. Préfixe(s)
2. Type de requête
3. Requête
4. Modificateur(s)

### Préfixes

Les préfixes sont des abréviations abrégées pour le %%Identifiants de ressources internationalisés (IRI)|internationalized-resource-identifier%% qui indiquent au point de terminaison SPARQL où aller chercher les données. Les préfixes sont placés en haut de votre requête afin que vous n'ayez pas à taper les IRI complets chaque fois que vous souhaitez vous y référer.

Dans l'exemple suivant, un préfixe a été ajouté pour le CWRC %%ontologie|ontology%%, le %%Resource Description Framework (RDF)|resource-description-framework%%, le %%Resource Description Framework Schema (RDFS)|resource-description-framework-schema%%, et le %%Système d'organisation des connaissances simples (SKOS)|simple-knowledge-organization-system%% :

```texte
PRÉFIXE cwrc : <http://sparql.cwrc.ca/ontologies/cwrc#>
PREFIXE rdf : <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIXE rdfs : <http://www.w3.org/2000/01/rdf-schema#>
PRÉFIXE skos : <http://www.w3.org/2004/02/skos/core#>
```

### Type de requête

Suite à vos préfixes, vous devez déclarer le type de requête SPARQL. Il existe quatre types de requêtes : ASK, SELECT, DESCRIBE et CONSTRUCT. Chaque type de requête SPARQL comprend les mêmes composants essentiels, mais chacun sert un objectif différent et vous donnera un type de résultats différent.

#### ASK Requête

Les requêtes ASK renvoient une réponse oui ou non.

#### SÉLECTIONNER la requête

Les requêtes SELECT renvoient une liste de toutes les choses qui correspondent à votre élément de requête.

#### DESCRIBE Requête

Les requêtes DESCRIBE renvoient toutes les informations connues sur un %%entité|entity%%.

#### CONSTRUIRE Requête

Les requêtes CONSTRUCT renvoient un nouveau %%triples|triple% en extrayant des informations de plusieurs triplets.

:::info

Bientôt disponible! Des exemples de requêtes seront fournis une fois que [LINCS SPARQL Endpoint](/docs/tools/sparql) sera en ligne.

:::

### Requête

#### Triplés

Après avoir déclaré le type de requête que vous allez construire, vous devez renseigner la structure de la requête. La structure de la requête est composée de triplets : un sujet, un prédicat et un objet. Chaque composant d'un triplet est soit un %%variable de requête|query-variable%% ou un %%Uniform Resource Identifier (URI)|uniform-resource-identifier%%.

Une variable de requête est l'objet que vous recherchez. Les variables sont indiquées par un point d'interrogation suivi d'un mot. Le mot que vous choisissez pour une variable est arbitraire, mais doit être lisible par l'homme pour faciliter la compréhension s'il est partagé avec d'autres. Il est important que vous utilisiez une variable de manière cohérente dans une requête.

```texte
?Nom

?Objet
```

Un URI est un identifiant unique qui représente une chose qui existe dans le triplestore LINCS. Il peut s'agir d'une %%property|property%%, entité, graphe, classe dans l'ontologie (ou les ontologies), ou même un <%%vocabulaire|vocabulary%% terme (type). Les URI sont généralement raccourcis à l'aide d'un préfixe ou %%espace de noms|namespace%%. Par exemple, l'URI complet de la propriété CWRC "woman", `<http://sparql.cwrc.ca/ontologies/cwrc#woman>`, peut être raccourci en `cwrc:woman` à l'aide de l'espace de noms CWRC.

#### Instruction WHERE

Chaque requête doit avoir une instruction WHERE. L'instruction WHERE suit la déclaration du type de requête et la liste des prédicats qui serviront d'en-têtes dans le tableau des résultats. Il vient avant le modèle de requête et indique que ce qui suit est WHERE pour rechercher le modèle auquel la requête doit correspondre.

#### Syntaxe

Votre requête ne fonctionnera que si vous utilisez la syntaxe appropriée.

Après votre instruction WHERE, utilisez une accolade pour entourer votre modèle de requête. Les accolades doivent apparaître par paires. Chaque parenthèse que vous ouvrez doit se fermer plus tard dans votre requête. Vous pouvez ajouter des accolades supplémentaires pour vous aider à organiser votre requête, mais chaque parenthèse ouvrante doit avoir une parenthèse fermante correspondante.

Chaque triplet d'une requête doit se terminer par un point.

Si un sujet utilisé dans une ligne est répété dans la ligne suivante, le sujet peut être omis tant qu'il y a un point-virgule à la fin de la première ligne pour indiquer l'utilisation du même sujet dans la deuxième ligne.

:::info

Bientôt disponible! Des exemples de requêtes seront fournis une fois que [LINCS SPARQL Endpoint](/docs/tools/sparql) sera en ligne.

:::

### Modificateurs

Dans une requête plus compliquée, vous pouvez ajouter des modificateurs pour enchaîner plusieurs critères. Pour une liste de tous les modificateurs possibles, voir %%W3C|world-wide-web-consortium%% [Modificateurs de séquence de solutions](https://www.w3.org/TR/2013/REC-sparql11-query- 20130321/#sparqlSolMod). Les modificateurs courants sont décrits ci-dessous.

#### Modificateur OPTIONNEL

Le modificateur FACULTATIF vous permet d'indiquer un élément supplémentaire que vous aimeriez voir inclus dans vos résultats, s'il est présent dans les données. Par exemple, vous pouvez demander des images facultatives ou des informations supplémentaires facultatives. L'utilisation du modificateur OPTIONAL signifie que vous obtiendrez des résultats même pour les éléments qui ne contiennent pas les informations que vous avez marquées comme facultatives. Par exemple, une requête avec des images facultatives renverra tous les résultats corrects avec et sans images, mais inclura les images là où elles sont disponibles.

#### Modificateur UNION

Le modificateur UNION vous permet de combiner les résultats de plusieurs graphiques. Vous pouvez l'utiliser pour rassembler plusieurs requêtes ou pour poser la même requête de plusieurs manières. Poser la même requête de plusieurs manières peut potentiellement élargir vos résultats.

#### Modificateur FILTRE

Le modificateur FILTER vous permet de filtrer vos résultats afin de ne voir qu'un sous-ensemble de ce qui apparaît dans les données. Par exemple, vous pouvez utiliser la fonction YEAR dans un filtre pour récupérer les résultats correspondant à une période spécifique, ou la fonction LANG dans un filtre si vous interrogez un ensemble de données comprenant plusieurs langues et que vous ne souhaitez voir les résultats que dans une seule langue. Langue.

#### ORDER BY ?variable Modificateur

Le modificateur de variable ORDER BY ? vous permet de trier l'ordre d'apparition de vos résultats, par exemple par ordre alphabétique ou chronologique.

#### LIMITE ?numéro Modificateur

Le modificateur LIMIT ?nombre vous permet de limiter le nombre de résultats qui reviennent. Ce modificateur est utile si vous souhaitez vérifier si votre requête fonctionne sans spouler des centaines ou des milliers de résultats, car plus votre requête génère de résultats, plus elle s'exécutera lentement.

## Déterminer les questions

Pour construire une requête SPARQL, vous devez d'abord déterminer ce que vous pouvez demander.

- Faites une liste des choses que vous voulez savoir.
- Divisez votre question en autant de petites questions que possible.
- Trouvez des réponses potentielles correctes et incorrectes afin que vous puissiez vérifier que votre requête fonctionne.
- Examinez vos données pour voir quelles informations elles contiennent et inventez une question qui vous ramènera à ces informations.

Voici un exemple simplifié de graphique montrant les résultats générés par une [requête SPARQL](https://rs-review.lincsproject.ca/sparql#query=PREFIX+rdfs%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-schema%23%3E%0APREFIX+crm%3A+%3Chttp%3A%2F%2Fwww.cidoc-crm.org%2Fcidoc-crm%2F%3E%0A%23Basic+information+about+la+peinture+%E2%80%9CPeuples+allant+dans+la+salle+de+danse%E2%80%9D%0ASELECT+%3FLabel+%3FjpgOfPainting+%3FTimeSpanDescription+%3FArtistName%0AWHERE+%7B+%0A+%3Chttps%3A%2F%2Fsaskcollections.org%2Fkenderdine%2FDetail%2Fobjects%2F4673%3E+rdfs%3Alabel+%3FLabel%3B%0A+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++crm%3AP108i_was_produced_by+%3FProductionEvent%3B%0A++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++crm%3AP138i_has_representation+%3FjpgOfPainting.%0A++%3FProductionEvent+crm%3AP4_has_time-span+%3FTimeSpan%3B%0A+++++++++++++++++++crm%3AP14_carried_out_by+%3FArtiste.%0A++%3FTimeSpan+crm%3AP82_at_some_time_within+%3FTimeSpanDescription.%0A++%3FArtiste+rdfs%3Alibellé+%3FNomArtiste.%0A%7D). Cet exemple, tiré de la [Collection d'art de l'Université de la Saskatchewan](https://saskcollections.org/kenderdine/Detail/objects/4673), présente des informations sur le tableau "People Going into the Dancing Hall", qui a été créé par Allen Sapp Au vingtième siècle.

Les ovales représentent des entités. Chaque entité est un URI et possède une étiquette lisible par l'homme. Les rectangles représentent %%littéraux|literal%% : chaînes de caractères lisibles par l'homme plutôt que traitables par une machine, telles que des noms et des dates vagues.

Bien que vous puissiez vous attendre à ce que le graphique soit centré sur l'objet d'art, les relations d'intérêt pour notre requête (qui a créé l'objet et quand) sont en fait liées par un intermédiaire %%nœud|node%% (en bleu), qui représente un événement, dans ce cas un événement de production. En effet, LINCS a adopté %%CIDOC CRM|cidoc-crm%% comme son ontologie de niveau supérieur. CIDOC-CRM est un %%modèle centré sur les événements|event-oriented-ontology%%, de nombreux triplets dans les ensembles de données hébergés par LINCS incluent des événements quelque part en leur sein. Comprendre le modèle de données est essentiel pour utiliser SPARQL. Si vous souhaitez en savoir plus sur le modèle de données, voici quelques ressources pour vous aider à démarrer :

- [Ontologies](/docs/get-started/linked-open-data-basics/concepts-ontologies)
- [CIDOC-CRM](/docs/terms/cidoc-crm)
- Profil d'application LINCS [Bientôt disponible !]
- Profils d'application spécifiques au projet [Bientôt disponible !]
- Utilisation de SPARQL avec les données LINCS [Bientôt disponible !]

:::tip

Conseils pour apprendre à créer des requêtes :

- Commencer petit
- Empruntez des composants à d'autres requêtes et modifiez-les petit à petit
- Faites des requêtes simples, puis cherchez des moyens de les rendre plus complexes
- Revenez en arrière et réessayez si votre requête échoue

:::

## Ressources

Pour en savoir plus sur les requêtes avec SPARQL, consultez les ressources suivantes.

**Informations préliminaires :**

- Blaney (2017) [« Introduction aux principes des données ouvertes liées »](https://programminghistorian.org/en/lessons/intro-to-linked-data)
- bobdc (2015) [“SPARQL en 11 Minutes”](https://www.youtube.com/watch?v=FvGndkpa4K0) [Vidéo]

**Informations pour les débutants :**

- Ontotext (2022) ["Qu'est-ce que SPARQL?"](https://www.ontotext.com/knowledgehub/fundamentals/what-is-sparql/)
- Stardog Union (2022) [“Apprendre SPARQL”](https://www.stardog.com/tutorials/sparql/#:~:text=The%20basic%20building%20block%20for,wildcards%20that%20match%20any%20node)
- W3C (2008) [« SPARQL par l'exemple : l'aide-mémoire »](https://www.iro.umontreal.ca/~lapalme/ift6281/sparql-1_1-cheat-sheet.pdf) [PowerPoint]

**Informations intermédiaires :**

- Feigenbaum (2009) [« SPARQL par exemple »](https://www.w3.org/2009/Talks/0615-qbe/)
- Gruber (2018) [« 0 à 60 sur les requêtes SPARQL en 50 minutes »](https://www.dublincore.org/webinars/2015/from_0_to_60_on_sparql_queries_in_50_minutes_redux/slides.pdf) [PowerPoint]
- Lincoln (2015) ["Utilisation de SPARQL pour accéder aux données ouvertes liées"](https://programminghistorian.org/en/lessons/retired/graph-databases-and-SPARQL)

**Informations avancées :**

- Apache Jena (2022) [« Tutoriel SPARQL »](https://jena.apache.org/tutorials/sparql.html)
- W3C (2008) [« Langage de requête SPARQL pour RDF »](https://www.w3.org/TR/rdf-sparql-query/)
- W3C (2013) [« Présentation de SPARQL 1.1 »](https://www.w3.org/TR/sparql11-overview/)
- W3C (2014) [“Concepts RDF 1.1 et syntaxe abstraite”](https://www.w3.org/TR/rdf11-concepts/#section-rdf-graph)

**Informations spécifiques à Wikidata :**

- Jones (2020) ["Connaissance informatique : Wikidata, service d'interrogation de Wikidata et femmes maires !"](https://techblog.wikimedia.org/2020/03/24/computational-knowledge-wikidata-wikidata-query-service-and-women-who-are-mayors/)
- Wikilivres (2017) [“SPARQL/Basics”](https://en.wikibooks.org/wiki/SPARQL/Basics)
- Wikidata (2020) ["Une introduction en douceur au service de requête Wikidata"](https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/A_gentle_introduction_to_the_Wikidata_Query_Service)
- Wikidata (2022) [« Tutoriel SPARQL »](https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial)
- Wiki Education (2022) ["Interroger Wikidata : SPARQL"](https://dashboard.wikiedu.org/training/wikidata-professional/querying-wikidata/getting-started-querying)
- Wikimedia Commons (2020) [“Wikidata Query Service in Brief”](https://commons.wikimedia.org/wiki/File:Wikidata_Query_Service_in_Brief.pdf)
- Wikimedia Foundation (2018) ["Interroger Wikidata avec SPARQL pour les débutants absolus"](https://www.youtube.com/watch?v=kJph4q0Im98) [Vidéo]
