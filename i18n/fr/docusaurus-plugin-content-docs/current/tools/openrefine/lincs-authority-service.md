---
title: "LINCS Authority Service"
description: "Reconcile entities with LINCS’s Knowledge Graph."
translated: 2023-05-01
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

<div className="function-button-row">
  <functionbutton buttonName="Reconcile"></functionbutton>
</div>

Le service d'autorité LINCS est un service de %%réconciliation|reconciliation%% qui peut être utilisé depuis [OpenRefine] (/docs/tools/openrefine/) pour réconcilier des %%entités|entity%% de vos données avec les entités du %%graphe de connaissances|knowledge-graph%% du LINCS. 

Le service d'autorité LINCS peut également être utilisé comme une %%Interface de Programmation d'Application (API)|application-programming-interface%% de liaison d'entités en dehors du contexte d'OpenRefine. Par exemple, le [Context Plugin](/docs/tools/context-plugin/) et le [NSSI](/docs/tools/nssi/) font appel au service. Voir notre [documentation API] (https://authority.lincsproject.ca/api-docs/) pour plus de détails.

<div className="primary-button-row">
  <primarybutton
    link="/docs/tools/lincs-authority-service/lincs-authority-service-documentation"
    buttonName="To the Documentation"
    icon={faUpRightFromSquare}></primarybutton>
  <primarybutton
    link="https://gitlab.com/calincs/conversion/authority-service"
    buttonName="To GitLab"
    icon={faUpRightFromSquare}></primarybutton>
</div>

## Service d'autorité LINCS et LINCS

Le service d'autorité LINCS est utile si vous voulez :

- Créer des %%Données liées (DL)|linked-data%% dans un [domaine lié aux données LINCS](/docs/about-lincs/#areas-of-inquiry).
- Publier des DL avec LINCS
- Enrichissez vos données avec des informations provenant du graphe de connaissances LINCS

## Conditions préalables

Les utilisateurs du service d'autorité LINCS :

- N'ont pas besoin de créer un compte utilisateur
- Doivent venir avec leur propre jeu de données
- Avoir une compréhension de base de [réconciliation](/docs/create-data/reconcile) et [data cleaning](/docs/create-data/clean)
- Vous devez savoir comment utiliser une API si vous utilisez le service en dehors d'OpenRefine.

Le service d'autorité LINCS prend en charge les informations suivantes en tant qu'entrées et sorties :

- **Entrée:** Le nom ou l'identifiant d'une entité dans vos données
- **Sortie:** Correspondances possibles ou contexte de l'entité

Le format de l'entrée et de la sortie dépendra du fait que vous accédiez au service d'autorité LINCS en tant qu'[API] (https://authority.lincsproject.ca/api-docs/) ou par l'intermédiaire d'un outil comme [OpenRefine] (https://gitlab.com/calincs/admin/lincsaurus/-/tree/main/docs/tools/openrefine).

Des informations sur l'équipe qui a développé le [Service d'autorité LINCS] (/docs/about-lincs/tools-credits#lincs-authority-service) sont disponibles sur la page qui mentionne les créateurs des outils.
