---
description: "PLACEHOLDER"
title: "XTriples"
---

<!-- @format -->

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

## <i className="fa-solid fa-person-digging"></i> **This page is under construction.** <i className="fa-solid fa-person-digging"></i>

Information about the team that developed [XTriples](/docs/about-lincs/tools-credits#XTriples) is available on the Tool Credits page.
