---
title: "XTriples Documentation"
---

<!-- @format -->

## Convert XTriples to CIDOC CRM

There are two ways to create TEI documents that LINCS XTriples can convert to CIDOC CRM:

1. Create documents using [LEAF-Writer](/docs/tools/leaf-writer)’s entity template files (records about people, places, organisations, or events) or LEAF-Writer’s Letter, Poem, and Prose templates with [reconciled](/docs/terms/reconciliation) references to people. 
2. Transform your existing reconciled TEI to match the CWRC entity templates.

## Create CIDOC-CRM from LEAF-Writer Files

Once you have filled in template files (records about people, places, organisations, or events) for your project in LEAF-Writer, upload them to LINCS XTriples:

1. Click **upload** and select the entity type that corresponds to your template file type (e.g., select LINCS Events from the dropdown menu if you are converting event records created in LEAF-Writer).
2. Choose the [serialisation](/docs/terms/resource-description-framework-serialization) (XML or TTL) that you would like your output to be in. Both output options are CIDOC CRM representations of your TEI data.

:::note

The LINCS triplestore takes TTL as an input. If you are publishing your data with LINCS, choose TTL. If you are planning to use the output in your own project, choose either XML or TTL.

:::

1. Click **download** and save the output on your own computer.
2. [optional] Submit your CIDOC CRM TTL to LINCS.

### Notes

The following notes outline which TEI elements and attributes LINCS XTriples uses to create CIDOC CRM from your TEI, and how to add these TEI elements, attributes, and values in LEAF-Writer.

#### Occupation

The LINCS CIDOC CRM representation of occupation draws on the &lt;occupation> @source value provided that value comes from the [LINCS Occupation Vocabulary](https://vocab.lincsproject.ca/Skosmos/occupation/en/). The @source values can be set to the LINCS occupations in LEAF-Writer.

In LEAF-Writer, click the **person icon** to tag a person by name. In the pop-up, select the person’s occupation from the role dropdown menu and then click **OK** to save your changes.

#### Gender

The LINCS XTriples CIDOC CRM representation of gender draws on the &lt;sex> @source value in the LINCS person template. We recommend using a gender [URI](/docs/terms/uniform-resource-identifier) from either the [Canadian Writing Research Collaboratory Vocabulary > Gender > Narrower Concepts](https://vocab.lincsproject.ca/Skosmos/cwrc/en/page/Gender) list or the [Homosaurus Gender Identity > Narrower Terms > Narrower Terms](https://homosaurus.org/v3/homoit0000571).

In LEAF-Writer, click the **raw XML icon** and select **edit raw XML** to cut and paste the most appropriate CWRC ontology gender instance URI into the @source value.

The LEAF-Writer templates will eventually be updated to reflect the latest TEI encoding guidelines for [sex and gender](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/ND.html#NDPERSEpc).

#### Nationality

LINCS XTriples does not currently process the `<nationality>` element.

## Create CIDOC-CRM directly from TEI ’ographies

LINCS XTriples can process any TEI ’ography files that conform to the LINCS entity templates. If you have experience writing eXtensible Stylesheet Transformations (XSLT), you can modify and run these XSLTs to convert your project’s ’ographies into the LINCS ‘ography TEI. If your TEI does not contain URIs, load your converted ’ography files into LEAF-Writer for reconciliation, and follow the steps in [Create CIDOC CRM from LEAF-Writer Files](#create-cidoc-crm-from-leaf-writer-files).