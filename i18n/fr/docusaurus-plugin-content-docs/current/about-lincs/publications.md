---
sidebar_position: 6
title: "Publications"
description: "Journal Articles, Position Papers"
---

<!-- @format -->

## Articles de journaux

**À l'étude :**

- Martin, Kim, Susan Brown, Abi Lemak, Alliyya Mo, Deb Stacey, Jasmine Drudge-Willson et Joel Cummings. "Formations culturelles : représentent l'intersectionnalité dans les données liées." _Journal d'analyse culturelle_.

**2021 :**

- Brown, Susan et John Simpson. "Qu'y a-t-il dans un nom ?, Les données ouvertes comme base d'une écologie pour la publication scientifique, dynamique et décentrée." Traduit par Jasmine Drudge-Willson. _Sens public_ (1er mars 2021). [http://sens-public.org/articles/1484/](http://sens-public.org/articles/1484/).
- Godard, Lisa. "Identifiants persistants en tant qu'infrastructure de recherche ouverte pour réduire la charge administrative." _Pop!_, non. 3 (9 novembre 2021). [https://doi.org/10.54590/pop.2021.006](https://doi.org/10.54590/pop.2021.006).
- Rockwell, Geoffrey, Kaylin Land et Andrew MacDonald. "Analyse sociale via Spyral." _Pop!_, non. 3 (9 novembre 2021). [https://doi.org/10.54590/pop.2021.004](https://doi.org/10.54590/pop.2021.004).

**2020 :**

- Brun, Suzanne. "Catégoriquement provisoire." _PMLA_ 135, non. 1 (janvier 2020) : 165–174. [https://doi.org/10.1632/pmla.2020.135.1.165](https://doi.org/10.1632/pmla.2020.135.1.165).

## Documents de position

**2020 :**

- Brown, Susan et Deb Stacey. "L'avenir du cloud en tant que DRI." _Nouvelle infrastructure de recherche numérique_. 2020.
- Brown, Susan et Jeffery Antoniuk. "L'interface compte." _Nouvelle infrastructure de recherche numérique_. 2020.
- Brun, Suzanne. « Soutenir l'infrastructure de recherche numérique dans les sciences humaines. _Nouvelle infrastructure de recherche numérique_. 2020.
