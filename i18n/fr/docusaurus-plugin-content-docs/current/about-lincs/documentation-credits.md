---
sidebar_position: 10
title: "Crédits Documentaires"
description: "Authors, Contributors"
---

<!-- @format -->

Pour plus d'informations sur la façon de construire des citations pour LINCS, voir [Cite](/docs/about-lincs/terms-and-conditions/).

## Explorer les données

- [Explorer les données liées](/docs/explore-data)
- **Auteur(s) :** Sam Peacock (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sarah Roger (2022)

## Créer des données

- [Créer des données liées](/docs/create-data)
- **Auteur(s) :** Sam Peacock (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sarah Roger (2022)
  \* [Présentation des flux de travail de conversion](/docs/create-data/publish-data)
- **Auteur(s) :** Kate LeBere (2022)
- **Contributeur(s) :** Sarah Roger (2022)
  \* [Flux de travail de données élémentaires](/docs/create-data/publish-data/)
- **Auteur(s) :** Natalie Hervieux (2022), Kate LeBère (2022)
- **Contributeur(s) :** Sam Peacock (2022), Sarah Roger (2022)
  \* [Flux de travail de données TEI](/docs/create-data/publish-data)
- **Auteurs):**
- **Contributeur(s) :**
  \* [Flux de travail de données en langage naturel](/docs/create-data/publish-data)
- **Auteurs):**
- **Contributeur(s) :**

## Outils

- [Aperçu des outils](/docs/tools)
- **Auteur(s) :** Kate LeBere (2022)
- **Contributeur(s) :** Sam Peacock (2022), Sarah Roger (2022)
- [Browser Plugin](/docs/tools/context-plugin)
- **Auteurs):**
- **Contributeur(s) :**
- [Corpora](/docs/tools/corpora)
- **Auteur(s) :** Sam Peacock (2022)
- **Contributeur(s) :** Kate LeBere (2022), Lauren Liebe (2022), Sarah Roger (2022)
- [CWRC 2.0](/docs/tools/cwrc2.0)
- **Auteurs):**
- **Contributeur(s) :**
- [LEAF-Writer](/docs/tools/leaf-writer)
- **Auteur(s) :** Kate LeBere (2022)
- **Contributeur(s) :** Sam Peacock (2022), Sarah Roger (2022)
- [Documentation LEAF-Writer](/docs/tools/leaf-writer/leaf-writer-documentation)
- **Auteur(s) :** Kate LeBere (2022), Megan Sellmer (2016)
- **Contributeur(s) :** Mihaela Ilovan (2022), Luciano Frizzera (2022), Sam Peacock (2022), Sarah Roger (2022)
- [Linked Data Enhancement API](/docs/tools/linked-data-enhancement-api)
- **Auteur(s) :** Sam Peacock (2022), Natalie Hervieux (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sarah Roger (2022), Susan Brown (2022)
- [NERVE](/docs/tools/nerve)
- **Auteur(s) :** Kate LeBere (2022)
- **Contributeur(s) :** Sarah Roger (2022)
- [NSSI](/docs/tools/nssi)
- **Auteur(s) :** Sam Peacock (2022), Huma Zafar (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sarah Roger (2022)
- [OpenRefine](/docs/tools/openrefine)
- **Auteur(s) :** Sam Peacock (2022)
- **Contributeur(s) :** Natalie Hervieux (2022), Kate LeBere (2022), Sarah Roger (2022), Hanna Stewart (2022)
- [ResearchSpace](/docs/tools/researchspace)
- **Auteurs):**
- **Contributeur(s) :**
- [Rich Prospect Browser](/docs/tools/rich-prospect-browser)
- **Auteurs):**
- **Contributeur(s) :**
- [SPARQL Endpoint](/docs/tools/sparql)
- **Auteur(s) :** Kate LeBere (2022), Sam Peacock (2022)
- **Contributeur(s) :** Jasmine Drudge-Wilson (2022), Jingyi Long (2022), Alliyya Mo (2022), Sarah Roger (2022), Hannah Stewart (2022)
- [Spyral](/docs/tools/spyral)
- **Auteur(s) :** Kate LeBere (2022)
- **Contributeur(s) :** Jingyi Long (2022), Sam Peacock (2022), Sarah Roger (2022)
  \* [VERSD](/docs/tools/versd)
- **Auteurs):**
- **Contributeur(s) :**
- [Voyant](/docs/tools/voyant)
- **Auteur(s) :** Jingyi Long (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sam Peacock (2022), Sarah Roger (2022)
- [X3ML](/docs/tools/x3ml)
- **Auteur(s) :** Kate LeBere (2022)
- **Contributeur(s) :** Erin Canning (2022), Jasmine Drudge-Wilson (2022), Sam Peacock (2022), Sarah Roger (2022), Jessica Ye (2022)

## À propos de LINCS

\* [À propos de LINCS](/docs/about-lincs/#areas-of-inquiry)

- **Auteur(s) :** Susan Brown (2022), Kim Martin (2022), Deb Stacey (2022)
- **Contributeur(s) :** Kate LeBere (2022)
- [Microgrants de mobilisation de données](/docs/about-lincs/get-involved/data-mobilization-microgrants)
- **Auteur(s) :** Sarah Roger (2022)
- **Contributeur(s) :** Kate LeBere (2022)
- [Publications](/docs/about-lincs/publications)
- **Auteur(s) :** Kate LeBère (2022), Sarah Roger (2022)
- **Contributeur(s) :** Sam Peacock (2022)
- [Présentations](/docs/about-lincs/presentations)
- **Auteur(s) :** Kate LeBère (2022), Sarah Roger (2022)
- **Contributeur(s) :** Sam Peacock (2022)
- [Licences](/docs/about-lincs/terms-and-conditions/terms)
- **Auteurs):**
- **Contributeur(s) :**
- [Cookies et confidentialité](/docs/about-lincs/terms-and-conditions/privacy)
- **Auteurs):**
- **Contributeur(s) :**
- [Citer](/docs/about-lincs/cite)
- **Auteur(s) :** Sam Peacock (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sarah Roger (2022)

## Débutant

- [Données liées](/docs/get-started/linked-open-data-basics/concepts-linked-data)
- **Auteur(s) :** Kate LeBere (2022), Sam Peacock (2022)
- **Contributeur(s) :** Sarah Roger (2022)
- [Ontologies](/docs/get-started/linked-open-data-basics/concepts-ontologies)
- **Auteurs):**
- **Contributeur(s) :**
- [Vocabulaires](/docs/get-started/linked-open-data-basics/concepts-vocabularies)
- **Auteurs):**
- **Contributeur(s) :**
- [SPARQL](/docs/get-started/linked-open-data-basics/concepts-sparql)
- **Auteur(s) :** Jasmine Drudge-Wilson (2022), Kate LeBere (2022), Jingyi Long (2022), Sam Peacock (2022), Sarah Roger (2022), Hannah Stewart (2022)
- **Contributeur(s) :** Alliyya Mo (2022)
- [Nettoyage des données](/docs/create-data/clean)
- **Auteurs):**
- **Contributeur(s) :**
- [Réconciliation](/docs/create-data/reconcile)
- **Auteur(s) :** Sam Peacock (2022)
- **Contributeur(s) :** Kate LeBere (2022), Sarah Roger (2022)
- [Mappage des données](/docs/create-data/convert)
- **Auteurs):**
- **Contributeur(s) :**
- [Glossaire](/docs/get-started/glossary)
- **Auteur(s) :** Erin Canning (2022), Kate LeBere (2022)
- **Contributeur(s) :** Jasmine Drudge-Wilson (2022), Sam Peacock (2022), Sarah Roger (2022)
