---
sidebar_position: 9
title: "Crédits D'outils"
description: "Directors, Developers, Contributors"
---

<!-- @format -->

Des crédits sont fournis pour les outils créés par les collaborateurs de LINCS. Pour plus d'informations sur la façon de construire des citations pour LINCS, voir [Cite](/docs/about-lincs/cite).

## Context Plugin

## CWRC 2.0

Pour plus d'informations, consultez les [Crédits et remerciements](https://cwrc.ca/about-lincs/credits-and-acknowledgments) de CWRC 2.0.

- **Susan Brown**, Directrice (XXXX–)
- **Jeffery Antoniuk**, directeur technique (2021–), programmeur et analyste de systèmes (2010–2021)
- **Michael Brundin**, Coordonnateur de l'intégrité des données et des acquis (2010-2017)
- **Mihaela Ilovan**, Directrice adjointe (2021–), Chef de projet (2014–2021), Consultante en conception et développement d'interfaces (XXXX–)

## LEAF-Writer

Pour plus d'informations, consultez [À propos](https://leaf-writer.stage.lincsproject.ca/) de LEAF-Writer.

- **Susan Brown**, réalisatrice (2011–)
- **James Chartrand**, développeur principal (2011-2018)
- **Luciano Frizzera**, Développeur principal (2020–), Développeur (2019–2020)
- **Andrew MacDonald**, Développeur principal (2018-2020), Développeur (2013-2018)
- **Mihaela Ilovan**, Chef de projet (2014–)
- **Geoffrey Rockwell**, responsable de la portée et de la direction (2011–2014)
- **Megan Sellmer**, testeuse et rédactrice de documentation (2011–2016)

## NERVE

- **Susan Brown**, Directrice (2015–)
- **Luciano Frizzera**, Développeur principal (2020–), Développeur (2019–2020)
- **Andrew MacDonald**, développeur principal (2019-2020)
- **Edward Armstrong**, développeur principal (2015–2019)
- **Mihaela Ilovan**, Chef de projet (2015–)
- **Huma Zafar**, Développeur (2020–2022)

## NSSI

Pour plus d'informations, consultez le [Wiki](https://gitlab.com/calincs/conversion/NSSI/-/wikis/home) de NSSI.

- **Huma Zafar**, développeur principal (2020-2022)
- **Dawson MacPhee**, Contributeur (2021–)
- **Natalie Hervieux**, Contributrice (2020-)

## Linked Data Enhancement API

- **Natalie Hervieux**, Développeuse (XXXX)
- **Justin Francis**, Développeur (XXXX)
- **Mohammed Marzookh Farook**, Développeur (XXXX)
- **Ananya Rao**, Développeur (XXXX)

## ResearchSpace

## Rich Prospect Browser

## ResearchSpace SPARQL Interface

## Yasgui SPARQL Interface

## Spyral

Pour plus d'informations, consultez les [Crédits](https://voyant.lincsproject.ca//#!/guide/about-section-credits) de Voyant/Spyral.

- **Stéfan Sinclair**, chef de projet, concepteur principal et programmeur principal (2017-2020)
- **Geoffrey Rockwell**, chef de projet (2020-)
- **Andrew MacDonald**, programmeur principal (2019-2020), programmeur (2008-2020)

## VERSD

## Voyant

Pour plus d'informations, consultez les [Crédits](https://voyant.lincsproject.ca//#!/guide/about-section-credits) de Voyant/Spyral.

- **Stéfan Sinclair**, chef de projet, concepteur principal et programmeur principal (2003-2020)
- **Geoffrey Rockwell**, chef de projet (2020-)
- **Andrew MacDonald**, Programmeur principal (2020-), Programmeur (2008-2020)
