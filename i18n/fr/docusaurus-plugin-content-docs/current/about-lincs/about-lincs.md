---
sidebar_position: 1
title: "À propos de LINCS"
sidebar_class_name: "landing-page"
edited: 1
---

<!-- @format -->

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

Les chercheurs en sciences humaines utilisent, analysent et synthétisent des corpus d'informations hétérogènes : des informations sur tout, des personnes et des organisations aux lieux et aux événements, en passant par les concepts et les artefacts, ainsi que sur les relations vitales entre eux. Ces informations permettent aux chercheurs de répondre à de nombreuses questions, notamment : quel est l'impact des agences gouvernementales canadiennes sur les industries créatives ? Comment les routes commerciales mondiales sont-elles liées aux emplacements des ressources ? Comment les cultures narratives autochtones négocient-elles les systèmes d'écriture et d'édition imprégnés de catégories coloniales ? Quel est l'impact des identités sociales sur les communautés ?

Bien que les chercheurs contemporains disposent d'une quantité sans précédent de matériel en ligne pour répondre à des questions aussi complexes, leurs recherches sont souvent entravées par le manque de liens significatifs entre les ressources. En conséquence, la plupart des chercheurs n'interagissent avec les données culturelles qu'en les lisant, et non en tirant parti de processus algorithmiques permettant d'exploiter un plus large éventail de preuves. Les chercheurs ont besoin d'un %%Web sémantique|semantic-web%% plus intelligent, qui donne du sens aux liens lisibles par les machines afin d'élucider les diverses interconnexions, les impacts et la signification des actions et des expressions humaines.

Les technologies du Web sémantique rendent le Web plus intelligent en structurant et en reliant les données. LINCS utilise ces technologies pour relier entre elles les données sur la recherche et le patrimoine canadiens provenant de l'ensemble du Web, en convertissant, connectant, améliorant et rendant accessibles des ensembles de données auparavant hétérogènes et cloisonnés. Ces liens ouvrent la voie à de nouvelles perspectives grâce à la production de connaissances en réseau, tant au Canada qu'à l'étranger.

En mobilisant les archives culturelles, LINCS transforme l'accès à la culture humaine. Le projet permet d'obtenir des informations nouvelles et inattendues sur les identités sociales changeantes liées au genre et à l'indigénéité. Il retrace les relations entre les produits de base et les ressources naturelles, et expose les fertilisations croisées et les tensions qui résultent de l'immigration, de la diversité culturelle et des mouvements de justice sociale. Grâce aux ressources et aux outils produits par LINCS, les Canadiens seront en mesure d'explorer et d'étudier la culture de manière transformatrice, en s'appuyant sur des livres, des manuscrits, des photographies, des périodiques, des cartes postales, de la musique, etc. nouvellement accessibles.

## Pages connexes

<DocCardList items={useCurrentSidebarCategory().items.slice(1)}/>
