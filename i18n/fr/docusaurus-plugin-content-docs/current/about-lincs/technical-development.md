---
sidebar_position: 2
title: "Développement Technique"
---

Le LINCS tire parti des solutions existantes et élargit l'infrastructure de l'[Alliance de recherche numérique du Canada](https://alliancecan.ca/fr) ​​​​pour mobiliser des ensembles de données hétérogènes à grande échelle en tant que LOD pour la recherche en sciences humaines.

Les ensembles de données source sont convertis en LOD grâce aux [outils de création](/docs/create-data) de LINCS, qui mobilisent, enrichissent et relient les données de recherche. Ces LOD sont ensuite stockés dans l'infrastructure nationale de stockage des LD de LINCS, le %%triplestore|triplestore%%.

Les [outils d'exploration](/docs/explore-data) créés par LINCS permettent aux utilisateurs de filtrer, d'interroger, d'analyser, de visualiser et d'annoter les matériaux culturels qui ont été convertis en LOD. Les utilisateurs peuvent modifier, évaluer, corriger ou rejeter les enrichissements sémantiques automatisés pour leurs données propres et pour celles des autres.

## Schéma du système

Le schéma du système LINCS fournit une carte plus détaillée de l'infrastructure. LINCS adhère aux normes établies par Tim Berners-Lee et d'autres architectes du World Wide Web pour le Web sémantique, en s'inspirant de solutions pratiques mises en œuvre pour les données culturelles.

| ![Schéma d'infrastructure de données](/img/development/system-diagram-(c-LINCS).png) |
|:--:|
|L'infrastructure de données de recherche de LINCS, montrant le chemin des ensembles de données source à mesure qu'ils passent par le processus de conversion et puis par le stockage et l'accès.|

LINCS fournit des liens et une documentation utilisateur pour divers outils et interfaces de création et de conversion de LOD, allant d'outils accessibles tels que [CWRC 2.0](/docs/tools/cwrc2.0) à des outils nécessitant des connaissances techniques plus poussées telles que [Spyral](/docs/tools/spyral). LINCS fournit également des liens vers la documentation technique des outils et du code connexe utilisés sur le projet, ainsi que vers les outils qu'il adopte ou adapte. Voir [Présentation des outils](/docs/tools) pour plus d'informations.

## Ensembles de données

LINCS a converti quatre principaux types d'ensembles de données.

| ![Coupe transversale des ensembles de données](/img/development/cross-section-(c-LINCS).jpeg) |
| :---------------------------------------------------------------------------------: |

### Recherche

Les ensembles de données soigneusement conservés par des chercheurs canadiens sont au cœur de LINCS, qui mobilise ce matériel et l'associe à d'autres contenus connexes. Les ensembles de données sources sont riches et diversifiés, tout comme les [thèmes de recherche](#areas-of-inquiry) qui sont développés en les reliant.

### Canadienne

Il existe sur le Web une grande quantité de contenu lié à la culture et à l'histoire du Canada : de petits ensembles de documents créés par des chercheurs et de grandes collections numérisées détenues par des institutions de mémoire comme [Canadiana](https: //www.canadiana.ca/) et [Bibliothèque et Archives Canada](https://www.bac-lac.gc.ca/fra/Pages/accueil.aspx). Nous avons besoin de meilleurs moyens d'accéder à ce contenu.

### Culturelle

Il ya a aussi des millions de livres, de périodiques et d'autres contenus qui ont été numérisés par des groupes tels que [Internet Archive](https://archive.org/), [The Hathi Trust](https://www .hathitrust.org/), et [Project Gutenberg](https://www.gutenberg.org/), ainsi qu'une grande quantité de contenu Web autochtone pertinent pour la recherche culturelle. LINCS offre de nouvelles façons de découvrir et d'utiliser ces types de matériaux.

### Liée

LINCS s'appuie sur de nombreux travaux existants réalisés en faveur d'un Web ouvert et sémantiquement structuré, des normes %%W3C|world-wide-web-consortium%% et des %%ontologies|ontology%% établies aux grands projets communautaires tels que [DBpedia](https://www.dbpedia.org/) et [Wikidata](https://www.wikidata. org/wiki/Wikidata:Main_Page). Notre objectif est de renforcer l'écologie LOD grâce à un contenu ouvert de haute qualité et à des outils de source ouverte.

| ![Conteneurs d'ensembles de données](/img/development/dataset-containers-(c-LINCS).png) |
|:--:|
|Les données source de LINCS comprennent des ensembles de données de chercheurs, des plateformes liées, des banques de données de partenaires et des sources de données protégées, qui sont tous stockés dans des capsules de données pour données protégées.|

## Ontologies

LINCS réutilise les ontologies et les vocabulaires existants dans la mesure du possible, en s'appuyant sur les travaux existants en matières de données liées et en recherchant des vocabulaires propres à un domaine à intégrer et à relier. Il s'inspire des meilleures pratiques établies par de grands projets, tels que [Europeana](https://pro.europea.eu/), la [Bibliothèque publique numérique d'Amérique (DPLA)](https://dp.la/) et [Données liées pour la production](https://wiki.lyrasis.org/display/LD4P/Learn+More) – et des fournisseurs de patrimoine culturel au Canada qui expérimentent les LOD. LINCS veille à ce que ses ontologies puissent représenter des épistémologies non hégémoniques et pousser des représentations de connaissances alternatives dans le Web sémantique. Ainsi, les ontologies LINCS sont sélectionnées, adoptées et développées en tenant compte de l'intersectionnalité, de la multiplicité et de la différence. Pour plus d'informations, voir [Ontologies](/docs/get-started/linked-open-data-basics/concepts-ontologies).

## Conversion

LINCS convertit les données existantes en LOD en extrayant les %%entités|entity%% et les relations des ensembles de données hétérogènes. Ce processus implique à la fois une conversion importante des données et une adaptation des outils. LINCS prend en charge la conversion à partir des formats les plus courants utilisés par la communauté de recherche en sciences humaines : structuré, %%TEI|text-encoding-initiative%% et langage naturel. Voir [Présentation des flux de travail de conversion](/docs/create-data/publish-data/publishing-workflows) pour plus d'informations.

Les processus concernés sont :

* **Détecter des entités** au cœur de l'histoire et de la culture humaines
* %%Réconcilier|reconciliation%% ou lier des entités à un ou plusieurs enregistrements de ces entités (le cas échéant) stockés dans des %%graphes de connaissances|knowledge-graph%% de référence du cloud LOD
* **Créer** des relations fondées sur des ontologies entre entités, soit en établissant des correspondances à partir de la structure existante dans les matériaux sources, soit en utilisant %%le traitement automatique des langues naturelles (TALN)|natural-language-processing%% et l'apprentissage automatique pour les détecter
* **Valider** les résultats pour assurer une précision suffisante, là où les ressources et l'expertise existent

Voir [Réconciliation](/docs/create-data/reconcile) et [Mise en correspondance des données](/docs/create-data/convert) pour plus d'informations.

### Conversion des données

Pour mobiliser rapidement un grand nombre de données pertinentes, LINCS donne la priorité à la conversion des ensembles de données, en commençant par le contenu le plus prêt pour LOD. Les ensembles de données de base des chercheurs reçoivent le traitement le plus complet, y compris le contrôle humain. Les autres données sont traitées automatiquement, avec des niveaux de confiance définis pour une grande précision afin de minimiser les faux positifs. Les millions de %%triplets|triple%% rendent ces matériaux immédiatement accessibles, prêts à être vérifiés au fur et à mesure que les chercheurs les utilisent.

La conversion est de deux types. La traduction d'un ensemble de données relationnelles au format %%RDF (Resource Description Framework)|resource-description-framework%% des cartes du Web sémantique à partir de structures existantes qui renvoient à un ensemble de données source actif ou archivé. L'extraction de LOD à partir d'un ensemble de données composé de langage naturel crée un RDF qui renvoie à la source sur le web. Dans les deux cas, les outils de conversion de données LINCS permettent de suivre la provenance des données à des fins scientifiques.

### Adaptation des outils

LINCS adopte des algorithmes standard pour le TNL et la mise en correspondance des entités pour ses processus et outils de conversion, et s'appuie sur les méthodes utilisées par d'autres projets de conversion LOD à grande échelle, notamment [Linked Data for Production](https://wiki.lyrasis.org/pages/viewpage.action?pageId=74515029). Les outils et processus existants sont adaptés pour convertir les ensembles de données en énoncés du Web sémantique (des triplets qui utilisent RDF).

LINCS s'appuie sur des algorithmes primés développés en Alberta qui effectuent la %%reconnaissance d'entités nommées (Named Entity Recognition - NER)|named-entity-recognition%% et %%la désambiguïsation d'entités nommées (Named Entity Disambiguation - NED)|named-entity-disambiguation%% par rapport à un ou plusieurs graphes de connaissances du projet, incorporent des ensembles de données étiquetées manuellement comme données d'entraînement pour les modèles, et fournissent une interface pour le réglage des paramètres. Les outils de conversion LINCS sont génériques, modulaires et fonctionnent avec plusieurs algorithmes de source ouverte. LINCS adopte ou adapte également plusieurs flux de travail et outils existants pour le nettoyage et le contrôle des données, y compris des interfaces adaptées aux experts en la matière.

| ![Portail de conversion LINCS](/img/development/conversion-portal-(c-LINCS).png) |
|:--:|
|LINCS se connecte avec une variété d'interfaces, d'outils et de services créés par LINCS et par des tiers.|

## Infrastructure

LINCS construit un magasin LOD national pour la diffusion des données qu'il convertit. Un système triplestore héberge les grands ensembles de données RDF qui contiennent des milliards de triples. L'environnement de stockage a été choisi pour assurer la compatibilité avec les ensembles de données participants, l'intégration des ontologies, le réglage de la fonctionnalité d'inférence et l'installation de des %%Interfaces de programmation d'applications (API)|application-programming-interface%% qui permettent aux fournisseurs de données de confiance d'introduire régulièrement des données.

L'infrastructure LINCS est hébergée sur le nuage de l'Alliance de recherche numérique du Canada [Digital Research Alliance of Canada](https://alliancecan.ca/fr). Il contient des plates-formes de calcul haute performance avec Apache Spark, plusieurs services Web déployés sur Kubernetes et un stockage de données avec un service compatible S3. Le LINCS consulte également les initiatives nationales de préservation des données de recherche concernant la gestion des données à long terme. Le projet a établi son dépôt de code sur GitLab avec une chaîne de traitement d'intégration continue/déploiement continu.

Le [Collaboratoire canadien de recherche en rédaction (CWRC)](https://cwrc.ca/) offre aux chercheurs canadiens un environnement de recherche virtuel accessible qui abrite vingt-cinq projets et plus de 250000 articles, et il héberge, diffuse et fournit des outils en ligne pour la recherche culturelle. Par le biais de LINCS et en collaboration avec l'Université Bucknell, le CWRC a mis à niveau sa plate-forme Fedora et son cadre d'interface Islandora afin d'être équipé pour pousser LOD dans LINCS. Le CWRC sert de rampe de lancement pour la recherche numérique et fournit un banc d'essai pour étudier les LOD qui est itératif, interactif et dynamique, comme le Web, et pose donc des défis en matière de versions fréquentes des données. Le CWRC montre comment d'autres environnements de recherche peuvent intégrer la fonctionnalité LINCS.

LINCS s'appuie sur des partenaires de l'écosystème numérique pour le stockage, la gestion et la préservation des sources de données. Les collections de source peuvent être hébergées de différentes manières afin d'obtenir des URL stables pour les métadonnées LINCS : par le biais de dépôts institutionnels, des sites de recherche stables ou de l'Internet Archive.

| ![Système de stockage LINCS](/img/development/storage-system-(c-LINCS).png) |
|:--:|
|Le système de stockage LINCS se compose du triplestore, d'un triplestore miroir et d'un site de préservation à long terme. Il se connecte également à un LOD externe et à une interface administrative.|

## Accès

LINCS permet d'accéder aux données converties grâce à ses [outils d'exploration](/docs/explore-data). Les résultats de la recherche sont également disponibles dans divers formats de liste ainsi que dans une visualisation graphique.

Dans la mesure du possible, les outils sont implémentés en tant que services Web autonomes, avec des API pour soutenir l'utilisation par des tiers. Les composants sont modulaires : ils peuvent fonctionner individuellement, être intégrés dans un flux de travail ou dans un autre système. Cette architecture permet de poursuivre le développement d'outils supplémentaires pour le large éventail de cas d'utilisation émergeant de l'académie et au-delà. Le code est de source ouverte. Les principes de conception ouverts permettent aux autres de créer des interfaces pour leurs données propres et d'intégrer les outils LINCS dans d'autres environnements.

### Accès aux données LINCS

LINCS est à la fois générique, en permettant différents types de requêtes sur des données converties à partir de sources très différentes, et précis, en permettant aux chercheurs d'explorer des vocabulaires de domaine spécifiques et des sous-ensembles de contenu hautement spécialisés. LINCS construit ses plans d'accès sur des modèles réussis, tels que le projet [Situated Networks and Archival Contexts](https://snaccooperative.org/) pour une interface d'accès, la DPLA pour une interface de développement et l'interface pour les humanités numériques [Humanities Networked Infrastructure]( https://huni.net.au/#/search) pour faire participer les utilisateurs aux données liées.

Grâce au système LINCS, les chercheurs canadiens ont un accès inégalé au contenu du patrimoine culturel. Cela comprend les données protégées par le droit d'auteur : nos publications sont si jeunes que le manque d'accès aux collections numériques pour l'analyse a considérablement entravé la recherche sur la culture canadienne, mais LINCS est en mesure d' élucider, par exemple, les ensembles de données massifs mais protégés [ensembles de données de la bibliothèque numérique HathiTrust](https://ieeexplore.ieee.org/abstract/document/7991585). Les outils d'exploration garantissent la mobilisation des données sur l'ensemble du spectre des chercheurs ordinaires et techniques.

### Accès a la conversion

Pour les interfaces, LINCS indique et documente un certain nombre d'options, plutôt que de créer une plate-forme unique. Les chercheurs expérimentés dotés d'outils pour préparer les données pour LINCS, et les moins techniques sont soutenus par des partenaires tels que le CWRC.

LINCS étend ou adopte plusieurs outils et plateformes existants conçus pour les chercheurs en sciences humaines. Par exemple, la sortie de l'interface [Recogito](https://recogito.pelagios.org/) développée par le projet [Pelagios](https://peripleo.pelagios.org/) pour le balisage spatial et la création de répertoires géographiques peut être convertie et améliorée pour produire une représentation RDF plus complète du contenu. LINCS étend CWRC et la suite d'analyse et de visualisation de texte [Voyant](/docs/tools/voyant) largement utilisée. La méthode du carnet de programmation documente les processus personnalisables pour la conversion de divers ensembles de données et contribue à promouvoir le développement des compétences en codage.

| ![Interface Web LINCS](/img/development/web-interface-(c-LINCS).png) |
|:--:|
|Les données source de LINCS comprennent des ensembles de données de chercheurs, des plateformes liées, des bases de données partenaires et des sources de données protégées, qui sont tous stockés dans des capsules de données pour données protégées.|
