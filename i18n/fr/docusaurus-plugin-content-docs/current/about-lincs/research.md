---
sidebar_position: 1
title: "Domaines de Recherche"
edited: 1
cited: "Brown, Susan, Erin Canning, Kim Martin, and Sarah Roger. “Ethical Considerations in the Development of Responsible Linked Open Data Infrastructure.” In Ethics in Linked Data, edited by Kathleen Burlingame, Alexandra Provo, and B.M. Watson. Litwin Books, 2023."
---

LINCS intègre un large éventail de données sur le patrimoine culturel. Le projet est ouvert à la prise d'ensembles de données structurées, semi-structurées et non structurées couvrant tout, des images aux textes, des cartes à la musique, à condition que les données sources elles-mêmes soient facilement accessibles. Les domaines de recherche suivent les intérêts des chercheurs impliqués dans les premières étapes du projet LINCS. Nous reconnaissons que ces domaines sont appelés à évoluer et à se développer au fil du temps et nous inclurons toute mise à jour future sur cette page.

### L'édition canadienne

Les chercheurs plongeront dans l'histoire de l'édition canadienne à la recherche de modèles et d'effets locaux - le genre de liens qui permettent d'élucider les récentes controverses sur les préjugés sexistes, les concepts de nation, ou les revendications d'identité autochtone au sein de l'establishment littéraire canadien.

### Géohumanités

Ce groupe se concentre sur les outils et les méthodes permettant de faire face aux changements historiques, à la réconciliation et aux différences culturelles et méthodologiques dans le cadre de l'utilisation d'entités spatiales dans les %%Données ouvertes liées (LOD)|linked-open-data%%. Les interconnexions des données avec les sources tireront parti du [tournant spatial](https://spatial.scholarslab.org/spatial-turn/what-is-the-spatial-turn/) largement quantitatif de la science récente de manière à soutenir également la recherche qualitative. Le groupe assurera la liaison avec le réseau Linked Pasts et le réseau Pelagios.

### Savoirs autochtones

Les chercheurs s'attaqueront aux défis liés à la représentation des matériaux et des perspectives autochtones sous forme de %%Données liées (LD)|linked-data%%, y compris les questions de souveraineté, de confiance, d'accès et de décolonisation des métadonnées et de relations avec la communauté et la terre. Ce groupe pourrait avancer indépendamment ou se combiner avec le groupe des épistémologies résistantes.

### Systèmes de connaissance

Les chercheurs spécialisés dans différentes formes de représentation de la connaissance s'attaqueront aux promesses et aux défis de LOD afin de faire progresser la recherche pour les chercheurs en sciences humaines du monde entier.

### Histoire de la littérature et du spectacle

Une couverture riche de l'histoire de la littérature et du spectacle, en particulier de l'écriture, des réseaux et de la réception des femmes, permettra aux chercheurs d'explorer les modèles de mouvement et les réseaux professionnels formateurs dans l'histoire du divertissement.

### Londres et l'Empire britannique

Les chercheurs étudieront l'impact culturel, politique et économique de la Grande-Bretagne et du Canada en tant que colonie de peuplement, grâce à l'agrégation de plusieurs ensembles de données allant du Moyen Âge à nos jours.

### Cultures matérielles et textuelles

Les chercheurs qui s'intéressent à la culture matérielle au sein de la communauté muséale mobiliseront LOD pour promouvoir une analyse basée sur l'agrégation des propriétés physiques de nombreux artefacts historiques. Les spécialistes de la culture textuelle numérique s'appuieront sur des documents exploitables par ordinateur pour identifier des modèles intertextuels qui sont actuellement inaccessibles.

### Prosopographie

Les méthodes numériques ont renouvelé le domaine de la prosopographie ou de la personnographie, l'étude des personnes en groupes, en mobilisant des données granulaires sur les individus comme base d'une recherche plus large. Les groupes prosopographiques bénéficieront de données biographiques liées pour exploiter les dimensions transdisciplinaires, transgénériques et transnationales non réalisées des écologies de recherche sémantique.

### Épistémologies résistantes

LINCS inclut des chercheurs dont les ensembles de données intègrent des compréhensions non hégémoniques de l'histoire ou de la culture contemporaine, et de la façon dont la connaissance fonctionne pour résister aux visions du monde dominantes. L'inclusion de données basées sur de telles épistémologies aidera à pousser les représentations de connaissances marginalisées et résistantes dans le Web sémantique.