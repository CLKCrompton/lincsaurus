---
sidebar_position: 3
title: "Comités et Conseils"
description: "Committee Members, Board Members"
---

<!-- @format -->

## Comité exécutif

- **Susan Brown**, chef de projet, Université de Guelph
- **Deborah Stacey**, directrice technique, Université de Guelph
- **Kim Martin**, présidente du conseil de recherche, Université de Guelph
- **Pieter Botha**, directeur technique, Université de Guelph
- **Sarah Roger**, gestionnaire de projet LINCS, Université de Guelph

## Conseil de la recherche

- **Kim Martin**, présidente du conseil de recherche, Université de Guelph
- **Susan Brown**, chef de projet, Université de Guelph
- **Stacy Allison-Cassin**, responsable du thème (Échelle de navigation), Université de Toronto
- **Jon Bath**, responsable du thème (Créer des connaissances), Université de la Saskatchewan
- **Janelle Jenstad**, responsable du thème (Making Connections), Université de Victoria

## Comité technique

- **Deborah Stacey**, directrice technique, Université de Guelph
- **Susan Brown**, responsable de composante (accès), Université de Guelph
- **Denilson Barbosa**, responsable de composante (conversion), Université de l'Alberta
- **Constance Crompton**, responsable technique du site (Ottawa), Université d'Ottawa
- **Kate Davis**, Responsable technique du site (Toronto/Scholars Portal), Scholars Portal/Université de Toronto
- **Lisa Goddard**, responsable de composante (infrastructure), Université de Victoria
- **Carlos McGregor**, Scholars Portal/Université de Toronto
- **Cecily Raynor**, responsable technique du site (McGill), Université McGill
- **Geoffrey Rockwell**, responsable de composante (Voyant), Université de l'Alberta
- **John Simpson**, responsable technique du site (Digital Research Alliance of Canada), Digital Research Alliance of Canada
- **Amaz Taufique**, Scholars Portal/Université de Toronto

## Conseil d'administration

- **Susan Brown**, chef de projet, Université de Guelph
- **Deborah Stacey**, directrice technique, Université de Guelph
- **Kim Martin**, présidente du conseil de recherche, Université de Guelph
- **Stacy Allison-Cassin**, responsable du thème (Échelle de navigation), Université de Toronto
- **Denilson Barbosa**, responsable de composante (conversion), Université de l'Alberta
- **Jon Bath**, responsable du thème (Créer des connaissances), Université de la Saskatchewan
- **Constance Crompton**, responsable technique du site (Ottawa), Université d'Ottawa
- **Kate Davis**, Responsable technique du site (Toronto/Scholars Portal), Scholars Portal/Université de Toronto
- **Lisa Goddard**, responsable de composante (infrastructure), Université de Victoria
- **Rebecca Graham**, bibliothécaire universitaire, Université de Guelph
- **Diane Jakacki**, responsable technique du site (LEAF), Université Bucknell
- **Janelle Jenstad**, responsable du thème (Making Connections), Université de Victoria
- **Cecily Raynor**, responsable technique du site (McGill), Université McGill

## Conseil consultatif technique

- **Arianna Ciula**, directrice adjointe et analyste principale de logiciels de recherche au King's Digital Lab, King's College London
- **Sandra Fauconnier**, Historienne de l'art indépendante et chargée de projet
- **Thomas Padilla** Directeur adjoint des services d'archivage et de données, Internet Archive
- **Cristina Pattuelli**, co-directrice du Semantic Lab, Pratt Institute School of Information
- **Stéphane Pouyllau**, Ingénieur de recherche CNRS ; Co-fondateur de Huma-Num, Huma-Num
- **Pat Riva**, bibliothécaire universitaire associée aux services des collections, Université Concordia
- **Rob Sanderson**, directeur des renforcés du patrimoine culturel, Université de Yale
- **Rainer Simon**, chercheur principal au sein du Data Science & Artificial Intelligence Group/Cultural Data Science, Austrian Institute of Technology
- **Peter Patel-Schneider**, directeur scientifique, PARC

## Structure organisationnelle

| ![Structure organisationnelle du LINCS](/img/lincs_org_chart-(c-LINCS).png) |
| :---------------------------------------------------------------: |
