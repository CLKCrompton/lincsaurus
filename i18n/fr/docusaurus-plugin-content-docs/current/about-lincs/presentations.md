---
sidebar_position: 5
title: "Présentations"
description: "Papers, Keynotes, Lectures, Talks, Workshops"
---

<!-- @format -->

## Documents de conférence

**2021 :**

- Brun, Suzanne. "Séance d'information sur les micro-subventions communautaires." _Conférence LINCS_. En ligne. 5 mai 2021.
- Brun, Suzanne. "Données ouvertes dans l'open source." Article présenté à la _World Open Library Foundation Conference (WOLFcon)_. En ligne. 2 juin 2021. [https://www.youtube.com/watch?v=kfq9w_hMNRc](https://www.youtube.com/watch?v=kfq9w_hMNRc).
- Brun, Suzanne. "(Re:) Plate-forme." Document présenté à _Infrastructural Interventions: Digital Humanities and Critical Infrastructure Studies_. King's College London, Londres, Angleterre. 20 juin 2021. Invitation.
- Brown, Susan, Kim Martin et Deb Stacey. "Aperçu du LINCS." _Conférence LINCS_. En ligne. 30 avril 2021.
- Mise en conserve, Erin. "Les ontologies au LINCS." _Conférence LINCS_. En ligne. 4 mai 2021.
- Crompton, Constance, Huma Zafar, Candice Lipski et Alice Defours. "Cartographie et vérification : conversion de la TEI en données liées." Communication présentée au _TEI 2021_. En ligne. 25-30 octobre 2021.
- Jakacki, Diane, Evan A. Klimas, Charlotte E. Simon et Twity Gitonga. « Fouiller et déchiffrer : de nouveaux chercheurs apportant de nouvelles perspectives aux projets en cours. » Communication présentée au _TEI 2021_. En ligne. 25-30 octobre 2021.
- Jakacki, Diane et John Douglas Bradley. "Combiner le modèle Factoid avec TEI : exemples et défis conceptuels." Communication présentée au _TEI 2021_. En ligne. 25-30 octobre 2021.
- Jakacki, Diane, John Edward Westbrook, Rebecca Marie Heintzelmann, Juliya Harnood et Sarah Harber. "Les leçons de choses ou la pédagogie de Suzette au cube." Communication présentée au _TEI 2021_. En ligne. 25-30 octobre 2021.
  \*Jenstad, Janelle. "Utiliser &lt;listPerson>, &lt;person>, &lt;name> et &lt;reg> pour faire des arguments éditoriaux sur les personnages du début du drame moderne." Communication présentée au _TEI 2021_. En ligne. 25-30 octobre 2021.
- Terre, Kaylin, Andrew MacDonald et Geoffrey Rockwell. "Spyral Notebooks en tant que complément collaboratif aux outils Voyant." Communication présentée au _CSDH-SCHN 2021 : Making the Network_. En ligne. Du 30 mai au 3 juin 2021. [https://doi.org/10.17613/2bsr-xp53](https://doi.org/10.17613/2bsr-xp53).
- Terre, Kaylin, Andrew MacDonald et Geoffrey Rockwell. "Spyral Notebooks en tant que complément collaboratif aux outils Voyant." Communication présentée à _European Association for Digital Humanities (EADH)_. En ligne. Du 21 au 25 septembre 2021. [https://doi.org/10.17613/2bsr-xp53](https://doi.org/10.17613/2bsr-xp53).
- Liebe, Lauren. "WOLFcon 2021 - Le consortium de recherche avancée." Article présenté à la _World Open Library Foundation (WOLFcon)_. En ligne. 3 juin 2021. [https://youtu.be/pl07jneFGfI](https://youtu.be/pl07jneFGfI).
- Mo, Alliyya, Sarah Roger, Thomas Smith et Hannah Stewart. "Agile LINCS : Construire une communauté d'étudiants grâce à la gestion de projet." Lightning Talk à _Digital Humanities Summer Institute (DHSI) 2021 : Project Management in the Humanities._ En ligne. 9 juin 2021.
- Schoenberger, Zacharie. « Technologies de données liées pour l'infrastructure canadienne de recherche en sciences humaines ». Lightning Talk à la _Conférence canadienne sur les logiciels de recherche_. En ligne. 7 juillet 2021.
- Tchoh, Bennett Kuwan, Kaylin Land, Andrew MacDonald, Milena Radzikowska, Stan Ruecker, Gerry Derksen, Jingwei Wang et Geoffrey Rockwell. "Spéculer avec Voyant : Conceptions pour les murs de données." Communication présentée au _CSDH-SCHN 2021 : Making the Network_. En ligne. Du 30 mai au 3 juin 2021. [https://doi.org/10.17613/fsre-kd07](https://doi.org/10.17613/fsre-kd07).

**2020 :**

- Brown, Susan et Kim Martin. "Lier les recherches (ers) In: Building Infrastructure for Open Scholarship." Document présenté à _Implementing New Knowledge Environments (INKE)_. En ligne. 10 décembre 2020.
- Brown, Susan et Kim Martin. "Vers une infrastructure liée pour une bourse culturelle en réseau." Document présenté au _CSDH-SCHN 2020 : Building Community Online_. En ligne. 2 juin 2020.
- Godard, Lisa. "Conservation à long terme des données liées." Article présenté à _Access 2020_. En ligne. 20 octobre 2020.
  \*Mandell, Laura. "Conservation des facultés et données ouvertes liées : BigDIVA.org." Communication présentée à _LD4 Conference_. En ligne. 23 juillet 2020. [https://youtu.be/Vt9aOZTmCqE](https://youtu.be/Vt9aOZTmCqE). [1:00:43]

## Panels de conférence

**2021 :**

- Botha, Pieter. « LINCS DevOps ». Panel à _LINCS Conference_. En ligne. 3 mai 2021. [https://youtu.be/3AVPWa9BcwU](https://youtu.be/3AVPWa9BcwU).
  \*Hervieux, Nathalie. "Améliorer l'efficacité de la liaison d'entités." Panel à _LINCS Conference_. En ligne. 3 mai 2021. [https://youtu.be/WF4YhGFvMig](https://youtu.be/WF4YhGFvMig).
- Martin, Kim, Diane Jakacki, Alison Hedley, Michelle Meagher et Jana Smith Elford. "Panel de questions-réponses pour les chercheurs LINCS." Panel à _LINCS Conference_. 5 mai 2021.
- Martin, Kim, Hannah Stewart, Rashmeet Kaur, Thomas Smith et Michaela Rye. "Discussions d'étudiants du laboratoire THINC et panel de questions-réponses." Panel à _LINCS Conference_. 29 avril 2021.
- Martin, Kim, Sarah Roger, Erin Canning, Zach Schoenberger et Susan Brown. "Lier les communautés de pratique." Panel au _CSDH-SCHN 2021 : Making the Network_. En ligne. 30 mai 2021.

**2020 :**

- Brun, Suzanne. « Partenariats et collaborations ». Panneau au _RCKN_. En ligne. 29 octobre 2020. [https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/3_Partnerships_0-Complete%20-%20EN.pdf](https://www.crkn-rcdr. ca /sites/crkn/files/2020-10/3_Partnerships_0-Complete%20-%20FR.pdf).
- Brown, Susan, Kim Martin, Lisa Goddard, Sharon Farnell, Dan Scott et Stacy Allison-Cassin. "[Constructions collaboratives : données liées et bourses d'études culturelles canadiennes](https://www.youtube.com/watch?v=4wEl2saJJ7s#t=2h41m0s)." Panneau à _Accès 2020_. En ligne. 20 octobre 2020. [https://youtu.be/4wEl2saJJ7s](https://youtu.be/4wEl2saJJ7s). [2:40:50]
- Godard, Lisa. « Identifiants persistants (PID) au Canada ». Panneau au _RCKN_. En ligne. 27 octobre 2020. [https://www.crkn-rcdr.ca/sites/crkn/files/2020-10/4_PIDs%20in%20Canada_0_Complete%20-%20FR.pdf](https://www.crkn-rcdr .ca/sites/crkn/files/2020-10/4_PIDs%20in%20Canada_0_Complete%20-%20FR.pdf)

## Discours d'ouverture

**2021 :**

- Susan Brown. "Collaborations significatives : technologies sémantiques et recherche culturelle". _Séminaire sur les humanités numériques dans et sur les Amériques_. Institut des Amériques, Université d'Avignon. En ligne. 11 février 2021.

**2020 :**

- Crompton, Constance. "Données liées à travers le temps et l'espace : le défi de la modélisation des identités temporelles et des lieux en tant que données liées." Discours d'ouverture à _Passés liés_. Université de Londres et British Library. Londres, Angleterre. 3 décembre 2020. [https://ics.sas.ac.uk/podcasts/linked-data-across-time-and-space-challenge-modelling-temporal-identities-and-places](https://ics . sas.ac.uk/podcasts/linked-data-across-time-and-space-challenge-modelling-temporal-identities-and-places).

## Présentations au sommet

**2020 :**

- Crompton, Constance. "Construire un Web inclusif." _Réunion de la Société Royale : Science Trust and Society_. Culture inclusive assiégée. Ottawa, Canada. 9 septembre 2020.
- Crompton, Constance. "Quand le Web est grand et les archives sont profondes : collaboration dans le projet de libération des lesbiennes et des gais au Canada." _Spectres de DH_. Université McGill. Montréal Canada. 19 novembre 2020.

## Conférences invitées

**2021 :**

- Brun, Suzanne. « Métadonnées pour le genre ». Conférence pour une réunion du groupe de travail sur la référence et la recherche et le groupe de travail sur les normes éditoriales des réseaux situés et des contextes archivistiques. En ligne. 28 mai 2021. Invitation.
- Mise en conserve, Erin. "Données ouvertes liées au LINCS." Conférence pour INF2186H : Schémas de perfectionnés et applications. Université de Toronto. Toronto, Canada. 2021.
- Rockwell, Geoffrey. « Voyant, Spyral : un discours sur la pratique dans les humanités numériques ». Conférence pour le Département de Langues, Littératures et Cultures. Université McGill. En ligne. novembre 2021.

## Discussions publiques

**2021 :**

\*Bain, Jon. "Est-ce que c'est des données ? Processus de recherche en tant que données dans les arts. Conférence publique pour "Research Data Management for Digitally Curious Humanists". En ligne. 14 juin 2021. [https://osf.io /6vepj/wiki/virtual%20panel%20--%20bath/](https://osf.io/6vepj/wiki/virtual%20panel%20--%20bains/).

- Brun, Suzanne. « PID : que doivent savoir les chercheurs  ? » Conférence publique pour « Qui, quoi et où des identificateurs persistants (PID) » du *RCKN* ! En ligne. 17 mars 2021. [https://vimeo.com/525702819](https://vimeo.com/525702819).
- Brown, Susan et Kim Martin. "Créer des liens avec LINCS." Conférence publique pour les « Mercredis Wikipédia » de la British Library. En ligne. 30 juin 2021.
- Godard, Lisa. « Quelle est la prochaine étape pour les PID au Canada ? » Conférence publique pour « Qui, quoi et où des identificateurs persistants (PID) » du *RCKN* ! En ligne. 12 mai 2021.

**2020 :**

- Huculak, Matthew, Sarah Simpkin, Constance Crompton, Kim Martin et Amy Tector. "L'importance de la collaboration dans les humanités numériques." Conférence publique pour le « Séminaire Wallot-Sylvestre » de Bibliothèque et Archives Canada. En ligne. 22 septembre 2020. [https://www.bac-lac.gc.ca/eng/about-us/events/Pages/2020/Wallot-Sylvestre-Seminar-Leslie-Weir-Librarian-and-Archivist-of-Canada .aspx](https://www.bac-lac.gc.ca/eng/about-us/events/Pages/2020/Wallot-Sylvestre-Seminar-Leslie-Weir-Librarian-and-Archivist-of-Canada . aspx). Inviter.

##Ateliers

**2021 :**

- Brun, Suzanne. «Collaboratoire canadien de recherche en rédaction». Atelier pour l'initiative _CIstudies_org_. 10 juin 2021. [https://youtu.be/pf8Z2lM96fA](https://youtu.be/pf8Z2lM96fA). Inviter.
- Brown, Susan et Kim Martin. "Une très brève introduction aux données ouvertes liées avec LINCS." Atelier pour _DH Toolbox_. Université d'Ottawa. En ligne. 10 juin 2021. [https://youtu.be/1Qe-k9mbErA](https://youtu.be/1Qe-k9mbErA).
- Brown, Susan et Kim Martin. "Lier les cultures." Atelier pour _DHSITE_. Université d'Ottawa. Ottawa, Canada. Mai 2021.
- Brown, Susan, Kim Martin et Deb Stacey. "Ontologies pour la diversité." Atelier pour _DH@Guelph_. En ligne. Du 14 au 17 juin 2021.
- Mise en conserve, Erin. "Vocabulaires vs / et ontologies : comparaison des ontologies CWRC et LINCS." Atelier pour _DH@Guelph_. En ligne. Du 14 au 17 juin 2021.
- Martin, Kim, Rashmeet Kaur et Kathleen McCulloch-Cop. "LINCS UX." Atelier pour _LD4_. En ligne. Octobre 2021.
- Martin, Kim, Susan Brown, Erin Canning, Rashmeet Kaur, Kathleen McCullough-Cop, Alliyya Mo et Sarah Roger. "Données liées pour le contexte." Atelier pour _LD4_. En ligne. 21 juillet 2021. [https://youtu.be/flaRrw6of-8](https://youtu.be/flaRrw6of-8).
- Terre de Rockwell, Geoffrey et Kaylin. "Utilisation des outils Voyant avec les cahiers Spyral pour l'analyse de texte." _Établi DH_. Université Ryerson. Toronto, Canada. 11 mars 2021.
- Terre de Rockwell, Geoffrey et Kaylin. "Voyant et Spyral." Atelier pour _DigiPhiLit_. Université d'Anvers. 2021.
- Terre de Rockwell, Geoffrey et Kaylin. "Voyant et Spyral." Atelier pour _Institut d'été des humanités numériques (DHSI) 2021_. En ligne. 23 juin 2021.
- Tarpley, Bryan et Lauren Liebe. « Démonstration de Corpora ». Atelier pour la*conférence LINCS*. En ligne. 3 mai 2021. [https://youtu.be/2X8gr3RoH18](https://youtu.be/2X8gr3RoH18).
- Zafar, Huma, Luciano Frizzera et Mihaela Ilovan. "Démonstration de NERVE." Atelier pour la*conférence LINCS*. En ligne. 4 mai 2021. [https://youtu.be/Empg691TkbE](https://youtu.be/Empg691TkbE).

**2020 :**

- Brown, Susan, Deb Stacey et Alliyya Mo. "CWRC Ontologies". Atelier pour _Projet LINCS_. En ligne. 13 août 2020.
- Crompton, Constance. "Données ouvertes : comprenez-les, utilisez-les, créez-les - pratique avec les requêtes Open Refine, Wikidata et SPARQL." Atelier pour _Projet LINCS_. En ligne. 28 août 2020. [https://youtu.be/uSKO_r6xlvs](https://youtu.be/uSKO_r6xlvs).
- Crompton, Constance. "Données ouvertes : comprenez-les, utilisez-les, créez-les—Introduction à LOD." Atelier pour _Projet LINCS_. En ligne. 14 août 2020. [https://youtu.be/mZzNWS7-Vpk](https://youtu.be/mZzNWS7-Vpk).
- Ilovan, Mihaela et Luciano Frizzera. "Éditeur XML CWRC-Writer et l'environnement de vérification de la reconnaissance d'entité nommée (NERVE)." Atelier pour _Projet LINCS_. En ligne. 29 avril 2020.
