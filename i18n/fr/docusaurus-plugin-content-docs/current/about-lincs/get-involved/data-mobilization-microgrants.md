---
sidebar_position: 3
title: "Micro-subventions de mobilisation de données"
description: "Resources, Eligibility, Application"
---

<!-- @format -->

## Détails du programme

LINCS invite les candidatures à son programme de micro-subventions pour fournir un financement et un soutien aux chercheurs qui disposent d'ensembles de données de matériel culturel qu'ils aimeraient convertir en %%Linked Open Data (LOD)|linked-open-data%% et contributeur à la LINCS %%triplestore|triplestore%%. Ces chercheurs et ensembles de données peuvent être soit ceux qui participent déjà au LINCS, soit ceux qui ne sont pas encore impliqués.

Les individus et les groupes qui postulent doivent disposer d'ensembles de données prêts à être mobilisés en tant que LOD et doivent avoir le temps de s'engager dans le [nettoyage des données](/docs/create-data/clean) et le [mappage des données](/docs/create-data/convert) traiter.

## Ressources fournies

Les chercheurs qui ont reçu un soutien pour préparer leurs ensembles de données à inclure dans LINCS ont bénéficié d'une sélection des éléments suivants, en fonction de leurs besoins en matière de recherche et de données :

- **Réunions d'assistance** avec l'équipe centrale du LINCS pour [la conversion des données](/docs/create-data/publish-data/publishing-workflows) et les applications de recherche
- **Fonds** pour soutenir un semestre étudiant (aux tarifs de l'établissement d'origine) pour la conversion et le nettoyage des données ; les fonds peuvent être utilisés pour un semestre d'aide aux étudiants (généralement 10 à 12 heures/semaine) ou peuvent être répartis sur plusieurs étudiants ou semestres
- **Formation** liée à la conversion et au nettoyage des données pour le chef de projet, le personnel du projet et les assistants de recherche
- **Inscription gratuite ou à prix réduit** pour participer aux ateliers, conférences et formations LINCS

## Engagement des chercheurs

Les bénéficiaires de subventions devront participer à des activités de formation, rejoindre un ou plusieurs [domaines d'enquête](/docs/about-lincs/#areas-of-inquiry), rédiger un article pour le [blog LINCS](https://portal.stage.lincsproject.ca/blog), présenter (à distance ou en personne) leurs travaux en cours ou leurs conclusions lors d'une [conférence ou d'un événement LINCS](/docs/about-lincs/get-involved/events), et—où possible - saisir l'opportunité de publier dans une publication en libre accès sur la recherche facilitée par LINCS.

## Admissibilité

LINCS ingère un large éventail de données culturelles. Le projet est ouvert à l'utilisation d'ensembles de données structurées, TEI et en langage naturel tant que les données sources elles-mêmes sont facilement disponibles. Pour plus d'informations, consultez la [Présentation des workflows de conversion](/docs/create-data/publish-data/publishing-workflows).

Notez que les chercheurs doivent être canadiens, basés dans un établissement canadien ou travaillant sur un ensemble de données à prédominance canadienne. Les projets liés aux nations autochtones territoriales et frontalières géographiquement contiguës sont encouragés à postuler. Les chercheurs de groupes sous-représentés ou travaillant avec des ensembles de données liés à des communautés sous-représentées à l'extérieur du Canada seront également pris en considération. Les ensembles de données doivent appartenir à l'un des [domaines d'enquête](/docs/about-lincs/#areas-of-inquiry#areas-of-inquiry) du LINCS.

:::note

Notez que LINCS ne stocke que des %%Données liées (LD)|linked-data%% et non des ensembles de données source. Les ensembles de données source pour LD ingérés dans LINCS doivent être stockés ailleurs. Le LINCS peut vous conseiller sur les options d'archivage appropriées des ensembles de données source afin qu'ils restent connectés au LD.

:::

## Soumissions

Pour postuler à une micro-subvention de mobilisation de données LINCS, nous vous demandons de remplir un [formulaire](https://docs.google.com/forms/d/e/1FAIpQLSdpU8jc9CuDK7Q2EePNvl-8S1DktmCWMtytEWq0L-zJNP20cg/viewform) avec les informations suivantes :

- Nom du projet
- Liste des candidats, avec un candidat identifié comme chef de projet
- CV résumé pour chaque candidat nommé (maximum 2 pages)
- Le ou les groupes de domaine d'enquête dans lesquels le projet s'inscrit
- Un échantillon de données (un petit échantillon de données sous sa forme actuelle, contenant environ 25 ensembles de relations qui deviendraient des triplets LOD)
- Proposition de projet, comprenant :
- Un **résumé du projet** décrivant les questions de recherche que vous souhaitez poser si vos données sont transformées en LOD (maximum 500 mots)
- Une description de votre **ensemble de données**, y compris des détails sur la taille de l'ensemble de données, le format des données et l'emplacement actuel des données (maximum 250 mots)
- Un **calendrier** pour l'ingestion des données, avec des informations sur le temps dont vous pouvez contribuer au projet et le travail que vous estimez nécessaire pour ingérer et nettoyer vos données (maximum 250 mots)
- Un **budget et une justification**, notant la valeur d'un semestre-étudiant aux tarifs actuels de votre établissement et comment vous envisagez de répartir le travail étudiant (nombre d'étudiants, nombre d'heures/semaine, période ( maximum 250 Veuillez noter également si vous avez d'autres financements pour votre projet de recherche plus vaste et/ou des ressources supplémentaires à apporter à ce travail.

:::note

Si votre ensemble de données a déjà été évoqué pour inclusion dans LINCS lors d'un entretien d'admission d'ensemble de données, nous vous encourageons à réutiliser le contenu de votre questionnaire d'admission d'ensemble de données pour votre candidature. Si votre ensemble de données n'a pas encore été évoqué pour inclusion, ou si vous n'avez pas eu d'entretien d'admission d'ensemble de données, veuillez nous contacter si vous souhaitez organiser votre entretien ou remplir votre questionnaire d' admission d'ensemble de données avant de postuler.

:::

## Contact

Pour plus d'informations veuillez contacter [lincs@uoguelph.ca](mailto:lincs@uoguelph.ca).
