---
sidebar_position: 1
title: "Événements"
description: "Conferences, Workshops, Talks"
---

<!-- @format -->

## Évènements à venir

Il n'y a actuellement aucun événement à venir.

---

## Événements passés

### Conférence LINCS 2021 (29 avril–06 mai)

La conférence 2021 Linked Infrastructure for Networked Cultural Scholarship (LINCS) a présenté des démonstrations d'outils, des discussions techniques et de recherche et des rassemblements sociaux. Des vidéos de certaines des sessions sont fournies ci-dessous !

#### Discussions et questions-réponses avec les étudiants du THINC Lab, 29 avril

**Présentateurs :** Hannah Stewart, Michaela Rye, Rashmeet Kaur, Thomas Smith

**Président :** Kim Martin

**Résumé :** THINC Lab ne fonctionnerait pas sans le dévouement de merveilleux étudiants de premier cycle ! Rejoignez-nous pour une discussion avec quatre de ces étudiants de premier cycle sur leurs recherches, leurs réflexions sur les humanités numériques et découvrez comment leur passage au THINC Lab a changé leur passage à l'université.

**Bios :** Hannah Stewart, Michaela Rye, Rashmeet Kaur et Thomas Smith sont tous des étudiants de premier cycle à l'Université de Guelph qui ont travaillé sur des projets DH en tant qu'assistants de recherche de premier cycle du THINC Lab .

#### Soirée portes ouvertes du THINC Lab, 29 avril

Pour célébrer la Journée de la DH 2021, le [laboratoire THINC](https://www.uoguelph.ca/arts/dhguelph/thinc) de l'Université de Guelph organise une journée portes ouvertes pour présenter les membres de la communauté de l'Université de Guelph et le grand public au laboratoire. Joignez-vous à nous pour une conversation décontractée qui vous donnera l'occasion de rencontrer des membres du laboratoire, d'en savoir plus sur leurs intérêts et projets de recherche et de discuter des opportunités potentielles de vous impliquer dans le laboratoire THINC en tant que professeur affilié ou boursier.

#### Coup d'envoi et aperçu de la conférence LINCS, 30 avril

**Présentatrices :** Susan Brown, Kim Martin et Deb Stacey

**Président :** Sarah Roger

**Résumé :** Cette session lancera définitivement la conférence en ligne, fournira un bref aperçu de ce que sont les données ouvertes liées, de ce que fait le LINCS et de ce qu'il a fait jusqu'à présent, suivi d' une session de questions-réponses.

#### Démo Corpora, 3 mai ([**Youtube**](https://www.youtube.com/watch?v=2X8gr3RoH18&ab_channel=-lincs))

**Présentateurs :** Bryan Tarpley et Lauren Liebe

**Président :** Pieter Botha

**Résumé :** Corpora est un "Dataset Studio for the Digital Humanities" en cours de développement au Center of Digital Humanities Research de la Texas A&M University. Il permet aux chercheurs de créer, rechercher, explorer et transformer de grands ensembles de données personnalisées. Cette démo présente aux membres de l'auditoire les différentes possibilités de corpus en parcourant le processus d'importation des données de l'Advanced Research Consortium, en présentant comment ces données sont enrichies via l'attribution d'URI faisant autorité pour les entités, et en montrant comment les données seront transformées en RDF (linked open data) adaptées à l'ingestion par le triplestore LINCS.

**Bios :** Bryan Tarpley est le développeur principal du Center of Digital Humanities Research de la Texas A&M University, ainsi que le directeur adjoint de la technologie pour l'Advanced Research Consortium. Lauren Liebe est chef de projet pour l'Advanced Research Consortium de la Texas A&M University et doctorante spécialisée dans les humanités numériques et le théâtre moderne.

#### Technique des panneaux LINCS, 3 mai

**Présentateurs :** Huma Zafar, Pieter Botha et Natalie Hervieux

**Président :** Deb Stacey

##### Conversion de données liées à l'aide de microservices, Huma Zafar ([**Youtube**](https://www.youtube.com/watch?v=tTUCt0atSPY&ab_channel=-lincs))

**Résumé :** La conversion de textes de sciences humaines en données ouvertes comprend trois étapes principales : la reconnaissance d'entités, la liaison/rapprochement d'entités (à la fois automatique et manuelle) et la génération de données, liées mais ce travail efficace à grande échelle peut être un défi. Dans cette conférence, nous présentons des concepts liés à l'utilisation des microservices et verrons comment ils peuvent aider à relever certains défis d'échelle. Nous discutons ensuite du travail que nous avons effectué au cours de l'année pour convertir le backend NERVE (Named Entity Recognition and Vetting Environment) en une architecture de microservices, et des avantages que nous avons constatés dans la suite de cette conception.

##### LINCS DevOps, Pieter Botha ([**Youtube**](https://www.youtube.com/watch?v=3AVPWa9BcwU&ab_channel=-lincs))

**Résumé :** DevOps est un sujet brûlant dans l'industrie informatique en ce moment et ce n'était pas quelque chose que LINCS pouvait ignorer lorsque nous avons décidé de planifier et de mettre en œuvre notre environnement de développement et notre infrastructure opérationnelle . Dans cette présentation, nous donnerons un aperçu rapide de DevOps, puis discuterons de la manière dont il a intégré l'architecture des systèmes LINCS d'un point de vue opérationnel. Nous montrerons ce que LINCS a mis en œuvre pour son environnement de développement et commentons cet environnement est connecté à l'infrastructure opérationnelle via les pipelines CI/CD. Nous conclurons la présentation avec les avantages que notre solution actuelle offre par rapport aux alternatives classiques et citerons les raisons de certains choix que nous avons faits.

##### Améliorer l'efficacité de la liaison d'entités, Natalie Hervieux ([**Youtube**](https://www.youtube.com/watch?v=WF4YhGFvMig&ab_channel=-lincs))

**Résumé :** La comparaison de deux ensembles de données est une tâche courante lors de la transition vers des données ouvertes liées. Cela peut se produire pendant la phase de nettoyage des données lorsque vous devez supprimer ou fusionner des entités en double. Il est également important lors de la phase de réconciliation lors de la liaison des entités de votre ensemble de données aux fichiers d'autorité ou à d'autres sources de données ouvertes liées. Cependant, cela peut être un processus lent lorsque vous travaillez avec des fichiers volumineux contenant des milliers d'entités. Dans cette présentation, nous parlerons de notre approche pour lier efficacement de grands ensembles de données d'entités bibliographiques à des fichiers d'autorité. Nous profitons du package de couplage d'enregistrements open source SPLINK et du moteur de traitement de données Apache Spark pour paralléliser le traitement.

**Bios :** Huma Zafar est un développeur du Humanities+Data Lab de l'Université d'Ottawa et un ancien ingénieur senior du monde de la technologie des bibliothèques. Pieter Botha est le responsable technique du LINCS. Natalie Hervieux est une développeuse du projet LINCS, responsable de la conversion des données intégrées et du texte en langage naturel en données ouvertes liées.

#### Ontologies au LINCS, 4 mai

**Présentatrice :** Erin Canning

**Président :** Susan Brown

**Résumé :** Les ontologies sont des outils clés pour structurer les données liées : elles sont compliquées des classes pour le typage des entités et les relations entre ces entités, permettent ainsi une vision du monde à partir de nos données. Cette conférence couvrira une introduction à ce que sont les ontologies dans le contexte des données liées, la théorie critique autour des ontologies en tant qu'infrastructures d'information, et détaillera le travail d'ontologie au LINCS. L'accent sera mis sur le travail d'ontologie LINCS, couvrant le travail théorique et politique qui a eu lieu, ainsi que les décisions d'ontologie qui ont été prises et mises en œuvre. Une décision clé en matière d'ontologie a été d'adopter CIDOC-CRM comme ontologie de base pour le projet ; Par conséquent, cette présentation comprendra également une introduction à l'ontologie CIDOC-CRM et à la modélisation des données centrées sur les événements. La conférence se terminera par un résumé du travail d'ontologie effectué au LINCS à ce jour, ainsi que les prochaines étapes du projet.

**Biographie :** Erin Canning est l'analyste des systèmes d'ontologie pour LINCS, responsable de la définition des ontologies à utiliser par le projet ainsi que de la supervision de la cartographie et de l'intégration des ensembles de données LINCS.

#### Démo NERVE, 4 mai ([**Youtube**](https://www.youtube.com/watch?v=Empg691TkbE&ab_channel=-lincs))

**Présentateurs :** Huma Zafar, Luciano Frizzera, Mihaela Ilovan

**Président :** Deb Stacey

**Résumé :** L'environnement de reconnaissance et de vérification des entités nommées, ou NERVE, est un outil frontal que le CWRC a commencé à développer en 2015 et qui est actuellement mis en œuvre en tant qu'extension de CWRC-Writer , l'éditeur en ligne XML et RDF. Nous ferons une démonstration de l'outil dans son incarnation actuelle, partagerons les structures filaires de sa prochaine version et discuterons de la manière dont nous prévoyons de l'intégrer au reste de la boîte à outils LINCS.

**Bios :** Huma Zafar est un développeur du Humanities+Data Lab de l'Université d'Ottawa et un ancien ingénieur senior du monde de la technologie des bibliothèques. Mihaela Ilovan est la directrice adjointe du CWRC. Elle gère le développement et la maintenance de CWRC-Writer et est impliquée dans NERVE depuis 2015. Luciano Frizzera est développeur Javascript au CWRC et candidat au doctorat en études des médias à l'Université Concordia.

#### Questions-réponses des chercheurs du LINCS, 5 mai

**Présentatrices :** Diane Jakacki, Alison Hedley, Michelle Meagher et Jana Smith Elford

**Président :** Kim Martin

**Résumé :** Au cours de l'année, les membres de l'équipe principale du LINCS ont rencontré des chercheurs en sciences humaines au sujet de leurs données et des étapes nécessaires pour les créer en tant que LOD. Joignez-vous à nous pour une discussion avec Diane Jakacki ([REED London](https://cwrc.ca/reed)), Alison Hedley ([Yellow 90s Personography](https://personography.1890s.ca/)) et Jana Smith Elford et Michelle Meagher (Ad Archive) pour savoir à quoi ressemblait le processus de travail avec LINCS et comment ils ont permis de répondre à leurs questions de recherche une fois qu'il fera partie du Web sémantique.

#### Séance d'information sur les microsubventions communautaires, 6 mai

**Présentatrice :** Susan Brown

**Président :** Sarah Roger

**Résumé :** Le LINCS convertit et interconnecte les ensembles de données de recherche canadiens sur les identités culturelles et le patrimoine culturel, les rendant accessibles en tant que données ouvertes au profit des universitaires et du public. Grâce à son programme de subventions communautaires, le LINCS fournit un soutien modeste sous plusieurs formes aux chercheurs qui disposent d'ensembles de données qu'ils aimeraient convertir mais qui ont besoin d'aide pour les traiter. Vous n'avez pas besoin d'avoir déjà été impliqué dans LINCS pour postuler. Venez en savoir plus sur les micro-subventions !

#### Heure sociale LINCS, 6 mai

Rejoignez LINCS sur rassemble.ville pour une heure sociale LINCS. Rencontrez de nouvelles personnes et rattrapez ceux que vous connaissez déjà. Sortez, discutez et jouez à des jeux - tout le plaisir d'une heure de café de conférence mais sans le café moche !
