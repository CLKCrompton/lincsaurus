---
sidebar_position: 2 # REPLACE NUMBER
title: « Exportation et transformation initiale » # REMPLACER LE TITRE
description: "Template for workflow conversion pages" # REPLACE with DESCRIPTION of page contents (this will appear in cards and social links and metadata about the page, keep it short)
draft: true # REMOVE THIS LINE
---

<!-- @format -->

## Introduction

Nisi ipsum ex pariatur dolor minim et sunt ad. Consequat adipisicing nostrud pariatur proident. Ut ipsum amet labore sunt. Voluptate ex aute Lorem quis consequat adipisicing deserunt officia. Reprehenderit excepteur sint ipsum occaecat adipisicing adipisicing cillum. Quis sunt labore sint laborum aliqua minim est voluptate culpa sint irure officia ut. Consequat ex quis veniam deserunt commodo ipsum.

<!-- REMPLACER LE TEXTE CI-DESSUS-->

## Configuration de vos données

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs>
<TabItem value="structured" label="Données structurées" par défaut>
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute inciddunt sit.</li>
</ol>
</TabItem>
<TabItem value="semistructured" label="Données semi-structurées">
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute inciddunt sit.</li>
</ol>
</TabItem>
<TabItem value="tei" label="Données TEI">
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute inciddunt sit.</li>
</ol>
</TabItem>
<TabItem value="natural" label="Données en langage naturel">
<ol>
<li>Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.</li>
<li>Non sint ipsum dolore laborum ex cillum ullamco.</li>
<li>Aute labore reprehenderit est aute inciddunt sit.</li>
</ol>
</TabItem>
</Tabs>

## Officia esse enim

Nisi ipsum ex pariatur dolor minim et sunt ad. Consequat adipisicing nostrud pariatur proident. Ut ipsum amet labore sunt. Voluptate ex aute Lorem quis consequat adipisicing deserunt officia. Reprehenderit excepteur sint ipsum occaecat adipisicing adipisicing cillum. Quis sunt labore sint laborum aliqua minim est voluptate culpa sint irure officia ut. Consequat ex quis veniam deserunt commodo ipsum.

<!-- REMPLACER LE TEXTE CI-DESSUS-->

## Officia esse enim

Nisi ipsum ex pariatur dolor minim et sunt ad. Consequat adipisicing nostrud pariatur proident. Ut ipsum amet labore sunt. Voluptate ex aute Lorem quis consequat adipisicing deserunt officia. Reprehenderit excepteur sint ipsum occaecat adipisicing adipisicing cillum. Quis sunt labore sint laborum aliqua minim est voluptate culpa sint irure officia ut. Consequat ex quis veniam deserunt commodo ipsum.

<!-- REMPLACER LE TEXTE CI-DESSUS-->

## Résultats attendus

Proident ea amet sit culpa eu adipisicing commodo eiusmod irure commodo id commodo et :

1. Excepteur commodo id amet esse eu dolor laboris non non sit officia pariatur.
2. Non sint ipsum dolore laborum ex cillum ullamco.
3. Aute labore reprehenderit est aute inciddunt sit.
4. Nostrud consequat ex voluptate aliquip enim consequat mollit magna aliquip aliqua ullamco ut.
<!-- REMPLACER LE TEXTE CI-DESSUS-->
