---
sidebar_position: 2 # ADD NUMBER
title: "Modèle d'introduction" # Remplacer par PAGE TITLE (Ce sera le premier titre et aussi le nom sur votre onglet)
# sidebar_class_name: "landing-page" # Remove beginning comment
description: "Template for default landing pages of categories" # REPLACE DESCRIPTION (this will appear in cards and social links and metadata about the page, keep it short)
draft: true # REMOVE THIS LINE
---

<!-- @format -->

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

# TITRE

<!-- REMPLACER LE TITRE CI-DESSUS -->

<!-- Le chemin des images sera dans `static/`, remplacez Placeholder.png par le chemin d'image correct-->
<div className="banner">
<img src="/img/Placeholder.png" alt="AJOUTER UN TEXTE ALT"/>
</div>

<!-- REMPLACER LE TEXTE CI-DESSOUS-->

Ipsum sint duis irure proident est nisi amet pariatur. Inciddunt eu laboris in mollit eu irure velit. Excepteur amet proident anim deserunt fugiat. Laboris deserunt dolore magna occaecat anim aliqua. Laboris tempor reprehenderit ea officia proident ea duis sunt eu sit anim incididunt proident minim. Mollit ut Lorem minim laboris. Excepteur ex occaecat tempor minim incididunt consectetur duis esse eu deserunt sint voluptate est proident.

Ut eu exercitation esse Lorem dolor proident esse. Enim laborum ut sint esse anim minim Lorem eiusmod incididunt duis. Deserunt anim velit qui qui mollit fugiat ea voluptate do pariatur Lorem. Adipisicing ex amet laboris enim irure ex consequat reprehenderit ex ullamco. Non fugiat cupidatat culpa sint eiusmod cupidatat nostrud commodo reprehenderit esse magna. Tempor irure esse tempor nisi culpa ad dolore aute amet cillum in. Laboris occaecat irure amet aliqua aliquip anim id sunt magna laboris tempor minim.

## Pages connexes

<DocCardList items={useCurrentSidebarCategory().items.slice(1)}/>

## Ea quis magna

Nostrud voluptate consectetur ad ipsum ad cillum ea aute deserunt enim esse dolore consequat. Velit ipsum eiusmod ex sint non minim nisi veniam amet velit excepteur. Aliqua excepteur aute excepteur enim. Ut sit laboris eu tempor deserunt tempor aute ea. Amet ut consequat culpa consectetur elit laborum pariatur ullamco deserunt aliquip dolor ipsum minim.

### mollit excepteur voluptate consequat adipisicing qui ad esse amet

<!-- REMPLACER LE TEXTE CI-DESSOUS-->

Fugiat culpa nostrud ex consequat. Dolor tempor velit laboris consectetur occaecat qui laboris elit excepteur Lorem sint aute. Dolor labore duis sit tempor ad velit minim deserunt do duis laborum anim nisi tempor. Esse ipsum cupidatat mollit non officia Lorem ex voluptate consequat labore ullamco nulla. In mollit reprehenderit tempor sunt labore tempor Lorem adipisicing ad consectetur. Dolor dolor consectetur culpa tempor exercitation elit ullamco veniam irure.

Exercitation incididunt et aute dolore esse ea. Qui cupidatat nostrud ullamco amet aliquip commodo aliquip nulla deserunt consequat ullamco magna minim. Exercitation officia sint consectetur id non qui occaecat labore mollit labore. Deserunt consequat do consequat aliqua dolor nisi amet commodo quis enim.

Laboris dolore aliquip eu incididunt non laborum sunt Lorem ipsum mollit laboris consectetur. Qui fugiat aute excepteur incididunt tempor magna qui esse Lorem in. Anim nulla minim enim consectetur laboris dolor ea et. Excepteur labore sit incididunt veniam nisi. Dolor ipsum excepteur dolore nostrud elit aliquip ex ipsum id et culpa. Nulla occaecat officia esse ut aliquip excepteur laborum laboris proident ut ut ex sint. Laborum nisi qui nostrud occaecat voluptate exercitation pariatur aliqua reprehenderit.
