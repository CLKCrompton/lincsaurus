---
draft: true # REMOVE THIS LINE
title: Modèle d'outil # Remplacer par PAGE TITLE (ce sera le premier titre et aussi le nom sur votre onglet)
description: "Template for overview of any particular tool" # REPLACE DESCRIPTION (this will appear in cards and social links and metadata about the page, keep it short)
---

<!-- @format -->

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

# {NOM DE L'OUTIL}

<!-- Pour plus de détails sur le fonctionnement du bouton de fonction, consultez ce lien : https://gitlab.com/calincs/admin/lincsaurus/-/snippets/2408164 -->
<div className="fonction-bouton-ligne">
<functionbutton link="/docs/explore-data" buttonName="Example Button" icon={faUpRightFromSquare}></functionbutton>
<functionbutton buttonName="Nettoyer"></functionbutton>
</div>

<!-- AJOUTER UNE LIGNE (BRÈVE DESCRIPTION)-->

Cupidatat ad nisi nostrud velit amet nisi.

<!-- Le chemin des images sera dans `static/` -->
<div className="banner">
<img src="/img/Placeholder.png" alt="AJOUTER UN TEXTE ALT"/>
</div>

<!-- REMPLACER `link=""` AVEC `link="/docs/PATH/TO/MARKDOWNFILE" ou link="N'IMPORTE QUELLE URL" -->
<div className="primary-button-row">
<primarybutton link="" buttonName="Vers l'outil" icon={faUpRightFromSquare}></primarybutton>
<primarybutton link="" buttonName="Vers la documentation" icon={faUpRightFromSquare}></primarybutton>
<primarybutton link="" buttonName="To GitLab" icon={faUpRightFromSquare}></primarybutton>
</div>

## {NOM DE L'OUTIL} et LINCS

Reprehenderit occaecat commodo ad officia anim eu culpa proident ipsum nostrud dolor amet duis. Aute sunt proident ullamco officia laborum Lorem ad ad enim. Aute Lorem aute commodo commodo ut cupidatat deserunt nulla et. Reprehenderit eiusmod incididunt fugiat laboris magna id non et anim ad.

Nulla nisi laborum nostrud officia aliqua aute amet officia adipisicing non voluptate velit. Dolor laborum commodo esse enim enim ex aliquip. Officia amet voluptate sint sint ipsum ullamco aute sint non commodo proident minim adipisicing ad. Adipisicing laborum anim dolor exercitation magna exercitation aliquip proident pariatur minim.

## Conditions préalables

Utilisateurs de {TOOL NAME} :

* {Besoin/Pas besoin} de venir avec leur propre ensemble de données
* {Besoin/Pas besoin} de créer un compte utilisateur

- Besoin d'un {TYPE DE CONNAISSANCES/CONTEXTES NÉCESSAIRES}
- {Besoin/Pas besoin} d'une formation technique

{TOOL NAME} prend en charge les entrées et sorties suivantes :

- **Entrée :** <!-- AJOUTER DES ENTRÉES -->
- **Sortie :** <!-- AJOUTER DES SORTIES -->

## Ressources

Pour en savoir plus sur {TOOL NAME}, consultez les ressources suivantes.

- {RESSOURCE 1} <!-- AJOUTER RESSOURCE -->
- {RESSOURCE 2} <!-- AJOUTER RESSOURCE -->
