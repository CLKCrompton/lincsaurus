---
sidebar_position: 1
draft: true 
sidebar_class_name: "landing-page"
---

# Modèles

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

<DocCardList items={useCurrentSidebarCategory().items.slice(1)}/>

