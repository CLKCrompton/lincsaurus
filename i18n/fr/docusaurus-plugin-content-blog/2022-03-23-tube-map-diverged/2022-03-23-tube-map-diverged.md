---
slug: tube-map-diverged
title: Deux routes ont divergé sur une carte de tube et j'ai pris celle qui est moins
authors:
  name: Evan Rees
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Kwan Fung Unsplash](./tube-map-diverged-unsplash-(cc0).jpeg)

La route vers UX pour moi a été longue et sinueuse, et j'aime beaucoup les utilisateurs de LINCS dans [Tube Map in Figma](https://www.figma.com/file/jsEmWNAXztLDxib8PDIPkJ/LINCS-Tube-Map-First-Draft---Summer-2021), je me suis retrouvé à différentes stations en cours de route, évaluant où je dois aller ensuite. Étudiant initial en sciences de la vie à l'Université de Toronto, j'ai fait la transition après la première année vers une spécialisation en anthropologie socioculturelle. Après avoir obtenu mon diplôme et ne sachant pas où aller avec ma carrière, j'ai commencé une maîtrise en affaires publiques et internationales à l'Université d'Ottawa. J'ai vite réalisé que j'étais trop passif par nature pour être diplomate, et qu'il me manquait la passion pour la politique que beaucoup de membres de ma cohorte partageaient. Ce que je voulais vraiment, c'était quelque chose lié à ma formation en anthropologie.

Et donc, entrez à la Faculté d'information de l'UofT...<!--truncate--> Je voulais étudier à nouveau à l'Université de Toronto et j'ai assisté à une séance d'information pour leur concentration en culture et technologie. Comme je pouvais assister à deux sessions le même jour, j'ai également assisté à celle de l'UXD. Au cours des cinq premières minutes, le professeur a mentionné que de nombreux étudiants en anthropologie poursuivent l'UXD en raison du besoin de compétences ethnographiques et des entretiens avec les utilisateurs pour évaluer les besoins en interfaces. J'ai été vendu.

LINCS a été mon deuxième stade en tant que spécialiste UX. Mon intérêt pour l'UX est multiforme, mais je veux en grande partie pouvoir contribuer à quelque chose de significatif en appliquant ma formation en sciences humaines et en utilisant l'empathie pour créer des produits accessibles, proposés et de grande portée. Travailler au LINCS m'a permis de faire tout cela. Créer des interfaces pour travailler avec le %%Le Web sémantique|semantic-web%% signifie essayer de comprendre comment les chercheurs pensent et ressentent, et comment ils veulent que leur travail soit utilisé. Issu d'une formation en sciences humaines, LINCS s'est senti comme chez moi, car il m'a permis de me mettre à la place de l'utilisateur plus que je ne l'avais imaginé.

Mon temps au LINCS s'est concentré sur ResearchSpace. J'ai examiné ResearchSpace à travers une lentille analytique et UX, en présentant les points de douleur possibles que les utilisateurs rencontreront lorsqu'ils naviguent dans l'outil et créent les connexions entre les ensembles de données. Pour déterminer ces points faibles, j'ai pris en compte la cohésion de la navigation, la clarté du langage utilisé, le jeu de couleurs en ce qui concerne les normes AODA et si les sections qui effectuent des tâches similaires pourraient être agrégées.
