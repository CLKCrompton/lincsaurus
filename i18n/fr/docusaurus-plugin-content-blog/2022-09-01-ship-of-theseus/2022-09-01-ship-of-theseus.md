---
slug: ship-of-theseus
title: Le navire de Thésée - Représenter la nuance dans les données des sciences humaines
authors:
  name: Kate LeBere
  title: LINCS Vocabularies and Documentation Co-op
tags: [digital humanities, research, metadata]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./ship-of-theseus-unsplash-(cc0).jpg)

Si chaque partie d'un navire a été remplacée au fil du temps, quand, si jamais, devient-il un nouveau navire ?

Héraclite, Platon et d'autres ne savaient pas que les problèmes posés par le paradoxe du "Vaisseau de Thésée" continueraient à vexer les humanistes numériques au XXIe siècle...<!--truncate-->

Au cours de ma coop MLIS au LINCS, j'ai contribué à un <%%vocabulaire|vocabulary%% qui a aligné les types de lieux de [REED London Online (REED)](https://cwrc.ca/reed), [Map of Early Modern London (MoEML )](https://mapoflondon.uvic.ca/), et [L'Arche numérique (Arche)](https://drc.usask.ca/projects/ark/public/about.php). Pour mener à bien cette tâche, nous devions d'abord déterminer comment chaque chercheur représentait la place dans ses données.

Ayant travaillé pour le MoEML pendant et après mon diplôme de premier cycle, je savais que ce processus ne serait pas simple. %%Les chercheurs en humanités numériques (DH)|digital-humanities%% utilisent des outils et des technologies numériques pour étudier la société et la culture humaines d'un point de vue critique. Les questions de recherche sont nuancées, ce qui crée une tension compliquée, quoique convaincante, entre les ambiguïtés des sciences humaines et les systèmes et règles explicites rendus nécessaires par la technologie. Cette tension entraîne un dilemme : les humanistes numériques doivent naviguer dans les biais et les valeurs intégrés dans les logiciels, les algorithmes et les normes d'encodage pour s'assurer que les nuances et les détails de leurs recherches ne sont pas injustement simplifiés.

Mon exemple préféré de cette tension est le débat entre fonction et structure : dans un jeu de données, un seul emplacement doit-il être représenté comme plusieurs entités si sa fonction ou sa structure change ? Au début de l'époque moderne, le site de la Chartreuse assuma de nombreuses fonctions : il fut à diverses époques un cimetière de pestiférés, une chartreuse, une résidence royale, un hôpital, une école et une maison de retraite. Charterhouse, le monastère, est-il le même lieu que Charterhouse, la maison des retraités ? Qu'en est-il de la structure : le bois du théâtre de 1576 a été utilisé pour construire le Globe de 1599. En 1613, ce Globe a brûlé et un nouveau Globe a été construit. D'autres lieux, comme Somerset House, ont subi d'importantes rénovations au début de la période moderne, sans oublier que des milliers de bâtiments, dont la cathédrale Saint-Paul, ont été partiellement ou entièrement détruits lors du grand incendie de Londres en 1666.

Affirmer qu'un emplacement est le même qu'un autre n'est pas simple. Cela dépend souvent des hypothèses sous-jacentes d'un projet, des questions de recherche et des objectifs scientifiques. Cette tâche devient encore plus compliquée lorsque l'on rassemble plusieurs projets, chacun avec ses propres objectifs, mais tous se référant aux mêmes lieux.

Dans le vocabulaire que nous avons développé avec REED, MoEML et Ark, nous avons décidé de diviser les lieux en quatre grandes catégories : unité administrative, lieu structurel, lieu fonctionnel et élément topographique. Ce faisant, nous avons pu distinguer les lieux que nous pensions être principalement définis par leur structure - par exemple, les ponts, les portes et les routes - de ceux principalement définis par leur fonction - par exemple, les prisons, les églises et les théâtres. Vous trouverez ci-dessous une visualisation du vocabulaire que nous avons développé.

![Visualisation SKOS.](./ship-of-theseus-skos-(c-LINCS).jpg)

_Vocabulaire « Place types in early modern London » développé en collaboration avec [REED London Online (REED)](https://cwrc.ca/reed), [Map of Early Modern London (MoEML)](https://mapoflondon .uvic.ca/), et [L'Arche numérique (Arche)](https://drc.usask.ca/projects/ark/public/about.php). Déclaré dans SKOS et visualisé dans [SKOS Play](https://skos-play.sparna.fr/play/)._

Néanmoins, certains cas extrêmes défient une simple catégorisation. Par exemple, Newgate est à la fois une porte (Structural Place) et une prison (Functional Place). London Bridge est un pont (Structural Place), mais pourrait aussi être considéré comme un lieu de commerce (Functional Place) car il abritait des centaines de boutiques au début de la période moderne.

L'absence d'une solution unique n'est pas nécessairement une mauvaise chose. Contempler des cas extrêmes avec les chercheurs a conduit à des discussions productives sur la manière de représenter la nuance tout en respectant la structure imposée par le vocabulaire. Plus important encore, la création du vocabulaire a aidé les chercheurs à repenser la façon dont ils catégorisaient et étiquetaient les lieux au sein de leurs ensembles de données.

Bien que notre vocabulaire n'ait pas encore été appliqué aux ensembles de données, il fera partie intégrante de la facilitation de l'interopérabilité et de la spécificité de la catégorisation des données. Plus important encore, son application conduira à plus d'analyses - et si cette analyse ressemble à notre travail précédent, elle sera compliquée, nuancée et passionnante.
