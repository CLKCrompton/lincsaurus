---
slug: user-problem-solving
title: Résolution de problèmes centrée sur l'utilisateur
authors:
  name: Sana Javeed
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./user-problem-solving-unsplash-(cc0).jpg)

Avant de devenir étudiant en UX Design à l'Université de Toronto, j'avais l'habitude de résoudre un problème simplement en identifiant quel était le problème, puis en proposant une solution de fortune. Cependant, au fil du temps, j'ai réalisé qu'en utilisant une telle approche - en optant pour la solution la plus simple - j'ignorais souvent activement le vrai problème et parfois même le laissais s'aggraver...<!--truncate-->

Si vous utilisiez un analgésique chaque fois que vous aviez mal à la tête, vous pourriez vous débarrasser momentanément de la douleur, mais les maux de tête continueraient à surgir. Est-ce la bonne approche pour les maux de tête récurrents ? Et bien non! Si vous voulez vraiment vous débarrasser de vos maux de tête, vous devez trouver ce qui les cause. Sont-ils dus à des problèmes de vision ? Grincement des dents ? Regarder l'écran pendant de longues heures ? Afin de trouver la cause première, vous devez analyser le problème en considérant différents angles. Ce n'est qu'une fois que vous avez fait cela que vous pouvez commencer à chercher la bonne solution. C'est ainsi que l'on résout efficacement les problèmes.

N'ayant aucune formation en sciences humaines, mes expériences au LINCS m'ont laissé en admiration devant le rôle que jouent les chercheurs dans le développement des technologies numériques. Les chercheurs ont collaboré sur des technologies numériques telles que Voyant, ResearchSpace, HuViz et bien d'autres pour enrichir les données de recherche. Ces outils ont été conçus, souvent par des chercheurs en sciences humaines, pour les aider, ainsi que d'autres, à collecter, connecter et interpréter des informations.

Alors, quel est exactement le problème à résoudre ici ? L'objectif de LINCS est de rendre l'information encore plus accessible. LINCS crée des outils open source et facilement accessibles pour les chercheurs, offrant aux chercheurs un point de départ pour leurs parcours de recherche d'informations spécifiques à un objectif. Le problème est de savoir comment concevoir et mettre en œuvre une plate-forme qui peut non seulement répondre à un large éventail d'objectifs spécifiques à l'utilisateur, mais également améliorer l'expérience du chercheur ? Avant de me lancer dans le brainstorming de solutions, j'avais besoin de comprendre le domaine et la place de l'UX.

![Illustration du Design Thinking](./designthinking_illustration-(c-Nielsen-Norman-Group).png)

_Gibbons, S. (2016, July 31). Design Thinking 101. Retrieved March 27, 2020, from [https://www.nngroup.com/articles/design-thinking](https://www.nngroup.com/articles/design-thinking)_

Lorsqu'ils entreprennent une conception UX, le modèle de réflexion sur le design de [Nielsen Norman](https://www.nngroup.com/) encourage les concepteurs à _empathie_ et _définir_ afin de voir la situation du point de vue des utilisateurs et de comprendre le problème à résoudre. . Cette compréhension peut être atteinte en déterminant le _quoi_, le _comment_ et le _qui_. Comment est-ce fait? Imaginez-vous essayer d'acheter un jouet pour un enfant. Si vous êtes comme moi, vous examinerez ce jouet pour déterminer de quoi il s'agit précisément et à qui il s'adresse. Pour savoir comment fonctionne le jouet et à quoi ressemble l'expérience de jouer avec, vous devez observer l'utilisateur (un enfant) pour voir _comment_ il interagit avec lui : quelles étapes suivent-ils pour effectuer une tâche ? Comment surmontent-ils la frustration lorsque le jouet ne fait pas ce qu'ils veulent ? À partir de là, vous apprendrez _ce_ qu'est* le jouet, *à qui* le jouet est destiné et \_comment* il fonctionne pour le public visé.

J'ai utilisé le modèle Nielsen Norman pour analyser l'interface d'accès LINCS. Tout d'abord, j'ai appris qu'ils étaient des chercheurs en sciences humaines, des assistants de recherche et des étudiants. L'étape suivante consistait à déterminer le _quoi_ en posant les bonnes questions :

1. Quels sont les objectifs, les besoins et les motivations des utilisateurs à utiliser un outil spécifique ?
2. Quelles connaissances les utilisateurs possèdent-ils actuellement et quelles connaissances doivent-ils acquérir pour atteindre leur objectif ?

Pour répondre aux questions ci-dessus, j'ai d'abord catégorisé globalement trois types d'utilisateurs différents : l'explorateur (débutant avec peu ou pas de connaissances sur les %%Linked Open Data|linked-open-data%%), le Contributeur (conservateur des Linked Open Data), et enfin le Codeur (avec une connaissance de base des Linked Open Data et une expérience de codage avec JavaScript ou Python).

Ce ne sont pas des définitions exactes, car les utilisateurs de chaque groupe ont en fait un large éventail de compétences et d'objectifs. Celles-ci vont de la simple volonté d'explorer l'ensemble de données à la volonté de manipuler ou même de créer des données bibliographiques.

Enfin, la plus grande étape dans la résolution de problèmes consiste à déterminer le _comment_. Cette étape étant complexe et chronophage, j'y travaille encore. Jusqu'à présent, j'ai découvert que la meilleure approche consiste à écouter ce que les utilisateurs ont à dire et à étudier leur comportement et leur environnement pour déterminer leur objectif. Faire preuve d'empathie envers les utilisateurs lorsque je les regarde utiliser les outils m'aide à comprendre leurs difficultés, à déterminer quels étaient leurs problèmes et ce qui les a amenés au point de douleur. L'écoute de l'utilisateur est essentielle pour trouver la source du problème. Ce n'est qu'une fois que vous avez identifié la pièce problématique du puzzle que vous pouvez mettre votre chapeau de réflexion ! Idée, prototype, test et répétition.

Alors maintenant, vous voyez comment l'approche centrée sur l'utilisateur aide à définir un problème grâce à l'empathie. D'après mon expérience, la compassion est le meilleur moyen d'identifier la cause profonde, car cela signifie comprendre et donc traiter le problème (les problèmes de vision, le grincement des dents, l'utilisation d'écrans) plutôt que son symptôme (le mal de tête). Une approche empathique et centrée sur l'utilisateur peut prendre plus de temps qu'une solution de fortune, mais elle garantit également que vos solutions seront mieux adaptées aux utilisateurs et à leurs problèmes sous-jacents. Cette approche compatissante peut être utilisée dans tous les domaines. Que vous soyez médecin, ingénieur, enseignant, représentant du service client, responsable ou caissier d'épicerie, quel que soit votre rôle, vous pouvez résoudre vos problèmes grâce à une approche centrée sur l'utilisateur.

---

### Ouvrages cités

Gibbons, Sarah. "Pensée de conception 101." _Groupe Nielsen Norman_. 31 juillet 2016. [https://www.nngroup.com/articles/design-thinking/](https://www.nngroup.com/articles/design-thinking/).
