---
slug: oral-history-LOD
title: Les connexions sont importantes ! Données ouvertes liées et histoire orale
authors:
  name: Gracy Go
  title: LINCS Undergraduate Research Assistant
tags: [digital humanities, linked open data, research]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./oral-history-LOD-unsplash-(cc0).jpg)

L'histoire a toujours été quelque chose qui m'a passionné, et en tant qu'étudiant de premier cycle approchant l'obtention de son diplôme, je suis devenu plus désireux de trouver des moyens de préserver les sources primaires. D'après mon expérience, avoir accès à des sources primaires rend le processus de recherche beaucoup plus facile, et ces sources n'existeraient pas s'il n'y avait pas de mesures appropriées pour les préserver.

Si vous avez déjà suivi un cours d'histoire, vous connaissez probablement la distinction entre les sources primaires et secondaires, et comment les sources primaires font partie intégrante de la fiabilité de tout article ou devoir d'histoire. Parmi les sources primaires de l'histoire, un type qui passe largement inaperçu et peut en fait être difficile à définir est l'histoire orale...<!--truncate-->

La meilleure description que j'ai pu trouver pour l'histoire orale provient du site Web de l'[Oral History Association](https://www.oralhistory.org/about/do-oral-history/) : « ... un champ de étude et une méthode de collecte, de préservation et d'interprétation des voix et des souvenirs des personnes, des communautés et des participants à des événements passés.

Dans de nombreux cas, les histoires orales offrent une perspective d'« histoire d'en bas ». "L'histoire d'en bas" fait référence à l'histoire racontée par votre personne ordinaire qui a une expérience directe des événements. C'est mon type d'histoire préféré car ils touchent souvent à des thèmes sociaux et ont le potentiel d'éduquer les autres sur les perspectives des communautés marginalisées. Je vais aborder un exemple de cela plus tard dans ce post.

J'ai travaillé avec toutes sortes de documents d'archives, allant de sources audiovisuelles telles que des entretiens oraux à des numérisations de documents manuscrits de la fin du XIXe au XXe siècle. Ce que je n'ai jamais abordé au cours de mes quatre années d'études de premier cycle, c'est le sujet de %%Données ouvertes liées (LOD)|linked-open-data%%. Ce que j'ai réalisé ces derniers mois, et qui m'étonne de plus en plus, c'est la façon dont ces deux choses peuvent se profiter l'une l'autre. Au cours de ma première semaine de travail avec LINCS, j'ai appris que LOD existe à peu près partout sur Internet. Comme beaucoup d'étudiants universitaires, je descends souvent dans divers terriers de lapin de Wikipedia. Ce n'est que très récemment que j'ai découvert que dans les coulisses, il y a une pléthore de métadonnées qui sous-tendent ces articles. Des sites Web tels que %%Wikimedia Foundation|wikimedia-foundation%% et %%Wikidata|wikidata%% fournit des informations pour créer des métadonnées pouvant être liées dans des collections ou des référentiels d'archives.

Une simple recherche sur Google vous fournira un exemple de ce que LOD peut faire. Par exemple:

![Sylvia Plath](./oral-history-LOD-plath-(c-LINCS).jpg)

Cette barre latérale ou Google Knowledge Panel qui apparaît lorsque vous recherchez une personne, un groupe, un film et bien d'autres choses est LOD ! J'avais l'habitude de penser que c'était juste un moyen pratique de trouver des informations rapides sur tout ce que je recherchais, ce que je tenais certainement pour acquis avant d'apprendre LOD et comment cela pourrait aider à contextualiser les histoires orales. Je n'ai jamais considéré le travail qui se passe dans les coulisses pour établir des liens vers des termes connexes jusqu'à ce que je commence à travailler avec LOD moi-même, où j'ai constaté à quel point c'était fastidieux.

Ce que je trouve fascinant, c'est la façon dont LOD peut fournir autant d'informations - il y a une quantité apparemment illimitée d'informations sur le Web. L'accessibilité à une richesse de connaissances et la façon dont ces liens fonctionnent les uns avec les autres sont l'une des choses les plus importantes avec le type de travail que je veux faire en tant qu'aspirant conservateur ou archiviste.

Alors, comment LOD est-il lié aux histoires orales et aux autres sources primaires ?

Eh bien, cela permet de lier des concepts similaires les uns aux autres, tels que des lieux, des dates et des personnes. Comme vous pouvez le voir dans l'exemple ci-dessus, Sylvia Plath est liée à des informations pertinentes telles que où et quand elle est née, quand elle est décédée et les œuvres pour lesquelles elle est connue.

Cette même chose peut être appliquée aux histoires orales. À titre d'exemple, je vais vous renvoyer à un vaste dépôt de documents d'archives qui englobe toute l'Europe, l'[Infrastructure européenne de recherche sur l'Holocauste](https://www.ehri-project.eu/) (EHRI). EHRI recueille des documents historiques et archivistiques liés à l'Holocauste. Leur objectif est de connecter le matériel de l'Holocauste qui existe dans les pays européens pour produire un grand référentiel pour contenir ce matériel. Comme vous pouvez l'imaginer, c'est beaucoup de matériel, mais chaque élément de la collection EHRI est tout aussi important que les autres et devrait être inclus pour fournir aux chercheurs la plus grande quantité d'informations et le contexte le plus complet. L'EHRI pourrait grandement bénéficier de l'utilisation de LOD, car cela permettrait aux chercheurs d'explorer du matériel à travers l'étendue et la profondeur de l'Holocauste.

De nos jours, de nombreux dépôts et collections de musées sont numérisés et facilement accessibles en ligne. LOD est particulièrement utile dans ces cas car il permet à l'utilisateur d'obtenir autant d'informations - ou autant d'informations spécifiques - qu'il le souhaite. Mais pour tirer le meilleur parti de LOD, les collections doivent être interopérables, ce qui signifie que les collections de toutes les institutions doivent utiliser les mêmes normes pour permettre la recherche partagée, le catalogage, l'interfaçage et de nombreux autres aspects.

Je mentionne le référentiel EHRI spécifiquement parce que l'Holocauste enveloppe toute l'Europe et qu'il doit y avoir de nombreux survivants et proches qui souhaitent trouver des informations sur ce qu'eux-mêmes ou leurs parents, grands-parents et arrière-grands-parents ont vécu. Sachant que ces informations existent et ne sont pas entièrement accessibles à ceux qui les recherchent, cela signifie qu'il reste un long chemin à parcourir pour aider les gens à retrouver des parties de leur passé. Quand je parle d'accessibilité, je ne veux pas dire seulement pour les chercheurs ou autres universitaires, je veux dire pour les personnes qui ont une expérience directe de ces événements et qui recherchent ces informations. Et ces personnes, ces événements, le matériel de l'EHRI doivent aider à servir les chercheurs ET les opprimés dont les histoires sont des histoires d'en bas.

L'information est là, et des collections comme l'EHRI s'efforcent de la mettre en un seul endroit, mais comment peut-on trouver des choses si elles ne sont pas connectées ? Les données ouvertes liées peuvent être un moyen de résoudre ce problème.
