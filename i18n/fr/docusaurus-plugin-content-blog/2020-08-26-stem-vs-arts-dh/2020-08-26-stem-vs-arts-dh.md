---
slug: stem-vs-arts-dh
title: Équilibrer votre cerveau - STEM vs Arts dans les humanités numériques
authors:
  name: Kathleen McCulloch-Cop
  title: LINCS Co-op
tags: [digital humanities]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash.](./stem-vs-arts-dh-unsplash-(cc0).jpg)

La psychologie pop a un concept que la plupart d'entre nous connaissent : le cerveau gauche linéaire et calculateur contre le cerveau droit intuitif et créatif. La théorie est que vous utilisez soit l'un soit l'autre, soit vous avez le cerveau gauche dominant, soit vous avez le cerveau droit dominant, mais vous ne pouvez pas être les deux. Ce mythe a été démystifié à plusieurs reprises, mais vous pouvez toujours trouver d'innombrables articles sur Facebook et des liens Pinterest avec des quiz pour vous dire quel côté de votre cerveau est celui qui est au volant. Même si vous n'adhérez pas à la théorie du cerveau gauche et droit, nous avons quand même réussi à essayer de séparer la logique de la créativité, les arts des sciences.

Pour moi, cela a toujours semblé un peu décalé...<!--truncate--> cela pourrait être parce que j'ai une formation en arts littéraires mais que j'ai choisi de poursuivre des études en génie logiciel. Je ne voulais pas abandonner la lecture et l'écriture, alors j'ai déclaré une mineure en anglais. Et puis j'ai découvert que ma calculatrice me manquait, alors j'ai déclaré une deuxième mineure en mathématiques. Quand je dis aux gens que j'aime le codage et le calcul autant que j'aime la critique littéraire et l'écriture créative, c'est toujours un choc. "Eh bien, qu'est-ce que tu vas faire avec ça?" ils demandent. J'ai l'intention d'enseigner, mais ce n'est pas le problème ici. C'est que les gens ne semblent pas pouvoir concevoir un endroit où les deux compétences seraient nécessaires.

Le domaine de %%Digital Humanities (DH)|digital-humanities%% est l'un de ces endroits. Il se situe à l'intersection entre la technologie et les sciences humaines. La DH n'est en aucun cas nouvelle. Il a ses racines dans les années 40 et 50, lorsque Roberto Busa, un prêtre italien, a collaboré avec IBM pour créer l'Index Thomisticus, une lemmatisation des œuvres de saint Thomas d'Aquin. Une lemmatisation est le processus de regroupement des formes fléchies d'un mot, en analysant sa signification et sa partie du discours non seulement dans la phrase dans laquelle il apparaît, mais également dans le contexte plus large des phrases environnantes. Ils l'ont fait pour environ 11 millions de mots, publiés en 56 volumes, permettant une puissance de recherche sans précédent dans les œuvres de Thomas d'Aquin.

![Carte perforée IBM](./-stem-vs-arts-dh-card-(cc0).jpg)

_Carte perforée IBM du milieu du 20e siècle de [Traitement électronique des données](https://en.wikipedia.org/wiki/Electronic_data_processing) sur Wikipedia._

Cependant, je suis très nouveau à DH. Travailler avec le projet LINCS et dans le %%THINC Lab|thinc-lab%% est mon premier pinceau avec un projet axé sur DH. Mais plus je travaille sur le projet et j'apprends ce qu'est le LINCS, plus je réalise à quel point nous avons tort d'essayer de séparer les STEM des arts. J'ai toujours eu l'impression qu'ils se complétaient, et maintenant je vois pour la première fois l'acte réel de mettre la technologie au travail avec le contenu des sciences humaines. Et honnêtement, aussi écrasant que cela puisse paraître d'essayer de traduire le langage humain en informations dynamiques lisibles à la fois par l'homme et la machine, cela a du sens. Pourquoi ne mettrions-nous pas ces incroyables machines capables de faire des fusées (que nous avons presque tous chez nous) pour rendre nos vies plus faciles et nos informations accessibles dans tous les domaines ? Pourquoi penserions-nous jamais que la capacité technologique pour la recherche et les matériaux en sciences humaines s'est arrêtée aux revues en ligne et aux PDF téléchargeables d'articles ?

Tout le monde n'essaie pas de séparer les STEM des arts - il y a en fait une poussée pour que les étudiants se concentrent sur les STEAM. Il s'agit de STEM, y compris les arts, et l'accent est mis sur l'enquête et l'apprentissage par problèmes que vous rencontrez souvent lorsque vous étudiez les arts, utilisés conjointement avec les concepts STEM. STEAM me semble être un moyen d'appliquer les fondements de DH à encore plus de domaines d'études scientifiques, d'introduire les concepts encore plus tôt, et c'est tellement excitant de voir se produire.

C'est très honnêtement un sentiment joyeux de travailler avec « les deux côtés » de mon cerveau. Appliquer une structure informatique au contenu créé par l'homme, travailler avec des matériaux qui ont été conçus pour les humains et les transformer en quelque chose que les machines peuvent lire, manipuler et analyser. C'est un témoignage des possibilités qui s'offrent aux chercheurs en sciences humaines, de ce qu'ils peuvent faire, créer et travailler avec l'infrastructure et la plate-forme techniques appropriées. C'est un travail vraiment passionnant, et il nécessite l'utilisation d'une pensée scientifique et d'approches logiques pour un contenu hautement créatif et ambigu.

Faire du travail DH m'a montré que la séparation des arts et des sciences n'est pas réellement réelle, et cela ne peut pas l'être si nous voulons continuer à créer un travail révolutionnaire et changer la façon dont nous utilisons et créons du contenu dans les sciences humaines. Avoir les capacités pour les arts et les sciences fait partie intégrante des deux domaines. La technologie nous permet de changer radicalement la façon dont nous interagissons avec les sciences humaines, et le défi de le faire stimule l'innovation dans la technologie. La création de l'Index Thomisticus aurait probablement été presque impossible sans l'infrastructure technologique - et tout aussi impossible si elle était abordée uniquement d'un point de vue informatique.

C'était la première fois qu'IBM utilisait un ordinateur pour l'analyse linguistique et lexicographique, et le travail publié a été annoncé comme une collection complète des connaissances de Thomas d'Aquin. Si nous voulons créer des projets aussi révolutionnaires que celui de Busa, nous devons utiliser les deux "côtés" de notre cerveau.

En complétant mes compétences et en apprenant à travailler avec la technologie et les sciences humaines côte à côte, on me demandera probablement encore: "Eh bien, qu'est-ce que tu vas faire avec ça?" Mais comme j'en découvre de plus en plus chaque jour, il n'y a pas de mauvaise réponse à cette question. En fait, DH peut ouvrir tellement de possibilités qu'il est difficile de choisir une seule réponse.
