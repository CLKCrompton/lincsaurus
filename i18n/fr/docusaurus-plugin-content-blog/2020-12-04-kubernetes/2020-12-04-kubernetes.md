---
slug: kubernetes
title: Stockage Kubernetes sur Calcul Canada - Un cas pour Longhorn
authors:
  name: Pieter Botha
  title: LINCS Technical Manager
tags: [technical]
hide_table_of_contents: true
---

<!-- @format -->

![Ordinateur portable](./kubernetes-unsplash-(cc0).jpg)

En tant que projet de cyberinfrastructure financé par la [Fondation canadienne pour l'innovation](https://www.innovation.ca/), LINCS construit son infrastructure sur les ressources de Calcul Canada qui exploite un service Cloud exécuté avec OpenStack.

Cinder est le fournisseur de stockage de volume par défaut pour les machines virtuelles provisionnées sur la plateforme OpenStack de Calcul Canada. Il s'agit de la dernière couche d'un système multilocataire complexe qui fait abstraction du stockage Ceph et des hyperviseurs au-dessus des disques durs. C'est cependant ce qui se rapproche le plus du système de stockage en réseau (NAS) de Calcul Canada...<!--truncate-->

Une façon d'exécuter Kubernetes et d'attacher du stockage consiste à créer des machines virtuelles dans OpenStack, à monter des volumes dans les systèmes hôtes, à installer et configurer Kubernetes sur chaque hôte et à exposer les volumes montés à Kubernetes via le fournisseur de stockage de chemin local. Bien que réalisable, cette solution n'est pas idéale. La gestion du stockage devient désormais une tâche de l'hôte et les performances ne seraient pas excellentes si un conteneur était déployé sur un hôte différent de son volume de stockage.

Rancher est une solution logicielle qui fournit Kubernetes en tant que service et peut également être configurée avec les pilotes OpenStack et Cinder pour permettre une expérience très intégrée via l'interface utilisateur Web Rancher. Vous pouvez créer un nouveau cluster à partir de Rancher et gérer toutes les ressources directement à partir de l'interface utilisateur de Rancher. Rancher créera la machine virtuelle sur OpenStack, installera Docker, installera Rancher Kubernetes Engine (RKE) et configurera automatiquement tous les services de base en arrière-plan. Et si vous configurez un fournisseur de stockage Cinder, tous les nouveaux déploiements sur votre cluster qui ont une demande de volume persistant (PVC) obtiendront automatiquement un nouveau volume Cinder monté directement sur le conteneur exécutant la charge de travail. La magie!

Cette solution est beaucoup plus maintenable. Les volumes peuvent être gérés directement depuis Rancher et seul l'espace de stockage demandé aux PVC est réservé sur le backend Cinder. Étant donné que les volumes Cinder sont directement montés sur des conteneurs, il n'y a pas de dépendances ni de couches supplémentaires susceptibles d'introduire des problèmes de latence et d'autres problèmes de performances. C'est parfait? Il s'avère que pas tellement.

### Défis rencontrés

Calcul Canada ne garantit pas le Cinder %%API|application-programming-interface%% pour être disponible lorsque les hôtes sont en cours d'exécution. Pendant les mises à niveau et la maintenance, l'API Cinder pourrait être mise hors ligne alors que les machines virtuelles pourraient encore fonctionner. Cela empêchera Kubernetes de connecter les charges de travail à leur stockage et de créer toutes sortes de ravages. Un autre problème que nous avions était que nous ne pouvions pas sauvegarder les volumes Cinder dans notre environnement Calcul Canada. Calcul Canada ne prend en charge que les sauvegardes basées sur des fichiers, ce qui nécessiterait de se connecter à des conteneurs pour accéder au système de fichiers et sauvegarder les fichiers sur chaque déploiement individuel. Pour couronner le tout, les volumes Cinder montés directement sur Calcul Canada ont été extrêmement lents avec nos charges de travail typiques.

Alors, que pourrions-nous faire ? En tant que client de Calcul Canada, nous n'avons pas accès au backend de stockage Ceph brut, car Calcul Canada ne peut pas provisionner ce stockage dans un environnement multi-locataire. Ainsi, provisionner Ceph avec, par exemple, Rook vers Kubernetes n'était pas une option. Nous devions faire quelque chose en plus des volumes de Cinder.

### Entrez Longhorn

Longhorn est une solution de stockage de conteneurs open source et native du cloud qui a été initialement développée par Rancher, mais qui est maintenant un projet sandbox officiel de la CNCF. Longhorn utilise des volumes montés sur des nœuds et les fournit à Kubernetes.

Attendez une minute! Cela ajoute simplement une autre couche par rapport au fournisseur de chemin local que j'ai mentionné au début de cet article. Comment cela résoudrait-il l'un des problèmes de Cinder, pourriez-vous demander.

- Tous les volumes Cinder sont montés directement dans les hôtes VM. Même pendant la maintenance lorsque l'API Cinder est hors ligne, les volumes Cinder resteront montés et seront accessibles tant que nous pourrons accéder à la machine virtuelle.
- Longhorn fournit une interface utilisateur facile à utiliser et la possibilité de détacher des volumes entiers, de créer des instantanés et de les sauvegarder sur un stockage S3 en un clic (ou automatisé si vous le souhaitez).
- En raison de la qualité de service des différentes couches de la configuration NAS de Compute Canada, cela aide en fait à créer des ensembles de bandes de volumes Cinder pour augmenter la bande passante de stockage, ce que nous avons fait et monté quatre volumes identiques sur chaque machine virtuelle qui servait le stockage au cluster.
- Longhorn implémente une mise en file d'attente et une mise en cache qui réduisent la latence et augmentent la bande passante par rapport aux montages Cinder directs. En moyenne, les performances ont été doublées pour l'infrastructure LINCS avec nos charges de travail typiques.
- L'expansion du volume fonctionne de manière cohérente avec l'approvisionneur Longhorn. Inutile de surprovisionner le stockage pour un volume.
- Longhorn ne mesure que l'utilisation réelle des données et vous permet d'avoir, par exemple, cinq volumes de 1 To sur un disque de 2 To tant que l'utilisation combinée ne dépasse pas 2 To.

Longhorn a vraiment sauvé la mise pour l'architecture LINCS et à tout le moins rendu ma vie d'administrateur système beaucoup, beaucoup plus facile.
