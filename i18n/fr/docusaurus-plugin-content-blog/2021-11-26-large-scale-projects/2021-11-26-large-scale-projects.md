---
slug: large-scale-projects
title: Plonger dans des projets à grande échelle en tant que novice complet
authors:
  name: Dawson MacPhee
  title: LINCS Computer Science Co-op
tags: [technical]
hide_table_of_contents: true
---

<!-- @format -->

![Roozebeh Eslami Unsplash](./large-scale-projects-unsplash-(cc0).jpg)

Au début de ma toute première recherche d'emploi coopératif, je n'avais aucune idée de ce que les employeurs attendraient de moi. C'était une tâche ardue de parcourir les offres d'emploi et de décider pour quoi je pensais être (quelque peu) qualifié. Après quelques entretiens d'embauche, j'ai postulé pour rejoindre le projet LINCS, convaincu que mes compétences correspondaient aux exigences du poste. Finalement, on m'a proposé et accepté un emploi chez LINCS, commençant un voyage qui a mis à l'épreuve mes capacités techniques chaque jour et qui s'est étendu bien au-delà de ce que je pensais faire à l'origine...<!--truncate-->

Mes contributions à LINCS ont été de portée très diverse. Je suis fier de dire que j'ai travaillé sur de nombreux aspects différents du projet et, ce faisant, j'ai fait appel à de nombreuses compétences différentes : j'ai utilisé trois langages de programmation différents et d'innombrables bibliothèques, frameworks, %%Interfaces de programmation d'applications (API)|application-programming-interface%% et logiciels. Grâce à cette expérience, j'ai acquis un aperçu de ce que c'est que d'utiliser et de contribuer à des projets logiciels à grande échelle en cours de développement. Je partage ces idées ici pour d'autres développeurs qui suivent mes traces, à la fois chez LINCS et en tant que contributeurs à d'autres projets à grande échelle.

Lorsque vous rejoignez un projet déjà commencé, commencez toujours par lire la documentation et, si possible, parlez à quelqu'un qui connaît déjà le projet. Vous ne devriez pas non plus étudier la documentation comme si c'était un manuel et essayer de vous souvenir de chaque petite chose. Votre premier objectif en contribuant ou en utilisant un projet est d'obtenir une vue d'ensemble de haut niveau et d'en apprendre davantage sur sa structure. Vous pouvez toujours faire référence à des parties spécifiques de la documentation plus tard, mais pour commencer, tout ce que vous voulez faire est d'apprendre (généralement) où tout se trouve et ce qu'il fait.

À partir de là, la prochaine étape devrait être de faire fonctionner le projet localement et de configurer votre environnement de développement (ou simplement de vous entraîner à utiliser le logiciel s'il est hébergé ailleurs). Souvent, le simple fait d'apprendre à exécuter un projet, à afficher ses journaux et à jouer avec le produit peut grandement contribuer à votre compréhension globale. Selon le projet, il peut être difficile de faire fonctionner les choses localement, car il peut y avoir des dépendances et d'autres exigences à gérer. Cela pourrait être le premier d'un grand nombre de dépannages que vous devrez effectuer, ce qui conduira à de nouvelles plongées dans la documentation et, éventuellement, à une meilleure compréhension du projet.

Les deux premières étapes pourraient être un processus lent. Honnêtement, le travail initial peut être ennuyeux et il est parfois très tentant de commencer à apporter des modifications. Pourtant, il est important de prendre votre temps et de bien comprendre comment la base de code préexistante est liée et interagit avec vos objectifs.

Ce que vous faites ensuite, c'est là où les choses deviennent malléables. Si vous envisagez de contribuer, commencez à apporter des modifications lentement, en effectuant de nombreux tests et en décomposant vos objectifs en petits morceaux. Référencez la documentation si nécessaire et, si possible, posez des questions à une personne connaissant bien le projet au fur et à mesure. Votre défi est d'apporter des changements sans casser ce qui est déjà établi.

Cela semble étrange, mais en tant que développeur, l'un de vos principaux objectifs est d'apporter le moins de modifications possible pour créer le résultat souhaité. Réutilisez ou réutilisez autant que possible ce qui existe déjà. Vous ne voulez pas créer de nouveaux actifs ou fonctions/méthodes qui existent déjà ailleurs, car la pratique de la duplication du travail peut devenir incontrôlable très rapidement si elle n'est pas maîtrisée. Sans oublier que si vous le faites, il vous sera très probablement demandé de supprimer tout travail qui répète ce qui existe ailleurs, ce qui vous fera perdre votre temps et celui de l'équipe.

Ce sont les étapes que j'utilise chaque fois que je dois apprendre ou contribuer à un nouveau projet : lire la documentation, parler et poser des questions aux développeurs précédents, configurer un environnement de développement local, puis apporter progressivement des modifications tout en gardant à l'esprit le l'étendue de votre tâche. Cela peut être un processus lent, mais précipiter les choses sans bien comprendre le projet peut entraîner des maux de tête lors de la refactorisation ou de la réaffectation ultérieure. Prenez le temps nécessaire dès le départ pour acquérir la confiance et les connaissances nécessaires, afin de pouvoir utiliser ou modifier le logiciel le plus efficacement possible. Suivre et pratiquer ces étapes peut grandement augmenter votre efficacité. J'ai trouvé qu'apprendre à aborder de nouveaux projets est une compétence importante, et c'est certainement celle qui s'améliore avec la pratique.
