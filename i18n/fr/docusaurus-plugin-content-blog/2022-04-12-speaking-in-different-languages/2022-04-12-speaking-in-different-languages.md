---
slug: speaking-in-different-languages
title: Parler dans différentes langues - Travailler à travers les groupes et les disciplines
authors:
  name: Nem Brunell
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Windows Unsplash](./speaking-in-different-languages-unsplash-(cc0).jpeg)

La situation est la suivante : on vous a demandé de concevoir un moyen pour un chercheur de se déplacer facilement entre deux outils. Vous vous familiarisez avec les deux outils, découvrez les problèmes connus et lisez ce qui a déjà été essayé. Vous passez quelques jours à décider du meilleur moyen d'aller d'un point A à un point B. Vous passez quelques jours de plus à concevoir à quoi cela ressemblera. Enfin, vous dévoilez votre prototype lors d'une réunion avec l'équipe de développement... pour découvrir que passer d'un outil à l'autre n'est techniquement pas possible. Ou que cela prendrait trop de temps pour construire votre conception. Ou que le déplacement entre les outils ouvre de nouveaux problèmes sur le back-end.

Comment avancez-vous ? ...<!--tronquer-->

L'équipe de l'expérience utilisateur (UX) de LINCS est un ensemble de concepteurs et de développeurs qui travaillent en tandem pour rendre l'utilisation des outils LINCS plus fluide pour les utilisateurs, travaillant sur des sujets variés tels que [les structures du système](/fr/blog/tube-map-diverged), [amélioration de l'accès](/fr/blog/user-problem-solving) ou [tests d'utilisabilité](/fr/blog/design-frolics). L'équipe se réunit deux fois par semaine pour prévenir des situations comme celles ci-dessus. En se connectant souvent, l'équipe est en mesure de se tenir au courant de ce sur quoi chacun travaille et peut ensuite peser sur les points positifs et les défis potentiels des projets de chacun.

Robin Bergart, UX Librarian à l'Université de Guelph, est au LINCS depuis avril 2021. Elle considère les réunions UX essentielles. « Des personnes différentes avec des compétences différentes apporteront des observations différentes. C'est surprenant quand ils voient des choses que vous n'avez pas. Lorsque les développeurs et les concepteurs partagent leurs plans et sont généreux avec leurs compétences, ils sont en mesure de s'assurer que les changements sont possibles (et souhaités !) Avant qu'un membre de l'équipe ne passe des heures sur quelque chose qui ne fonctionnera probablement pas.

[Dawson MacPhee](/fr/blog/large-scale-projects), qui travaille comme développeur avec LINCS depuis mai 2021, aime utiliser ses connaissances techniques pour aider l'équipe UX à faire des choix de conception éclairés. Être impliqué dans la conception le maintient également engagé : "Si je construisais une interface et que je n'étais pas autorisé à contribuer à la conception, il serait plus difficile de trouver de l'intérêt et de la motivation."

Les réunions UX ne sont cependant pas sans défis. L'écart entre ce que les concepteurs peuvent vouloir faire et ce que les développeurs sont capables de faire peut être difficile à combler. Justifier pourquoi ils disent non à une conception proposée peut être difficile pour les développeurs : "C'est un défi de convaincre le groupe que quelque chose ne vaudrait pas notre temps", déclare Dawson. "Il peut être difficile de trouver les bons mots pour des choses en termes non techniques."

Un projet de toute envergure profite lorsque ses participants se réunissent pour partager leurs connaissances et leurs ressources. "Nous sommes en mesure d'avoir des interactions plus réfléchies... parce que nous avons un contexte partagé", explique Alliyya Mo, développeur chez LINCS depuis mai 2020. "Pourtant, le contexte manquant conduit parfois à des malentendus. Mais en parvenant à un consensus partagé avec le langage que nous utilisons, nous pouvons mieux communiquer des idées, conduisant à un meilleur produit.

L'équipe UX apporte ses diverses forces pour travailler sur de nombreuses parties d'un ensemble complexe - se réunir en tant que groupe nous permet non seulement de combiner nos compétences pour créer de meilleurs outils pour LINCS, mais cela nous permet également d'apporter ce que nous avons appris notre travail individuel.

En travaillant ensemble, la situation ci-dessus passe d'un échec de conception à une opportunité de collaboration. Présenter le projet à l'équipe à un stade plus précoce ouvre la discussion sur l'apprentissage des limites d'une conception afin que nous puissions en apprendre davantage sur de nouvelles options, sans nous arrêter dans notre élan. Nous sommes en mesure de faire des choix de conception plus solides et plus éclairés en reconnaissant le processus comme quelque chose dont nous faisons tous partie. En ayant un espace pour partager et apprendre les uns des autres, nous sommes en mesure de nous assurer que chacun participe au travail et a la compréhension nécessaire pour développer quelque chose de significatif.
