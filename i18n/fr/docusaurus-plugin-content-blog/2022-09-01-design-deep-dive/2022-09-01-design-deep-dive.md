---
slug: design-deep-dive
title: La conception approfondie
authors:
  name: Farhad Omarzad
  title: LINCS UX Co-op
tags: [UX]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./design-deep-dive-(cc0).jpg)

Pendant mon séjour chez LINCS, l'équipe UX a jeté les bases de la mise en œuvre d'un flux utilisateur intuitif et sans effort pour ResearchSpace. Pour ce faire, nous avons mené de nombreux tests d'utilisation et d'utilisabilité, des entretiens et des tris de cartes.

Travailler sur ResearchSpace a été ma première véritable expérience dans le monde de l'UI/UX. C'était aussi la première fois que je travaillais dans une équipe de conception, et j'ai été époustouflé par le talent de mes pairs...<!--truncate--> J'ai apprécié le défi d'apprendre de nouveaux concepts UX et d'en apprendre davantage sur la conception et processus de test utilisateur. [Evan Rees](/fr/blog/tube-map-diverged), pour sa part, avait une habileté incroyable lorsqu'il s'agissait de répondre et de poser des questions pendant un test utilisateur sans pousser aucune sorte de biais ou de leader. Par exemple, au lieu de demander à un utilisateur : "Est-ce que cela vous frustre ?" quand ils étaient clairement frustrés, Evan leur demandait: "Comment cela vous fait-il sentir?" Au lieu. La question soigneusement formulée d'Evan a permis à l'utilisateur de répondre honnêtement sans être poussé dans une certaine direction. Jordan Lum et [Nem Brunell](/fr/blog/speaking-in-different-languages) m'ont tous deux beaucoup appris sur l'utilisation Figma pour organiser mon travail et créer des composants.

Mon expérience précédente en design m'avait seulement appris à créer quelque chose qui serait visuellement agréable. Travailler en tant que designer UX a radicalement changé ma perspective. J'ai appris à me mettre à la place de l'utilisateur, à essayer de comprendre ce qu'il attendrait d'un design. L'ajout de nouveaux éléments de convivialité et de praticité a considérablement amélioré mes compétences en matière de conception. Par exemple, lorsque je crée des designs maintenant, je détermine l'ambiance que je veux que le spectateur ressente, puis je choisis les couleurs, la typographie, etc. en conséquence. Même si je reviens au travail de conception traditionnel après ce stage, je continuerai à réfléchir à la façon dont la typographie, les choix de couleurs et le placement du texte seront perçus par le spectateur.

L'un des défis auxquels j'ai été confronté lorsque je travaillais au sein d'une équipe de développement UX et plus large a été d'apprendre à accepter que mes idées ne soient pas toujours suffisamment pratiques pour être mises en œuvre. Il y a eu des cas où j'ai pensé que j'avais trouvé un design qui résoudrait les problèmes d'un utilisateur, seulement pour qu'il soit abattu en une seconde. Par exemple, lorsque l'équipe UX essayait de résoudre le problème des icônes dépendantes du survol sur les cartes ResearchSpace pour les utilisateurs de tablettes, j'ai proposé un menu déroulant intégré à la carte d'entité comme solution. Cependant, si un menu déroulant avait été mis en place, toutes les cartes à l'écran auraient dû se déplacer vers le bas de l'écran pour faire de la place, ce qui aurait été difficile à mettre en place. L'équipe UX a fini par ajouter les icônes au bas de la carte sans modifier les dimensions de la carte - une solution qui pourrait être mise en œuvre de manière plus pratique ! Je n'ai jamais pris ces critiques personnellement et je suis heureux de dire que ces expériences m'ont aidé à développer mes compétences créatives en résolution de problèmes.

Travailler au sein de l'équipe UX Design a été une expérience enrichissante qui m'a déjà aidé à décrocher un emploi en design pour le prochain semestre d'automne et d'hiver à l'Université de Guelph. J'ai eu le plaisir de rencontrer tant de personnes talentueuses et je suis plus que reconnaissante d'avoir eu l'opportunité de travailler chez LINCS !
