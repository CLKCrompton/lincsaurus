---
slug: geospatial-ontologies
title: Le paysage changeant des ontologies géospatiales
authors:
  name: Thomas Smith
  title: LINCS Undergraduate Research Assistant
tags: [CWRC, ontologies]
hide_table_of_contents: true
---

<!-- @format -->

![Pin Map](./geospatial-ontologies-unsplash-(cc0).jpeg)

Au cours des nombreuses années où j'ai travaillé sur le projet LINCS, j'ai rencontré de nombreux nouveaux termes. Assis à mon bureau dans %%THINC Lab|thinc-lab%%, j'avais l'habitude d'entendre des gens discuter de quelque chose appelé %%ontologies|ontology%%. J'écoutais des réunions sur la création de l'[ontologie CWRC](https://sparql.cwrc.ca/ontologies/cwrc.html), et finalement on m'a demandé de contribuer en lisant les définitions proposées pour cela. Pensant que ce serait l'intégralité de mon expérience en ontologie, j'ai fait mon travail mais n'ai creusé aucun des concepts techniques sous-jacents...<!--truncate-->

Mon ignorance des concepts plus larges liés aux ontologies a pris fin récemment, lorsque j'ai pris un rôle dans l'équipe d'ontologie LINCS à l'été 2021. Ma mineure est en systèmes d'information géographique et analyse environnementale, on m'a donc demandé d'étudier les ontologies pour soutenir Données géospatiales du LINCS.

C'était une grande opportunité pour moi. J'ai eu la chance de pouvoir connecter quelque chose de nouveau avec les données géospatiales, sur lesquelles je connaissais déjà beaucoup de choses. Je n'aurais pas dû m'inquiéter autant de ce que j'ignorais cependant : il s'avère qu'il existe une communauté d'histoire spatiale très active, avec une abondante documentation écrite sur le sujet pour guider ceux qui, comme moi, sont nouveaux dans le domaine.

En me joignant à la conversation, mon intérêt a été attiré par l'un des plus grands problèmes auxquels sont confrontées les ontologies : comment représentons-nous l'évolution des lieux au fil du temps ? Je dis _places_ spécifiquement, car il y a une poussée dans ce domaine pour séparer le concept de _place_ de celui d'_space_ - l'explication la plus courante étant que tous les lieux se composent d'espace, mais doivent être nommés et habités par des humains pour être un lieu.

L'idée que les lieux changent avec le temps peut être démontrée en regardant la ville d'Helsinki, en Finlande, qui a changé de mains plusieurs fois au cours de son histoire. Il était sous le contrôle du Royaume de Suède avant 1809, puis de la Russie, puis de la République socialiste ouvrière finlandaise pendant une brève période en 1918 pendant la révolution finlandaise. Comment les historiens peuvent-ils cartographier une ville avec une histoire aussi incohérente ? Bien que les répertoires géographiques soient d'excellentes sources d'informations historiques, ils sont statiques et ne reflètent pas les changements dans le temps.

Un problème similaire existe lorsque vous essayez de représenter des lieux qui existent dans une surface en constante évolution, comme [la mort de Lord Nelson en mer](https://en.wikipedia.org/wiki/Horatio_Nelson,_1st_Viscount_Nelson#Wounding_and_death). L'endroit où Lord Nelson est mort est historiquement significatif, mais il est difficile de déterminer l'emplacement géographique où sa mort s'est produite. Le navire sur lequel il se trouvait était en mouvement constant, alors le point de sa mort serait-il une tache à la surface de l'eau ? Ou est-ce que l'emplacement est le fond marin directement sous le navire ?

Des solutions à ce problème sont proposées par une variété d'ontologies géospatiales, mais je crois que %%[CRMgeo](https://cidoc-crm.org/crmgeo/home-5) du CIDOC CRM|cidoc-crm"%% capture tous les aspects souhaités de l'histoire spatiale ; c'est aussi la méthode la plus communément acceptée pour représenter l'évolution des lieux au cours de l'histoire. Avec le vaisseau de Lord Nelson, les différents événements qui composent la bataille dans laquelle il est mort sont divisés en sous-classes, comme l'identifiant [crm:E93_Presence](https://cidoc-crm.org/Entity/E93-Presence/version-7.1.1) montrant la présence de navires spécifiques à des moments cruciaux de la bataille, ou un [crm:E5_Event](https://cidoc-crm.org/entity/e5-event/version-7.1) représentant le naufrage du Lord Le navire de Nelson. Cette représentation du combat est humainement compréhensible, et sa présence temporelle et spatiale peut être représentée séparément. Des lieux phénoménaux de l'emplacement actuel et moderne du navire peuvent être désignés, tandis que les lieux déclaratifs représentent l'emplacement approximatif trouvé sur les cartes marines ennemies. Des lieux plus déclaratifs peuvent être représentés sous la forme d'un polygone, comme la zone qui contient les restes coulés du navire ; [geo:SP7_Declarative spacetime volume](https://ontome.net/class/351) introduit dans CRMgeo permet aux objets d'avoir des identités en relation avec leur espace et leur temps sur Terre, et [geo:SP1_Phenomenal spacetime volume](https://ontome.net/class/345/namespace/9) autorise les événements et autres éléments persistants pour lesquels une existence est floue par rapport à d'autres phénomènes.

Bien que je sois encore très novice en matière d'ontologies, j'ai vraiment aimé rechercher comment les gens représentent des espaces - et des lieux - importants pour eux. Les exemples ci-dessus me fascinent et je suis intrigué par l'avancement de la compréhension de l'humanité de la relation entre l'espace et le temps. Je ne suis toujours pas en mesure de suivre le jargon technique de certains articles, mais je suis reconnaissant que l'équipe LINCS m'ait permis d'explorer ce domaine en évolution.
