---
slug: collabortation-social-isolation
title: Collaboration en période d'isolement social
authors:
  name: Thomas Smith
  title: LINCS Undergraduate Research Assistant
tags: [CWRC, digital humanities]
hide_table_of_contents: true
---

<!-- @format -->

![Unsplash](./collaboration-social-isolation-unsplash-(cc0).jpg)

Pour éviter l'épidémie de COVID-19, j'ai déménagé de Guelph à la ville de Bowmanville, une communauté d'environ 40 000 personnes. Ma famille a vendu notre maison de vingt ans juste avant la pandémie, en achetant une petite maison pittoresque à Minden, en Ontario, une ville de seulement 4 000 habitants. Je me considère chanceux d'être revenu avec ma famille lorsque l'épidémie a commencé à s'aggraver, même si je suis passé de l'Internet haute vitesse sur le campus de l'Université de Guelph à la lutte pour la bande passante avec les voisins. Même si je dois aussi vérifier s'il y a des ours en quittant la maison...<!--truncate-->

Au cours des deux dernières années, j'ai été assistante de recherche de premier cycle dans le %%Laboratoire THINC|thinc-lab%%. Le laboratoire THINC est une salle située au deuxième étage de la bibliothèque Mclaughlin de l'Université de Guelph. Là, j'ai rencontré des dizaines d'universitaires du monde entier qui sont venus au THINC Lab à la recherche d'une collaboration, pour aider les chercheurs dans leurs recherches et pour donner des conférences dans la grande boîte en verre que nous appelons le THINC Lab. Le THINC Lab offre aux chercheurs un espace de collaboration, de développement d'outils numériques et de formation à l'utilisation des outils numériques. Il accueille également la série DH@Guelph et des événements partenaires.

Bien que je sois au milieu des bois, la camaraderie du laboratoire THINC demeure. Grâce à Google Meet, aux chaînes Slack et aux photos de nos [tasses THINC Lab](https://twitter.com/search?q=%23showusyourMUG%20%40dhatguelph&src=typed_query) partagées sur les réseaux sociaux, nous restons connectés en tant que grand universitaire famille. Avec des chercheurs associés au laboratoire ayant des intérêts divers, un jour, vous pouvez parler à un professeur d'informatique de vos émissions de télévision préférées sur les hackers, tandis qu'un autre vous pourriez parler à un professeur d'anglais de l'histoire de la reliure. Tout le monde dans le laboratoire THINC partage ses connaissances pour favoriser la communauté et enseigner aux autres. Une description qui, selon moi, résume le THINC Lab se trouve sur son site Web, où il est décrit comme un espace sûr pour «prendre des risques, échouer et ainsi apprendre».

Au cours des dernières semaines, les autres assistants de recherche de premier cycle et moi-même avons été formés à l'utilisation des outils du [Collaboratoire canadien de recherche en rédaction (CWRC)](https://cwrc.ca/). Ce qui aurait été des réunions autour d'une grande table dans le THINC Lab sont devenues des sessions virtuelles dans Google Meet. Poser des questions à des collègues nécessite d'attendre la réponse d'un message Slack, et au lieu de pouvoir se rassembler autour d'un ordinateur, nous devons maintenant mettre en place des sessions de partage d'écran. Bien que nos méthodes de collaboration aient changé, l'esprit du THINC Lab est toujours le même. Je peux parler à mes collègues comme si je leur parlais au laboratoire. Dans ces phases naissantes de ma carrière universitaire, je ressens toujours un sentiment de communauté dans lequel je peux partager mes idées avec d'autres chercheurs, même si je suis à 260 km.

Le même environnement collaboratif que j'ai connu dans le laboratoire THINC a été créé en ligne par le biais du CWRC. La diversité des projets est présentée sur la [page Projets](https://cwrc.ca/projects). Dans quel autre espace pourriez-vous en apprendre davantage sur un cabaret en même temps que vous découvrez l'implication du Canada dans la guerre civile espagnole ? De la même manière que n'importe qui pourrait venir au laboratoire THINC pour entendre parler des projets d'autres personnes et s'impliquer, n'importe qui peut envoyer un message à un chercheur d'un projet CWRC pour exprimer son intérêt à collaborer. La transition d'un espace physique à un espace numérique a été pleinement adoptée par les personnes associées au THINC Lab.

Même si nous ne pouvons pas nous voir en personne, nous restons dans un monde très connecté. Vous ne savez jamais les situations auxquelles vos collègues sont confrontés ces jours-ci. Prenez peut-être une minute pour contacter vos amis de travail, vos collègues ou d'autres personnes qui ont joué un rôle important dans votre vie ou vos recherches. Je voudrais étendre le modèle d'inclusivité du THINC Lab dans les espaces de travail temporaires des personnes, ainsi que rappeler à chacun d'accueillir les différences de chacun comme source de force intellectuelle et culturelle.
